{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 1,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 225.0, 96.0, 983.0, 787.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 13.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 5,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "Dark Mode",
		"subpatcher_template" : "DarkModeSignature",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2839.0, 779.0, 150.0, 21.0 ],
					"text" : "increment by one"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2839.0, 749.0, 150.0, 21.0 ],
					"text" : "get current index"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-263",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3259.5, 758.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3259.5, 545.5, 140.0, 23.0 ],
					"text" : "send increase-count-id"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3259.5, 610.0, 154.0, 23.0 ],
					"text" : "receive increase-count-id"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 3259.5, 684.0, 61.0, 23.0 ],
					"text" : "counter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3118.0, 1302.0, 86.0, 21.0 ],
					"text" : "param"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-256",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3029.0, 1302.0, 86.0, 21.0 ],
					"text" : "name"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2940.0, 1302.0, 86.0, 21.0 ],
					"text" : "number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-253",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2901.0, 1060.0, 100.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-252",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2940.0, 1236.0, 53.0, 23.0 ],
					"text" : "random"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hint" : "",
					"id" : "obj-251",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3510.0, 1249.0, 38.0, 23.0 ],
					"text" : "print"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3498.0, 1284.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3458.0, 1209.0, 54.0, 23.0 ],
					"text" : "join 0 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3519.0, 1132.0, 41.0, 23.0 ],
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-245",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3286.5, 1269.0, 181.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-243",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 2940.0, 1173.0, 50.5, 23.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3079.21428571428487, 1060.0, 50.0, 23.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-241",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3018.999999999999545, 1060.0, 50.0, 23.0 ],
					"text" : "15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 3079.0, 1013.0, 50.0, 23.0 ],
					"text" : "unpack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3101.785714285714221, 952.0, 50.0, 23.0 ],
					"text" : "15 0.5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3202.5, 984.0, 50.0, 52.0 ],
					"text" : "plug AUFilter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3175.0, 943.0, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-207",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3167.0, 1042.0, 159.0, 23.0 ],
					"text" : "print pattr-out @popup 1",
					"varname" : "helppattr-1[1]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 3233.5, 943.0, 114.0, 23.0 ],
					"restore" : [ "plug", "AUFilter" ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0,
						"parameter_mappable" : 0
					}
,
					"text" : "pattr",
					"varname" : "u618003574"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 3332.0, 819.0, 40.0, 23.0 ],
					"restore" : [ 0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0,
						"parameter_mappable" : 0
					}
,
					"text" : "pattr",
					"varname" : "u608003576"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 3012.0, 574.0, 62.0, 23.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-218",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3233.5, 851.5, 37.0, 23.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"fontsize" : 13.0,
					"id" : "obj-221",
					"ignoreclick" : 1,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 3098.5, 903.5, 99.0, 23.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[21]",
							"parameter_shortname" : "vst~[1]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "AUFilter.auinfo",
							"plugindisplayname" : "AUFilter",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1718385248,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"blob" : "289.hAGaoMGcv.y0AHv.DTfAGfPBJr.CM3vVFIWYwUWYtMVdOsDWsEla0YVXiQWcxUlbTQVXzE1UyUmXzkGbkckckI2bo8laTQWdvUFUtEVakAQ.RDFbvw1SPPH..............7...............DfPHC......B.........v.DwAP.....P..........E.D.......fAEwAP.....b..........H.D.......PBEwIP.....n..........K.D........C.........zfQb.D.....N.....fDlkFazAA.RDVclgGVU4FcoQGakQFBWLBL0zSQJ8TTV0s3jmN.......P.A.........vC....................xC"
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "AUFilter",
									"origin" : "AUFilter.auinfo",
									"type" : "AudioUnit",
									"subtype" : "AudioEffect",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "AUFilter.auinfo",
										"plugindisplayname" : "AUFilter",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1718385248,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"blob" : "289.hAGaoMGcv.y0AHv.DTfAGfPBJr.CM3vVFIWYwUWYtMVdOsDWsEla0YVXiQWcxUlbTQVXzE1UyUmXzkGbkckckI2bo8laTQWdvUFUtEVakAQ.RDFbvw1SPPH..............7...............DfPHC......B.........v.DwAP.....P..........E.D.......fAEwAP.....b..........H.D.......PBEwIP.....n..........K.D........C.........zfQb.D.....N.....fDlkFazAA.RDVclgGVU4FcoQGakQFBWLBL0zSQJ8TTV0s3jmN.......P.A.........vC....................xC"
									}
,
									"fileref" : 									{
										"name" : "AUFilter",
										"filename" : "AUFilter.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "45f9eb03ab4ac6e71804c89412f51809"
									}

								}
 ]
						}

					}
,
					"text" : "vst~",
					"varname" : "vst~[5]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3098.5, 859.5, 84.0, 23.0 ],
					"text" : "prepend plug"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-228",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3142.0, 736.5, 37.0, 23.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"dontreplace" : 1,
					"fontsize" : 13.0,
					"id" : "obj-231",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3233.5, 903.5, 358.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-235",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3012.0, 610.0, 42.0, 23.0 ],
					"text" : "listau"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-236",
					"items" : [ "3-Band EQ", ",", "Arcade", ",", "Atlas", ",", "AUAudioFilePlayer", ",", "AUBandpass", ",", "AUDelay", ",", "AUDistortion", ",", "AUDynamicsProcessor", ",", "AUFilter", ",", "AUGraphicEQ", ",", "AUHighShelfFilter", ",", "AUHipass", ",", "AULowpass", ",", "AULowShelfFilter", ",", "AUMatrixMixer", ",", "AUMatrixReverb", ",", "AUMIDISynth", ",", "AUMixer", ",", "AUMixer3D", ",", "AUMultibandCompressor", ",", "AUMultiChannelMixer", ",", "AUMultiSplitter", ",", "AUNBandEQ", ",", "AUNetReceive", ",", "AUNetSend", ",", "AUNewPitch", ",", "AUParametricEQ", ",", "AUPeakLimiter", ",", "AUPitch", ",", "AUReverb2", ",", "AURogerBeep", ",", "AURoundTripAAC", ",", "AUSampleDelay", ",", "AUSampler", ",", "AUScheduledSoundPlayer", ",", "AUSoundFieldPanner", ",", "AUSpatialMixer", ",", "AUSpeechSynthesis", ",", "AUSphericalHeadPanner", ",", "AUVectorPanner", ",", "Battery 4", ",", "Bitcrush", ",", "Carve EQ", ",", "Chorus", ",", "Comb Filter", ",", "Compressor", ",", "Crave EQ", ",", "Cypher2", ",", "Delay", ",", "Dimension Expander", ",", "Disperser", ",", "Distortion", ",", "DLSMusicDevice", ",", "Ensemble", ",", "Equator", ",", "Faturator", ",", "FF Pro-C 2", ",", "FF Pro-L 2", ",", "FF Pro-MB", ",", "FF Pro-Q 3", ",", "FF Pro-R", ",", "Filter", ",", "Flanger", ",", "FM8 MFX", ",", "FM8", ",", "Formant Filter", ",", "Frequency Shifter", ",", "Gain", ",", "Gate", ",", "Haas", ",", "HRTFPanner", ",", "kHs ONE", ",", "Kontakt", ",", "Ladder Filter", ",", "Limiter", ",", "Massive X", ",", "MSoundFactory", ",", "MSoundFactory6out", ",", "Multipass", ",", "Ozone Imager", ",", "Phase Distortion", ",", "Phase Plant", ",", "Phaser", ",", "Pitch Shifter", ",", "Reaktor 6 MFX", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6", ",", "Resonator", ",", "Reverb", ",", "Reverser", ",", "Ring Mod", ",", "ROLI Studio Drums", ",", "ROLI Studio Player", ",", "Serum", ",", "SerumFX", ",", "Slice EQ", ",", "Snap Heap", ",", "Stereo", ",", "Strobe2", ",", "SubLab", ",", "Supercharger", ",", "Tape Stop", ",", "TDR Nova", ",", "Trance Gate", ",", "Transient Shaper", ",", "VocalSynth 2", ",", "XO", ",", "3-Band EQ", ",", "Arcade", ",", "Atlas", ",", "AUAudioFilePlayer", ",", "AUBandpass", ",", "AUDelay", ",", "AUDistortion", ",", "AUDynamicsProcessor", ",", "AUFilter", ",", "AUGraphicEQ", ",", "AUHighShelfFilter", ",", "AUHipass", ",", "AULowpass", ",", "AULowShelfFilter", ",", "AUMatrixMixer", ",", "AUMatrixReverb", ",", "AUMIDISynth", ",", "AUMixer", ",", "AUMixer3D", ",", "AUMultibandCompressor", ",", "AUMultiChannelMixer", ",", "AUMultiSplitter", ",", "AUNBandEQ", ",", "AUNetReceive", ",", "AUNetSend", ",", "AUNewPitch", ",", "AUParametricEQ", ",", "AUPeakLimiter", ",", "AUPitch", ",", "AUReverb2", ",", "AURogerBeep", ",", "AURoundTripAAC", ",", "AUSampleDelay", ",", "AUSampler", ",", "AUScheduledSoundPlayer", ",", "AUSoundFieldPanner", ",", "AUSpatialMixer", ",", "AUSpeechSynthesis", ",", "AUSphericalHeadPanner", ",", "AUVectorPanner", ",", "Battery 4", ",", "Bitcrush", ",", "Carve EQ", ",", "Chorus", ",", "Comb Filter", ",", "Compressor", ",", "Crave EQ", ",", "Cypher2", ",", "Delay", ",", "Dimension Expander", ",", "Disperser", ",", "Distortion", ",", "DLSMusicDevice", ",", "Ensemble", ",", "Equator", ",", "Faturator", ",", "FF Pro-C 2", ",", "FF Pro-L 2", ",", "FF Pro-MB", ",", "FF Pro-Q 3", ",", "FF Pro-R", ",", "Filter", ",", "Flanger", ",", "FM8 MFX", ",", "FM8", ",", "Formant Filter", ",", "Frequency Shifter", ",", "Gain", ",", "Gate", ",", "Haas", ",", "HRTFPanner", ",", "kHs ONE", ",", "Kontakt", ",", "Ladder Filter", ",", "Limiter", ",", "Massive X", ",", "MSoundFactory", ",", "MSoundFactory6out", ",", "Multipass", ",", "Ozone Imager", ",", "Phase Distortion", ",", "Phase Plant", ",", "Phaser", ",", "Pitch Shifter", ",", "Reaktor 6 MFX", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6", ",", "Resonator", ",", "Reverb", ",", "Reverser", ",", "Ring Mod", ",", "ROLI Studio Drums", ",", "ROLI Studio Player", ",", "Serum", ",", "SerumFX", ",", "Slice EQ", ",", "Snap Heap", ",", "Stereo", ",", "Strobe2", ",", "SubLab", ",", "Supercharger", ",", "Tape Stop", ",", "TDR Nova", ",", "Trance Gate", ",", "Transient Shaper", ",", "VocalSynth 2", ",", "XO", ",", "3-Band EQ", ",", "Arcade", ",", "Atlas", ",", "AUAudioFilePlayer", ",", "AUBandpass", ",", "AUDelay", ",", "AUDistortion", ",", "AUDynamicsProcessor", ",", "AUFilter", ",", "AUGraphicEQ", ",", "AUHighShelfFilter", ",", "AUHipass", ",", "AULowpass", ",", "AULowShelfFilter", ",", "AUMatrixMixer", ",", "AUMatrixReverb", ",", "AUMIDISynth", ",", "AUMixer", ",", "AUMixer3D", ",", "AUMultibandCompressor", ",", "AUMultiChannelMixer", ",", "AUMultiSplitter", ",", "AUNBandEQ", ",", "AUNetReceive", ",", "AUNetSend", ",", "AUNewPitch", ",", "AUParametricEQ", ",", "AUPeakLimiter", ",", "AUPitch", ",", "AUReverb2", ",", "AURogerBeep", ",", "AURoundTripAAC", ",", "AUSampleDelay", ",", "AUSampler", ",", "AUScheduledSoundPlayer", ",", "AUSoundFieldPanner", ",", "AUSpatialMixer", ",", "AUSpeechSynthesis", ",", "AUSphericalHeadPanner", ",", "AUVectorPanner", ",", "Battery 4", ",", "Bitcrush", ",", "Carve EQ", ",", "Chorus", ",", "Comb Filter", ",", "Compressor", ",", "Crave EQ", ",", "Cypher2", ",", "Delay", ",", "Dimension Expander", ",", "Disperser", ",", "Distortion", ",", "DLSMusicDevice", ",", "Ensemble", ",", "Equator", ",", "Faturator", ",", "FF Pro-C 2", ",", "FF Pro-L 2", ",", "FF Pro-MB", ",", "FF Pro-Q 3", ",", "FF Pro-R", ",", "Filter", ",", "Flanger", ",", "FM8 MFX", ",", "FM8", ",", "Formant Filter", ",", "Frequency Shifter", ",", "Gain", ",", "Gate", ",", "Haas", ",", "HRTFPanner", ",", "kHs ONE", ",", "Kontakt", ",", "Ladder Filter", ",", "Limiter", ",", "Massive X", ",", "MSoundFactory", ",", "MSoundFactory6out", ",", "Multipass", ",", "Ozone Imager", ",", "Phase Distortion", ",", "Phase Plant", ",", "Phaser", ",", "Pitch Shifter", ",", "Reaktor 6 MFX", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6", ",", "Resonator", ",", "Reverb", ",", "Reverser", ",", "Ring Mod", ",", "ROLI Studio Drums", ",", "ROLI Studio Player", ",", "Serum", ",", "SerumFX", ",", "Slice EQ", ",", "Snap Heap", ",", "Stereo", ",", "Strobe2", ",", "SubLab", ",", "Supercharger", ",", "Tape Stop", ",", "TDR Nova", ",", "Trance Gate", ",", "Transient Shaper", ",", "VocalSynth 2", ",", "XO" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3012.0, 820.5, 192.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-237",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3012.0, 736.5, 112.0, 23.0 ],
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-238",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 3012.0, 681.5, 199.0, 23.0 ],
					"text" : "route plug_vst plug_au plug_vst3"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3012.0, 646.5, 58.0, 23.0 ],
					"text" : "vstscan"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-212",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2361.0, 296.0, 87.0, 22.0 ],
					"text" : "plug $1, get -4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2247.75, 394.0, 50.0, 38.0 ],
					"text" : "271 0.5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2196.75, 394.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2455.75, 394.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2301.0, 394.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2354.0, 394.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2404.75, 394.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 2313.0, 334.0, 92.5, 23.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[19]",
							"parameter_shortname" : "vst~[19]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.auinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1632983935,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"blob" : "5445.hAGaoMGcv.y0AHv.DTfAGfPBJr.CM3PWPI2amIWXs4TcsIVYxwUag4VclE1XzUmbkI2U1MGcjEFcgc0b0IFc4AWYWYWYxMWZu4FUzkGbkQkag0VYP.fDXYTQR8TDTj3Pi41R...EAZDTCgF...P.XY1bXA...D...DPDBEDH3HVZzY1cgAGHacTRcA..................TzDdAzNWMPEUGWgGPEAL9CDEQ7uGXi+hff5hRXuifxelUAw+wehK3BrVXAYef+zlbddHIEqsQsnsZnop01nUsFywlVSyIwZw1nMXkTZhDSLh0lSDSpZzZ0XNMamYduY22trX1bZs4b1clyY16L246dmYtyctm2adu2hPzjBVMiLWbYVxraDNQh5kZKJHGkirf5yUmOj5llAGmkR5ABEha3BxwrPUxjQm9PnrPVChvsenvzzWvNrVtkBHUTV41W9qq0unnCEgn3RDMdMbcyAJhvYkMrMC79kLRryvMOjYMbc2ApDjUZkILzSCZ5qR4zRuFp9dVzN0v0CGjt0BshoRuHGWADEVNE2Sg1rFtPbfJiLbIo1x+jbbnZI.o3dFzV0v0SGQ7PHztWQaF2we9OxwYAkOxL0t3Z9FpCYq0x520F0p43PylL9o5KezxzzWXj4a2okUFcnC33ZyC53qjnILcYWB2AZkDtjzeexNmGnhICZJtIfRf1DRQoWNJYtVLSKapzKv6W5zHeJtjIHoIEkGxgpUFQrwLNLZtjNglZ.EAcXRv0aGVx0VIjh5wUU4VlNqObgqONn3II83plnN1ZjKb80g1fncGNtHEqfJrCB+.w9.Qb.Qb.Qb.Qb.db.1ESn9S6ZWSwMDTlAI.vNHwtJQ0aJvomfvOP0TD.r9q4mKkyLRc91sTsSW.VAgePflef8TQoWT4VjE9ATKPfy9+N6mGAhbjQs3Ne0ZA11EpMHvwuXolVX5KHSOW28rdfi8PclG.NeSAIgJP6Pgc5+K7CBz7Gr+3UXoB6xryZ1UbPgePfle.pOnTQCAYHXRhL2wW8PGk7LPXOSBbPjD2ivKT5YK3L6viT5jF0m8h7jmazf0NeBu0p34bHddOhm2EYmA+78DTw48SiWJ7CD9AB+.2tjg.v6iiM+Cbm2gI4lCfVk.W6gpAHva9aSt500yN4JD3YGBTW+4yaqkZ1lbkUmn6tBB+.t8IPgZo3JWoUaklp6yWtWgvevc6h+q8Phujyn74ImIutfpZQ7esCykujK7CHV.+204uT+XYxGcgcxK+uvOHf1OX247gj4ePH5y1HkPNaJmNH4nWHJXT9qzrrY61SN0XiM+BxaV4IMu4O6LmarwRdMZpozZrKSdeZVmkxjVoshjjqwlER0Iktz3kRJkIS9UtLKR1sTr7HLL8rl3TlzDSnhplH4AqTEJEzjHOEDIx2piDJIRsIqUVl3HZgT1N42hQxjRYlWLjyzYZzy0ws7FqOTnsV1Nbr91D84xPSz1IIEMbcVF01o3XsQq5gLJNeFOtZCg53PWE5H9yAGN6KYzkNTv+qQuT92EiFaD5GTa7ooMFPcDuUbglhfzGJrwTZS3UbNVU5dGFox6cc59yaxUepVpsFU.Z9LM2L7b6bPXZtqJSj.2zs2OPya92ea13eLKcyZySE729duMDmsYi2X82D12UFMb1+1Vgm9leA2Nft1HXkcN+n5SKSHr4EkxsCZsomuJ9yzrItbTplLr1nkoie5XiLW1IquUwy0iDQFZFgnySJNZd5eV2ITElLT4ZqksQaC89Y+9L6Rgl5GSFJOdRUdE3QxMRVaWaDKx3Ktm2gz279hijR47nT2KevCDN9fG3pveswky5SMozvwvR0u9rFDUc87YVjNrJ38U3oH5QAO3YtDFeSYmLi156lBiZn+IwnuvQUomMtgwpejqOelbq6Lp3ZcwpT09lONzSYk0FKThm0c1DQ+cJ4MdcBzWEFd73QCXpRrQXur2xSSG2dqQCxKs6b8ihgimTzIeMYXw17TdZchLpX0sGkiyScp2WmiQSGp8MeerpdoM46iCs89783DYkH8OMyzMIFAKFBUoTdNS5rJD9JNiKPAP0o9XCDVN0mpBzIsJCmwKUiSpf4wMUatS+51XQqUdenGLCGO9JOdKO9q5Wupd3HLONlNtdquPthy044hNYYE0Euys0Rs3pH1WPpNgnZj987R42CRl9soRduIYeMt7yWj9BRLCqkZU1b4INCq1kklRQVkkxZMlqJg0XtVRihjvBHr.90Vf7plbPyVK1dhoOcoxpohjD678qWt8bxMwuP6J23T9Exw.FbPogBdCogp6DDZPF2RFwZre0ktwszjYigF7FLlSba0X8YrGimpnWxXn0cBi4r22xX8McIisb4OkzdPvniLLHm3h.JK4AA0mQrv9KXLvoJJI3JUOUHz5RGhugbAS6cNP4Gcwv2qIyvgZ0Jz7kWM7I2ZcPHAuAHpveZXjQtQvPLeeHq31Jrnwtcht1Ir1Tedh91CriY9yI57W.GqveIQuuDz1pdYhteE3tq+0H5+DPLa5O.isgSCo13YfYt22BJ7fuMrpi9tv5e0K.e2ltD7bu4GBGn0Nfe668Of+zk+T37e7sgqdqOCt2m+uI8eP39DR2vQEdOvCqO8DOxHCCmPT8BaHldigg0WbVwEAd1i7gwKXrC.u7DFHtjjGD1lgAiqM0gheRX33mIiXwOaVi.+Cm4if+IydT3WnfwfO7BFG9WW33wGe4IheihRBe1RlH9bqZx3KZKE7GU8TwWu1GEem0aD63Iw3dVW5399clNdfaJS7v2b13Q0Pt3D+QOFNkFmEdZ6JObN6cN3722bwK5fyGuhWbg3xN5hwU8aVBdsu5xvJG+ww02jY7Vdihw63Msf28YKEu+VshOx49l3i8dUf+cWrR7ot7pws7Q1ws8w0fa+5qAekasN7Mty2Be2O+IXYw5e.+5u38xx82KKcwN4m6b6ZWW+CdZRZmAmtAAo3++GGjiJLQ5IEpNN38OeT8f2Nn1SsiUOCRSZc7WeiC90KceeyxknCyLRmc+UkPt8JokjctKixSeJ5ob31FN6LgnbyV6E.POh6W4xjkqJ0DSzRsUVtUaxVrImPwUVw8SfGvsYJexoOee5C0+sh5Z.+21NQyj8lJz8meMRo8syrmSVciqNggJmtjhG0Y3Yse8W6NN8YN3AFqQZVmfdsn1Y8301DLEV.gEPXA7Cr.ZwLYwNcXvfAGz4jq+G55Gm2WBtH7QbQ5i3dXeDW+8QbCvGwEkOhaf9Htn8QbCxGwEiOhav9Htg3i3FpOhaX9HtgyvYP8WhWG0yya4csqcYHD3j2rmvLd4vfc7D8BZ4w5MDpTeg7uW+fic9Hgkdh9ClNbTvOdOQCqnwXfeUiCA1+OcXv7NhDz7IiChu8uA7TAOJ3eNtw.0uzwAqXaiG1RqIBSJljYOifI85FfSN3oBidCOJnbMiP6EOMnrKkADeoYB4e6rgOYSyDhdbyBt6eIO3mUWADYBV8OGR+fM++udJbiy2cbBMDBVZNghem9GNtjOnWr3FQV2twWa4QfysslW+Er+ATdh6a186aVXOD1Cw9BQbAQb.Qb.Qb.2tvj1YWCgqywkeNlBppYx+yNvuS.2bCPB+.+006tZdI7CnVF+u82c05cWwW3GH7Cn6Cb2OfuuP0qQrOgaO7+oB+.Q7.Q7.++84c00C3IeQ7.Q7.8wC3+uVRrJzyNfkeP++ZopOo2+8+.bM5VdMdAzA02ggs0yuwwOau268S68du+1du268d8n8jORRRRRRRxijjjjjjj5ijjjjjjj7HIIIIIIjeu528wwq62eOuttOt+iq+3J4c+Tafu1vjAEShlIS1njYmIvpwfckCkIRwkvUv0vj3l414to4g4w4oYx7R7Z7VjMNYpY5YlY1YtY9YgYBrTrbrRrZrVrdrQLXKYaYGYWYOYe4.4P4H4X4DYhb5b1b9TbgbQbwbIbobYb4bEbkbUb0bMbsbcb8LItAtQtItYtEtUtMtctCtStKtatGtWtOZd.dPdHdXdDdTdLdbdBdRdJdZdFdVdNlLOOu.uHuDuLuBuJuFuNuAuIuEuM+GdGxljLELkLUL0LMLsLcL8LCLiLSLyLKLqLaL6LGLmLWL2LOLuLeL+r.rfrPrvrHrnrXLAVbVBVRVJVZVFVVVNVdVAVQVIVYVEVUVMVcVCVSVKVaVGVWVOVe1.1P1H1X1D1TFrYr4rErkrUr0rMrsrcr8rCrirSryrKrqrar6rGrmrWr2rOrurer+b.bfbPbvbHbnbXb3bDbjbTbzbLbrbbb7bBbhbRbxbJLQdObpbZb5bFblbVb1bNbtbdb9bA7d48QsIoHuekbgJ4CnjKRIePkbwJ4CojKQIeXkboJ4injKSIeTkb4J4iojqPIebkbkJ4SnjqRIeRkb0J4SojqQIeZkbsJ4ynjqSIeVkb8J4yojIojOuRtAk7ETxMpjunRtIk7kTxMqjurRtEk7UTxspjupRtMk70TxsqjutRtCk7MTxcpjuoRtKk7sTxcqjusRtGk7cTx8pjuqRtOkb+JoUx2SIOfR99J4AUxOPIOjR9gJ4gUxORIOhR9wJ4QUxOQIOlR9oJ4wUxOSIOgR94J4IUxuPIOkR9kJ4oUxuRIOiR90J4YUxuQIOmR9sJYxJ42ojmWI+dk7BJ4OnjWTI+Qk7RJ4OojWVI+Yk7JJ4unjWUI+Uk7ZJ4uojWWI+ck7FJ4enj2TI+Sk7VJ4eoj2VI+ak7eTx+UIuiR9eJdIXFTzjovlhlLk1TzjoxlhlL01TzjowlhlLs1TzjoylhlL81TzjYvlhlLi1TzjYxlhlLy1TzjYwlhlLq1TzjYylhlL61Tzj4vlhlLm1Tzj4xlhlL21Tzj4wlhlLu1Tzj4ylhlL+1TzjEvlhlrf1TzjExlhlrv1TzjEwlhlrn1TzjEylhlLAaJZxhaSQSVBaJZxRZSQSVJaJZxRaSQSVFaJZxxZSQSVNaJZxxaSQSVAaJZxJZSQSVIaJZxJaSQSVEaJZxpZSQSVMaJZxpaSQSVCaJZxZZSQSVKaJZxZaSQSVGaJZx5ZSQSVOaJZx5aSQS1.aJZxFZSQS1HaJZxFaSQS1DaJZxlZSQiQFTzjMylhlr41Tzjsvlhlrk1Tzjsxlhlr01Tzjswlhlrs1Tzjsylhlr81Tzjcvlhlri1Tzjcxlhlry1Tzjcwlhlrq1Tzjcylhlr61Tzj8vlhlrm1Tzj8xlhlr21Tzj8wlhlru1Tzj8ylhlr+1TzjCvlhlbf1TzjCxlhlbv1TzjCwlhlbn1TzjCylhlb31Tzjivlhlbj1Tzjixlhlbz1Tzjiwlhlbr1Tzjiylhlb71TzjSvlhlbh1TzjSxlhlbx1TzjSwlhlLQaJZx6wlhlbp1TzjSylhlb51Tzjyvlhlbl1Tzjyxlhlb11Tzjywlhlbt1Tzjyylhlb91TzjKvlhl7dsonIuOaJ5+e2uy2uqc+t2862XyfPeA1LHzmuMCB84YyfPet1LHzmiMCB8YayfPeV1LHzmoMCB8YXyfPe51LHzmlMCB8oZyfP+drYPnmnMCB8oXyfPex1LHzmjMCB8IZyfPeB1LHzGuMCB8wYyfPer1LHzGiMCB8QayfPeT1LHzGoMCB8QXyfPe31LHzGlMCB8gZyfPeH1LHzGrMCB8AYyfPef1LHzGfMCB89ayfPue1LHz6qMCB89XyfPu21LHz6kMCB8dZyfPuG1LHz6tMCB8tYyfPuq1LHz6hMCB8NayfPuS1LHz6nMCB8NXyfPu81LHzamMCB81ZyfPuM1LHzasMCB8VYyfPuk1LHzagMCB8layfPuY1LHzCaFD5M0lAgdSrYPn2XaFD5MxlAgdCsYPn2.aFD502lAgd8rYPnWWaFD50wlAgdssYPnWKaFD50zlAgdMrYPnWcaFD5UylAgdUsYPnWEaFD5U1lAgdkrYPnWQaFD5UvlAgd4sYPnWNaFD5k0lAgdYrYPnWZaFD5kxlAgdIsYPnWBaFD5E2lAgdB1LHzKlMCB8hZyfPuH1LHzKrMCB8BYyfPuf1LHzKfMCB87ayfPOe1LHzyqMCB87XyfPO21LHzykMCB8bZyfPOG1LHzytMCB8rYyfPOq1LHzyhMCB8LayfPOS1LHzynMCB8LXyfPO81LHzSmMCB8zZyfPOM1LHzSsMCB8TYyfPOk1LHzSgMCBcrYPd2y+u6Kg5cTp+qR8eTp+sR81J0+RodKk5epTuoR8OTp2Po96J0qqT+Mk50Tp+pR8pJ0eQodEk5OqTurR8mTpWRo9iJ0KpT+Ak5ETpeuR87J0uSolrR8aUpmSo9MJ0ypT+Zk5YTpekR8zJ0uTodJk5WnTOoR8yUpmPo9YJ0iqT+Tk5wTpehR8nJ0OVodDk5GoTOrR8CUpGRo9AJ0CpTeek5ATpumR0J08qT2mR8cUp6Uo9NJ08nTeak5tUpukRcWJ02TotSk5anT2gR80UpaWo9ZJ0soTeUk5VUpuhRcKJ0WVotYk5KoT2jR8EUpaTo9BJ0MnTedkZRJ0mSotdk5ypTWmR8YTpqUo9zJ00nTeJk5pUpOoRcUJ0mPotRk5iqTWgR8wTpKWo9nJ0koTeDk5RUpOrRcIJ0GRotXk5CpTWjR8ATpKTod+JUouq2GuWt.NeNONWNGNaNKNSNCNcNMNUdOLQNENYNINQNANdNNNVNFNZNJNRNBNbNLNTNDNXNHNPN.1e1O1W1G1a1K1S1C1c1M1U1E1Y1I1Q1A1d1N1V1F1Z1J1R1B1b1LFrorIrwrQrgrAr9rdrtrNr1rVrlrFr5rZrprJrxrRrhrBr7rbrrrLrzrTrjrDr3LAVLVTVDVXVHVPV.lelOlWlGlalKlSlClclMlUlElYlIlQlAldlNlVlFlZlJlRlBBuy20644s4s3M4M30403U4U3k4k3E4E34Yx7b7r7L7z7T7j7D737X7n7H7v7P7f7.zbebubOb2bWbmbGb6babqbKbybSbibCLItdtNtVtFtZtJtRtBtbtLtTtDtXtHtPJNeNaNclHmHGKGIGJGH6K6I6J6HaKaIC1HVOVKVMVIVNVJl.KLyOyMyNyLSOSMg25d8blWhIySyiyCSycysyMyj3Z3J3RnXhbnrqLX0XBL6Dl72w+CShhA4cOeOZ9+.j1U+.bSJ...RfkYygEDAHQX00VceAADBEDH3HVZzY1cgAGHacTRcA.B.bA.k.fL.nC.BAfR.7D.TAfU.rEEnSQ6T7NEzC.......HP..........7...................TwA"
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.auinfo",
									"type" : "AudioUnit",
									"subtype" : "Instrument",
									"embed" : 0,
									"snapshot" : 									{
										"pluginname" : "Serum.auinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1632983935,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"blob" : "5445.hAGaoMGcv.y0AHv.DTfAGfPBJr.CM3PWPI2amIWXs4TcsIVYxwUag4VclE1XzUmbkI2U1MGcjEFcgc0b0IFc4AWYWYWYxMWZu4FUzkGbkQkag0VYP.fDXYTQR8TDTj3Pi41R...EAZDTCgF...P.XY1bXA...D...DPDBEDH3HVZzY1cgAGHacTRcA..................TzDdAzNWMPEUGWgGPEAL9CDEQ7uGXi+hff5hRXuifxelUAw+wehK3BrVXAYef+zlbddHIEqsQsnsZnop01nUsFywlVSyIwZw1nMXkTZhDSLh0lSDSpZzZ0XNMamYduY22trX1bZs4b1clyY16L246dmYtyctm2adu2hPzjBVMiLWbYVxraDNQh5kZKJHGkirf5yUmOj5llAGmkR5ABEha3BxwrPUxjQm9PnrPVChvsenvzzWvNrVtkBHUTV41W9qq0unnCEgn3RDMdMbcyAJhvYkMrMC79kLRryvMOjYMbc2ApDjUZkILzSCZ5qR4zRuFp9dVzN0v0CGjt0BshoRuHGWADEVNE2Sg1rFtPbfJiLbIo1x+jbbnZI.o3dFzV0v0SGQ7PHztWQaF2we9OxwYAkOxL0t3Z9FpCYq0x520F0p43PylL9o5KezxzzWXj4a2okUFcnC33ZyC53qjnILcYWB2AZkDtjzeexNmGnhICZJtIfRf1DRQoWNJYtVLSKapzKv6W5zHeJtjIHoIEkGxgpUFQrwLNLZtjNglZ.EAcXRv0aGVx0VIjh5wUU4VlNqObgqONn3II83plnN1ZjKb80g1fncGNtHEqfJrCB+.w9.Qb.Qb.Qb.Qb.db.1ESn9S6ZWSwMDTlAI.vNHwtJQ0aJvomfvOP0TD.r9q4mKkyLRc91sTsSW.VAgePflef8TQoWT4VjE9ATKPfy9+N6mGAhbjQs3Ne0ZA11EpMHvwuXolVX5KHSOW28rdfi8PclG.NeSAIgJP6Pgc5+K7CBz7Gr+3UXoB6xryZ1UbPgePfle.pOnTQCAYHXRhL2wW8PGk7LPXOSBbPjD2ivKT5YK3L6viT5jF0m8h7jmazf0NeBu0p34bHddOhm2EYmA+78DTw48SiWJ7CD9AB+.2tjg.v6iiM+Cbm2gI4lCfVk.W6gpAHva9aSt500yN4JD3YGBTW+4yaqkZ1lbkUmn6tBB+.t8IPgZo3JWoUaklp6yWtWgvevc6h+q8Phujyn74ImIutfpZQ7esCykujK7CHV.+204uT+XYxGcgcxK+uvOHf1OX247gj4ePH5y1HkPNaJmNH4nWHJXT9qzrrY61SN0XiM+BxaV4IMu4O6LmarwRdMZpozZrKSdeZVmkxjVoshjjqwlER0Iktz3kRJkIS9UtLKR1sTr7HLL8rl3TlzDSnhplH4AqTEJEzjHOEDIx2piDJIRsIqUVl3HZgT1N42hQxjRYlWLjyzYZzy0ws7FqOTnsV1Nbr91D84xPSz1IIEMbcVF01o3XsQq5gLJNeFOtZCg53PWE5H9yAGN6KYzkNTv+qQuT92EiFaD5GTa7ooMFPcDuUbglhfzGJrwTZS3UbNVU5dGFox6cc59yaxUepVpsFU.Z9LM2L7b6bPXZtqJSj.2zs2OPya92ea13eLKcyZySE729duMDmsYi2X82D12UFMb1+1Vgm9leA2Nft1HXkcN+n5SKSHr4EkxsCZsomuJ9yzrItbTplLr1nkoie5XiLW1IquUwy0iDQFZFgnySJNZd5eV2ITElLT4ZqksQaC89Y+9L6Rgl5GSFJOdRUdE3QxMRVaWaDKx3Ktm2gz279hijR47nT2KevCDN9fG3pveswky5SMozvwvR0u9rFDUc87YVjNrJ38U3oH5QAO3YtDFeSYmLi156lBiZn+IwnuvQUomMtgwpejqOelbq6Lp3ZcwpT09lONzSYk0FKThm0c1DQ+cJ4MdcBzWEFd73QCXpRrQXur2xSSG2dqQCxKs6b8ihgimTzIeMYXw17TdZchLpX0sGkiyScp2WmiQSGp8MeerpdoM46iCs89783DYkH8OMyzMIFAKFBUoTdNS5rJD9JNiKPAP0o9XCDVN0mpBzIsJCmwKUiSpf4wMUatS+51XQqUdenGLCGO9JOdKO9q5Wupd3HLONlNtdquPthy044hNYYE0Euys0Rs3pH1WPpNgnZj987R42CRl9soRduIYeMt7yWj9BRLCqkZU1b4INCq1kklRQVkkxZMlqJg0XtVRihjvBHr.90Vf7plbPyVK1dhoOcoxpohjD678qWt8bxMwuP6J23T9Exw.FbPogBdCogp6DDZPF2RFwZre0ktwszjYigF7FLlSba0X8YrGimpnWxXn0cBi4r22xX8McIisb4OkzdPvniLLHm3h.JK4AA0mQrv9KXLvoJJI3JUOUHz5RGhugbAS6cNP4Gcwv2qIyvgZ0Jz7kWM7I2ZcPHAuAHpveZXjQtQvPLeeHq31Jrnwtcht1Ir1Tedh91CriY9yI57W.GqveIQuuDz1pdYhteE3tq+0H5+DPLa5O.isgSCo13YfYt22BJ7fuMrpi9tv5e0K.e2ltD7bu4GBGn0Nfe668Of+zk+T37e7sgqdqOCt2m+uI8eP39DR2vQEdOvCqO8DOxHCCmPT8BaHldigg0WbVwEAd1i7gwKXrC.u7DFHtjjGD1lgAiqM0gheRX33mIiXwOaVi.+Cm4if+IydT3WnfwfO7BFG9WW33wGe4IheihRBe1RlH9bqZx3KZKE7GU8TwWu1GEem0aD63Iw3dVW5399clNdfaJS7v2b13Q0Pt3D+QOFNkFmEdZ6JObN6cN3722bwK5fyGuhWbg3xN5hwU8aVBdsu5xvJG+ww02jY7Vdihw63Msf28YKEu+VshOx49l3i8dUf+cWrR7ot7pws7Q1ws8w0fa+5qAekasN7Mty2Be2O+IXYw5e.+5u38xx82KKcwN4m6b6ZWW+CdZRZmAmtAAo3++GGjiJLQ5IEpNN38OeT8f2Nn1SsiUOCRSZc7WeiC90KceeyxknCyLRmc+UkPt8JokjctKixSeJ5ob31FN6LgnbyV6E.POh6W4xjkqJ0DSzRsUVtUaxVrImPwUVw8SfGvsYJexoOee5C0+sh5Z.+21NQyj8lJz8meMRo8syrmSVciqNggJmtjhG0Y3Yse8W6NN8YN3AFqQZVmfdsn1Y8301DLEV.gEPXA7Cr.ZwLYwNcXvfAGz4jq+G55Gm2WBtH7QbQ5i3dXeDW+8QbCvGwEkOhaf9Htn8QbCxGwEiOhav9Htg3i3FpOhaX9HtgyvYP8WhWG0yya4csqcYHD3j2rmvLd4vfc7D8BZ4w5MDpTeg7uW+fic9Hgkdh9ClNbTvOdOQCqnwXfeUiCA1+OcXv7NhDz7IiChu8uA7TAOJ3eNtw.0uzwAqXaiG1RqIBSJljYOifI85FfSN3oBidCOJnbMiP6EOMnrKkADeoYB4e6rgOYSyDhdbyBt6eIO3mUWADYBV8OGR+fM++udJbiy2cbBMDBVZNghem9GNtjOnWr3FQV2twWa4QfysslW+Er+ATdh6a186aVXOD1Cw9BQbAQb.Qb.Qb.2tvj1YWCgqywkeNlBppYx+yNvuS.2bCPB+.+006tZdI7CnVF+u82c05cWwW3GH7Cn6Cb2OfuuP0qQrOgaO7+oB+.Q7.Q7.++84c00C3IeQ7.Q7.8wC3+uVRrJzyNfkeP++ZopOo2+8+.bM5VdMdAzA02ggs0yuwwOau268S68du+1du268d8n8jORRRRRRRxijjjjjjj5ijjjjjjj7HIIIIIIjeu528wwq62eOuttOt+iq+3J4c+Tafu1vjAEShlIS1njYmIvpwfckCkIRwkvUv0vj3l414to4g4w4oYx7R7Z7VjMNYpY5YlY1YtY9YgYBrTrbrRrZrVrdrQLXKYaYGYWYOYe4.4P4H4X4DYhb5b1b9TbgbQbwbIbobYb4bEbkbUb0bMbsbcb8LItAtQtItYtEtUtMtctCtStKtatGtWtOZd.dPdHdXdDdTdLdbdBdRdJdZdFdVdNlLOOu.uHuDuLuBuJuFuNuAuIuEuM+GdGxljLELkLUL0LMLsLcL8LCLiLSLyLKLqLaL6LGLmLWL2LOLuLeL+r.rfrPrvrHrnrXLAVbVBVRVJVZVFVVVNVdVAVQVIVYVEVUVMVcVCVSVKVaVGVWVOVe1.1P1H1X1D1TFrYr4rErkrUr0rMrsrcr8rCrirSryrKrqrar6rGrmrWr2rOrurer+b.bfbPbvbHbnbXb3bDbjbTbzbLbrbbb7bBbhbRbxbJLQdObpbZb5bFblbVb1bNbtbdb9bA7d48QsIoHuekbgJ4CnjKRIePkbwJ4CojKQIeXkboJ4injKSIeTkb4J4iojqPIebkbkJ4SnjqRIeRkb0J4SojqQIeZkbsJ4ynjqSIeVkb8J4yojIojOuRtAk7ETxMpjunRtIk7kTxMqjurRtEk7UTxspjupRtMk70TxsqjutRtCk7MTxcpjuoRtKk7sTxcqjusRtGk7cTx8pjuqRtOkb+JoUx2SIOfR99J4AUxOPIOjR9gJ4gUxORIOhR9wJ4QUxOQIOlR9oJ4wUxOSIOgR94J4IUxuPIOkR9kJ4oUxuRIOiR90J4YUxuQIOmR9sJYxJ42ojmWI+dk7BJ4OnjWTI+Qk7RJ4OojWVI+Yk7JJ4unjWUI+Uk7ZJ4uojWWI+ck7FJ4enj2TI+Sk7VJ4eoj2VI+ak7eTx+UIuiR9eJdIXFTzjovlhlLk1TzjoxlhlL01TzjowlhlLs1TzjoylhlL81TzjYvlhlLi1TzjYxlhlLy1TzjYwlhlLq1TzjYylhlL61Tzj4vlhlLm1Tzj4xlhlL21Tzj4wlhlLu1Tzj4ylhlL+1TzjEvlhlrf1TzjExlhlrv1TzjEwlhlrn1TzjEylhlLAaJZxhaSQSVBaJZxRZSQSVJaJZxRaSQSVFaJZxxZSQSVNaJZxxaSQSVAaJZxJZSQSVIaJZxJaSQSVEaJZxpZSQSVMaJZxpaSQSVCaJZxZZSQSVKaJZxZaSQSVGaJZx5ZSQSVOaJZx5aSQS1.aJZxFZSQS1HaJZxFaSQS1DaJZxlZSQiQFTzjMylhlr41Tzjsvlhlrk1Tzjsxlhlr01Tzjswlhlrs1Tzjsylhlr81Tzjcvlhlri1Tzjcxlhlry1Tzjcwlhlrq1Tzjcylhlr61Tzj8vlhlrm1Tzj8xlhlr21Tzj8wlhlru1Tzj8ylhlr+1TzjCvlhlbf1TzjCxlhlbv1TzjCwlhlbn1TzjCylhlb31Tzjivlhlbj1Tzjixlhlbz1Tzjiwlhlbr1Tzjiylhlb71TzjSvlhlbh1TzjSxlhlbx1TzjSwlhlLQaJZx6wlhlbp1TzjSylhlb51Tzjyvlhlbl1Tzjyxlhlb11Tzjywlhlbt1Tzjyylhlb91TzjKvlhl7dsonIuOaJ5+e2uy2uqc+t2862XyfPeA1LHzmuMCB84YyfPet1LHzmiMCB8YayfPeV1LHzmoMCB8YXyfPe51LHzmlMCB8oZyfP+drYPnmnMCB8oXyfPex1LHzmjMCB8IZyfPeB1LHzGuMCB8wYyfPer1LHzGiMCB8QayfPeT1LHzGoMCB8QXyfPe31LHzGlMCB8gZyfPeH1LHzGrMCB8AYyfPef1LHzGfMCB89ayfPue1LHz6qMCB89XyfPu21LHz6kMCB8dZyfPuG1LHz6tMCB8tYyfPuq1LHz6hMCB8NayfPuS1LHz6nMCB8NXyfPu81LHzamMCB81ZyfPuM1LHzasMCB8VYyfPuk1LHzagMCB8layfPuY1LHzCaFD5M0lAgdSrYPn2XaFD5MxlAgdCsYPn2.aFD502lAgd8rYPnWWaFD50wlAgdssYPnWKaFD50zlAgdMrYPnWcaFD5UylAgdUsYPnWEaFD5U1lAgdkrYPnWQaFD5UvlAgd4sYPnWNaFD5k0lAgdYrYPnWZaFD5kxlAgdIsYPnWBaFD5E2lAgdB1LHzKlMCB8hZyfPuH1LHzKrMCB8BYyfPuf1LHzKfMCB87ayfPOe1LHzyqMCB87XyfPO21LHzykMCB8bZyfPOG1LHzytMCB8rYyfPOq1LHzyhMCB8LayfPOS1LHzynMCB8LXyfPO81LHzSmMCB8zZyfPOM1LHzSsMCB8TYyfPOk1LHzSgMCBcrYPd2y+u6Kg5cTp+qR8eTp+sR81J0+RodKk5epTuoR8OTp2Po96J0qqT+Mk50Tp+pR8pJ0eQodEk5OqTurR8mTpWRo9iJ0KpT+Ak5ETpeuR87J0uSolrR8aUpmSo9MJ0ypT+Zk5YTpekR8zJ0uTodJk5WnTOoR8yUpmPo9YJ0iqT+Tk5wTpehR8nJ0OVodDk5GoTOrR8CUpGRo9AJ0CpTeek5ATpumR0J08qT2mR8cUp6Uo9NJ08nTeak5tUpukRcWJ02TotSk5anT2gR80UpaWo9ZJ0soTeUk5VUpuhRcKJ0WVotYk5KoT2jR8EUpaTo9BJ0MnTedkZRJ0mSotdk5ypTWmR8YTpqUo9zJ00nTeJk5pUpOoRcUJ0mPotRk5iqTWgR8wTpKWo9nJ0koTeDk5RUpOrRcIJ0GRotXk5CpTWjR8ATpKTod+JUouq2GuWt.NeNONWNGNaNKNSNCNcNMNUdOLQNENYNINQNANdNNNVNFNZNJNRNBNbNLNTNDNXNHNPN.1e1O1W1G1a1K1S1C1c1M1U1E1Y1I1Q1A1d1N1V1F1Z1J1R1B1b1LFrorIrwrQrgrAr9rdrtrNr1rVrlrFr5rZrprJrxrRrhrBr7rbrrrLrzrTrjrDr3LAVLVTVDVXVHVPV.lelOlWlGlalKlSlClclMlUlElYlIlQlAldlNlVlFlZlJlRlBBuy20644s4s3M4M30403U4U3k4k3E4E34Yx7b7r7L7z7T7j7D737X7n7H7v7P7f7.zbebubOb2bWbmbGb6babqbKbybSbibCLItdtNtVtFtZtJtRtBtbtLtTtDtXtHtPJNeNaNclHmHGKGIGJGH6K6I6J6HaKaIC1HVOVKVMVIVNVJl.KLyOyMyNyLSOSMg25d8blWhIySyiyCSycysyMyj3Z3J3RnXhbnrqLX0XBL6Dl72w+CShhA4cOeOZ9+.j1U+.bSJ...RfkYygEDAHQX00VceAADBEDH3HVZzY1cgAGHacTRcA.B.bA.k.fL.nC.BAfR.7D.TAfU.rEEnSQ6T7NEzC.......HP..........7...................TwA"
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum.maxsnap",
										"filepath" : "~/Documents/Max 8/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "c4311036f1d2471d35651d8515ff05ba"
									}

								}
, 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum[1]",
									"origin" : "Serum.auinfo",
									"type" : "AudioUnit",
									"subtype" : "Instrument",
									"embed" : 0,
									"fileref" : 									{
										"name" : "Serum[1]",
										"filename" : "Serum[1].maxsnap",
										"filepath" : "~/Documents/Max 8/Snapshots",
										"filepos" : -1,
										"snapshotfileid" : "3ea075b958fc18fb09c42cc6fafffbbe"
									}

								}
 ]
						}

					}
,
					"text" : "vst~",
					"varname" : "vst~[3]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 2477.0, 873.0, 42.0, 23.0 ],
					"text" : "select"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2465.5, 813.0, 181.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-232",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3495.0, 1080.0, 40.0, 23.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3376.0, 1178.0, 50.0, 23.0 ],
					"text" : "6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3336.25, 1080.0, 24.0, 23.0 ],
					"text" : "r b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3395.5, 1066.0, 26.0, 23.0 ],
					"text" : "s b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3360.0, 1115.0, 34.0, 23.0 ],
					"text" : "6 45"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 3454.0, 1131.0, 50.5, 23.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-222",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3420.0, 1033.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"int" : 1,
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3434.0, 1080.0, 39.0, 32.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2571.0, 690.0, 150.0, 21.0 ],
					"text" : "coll 366"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-217",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3537.0, 1045.0, 38.0, 23.0 ],
					"text" : "route"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2397.0, 567.0, 150.0, 21.0 ],
					"text" : "route"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2397.0, 543.0, 150.0, 21.0 ],
					"text" : "store"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2397.0, 519.5, 150.0, 21.0 ],
					"text" : "ask number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2550.0, 467.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2353.0, 467.0, 181.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2442.0, 598.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 2264.0, 543.0, 50.5, 23.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2085.106043291900733, 607.5, 50.0, 52.0 ],
					"text" : "5 0.510752"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2010.106043291900733, 607.5, 50.0, 52.0 ],
					"text" : "5 0.510752"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-197",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3120.0, 270.0, 105.0, 23.0 ],
					"text" : "r dim_command"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-156",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2697.0, 488.5, 169.0, 21.0 ],
					"text" : "set dimensional justification"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-162",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2686.0, 272.0, 150.0, 21.0 ],
					"text" : "set dimensional sizes"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-163",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2770.0, 299.0, 50.0, 21.0 ],
					"text" : "row"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-164",
					"maxclass" : "number",
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2820.0, 325.0, 46.0, 23.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-165",
					"maxclass" : "number",
					"maximum" : 100,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2820.0, 299.0, 46.0, 23.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2813.0, 360.0, 53.0, 23.0 ],
					"text" : "pak 0 0"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-169",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2813.0, 389.0, 107.0, 23.0 ],
					"text" : "row $1 height $2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-172",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2770.0, 325.0, 47.0, 21.0 ],
					"text" : "height"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-174",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2644.0, 299.0, 52.0, 21.0 ],
					"text" : "column"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-176",
					"maxclass" : "number",
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2697.0, 325.0, 42.0, 23.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-178",
					"maxclass" : "number",
					"maximum" : 100,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2697.0, 299.0, 42.0, 23.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-180",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2686.0, 360.0, 53.0, 23.0 ],
					"text" : "pak 0 0"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-182",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2686.0, 389.0, 97.0, 23.0 ],
					"text" : "col $1 width $2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2686.0, 426.0, 107.0, 23.0 ],
					"text" : "s dim_command"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-186",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2644.0, 325.0, 41.0, 21.0 ],
					"text" : "width"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-189",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2718.0, 602.0, 107.0, 23.0 ],
					"text" : "s dim_command"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2832.0, 545.5, 30.0, 23.0 ],
					"text" : "- 1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-191",
					"items" : [ "default", ",", "left", ",", "center", ",", "right" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2832.0, 519.5, 80.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-192",
					"maxclass" : "number",
					"maximum" : 4,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2770.0, 519.5, 37.0, 23.0 ],
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-195",
					"items" : [ "col", ",", "row" ],
					"labelclick" : 1,
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2698.0, 519.5, 59.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-196",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2718.0, 574.0, 133.0, 23.0 ],
					"text" : "pak col 0 just 0"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-135",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3005.0, 275.0, 102.0, 23.0 ],
					"text" : "set 0 0 Text Test"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 3194.0, 312.0, 50.5, 23.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"cellmap" : [ [ 1, 0, 2.25 ], [ 0, 0, "Text", "Test" ] ],
					"coldef" : [ [ 0, 138, 1, 0.0, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ] ],
					"cols" : 2,
					"datadirty" : 1,
					"fgcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ],
					"fontface" : 0,
					"fontname" : "Ableton Sans Medium",
					"fontsize" : 13.0,
					"hcellcolor" : [ 0.0, 0.905882352941176, 0.996078431372549, 0.81 ],
					"id" : "obj-125",
					"maxclass" : "jit.cellblock",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "list", "", "", "" ],
					"patching_rect" : [ 2995.0, 377.688196420669556, 380.0, 111.0 ],
					"rowdef" : [ [ 0, 32, 1, 0.0, 0.0, 0.0, 1.0, 1, 0.0, 0.0, 0.0, 1.0, -1, -1, -1 ] ],
					"savemode" : 1,
					"sccolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"sgcolor" : [ 0.2, 0.2, 0.235294117647059, 1.0 ],
					"stcolor" : [ 0.294117647058824, 0.294117647058824, 0.294117647058824, 1.0 ],
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2006.0, 732.0, 29.5, 23.0 ],
					"text" : "4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2739.5, 755.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2625.5, 755.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2691.5, 755.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2544.0, 755.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 0,
					"patching_rect" : [ 1335.0, 1840.0, 80.0, 23.0 ],
					"text" : "dac~ 3 4 5 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1571.0, 1716.0, 78.0, 23.0 ],
					"text" : "cycle~ 1205"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1479.5, 1716.0, 78.0, 23.0 ],
					"text" : "cycle~ 1200"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1297.5, 1716.0, 71.0, 23.0 ],
					"text" : "cycle~ 500"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1218.0, 1716.0, 71.0, 23.0 ],
					"text" : "cycle~ 502"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1539.0, 1634.0, 37.0, 23.0 ],
					"text" : "dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
					"patching_rect" : [ 1521.0, 1198.0, 164.0, 23.0 ],
					"text" : "mc.poly~ Synth 4 @steal 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1766.0, 1261.0, 184.0, 52.0 ],
					"text" : ";\r#SM createinport Strings-msp coremidi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1510.0, 1087.0, 52.0, 67.0 ],
					"text" : "mpeevent 1 1 8 136 69 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1830.0, 1155.0, 52.0, 52.0 ],
					"text" : "mpeevent 1 1 3 211 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 1732.395222187042236, 1111.0, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1729.0, 1060.0, 53.0, 23.0 ],
					"text" : "midiin d"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1405.0, 1028.0, 53.0, 23.0 ],
					"text" : "midiin c"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1255.5, 1241.0, 56.0, 23.0 ],
					"text" : "voices 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.25, 1241.0, 56.0, 23.0 ],
					"text" : "voices 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1392.5, 1529.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1392.5, 1587.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1392.5, 1475.0, 62.0, 23.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"align" : 0,
					"attr" : "autoconnect",
					"id" : "obj-140",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2057.0, 847.0, 122.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1423.0, 1434.0, 50.0, 23.0 ],
					"text" : "157"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1606.72613525390625, 1436.0, 89.0, 23.0 ],
					"text" : "loadmess 157"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2038.0, 179.259320139884949, 29.5, 38.0 ],
					"text" : "74 25"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2126.0, 202.0, 50.0, 67.0 ],
					"text" : "midievent 176 1 20"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2067.0, 202.0, 55.0, 38.0 ],
					"text" : "midievent 211 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-185",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "int", "int", "", "int", "" ],
					"patching_rect" : [ 1981.0, 108.0, 92.5, 23.0 ],
					"text" : "midiparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2529.0, 156.0, 50.0, 23.0 ],
					"text" : "4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2475.0, 156.0, 50.0, 23.0 ],
					"text" : "64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2399.0, 154.0, 50.0, 23.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-177",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2322.0, 149.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2256.0, 149.0, 50.0, 23.0 ],
					"text" : "74 25"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2174.0, 148.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2099.0, 136.0, 50.0, 23.0 ],
					"text" : "67 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "int", "int", "", "int", "" ],
					"patching_rect" : [ 2091.0, 95.0, 92.5, 23.0 ],
					"text" : "midiparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2024.0, 60.0, 42.0, 23.0 ],
					"text" : "midiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1473.0, 945.0, 52.0, 67.0 ],
					"text" : "mpeevent 1 1 8 136 69 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1961.0, 1002.0, 50.0, 23.0 ],
					"text" : "15"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1903.0, 1002.0, 50.0, 23.0 ],
					"text" : "64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1845.0, 1002.0, 50.0, 23.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1787.0, 1002.0, 50.0, 23.0 ],
					"text" : "74 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1729.0, 1002.0, 50.0, 23.0 ],
					"text" : "69 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1671.0, 1002.0, 50.0, 23.0 ],
					"text" : "69 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 6,
					"outlettype" : [ "list", "list", "int", "int", "int", "int" ],
					"patching_rect" : [ 1702.0, 945.0, 309.0, 23.0 ],
					"text" : "mc.noteallocator~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1542.682963609695435, 614.634160995483398, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 781.0, 869.0, 29.5, 23.0 ],
					"text" : "4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 808.0, 819.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@autoconnect", 0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-126",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "demompe.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2057.0, 880.5, 207.0, 42.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "risekeys",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2057.0, 953.0, 305.5, 103.0 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-83",
					"knobshape" : 5,
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1453.33331298828125, 1119.5, 242.392822265625, 18.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1453.33331298828125, 1148.0, 33.0, 23.0 ],
					"text" : "sig~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1261.0, 1309.0, 40.0, 23.0 ],
					"text" : "poly~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 14,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1335.0, 1253.0, 155.5, 23.0 ],
					"text" : "mc.combine~ 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1569.0, 1436.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-76",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1569.0, 1463.0, 70.0, 23.0 ],
					"text" : "parallel $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-77",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1592.5, 1529.5, 98.0, 23.0 ],
					"text" : "threadcount $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-81",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1592.5, 1499.5, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1665.833396553993225, 1420.0, 112.0, 23.0 ],
					"text" : "poly~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 2082.0, 1189.0, 40.0, 23.0 ],
					"restore" : [ 0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0,
						"parameter_mappable" : 0
					}
,
					"text" : "pattr",
					"varname" : "u457004266"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 2082.0, 1081.0, 40.0, 23.0 ],
					"restore" : [ 0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0,
						"parameter_mappable" : 0
					}
,
					"text" : "pattr",
					"varname" : "u482004268"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1691.0, 1189.0, 92.5, 23.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[2]",
							"parameter_shortname" : "vst~[2]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"blob" : "5097.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQq3EP6b0GaTUDD+cGP+B4iVjhsHziZ3qJeX4THf71osDgVjODLh.pnEhPKJEoHEnFadjF0p3GfMp7QTgznhsJJoQEknXSigpzJPHFpRPJow.BZ.RiBBQO2ce6d8cau1tg+nE5a1jgYmc9syt6LyN7t2coFFFfAuYI31RkZDqGVOKKi.1izd37nINuZhqaZhq6ZhqGZhKBMwEol3hRSbQqItXzDWO0D2MoItdoItdqIt9Hw0Pf.mDn4XHG8CXd.dO.qCf0Av5.Xc.Yc.wiexXMvxKnsKfbWkefeXc7OXdfsyvscOvQJ.uKlGf4ALO.lG3tyCj0As8B3yGH8GtEtZbWU1s3Gb6mS03tpra2+3VN+pwcUY2hevseNML7RaznO68EDj7PaxLhvvChiMm.JszoC5jBy7augjuWCjiumSVtFlGf4AXd.dO.qCf0AX0Abzb6O+la876HEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9Ukkii7t10MTi6pxX7uqc7WFeUi6pxRbHuqc9fZbWUFi+csi+x3qgQ67caHSL5v3986m9NMRi8dMTI1dPpSzmwrD3tVliEw1lAWKlAoMaas6ocJSaYldK3uGwCSwawVufsWufQMYp.TWs0JwRE4XfvM+vMFa9JDUjOFiaT+1sHLZseS.xUJJEfQsVe17pq1YP76uRNwMfgEuOa7qTT4barssl.vHFdAYCku249EgrS8VhyNSOuuXtNG2FOcsj10wb35n10R5un6gsxiA13k1wGcNLxvf5WUhQowmiXdh8nbdFh0RLL2WvluEYnYEmTmjGDTa2gYaKnhxigRmksWb135B0Wv2CNWCAF13La0Qy4qoy8bq024dVhIbiI0cMwU95QcMhTmkH9GxcKUenzeK4gQev3ICiCbAGWLmVccnywFqi6nx0Q0lp46s.mntmi4o+9PdGVbGmZaeT6vHCNI1er0jIGr43KimNtk.eZbLLaJsqXBB8gZifFiZa55HpUZQZtuCDN5xpq5X8kZjqgT1ImqSVuUV+0I.QeJN9YwoJ1bCWqsFWUmPNz7AoOu6JVmc1X+.H5FkX55AkhfRQRonnTzTBanG.8.taOvryeYYmWtKY0iM8o3Km0rhTGyZyt.2sGwcc5kO31+I6H3bufWOS1v6FlrQwUQ4dL2TFCwruEmt4lpNayn7tAyLSdylkjwNMqYw6wLphqxLyxNhYIUeJyC23Eo58PFQbQSxL4XI4LtDHkjwPH6ZtijTyhSkbl7mHIphSmLpRyhLixlC4IqbAjWt5rIezQykTaiqh7GMsdRDd2.I9XdNxvh6EIiOwWgL0j2LY9o7FTasUx5lzaSs2NIaY5uG0leHYuK7io1cOj5W9mQs8WRtbgeM09UQRbieGIkR+dxj1dcjoW1QHKrhehr7J+YRg66DjWp5SQ11A+MR4G82Ie0w+SxOz3EI+x49KxYa5eHW4p+Kc88.8NhtAwGSOfA26HggEWzvXhumv3SrW.Yv8AlZxwByZX8ClWJ8GVzXF.rzwk.j23GHTvjFDTDII34yXHvqN0aCdyoOT3cl0vg2etiD187tc3yW3ng8unwBGXwoBGZo9gis76BNYdS.Nc9SDNeA2MboBMg.EAPjEmNzmWXJv.138.I8ZSCFdoYAi8stWXBaelPZu6rgLKaNv88A2OL+Jd.3w9jGDxoxE.O0W7Pv512i.V6+QgRpNaXSGXIvVN3iC63PKC10QyE9zi8DvdO9Jfu8jqDpowUAG9zqFp+bqAZ37qENSSqGtvkdF3xW8Y4DF+c8we72kTn+tjb7eLHeuaMHdl7NJtis.uam89Pt9x8UGkePtNx0syeez7urbKZNAiDMeTNiL7MZeYkWtOMkwjBWa20mTvOmtulagCZKFyWKFoycfYl8Rxek9RsU2D15GW6n2e6n+NaU8TEx3PmHmsGBRp6UG6qVfgMu1pET+ONwkwdunb4JJOESF0VSjgU7tdZaXnVzCfd.zCbiqGPTizt1n7Xz7eeC6a.wXsCtX0DWbZhqeZh6l0DW+0DW7Zha.Zh6VzDWBZhKQMwMPMwcqZhaPZhavZhKINNkWmR3EkIkW2v8dGW2rUtAYiDWw6.Xa0rpu1BOwp+UVe7yMG5maF8Gn+.uWf0Ev5.Xc.rNPHOWSC7mc.+6Vk7825V3gjDvdjQLOf6RbKwe44DyCr8.R+gaki4AXdfyb.48.4XRYj6NxSTi6pxXd.lGv7.Xd.lGf4Atm6AW+820x+G0pAR+hW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82kF..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 0,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"blob" : "5097.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQq3EP6b0GaTUDD+cGP+B4iVjhsHziZ3qJeX4THf71osDgVjODLh.pnEhPKJEoHEnFadjF0p3GfMp7QTgznhsJJoQEknXSigpzJPHFpRPJow.BZ.RiBBQO2ce6d8cau1tg+nE5a1jgYmc9syt6LyN7t2coFFFfAuYI31RkZDqGVOKKi.1izd37nINuZhqaZhq6ZhqGZhKBMwEol3hRSbQqItXzDWO0D2MoItdoItdqIt9Hw0Pf.mDn4XHG8CXd.dO.qCf0Av5.Xc.Yc.wiexXMvxKnsKfbWkefeXc7OXdfsyvscOvQJ.uKlGf4ALO.lG3tyCj0As8B3yGH8GtEtZbWU1s3Gb6mS03tpra2+3VN+pwcUY2hevseNML7RaznO68EDj7PaxLhvvChiMm.JszoC5jBy7augjuWCjiumSVtFlGf4AXd.dO.qCf0AX0Abzb6O+la876HEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9Ukkii7t10MTi6pxX7uqc7WFeUi6pxRbHuqc9fZbWUFi+csi+x3qgQ67caHSL5v3986m9NMRi8dMTI1dPpSzmwrD3tVliEw1lAWKlAoMaas6ocJSaYldK3uGwCSwawVufsWufQMYp.TWs0JwRE4XfvM+vMFa9JDUjOFiaT+1sHLZseS.xUJJEfQsVe17pq1YP76uRNwMfgEuOa7qTT4barssl.vHFdAYCku249EgrS8VhyNSOuuXtNG2FOcsj10wb35n10R5un6gsxiA13k1wGcNLxvf5WUhQowmiXdh8nbdFh0RLL2WvluEYnYEmTmjGDTa2gYaKnhxigRmksWb135B0Wv2CNWCAF13La0Qy4qoy8bq024dVhIbiI0cMwU95QcMhTmkH9GxcKUenzeK4gQev3ICiCbAGWLmVccnywFqi6nx0Q0lp46s.mntmi4o+9PdGVbGmZaeT6vHCNI1er0jIGr43KimNtk.eZbLLaJsqXBB8gZifFiZa55HpUZQZtuCDN5xpq5X8kZjqgT1ImqSVuUV+0I.QeJN9YwoJ1bCWqsFWUmPNz7AoOu6JVmc1X+.H5FkX55AkhfRQRonnTzTBanG.8.taOvryeYYmWtKY0iM8o3Km0rhTGyZyt.2sGwcc5kO31+I6H3bufWOS1v6FlrQwUQ4dL2TFCwruEmt4lpNayn7tAyLSdylkjwNMqYw6wLphqxLyxNhYIUeJyC23Eo58PFQbQSxL4XI4LtDHkjwPH6ZtijTyhSkbl7mHIphSmLpRyhLixlC4IqbAjWt5rIezQykTaiqh7GMsdRDd2.I9XdNxvh6EIiOwWgL0j2LY9o7FTasUx5lzaSs2NIaY5uG0leHYuK7io1cOj5W9mQs8WRtbgeM09UQRbieGIkR+dxj1dcjoW1QHKrhehr7J+YRg66DjWp5SQ11A+MR4G82Ie0w+SxOz3EI+x49KxYa5eHW4p+Kc88.8NhtAwGSOfA26HggEWzvXhumv3SrW.Yv8AlZxwByZX8ClWJ8GVzXF.rzwk.j23GHTvjFDTDII34yXHvqN0aCdyoOT3cl0vg2etiD187tc3yW3ng8unwBGXwoBGZo9gis76BNYdS.Nc9SDNeA2MboBMg.EAPjEmNzmWXJv.138.I8ZSCFdoYAi8stWXBaelPZu6rgLKaNv88A2OL+Jd.3w9jGDxoxE.O0W7Pv512i.V6+QgRpNaXSGXIvVN3iC63PKC10QyE9zi8DvdO9Jfu8jqDpowUAG9zqFp+bqAZ37qENSSqGtvkdF3xW8Y4DF+c8we72kTn+tjb7eLHeuaMHdl7NJtis.uam89Pt9x8UGkePtNx0syeez7urbKZNAiDMeTNiL7MZeYkWtOMkwjBWa20mTvOmtulagCZKFyWKFoycfYl8Rxek9RsU2D15GW6n2e6n+NaU8TEx3PmHmsGBRp6UG6qVfgMu1pET+ONwkwdunb4JJOESF0VSjgU7tdZaXnVzCfd.zCbiqGPTizt1n7Xz7eeC6a.wXsCtX0DWbZhqeZh6l0DW+0DW7Zha.Zh6VzDWBZhKQMwMPMwcqZhaPZhavZhKINNkWmR3EkIkW2v8dGW2rUtAYiDWw6.Xa0rpu1BOwp+UVe7yMG5maF8Gn+.uWf0Ev5.Xc.rNPHOWSC7mc.+6Vk7825V3gjDvdjQLOf6RbKwe44DyCr8.R+gaki4AXdfyb.48.4XRYj6NxSTi6pxXd.lGv7.Xd.lGf4Atm6AW+820x+G0pAR+hW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82kF..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20200103.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "ff81f9e230c0d2118bb17816df7b0acf"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum",
					"varname" : "vst~[2]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-78",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1584.0, 1229.0, 48.0, 23.0 ],
					"text" : "open 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 1340.0, 1081.0, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 987.0, 753.0, 50.0, 23.0 ],
					"text" : "1 25"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1111.0, 722.0, 36.0, 23.0 ],
					"text" : "pack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1055.0, 731.0, 29.5, 23.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1048.0, 687.0, 50.0, 23.0 ],
					"text" : "unpack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1043.0, 608.0, 50.0, 23.0 ],
					"text" : "74 25"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "setvalue", "int" ],
					"patching_rect" : [ 1171.0, 838.0, 74.0, 23.0 ],
					"text" : "mc.target 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1584.0, 1149.0, 60.0, 23.0 ],
					"text" : "target $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1584.0, 1091.0, 32.5, 23.0 ],
					"text" : "- 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-26",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1584.0, 1119.5, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"items" : [ "none", ",", "all", ",", 1, ",", 2, ",", 3, ",", 4, ",", 5, ",", 6 ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1584.0, 1060.0, 57.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-150",
					"maxclass" : "number",
					"maximum" : 128,
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1584.0, 1185.5, 54.0, 23.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ 60 ],
							"parameter_shortname" : "number[1]",
							"parameter_type" : 3,
							"parameter_mmin" : 1.0,
							"parameter_longname" : "number[1]",
							"parameter_initial_enable" : 1,
							"parameter_invisible" : 1,
							"parameter_mmax" : 128.0
						}

					}
,
					"varname" : "number[1]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1014.0, 885.0, 60.0, 23.0 ],
					"text" : "thispoly~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1275.0, 1384.0, 22.0, 140.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1495.0, 1158.0, 40.0, 23.0 ],
					"text" : "poly~"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 717.0, 939.0, 92.5, 23.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[1]",
							"parameter_shortname" : "vst~[1]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"blob" : "5097.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQq3EP6b0GaTUDD+cGP+B4iVjhsHziZ3qJeX4THf71osDgVjODLh.pnEhPKJEoHEnFadjF0p3GfMp7QTgznhsJJoQEknXSigpzJPHFpRPJow.BZ.RiBBQO2ce6d8cau1tg+nE5a1jgYmc9syt6LyN7t2coFFFfAuYI31RkZDqGVOKKi.1izd37nINuZhqaZhq6ZhqGZhKBMwEol3hRSbQqItXzDWO0D2MoItdoItdqIt9Hw0Pf.mDn4XHG8CXd.dO.qCf0Av5.Xc.Yc.wiexXMvxKnsKfbWkefeXc7OXdfsyvscOvQJ.uKlGf4ALO.lG3tyCj0As8B3yGH8GtEtZbWU1s3Gb6mS03tpra2+3VN+pwcUY2hevseNML7RaznO68EDj7PaxLhvvChiMm.JszoC5jBy7augjuWCjiumSVtFlGf4AXd.dO.qCf0AX0Abzb6O+la876HEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9Ukkii7t10MTi6pxX7uqc7WFeUi6pxRbHuqc9fZbWUFi+csi+x3qgQ67caHSL5v3986m9NMRi8dMTI1dPpSzmwrD3tVliEw1lAWKlAoMaas6ocJSaYldK3uGwCSwawVufsWufQMYp.TWs0JwRE4XfvM+vMFa9JDUjOFiaT+1sHLZseS.xUJJEfQsVe17pq1YP76uRNwMfgEuOa7qTT4barssl.vHFdAYCku249EgrS8VhyNSOuuXtNG2FOcsj10wb35n10R5un6gsxiA13k1wGcNLxvf5WUhQowmiXdh8nbdFh0RLL2WvluEYnYEmTmjGDTa2gYaKnhxigRmksWb135B0Wv2CNWCAF13La0Qy4qoy8bq024dVhIbiI0cMwU95QcMhTmkH9GxcKUenzeK4gQev3ICiCbAGWLmVccnywFqi6nx0Q0lp46s.mntmi4o+9PdGVbGmZaeT6vHCNI1er0jIGr43KimNtk.eZbLLaJsqXBB8gZifFiZa55HpUZQZtuCDN5xpq5X8kZjqgT1ImqSVuUV+0I.QeJN9YwoJ1bCWqsFWUmPNz7AoOu6JVmc1X+.H5FkX55AkhfRQRonnTzTBanG.8.taOvryeYYmWtKY0iM8o3Km0rhTGyZyt.2sGwcc5kO31+I6H3bufWOS1v6FlrQwUQ4dL2TFCwruEmt4lpNayn7tAyLSdylkjwNMqYw6wLphqxLyxNhYIUeJyC23Eo58PFQbQSxL4XI4LtDHkjwPH6ZtijTyhSkbl7mHIphSmLpRyhLixlC4IqbAjWt5rIezQykTaiqh7GMsdRDd2.I9XdNxvh6EIiOwWgL0j2LY9o7FTasUx5lzaSs2NIaY5uG0leHYuK7io1cOj5W9mQs8WRtbgeM09UQRbieGIkR+dxj1dcjoW1QHKrhehr7J+YRg66DjWp5SQ11A+MR4G82Ie0w+SxOz3EI+x49KxYa5eHW4p+Kc88.8NhtAwGSOfA26HggEWzvXhumv3SrW.Yv8AlZxwByZX8ClWJ8GVzXF.rzwk.j23GHTvjFDTDII34yXHvqN0aCdyoOT3cl0vg2etiD187tc3yW3ng8unwBGXwoBGZo9gis76BNYdS.Nc9SDNeA2MboBMg.EAPjEmNzmWXJv.138.I8ZSCFdoYAi8stWXBaelPZu6rgLKaNv88A2OL+Jd.3w9jGDxoxE.O0W7Pv512i.V6+QgRpNaXSGXIvVN3iC63PKC10QyE9zi8DvdO9Jfu8jqDpowUAG9zqFp+bqAZ37qENSSqGtvkdF3xW8Y4DF+c8we72kTn+tjb7eLHeuaMHdl7NJtis.uam89Pt9x8UGkePtNx0syeez7urbKZNAiDMeTNiL7MZeYkWtOMkwjBWa20mTvOmtulagCZKFyWKFoycfYl8Rxek9RsU2D15GW6n2e6n+NaU8TEx3PmHmsGBRp6UG6qVfgMu1pET+ONwkwdunb4JJOESF0VSjgU7tdZaXnVzCfd.zCbiqGPTizt1n7Xz7eeC6a.wXsCtX0DWbZhqeZh6l0DW+0DW7Zha.Zh6VzDWBZhKQMwMPMwcqZhaPZhavZhKINNkWmR3EkIkW2v8dGW2rUtAYiDWw6.Xa0rpu1BOwp+UVe7yMG5maF8Gn+.uWf0Ev5.Xc.rNPHOWSC7mc.+6Vk7825V3gjDvdjQLOf6RbKwe44DyCr8.R+gaki4AXdfyb.48.4XRYj6NxSTi6pxXd.lGv7.Xd.lGf4Atm6AW+820x+G0pAR+hW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82kF..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 0,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"blob" : "5097.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQq3EP6b0GaTUDD+cGP+B4iVjhsHziZ3qJeX4THf71osDgVjODLh.pnEhPKJEoHEnFadjF0p3GfMp7QTgznhsJJoQEknXSigpzJPHFpRPJow.BZ.RiBBQO2ce6d8cau1tg+nE5a1jgYmc9syt6LyN7t2coFFFfAuYI31RkZDqGVOKKi.1izd37nINuZhqaZhq6ZhqGZhKBMwEol3hRSbQqItXzDWO0D2MoItdoItdqIt9Hw0Pf.mDn4XHG8CXd.dO.qCf0Av5.Xc.Yc.wiexXMvxKnsKfbWkefeXc7OXdfsyvscOvQJ.uKlGf4ALO.lG3tyCj0As8B3yGH8GtEtZbWU1s3Gb6mS03tpra2+3VN+pwcUY2hevseNML7RaznO68EDj7PaxLhvvChiMm.JszoC5jBy7augjuWCjiumSVtFlGf4AXd.dO.qCf0AX0Abzb6O+la876HEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9Ukkii7t10MTi6pxX7uqc7WFeUi6pxRbHuqc9fZbWUFi+csi+x3qgQ67caHSL5v3986m9NMRi8dMTI1dPpSzmwrD3tVliEw1lAWKlAoMaas6ocJSaYldK3uGwCSwawVufsWufQMYp.TWs0JwRE4XfvM+vMFa9JDUjOFiaT+1sHLZseS.xUJJEfQsVe17pq1YP76uRNwMfgEuOa7qTT4barssl.vHFdAYCku249EgrS8VhyNSOuuXtNG2FOcsj10wb35n10R5un6gsxiA13k1wGcNLxvf5WUhQowmiXdh8nbdFh0RLL2WvluEYnYEmTmjGDTa2gYaKnhxigRmksWb135B0Wv2CNWCAF13La0Qy4qoy8bq024dVhIbiI0cMwU95QcMhTmkH9GxcKUenzeK4gQev3ICiCbAGWLmVccnywFqi6nx0Q0lp46s.mntmi4o+9PdGVbGmZaeT6vHCNI1er0jIGr43KimNtk.eZbLLaJsqXBB8gZifFiZa55HpUZQZtuCDN5xpq5X8kZjqgT1ImqSVuUV+0I.QeJN9YwoJ1bCWqsFWUmPNz7AoOu6JVmc1X+.H5FkX55AkhfRQRonnTzTBanG.8.taOvryeYYmWtKY0iM8o3Km0rhTGyZyt.2sGwcc5kO31+I6H3bufWOS1v6FlrQwUQ4dL2TFCwruEmt4lpNayn7tAyLSdylkjwNMqYw6wLphqxLyxNhYIUeJyC23Eo58PFQbQSxL4XI4LtDHkjwPH6ZtijTyhSkbl7mHIphSmLpRyhLixlC4IqbAjWt5rIezQykTaiqh7GMsdRDd2.I9XdNxvh6EIiOwWgL0j2LY9o7FTasUx5lzaSs2NIaY5uG0leHYuK7io1cOj5W9mQs8WRtbgeM09UQRbieGIkR+dxj1dcjoW1QHKrhehr7J+YRg66DjWp5SQ11A+MR4G82Ie0w+SxOz3EI+x49KxYa5eHW4p+Kc88.8NhtAwGSOfA26HggEWzvXhumv3SrW.Yv8AlZxwByZX8ClWJ8GVzXF.rzwk.j23GHTvjFDTDII34yXHvqN0aCdyoOT3cl0vg2etiD187tc3yW3ng8unwBGXwoBGZo9gis76BNYdS.Nc9SDNeA2MboBMg.EAPjEmNzmWXJv.138.I8ZSCFdoYAi8stWXBaelPZu6rgLKaNv88A2OL+Jd.3w9jGDxoxE.O0W7Pv512i.V6+QgRpNaXSGXIvVN3iC63PKC10QyE9zi8DvdO9Jfu8jqDpowUAG9zqFp+bqAZ37qENSSqGtvkdF3xW8Y4DF+c8we72kTn+tjb7eLHeuaMHdl7NJtis.uam89Pt9x8UGkePtNx0syeez7urbKZNAiDMeTNiL7MZeYkWtOMkwjBWa20mTvOmtulagCZKFyWKFoycfYl8Rxek9RsU2D15GW6n2e6n+NaU8TEx3PmHmsGBRp6UG6qVfgMu1pET+ONwkwdunb4JJOESF0VSjgU7tdZaXnVzCfd.zCbiqGPTizt1n7Xz7eeC6a.wXsCtX0DWbZhqeZh6l0DW+0DW7Zha.Zh6VzDWBZhKQMwMPMwcqZhaPZhavZhKINNkWmR3EkIkW2v8dGW2rUtAYiDWw6.Xa0rpu1BOwp+UVe7yMG5maF8Gn+.uWf0Ev5.Xc.rNPHOWSC7mc.+6Vk7825V3gjDvdjQLOf6RbKwe44DyCr8.R+gaki4AXdfyb.48.4XRYj6NxSTi6pxXd.lGv7.Xd.lGf4Atm6AW+820x+G0pAR+hW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82kF..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20200102.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "80151477fda390595a0e900a033551a6"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum",
					"varname" : "vst~[1]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1491.0, 1409.0, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_shortname" : "mc.live.gain~[1]",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "mc.live.gain~[4]",
							"parameter_mmax" : 6.0
						}

					}
,
					"varname" : "mc.live.gain~[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1521.0, 1287.0, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_shortname" : "mc.live.gain~[1]",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "mc.live.gain~[3]",
							"parameter_mmax" : 6.0
						}

					}
,
					"varname" : "mc.live.gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "gain~",
					"multichannelvariant" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 882.0, 1164.0, 22.0, 140.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1389.0, 917.0, 58.0, 23.0 ],
					"text" : "receive t"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1081.0, 1969.0, 100.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "setvalue", "int" ],
					"patching_rect" : [ 399.090154647827148, 1905.0, 79.0, 23.0 ],
					"text" : "mc.targetlist"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1098.0, 2031.0, 42.0, 23.0 ],
					"text" : "select"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 937.0, 1988.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 951.0, 2097.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 935.0, 2029.0, 74.0, 23.0 ],
					"text" : "counter 0 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"lastchannelcount" : 20,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1569.0, 1287.0, 187.0, 132.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_shortname" : "mc.live.gain~[1]",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "mc.live.gain~[1]",
							"parameter_mmax" : 6.0
						}

					}
,
					"varname" : "mc.live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "mc.ezdac~",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1335.0, 1431.5, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "gain~",
					"multichannelvariant" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1335.0, 1389.0, 107.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1335.0, 1309.0, 177.0, 23.0 ],
					"text" : "mc.mixdown~ 2 @autogain 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "multichannelsignal" ],
					"patching_rect" : [ 1335.0, 1198.0, 164.0, 23.0 ],
					"text" : "mc.poly~ Synth 4 @steal 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1150.0, 1890.0, 52.0, 67.0 ],
					"text" : "mpeevent 1 1 2 130 50 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 924.0, 1883.0, 52.0, 67.0 ],
					"text" : "mpeevent 1 1 4 132 49 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 791.0, 1835.0, 52.0, 67.0 ],
					"text" : "mpeevent 1 1 7 135 48 14"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-513",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 865.446590721607208, 1505.640767395496368, 121.912619948387146, 23.0 ],
					"saved_object_attributes" : 					{
						"filename" : "",
						"parameter_enable" : 0
					}
,
					"text" : "js"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-512",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1023.563091516494751, 1580.922320008277893, 40.0, 23.0 ],
					"text" : "poly~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-511",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "list" ],
					"patching_rect" : [ 1096.514548718929291, 1667.776687502861023, 50.5, 23.0 ],
					"text" : "poly"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-479",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "demompe.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "int" ],
					"patching_rect" : [ 420.590154647827148, 1463.245887756347656, 207.0, 42.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-480",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 933.590154647827148, 1808.245887756347656, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-481",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 873.590154647827148, 1808.245887756347656, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-482",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 782.590154647827148, 1808.245887756347656, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-483",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 722.590154647827148, 1808.245887756347656, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-484",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 631.590154647827148, 1808.245887756347656, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-485",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 571.590154647827148, 1808.245887756347656, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-486",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 480.590154647827148, 1808.245887756347656, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-487",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 420.590154647827148, 1808.245887756347656, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-69",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 431.090154647827148, 1687.245887756347656, 50.0, 21.0 ],
					"text" : "note: C",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-488",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 480.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-489",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 420.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-490",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 512.590154647827148, 1745.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-491",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 420.590154647827148, 1745.245887756347656, 83.0, 23.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-492",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 631.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-493",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 571.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-494",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 663.590154647827148, 1745.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-495",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 571.590154647827148, 1745.245887756347656, 83.0, 23.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-496",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 782.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-497",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 722.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-498",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 814.590154647827148, 1742.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-499",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 722.590154647827148, 1742.245887756347656, 83.0, 23.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-500",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 933.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-501",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 873.590154647827148, 1782.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-502",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 961.590154647827148, 1742.245887756347656, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-503",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 873.590154647827148, 1742.245887756347656, 83.0, 23.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-504",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 873.590154647827148, 1710.245887756347656, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-505",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 722.590154647827148, 1710.245887756347656, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-506",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 571.590154647827148, 1710.245887756347656, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-507",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 420.590154647827148, 1710.245887756347656, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-508",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "int" ],
					"patching_rect" : [ 420.590154647827148, 1627.245887756347656, 118.0, 23.0 ],
					"text" : "mperoute 48 49 50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-478",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 750.278668403625488, 1454.868834495544434, 63.0, 23.0 ],
					"text" : "mperoute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-476",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 417.08430814743042, 1993.084264755249023, 150.0, 21.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1039.0, 652.0, 50.0, 23.0 ],
					"text" : "67 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "setvalue", "int" ],
					"patching_rect" : [ 1063.0, 1497.0, 79.0, 23.0 ],
					"text" : "mc.targetlist"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1254.0, 722.0, 50.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1262.0, 542.0, 150.0, 21.0 ],
					"text" : "@mpemode 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 939.0, 644.0, 29.5, 23.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 766.0, 655.0, 50.0, 23.0 ],
					"text" : "67 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 820.0, 655.0, 50.0, 23.0 ],
					"text" : "67 0"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-44",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1263.0, 895.5, 100.0, 23.0 ],
					"text" : "setvalue 2 open"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-45",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1263.0, 847.0, 100.0, 23.0 ],
					"text" : "setvalue 1 open"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 845.0, 1044.5, 177.0, 23.0 ],
					"text" : "mc.mixdown~ 2 @autogain 1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 845.0, 1002.0, 86.0, 23.0 ],
					"text" : "mc.combine~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1111.0, 667.0, 50.0, 23.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1145.0, 1210.0, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_shortname" : "mc.live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "mc.live.gain~",
							"parameter_mmax" : 6.0
						}

					}
,
					"varname" : "mc.live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1237.0, 975.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1263.0, 932.0, 92.5, 23.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~",
							"parameter_shortname" : "vst~",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"blob" : "5129.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQy3EP6bs.aTVDD9+Nf9B4QKRwVD5QM75jGVNEBH2NskHzh7PvHBnhVHBTTJRQJPM17SZTKhO.aT4QTgznhsJJoQEknHow.JEABwPUBRIMFPPCPHJHD8bm8e2i++q20tgDf5cylLc1cmuc18elYmt+6csFFFfgnXh7.d85M.1rRijcgbSyf80J3boFaqfysl3Zml3Zul35fl3hSSbwqItDzDWhZhKIMw0QMwcSZhqSZhqyZhqKJbMFHvwvXFhS1AJNf1GP4An7.Td.JOfJO.dLTYoQ4YKOKwEVjne6.99Hp2IQEEv2ZP9+XD+ukO+r.EGfVhn+86J+cj3Tb.EGf6CbFGn1WXE0P6ST1ineNEGP4Cn7AQ+6yiz4ABseJe.kOvd9.27B2hfuuXPxEuXE2D1eFDGNl.gTxg2ocJrZnk6TcuFDmtmSLVihCn3.JNf1GP4An7.Xd.aE5b8VFiXG6fy2eQEJD677Gq4uizyKEGfVFJtmhCn3.62mgy7EVsn8IwN4IblOP8bSwAN2WnrKQubJN.83Qu9Wcimo3.JNv94CvOaCULQaAtOe932oQ138ZDJgNNkLYcjYJwc0LFSlkNCNWnB4EKcs0wcb+VsQ4lveMfGli2DmufkWqjAMZdCXe0WuBKuo.CDtwGt9vwGBwaJ5C4FMrQSFRK6qCvtTYdAjhTcbb6q9Iv74qVAITfgonN1+kJqZgN1v5SCPBwKIKnh0tvtHaaWto7YGkKpKGq89svymKkdsMFgLtdMU1K9ZX8BefEdkd7vGCRFFb6ZH9nrEiQNN4ZTMNC4bI6VXKvwax5a9onjo3AA0xUPcaB0TcRb5T3ZwdQHyosPrFrOGRLX+nttdyEyo80bjpaeMqvDt9Txtp3g7wiFyzjarj9eG6sB0Fpr2JdXjGzehXrgKX+xwDw4gOFKr11iplmP0Ynw6MCmLumswo+5PsGVtGmqaOb8fjgfjqObNw1AK19v348aJwms.CpSkdkCPJ2oNBpLtt4yiLWoI6J0sgvVULups4WIQMGp114BYp7sp7u1AHqywIdVrKBGa3JsT+gJS11Y7fxl29PzN9rgeAHZGmPYcfSwwo34TBbJQNQExBPVfXaKvjKddETTgyYICMmw3Y9KcgYMjkUPIw1VjXqmd0A29WUEIWXEb6ZzFtW4nMJeWbtK+qI293uqkmi+0TWA9Sv8J8mWlq0eE4tY+6Y1ayeBkuK+4U0A8WQcG2+AZ5bb4tXCHkDY4kYxr4OrzXUjaeXaYpCjsmYmE6jEORVBkmCaPUlOaBUME1SV6LXuTcEv9vCUHq9lVL62O+JXw4dkrTS54X8KkUwFd5uLarYtV1z895bcsd1xG0aw02lYqa7uKWme.a6y7i35carFVvmx08WvtXoeEW+6hk9p+Vl2J+N1n139XiupCxlYM+HaA09SrR2wQYuXcGmsg89qrpOzuw9xi7Gruuoyw94S+mrSc9+lcoK+O742Ez43ZGjZRc.5cmiG5WJIBCI0NBCO8NArd2EXrYlLLo90MXZd6NLqgzCXtCKMnng2SnjQ0KnLVFvymaefWYr2F7FiuuvaOo9Cu2TGHr0oc6vmMyAC6bVCE18ryB1+b8AGdA2EbrhFAbhhGIblRta3Bk5GBTF.wWdNPWdgw.8X02CjwqNNn+UlOLz27dgQrwIBY+NSFxqpo.226e+vzq4AfG6iePX90NC3o97GBV9NdDvbmOJTQcE.qY2yAV2debXS6edvVNTgvmb3m.19QVH7MGaQvdZZwvANwRfFN8RgFOyxfSd9U.m8BOCbwK+rBh7+w79e56kjyuWR19ECp6etQ4Yxu1yU2omsEAu5M50gZ9Uqpq81AqYpwV4uCrq+qC9suJ+6U1jGSfjgA5yL7vqfjgmA6I+hJ7o4LrU3JasgLB9d5dtRIbPaVedZVO2X6XhELmhWjmrh3hvR9vZE49ZE42YDkyEn7C2.43ZHHE5Z015pYXvw0Rkfx+gQNO7dQEsqoZu9QpkFHhUdWOsLLRJYAHK.YA9+qEPliTjaT96ls++2vtp5qUvkrl3RQSbcSSb2rl35tl3RUSb8PSb2hl3RSSboqItdpItaUSb8RSb8VSbYHvEx0oD9ls41a49NZysjZiufRo7MIxejeC0W5QWxuf0o2a146MS1CxdP6Kn7BTd.JO.kGvw4YZTb1gqbOtp6Sk3VlonO6P3uGeJNHZ0eGomKJN.sLQe6uij+NR8SwATbf8+NNbFmX0h1mD6jmvY9.0yMEG3begxtD8xo3.ziG85e0MdlhCn3.6mOns1+WK+O.raLEf3EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+cqA..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 0,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"blob" : "5129.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQy3EP6bs.aTVDD9+Nf9B4QKRwVD5QM75jGVNEBH2NskHzh7PvHBnhVHBTTJRQJPM17SZTKhO.aT4QTgznhsJJoQEknHow.JEABwPUBRIMFPPCPHJHD8bm8e2i++q20tgDf5cylLc1cmuc18elYmt+6csFFFfgnXh7.d85M.1rRijcgbSyf80J3boFaqfysl3Zml3Zul35fl3hSSbwqItDzDWhZhKIMw0QMwcSZhqSZhqyZhqKJbMFHvwvXFhS1AJNf1GP4An7.Td.JOfJO.dLTYoQ4YKOKwEVjne6.99Hp2IQEEv2ZP9+XD+ukO+r.EGfVhn+86J+cj3Tb.EGf6CbFGn1WXE0P6ST1ineNEGP4Cn7AQ+6yiz4ABseJe.kOvd9.27B2hfuuXPxEuXE2D1eFDGNl.gTxg2ocJrZnk6TcuFDmtmSLVihCn3.JNf1GP4An7.Xd.aE5b8VFiXG6fy2eQEJD677Gq4uizyKEGfVFJtmhCn3.62mgy7EVsn8IwN4IblOP8bSwAN2WnrKQubJN.83Qu9Wcimo3.JNv94CvOaCULQaAtOe932oQ138ZDJgNNkLYcjYJwc0LFSlkNCNWnB4EKcs0wcb+VsQ4lveMfGli2DmufkWqjAMZdCXe0WuBKuo.CDtwGt9vwGBwaJ5C4FMrQSFRK6qCvtTYdAjhTcbb6q9Iv74qVAITfgonN1+kJqZgN1v5SCPBwKIKnh0tvtHaaWto7YGkKpKGq89svymKkdsMFgLtdMU1K9ZX8BefEdkd7vGCRFFb6ZH9nrEiQNN4ZTMNC4bI6VXKvwax5a9onjo3AA0xUPcaB0TcRb5T3ZwdQHyosPrFrOGRLX+nttdyEyo80bjpaeMqvDt9Txtp3g7wiFyzjarj9eG6sB0Fpr2JdXjGzehXrgKX+xwDw4gOFKr11iplmP0Ynw6MCmLumswo+5PsGVtGmqaOb8fjgfjqObNw1AK19v348aJwms.CpSkdkCPJ2oNBpLtt4yiLWoI6J0sgvVULups4WIQMGp114BYp7sp7u1AHqywIdVrKBGa3JsT+gJS11Y7fxl29PzN9rgeAHZGmPYcfSwwo34TBbJQNQExBPVfXaKvjKddETTgyYICMmw3Y9KcgYMjkUPIw1VjXqmd0A29WUEIWXEb6ZzFtW4nMJeWbtK+qI293uqkmi+0TWA9Sv8J8mWlq0eE4tY+6Y1ayeBkuK+4U0A8WQcG2+AZ5bb4tXCHkDY4kYxr4OrzXUjaeXaYpCjsmYmE6jEORVBkmCaPUlOaBUME1SV6LXuTcEv9vCUHq9lVL62O+JXw4dkrTS54X8KkUwFd5uLarYtV1z895bcsd1xG0aw02lYqa7uKWme.a6y7i35carFVvmx08WvtXoeEW+6hk9p+Vl2J+N1n139XiupCxlYM+HaA09SrR2wQYuXcGmsg89qrpOzuw9xi7Gruuoyw94S+mrSc9+lcoK+O742Ez43ZGjZRc.5cmiG5WJIBCI0NBCO8NArd2EXrYlLLo90MXZd6NLqgzCXtCKMnng2SnjQ0KnLVFvymaefWYr2F7FiuuvaOo9Cu2TGHr0oc6vmMyAC6bVCE18ryB1+b8AGdA2EbrhFAbhhGIblRta3Bk5GBTF.wWdNPWdgw.8X02CjwqNNn+UlOLz27dgQrwIBY+NSFxqpo.226e+vzq4AfG6iePX90NC3o97GBV9NdDvbmOJTQcE.qY2yAV2debXS6edvVNTgvmb3m.19QVH7MGaQvdZZwvANwRfFN8RgFOyxfSd9U.m8BOCbwK+rBh7+w79e56kjyuWR19ECp6etQ4Yxu1yU2omsEAu5M50gZ9Uqpq81AqYpwV4uCrq+qC9suJ+6U1jGSfjgA5yL7vqfjgmA6I+hJ7o4LrU3JasgLB9d5dtRIbPaVedZVO2X6XhELmhWjmrh3hvR9vZE49ZE42YDkyEn7C2.43ZHHE5Z015pYXvw0Rkfx+gQNO7dQEsqoZu9QpkFHhUdWOsLLRJYAHK.YA9+qEPliTjaT96ls++2vtp5qUvkrl3RQSbcSSb2rl35tl3RUSb8PSb2hl3RSSboqItdpItaUSb8RSb8VSbYHvEx0oD9ls41a49NZysjZiufRo7MIxejeC0W5QWxuf0o2a146MS1CxdP6Kn7BTd.JO.kGvw4YZTb1gqbOtp6Sk3VlonO6P3uGeJNHZ0eGomKJN.sLQe6uij+NR8SwATbf8+NNbFmX0h1mD6jmvY9.0yMEG3begxtD8xo3.ziG85e0MdlhCn3.6mOns1+WK+O.raLEf3EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+cqA..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20191229_2.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "0f008df5fbf1b024da2d2d22a01951e1"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum",
					"varname" : "vst~",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"data" : 					{
						"autosave" : 1,
						"snapshot" : 						{
							"filetype" : "C74Snapshot",
							"version" : 2,
							"minorversion" : 0,
							"name" : "snapshotlist",
							"origin" : "vst~",
							"type" : "list",
							"subtype" : "Undefined",
							"embed" : 1,
							"snapshot" : 							{
								"pluginname" : "Serum.vstinfo",
								"plugindisplayname" : "Serum",
								"pluginsavedname" : "",
								"pluginsaveduniqueid" : 1483109208,
								"version" : 1,
								"isbank" : 0,
								"isbase64" : 1,
								"blob" : "5097.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQq3EP6b0GaTUDD+cGP+B4iVjhsHziZ3qJeX4THf71osDgVjODLh.pnEhPKJEoHEnFadjF0p3GfMp7QTgznhsJJoQEknXSigpzJPHFpRPJow.BZ.RiBBQO2ce6d8cau1tg+nE5a1jgYmc9syt6LyN7t2coFFFfAuYI31RkZDqGVOKKi.1izd37nINuZhqaZhq6ZhqGZhKBMwEol3hRSbQqItXzDWO0D2MoItdoItdqIt9Hw0Pf.mDn4XHG8CXd.dO.qCf0Av5.Xc.Yc.wiexXMvxKnsKfbWkefeXc7OXdfsyvscOvQJ.uKlGf4ALO.lG3tyCj0As8B3yGH8GtEtZbWU1s3Gb6mS03tpra2+3VN+pwcUY2hevseNML7RaznO68EDj7PaxLhvvChiMm.JszoC5jBy7augjuWCjiumSVtFlGf4AXd.dO.qCf0AX0Abzb6O+la876HEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9Ukkii7t10MTi6pxX7uqc7WFeUi6pxRbHuqc9fZbWUFi+csi+x3qgQ67caHSL5v3986m9NMRi8dMTI1dPpSzmwrD3tVliEw1lAWKlAoMaas6ocJSaYldK3uGwCSwawVufsWufQMYp.TWs0JwRE4XfvM+vMFa9JDUjOFiaT+1sHLZseS.xUJJEfQsVe17pq1YP76uRNwMfgEuOa7qTT4barssl.vHFdAYCku249EgrS8VhyNSOuuXtNG2FOcsj10wb35n10R5un6gsxiA13k1wGcNLxvf5WUhQowmiXdh8nbdFh0RLL2WvluEYnYEmTmjGDTa2gYaKnhxigRmksWb135B0Wv2CNWCAF13La0Qy4qoy8bq024dVhIbiI0cMwU95QcMhTmkH9GxcKUenzeK4gQev3ICiCbAGWLmVccnywFqi6nx0Q0lp46s.mntmi4o+9PdGVbGmZaeT6vHCNI1er0jIGr43KimNtk.eZbLLaJsqXBB8gZifFiZa55HpUZQZtuCDN5xpq5X8kZjqgT1ImqSVuUV+0I.QeJN9YwoJ1bCWqsFWUmPNz7AoOu6JVmc1X+.H5FkX55AkhfRQRonnTzTBanG.8.taOvryeYYmWtKY0iM8o3Km0rhTGyZyt.2sGwcc5kO31+I6H3bufWOS1v6FlrQwUQ4dL2TFCwruEmt4lpNayn7tAyLSdylkjwNMqYw6wLphqxLyxNhYIUeJyC23Eo58PFQbQSxL4XI4LtDHkjwPH6ZtijTyhSkbl7mHIphSmLpRyhLixlC4IqbAjWt5rIezQykTaiqh7GMsdRDd2.I9XdNxvh6EIiOwWgL0j2LY9o7FTasUx5lzaSs2NIaY5uG0leHYuK7io1cOj5W9mQs8WRtbgeM09UQRbieGIkR+dxj1dcjoW1QHKrhehr7J+YRg66DjWp5SQ11A+MR4G82Ie0w+SxOz3EI+x49KxYa5eHW4p+Kc88.8NhtAwGSOfA26HggEWzvXhumv3SrW.Yv8AlZxwByZX8ClWJ8GVzXF.rzwk.j23GHTvjFDTDII34yXHvqN0aCdyoOT3cl0vg2etiD187tc3yW3ng8unwBGXwoBGZo9gis76BNYdS.Nc9SDNeA2MboBMg.EAPjEmNzmWXJv.138.I8ZSCFdoYAi8stWXBaelPZu6rgLKaNv88A2OL+Jd.3w9jGDxoxE.O0W7Pv512i.V6+QgRpNaXSGXIvVN3iC63PKC10QyE9zi8DvdO9Jfu8jqDpowUAG9zqFp+bqAZ37qENSSqGtvkdF3xW8Y4DF+c8we72kTn+tjb7eLHeuaMHdl7NJtis.uam89Pt9x8UGkePtNx0syeez7urbKZNAiDMeTNiL7MZeYkWtOMkwjBWa20mTvOmtulagCZKFyWKFoycfYl8Rxek9RsU2D15GW6n2e6n+NaU8TEx3PmHmsGBRp6UG6qVfgMu1pET+ONwkwdunb4JJOESF0VSjgU7tdZaXnVzCfd.zCbiqGPTizt1n7Xz7eeC6a.wXsCtX0DWbZhqeZh6l0DW+0DW7Zha.Zh6VzDWBZhKQMwMPMwcqZhaPZhavZhKINNkWmR3EkIkW2v8dGW2rUtAYiDWw6.Xa0rpu1BOwp+UVe7yMG5maF8Gn+.uWf0Ev5.Xc.rNPHOWSC7mc.+6Vk7825V3gjDvdjQLOf6RbKwe44DyCr8.R+gaki4AXdfyb.48.4XRYj6NxSTi6pxXd.lGv7.Xd.lGf4Atm6AW+820x+G0pAR+hW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82kF..."
							}
,
							"snapshotlist" : 							{
								"current_snapshot" : 0,
								"entries" : [ 									{
										"filetype" : "C74Snapshot",
										"version" : 2,
										"minorversion" : 0,
										"name" : "Serum",
										"origin" : "Serum.vstinfo",
										"type" : "VST",
										"subtype" : "Instrument",
										"embed" : 0,
										"snapshot" : 										{
											"pluginname" : "Serum.vstinfo",
											"plugindisplayname" : "Serum",
											"pluginsavedname" : "",
											"pluginsaveduniqueid" : 1483109208,
											"version" : 1,
											"isbank" : 0,
											"isbase64" : 1,
											"blob" : "5097.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQq3EP6b0GaTUDD+cGP+B4iVjhsHziZ3qJeX4THf71osDgVjODLh.pnEhPKJEoHEnFadjF0p3GfMp7QTgznhsJJoQEknXSigpzJPHFpRPJow.BZ.RiBBQO2ce6d8cau1tg+nE5a1jgYmc9syt6LyN7t2coFFFfAuYI31RkZDqGVOKKi.1izd37nINuZhqaZhq6ZhqGZhKBMwEol3hRSbQqItXzDWO0D2MoItdoItdqIt9Hw0Pf.mDn4XHG8CXd.dO.qCf0Av5.Xc.Yc.wiexXMvxKnsKfbWkefeXc7OXdfsyvscOvQJ.uKlGf4ALO.lG3tyCj0As8B3yGH8GtEtZbWU1s3Gb6mS03tpra2+3VN+pwcUY2hevseNML7RaznO68EDj7PaxLhvvChiMm.JszoC5jBy7augjuWCjiumSVtFlGf4AXd.dO.qCf0AX0Abzb6O+la876HEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9Ukkii7t10MTi6pxX7uqc7WFeUi6pxRbHuqc9fZbWUFi+csi+x3qgQ67caHSL5v3986m9NMRi8dMTI1dPpSzmwrD3tVliEw1lAWKlAoMaas6ocJSaYldK3uGwCSwawVufsWufQMYp.TWs0JwRE4XfvM+vMFa9JDUjOFiaT+1sHLZseS.xUJJEfQsVe17pq1YP76uRNwMfgEuOa7qTT4barssl.vHFdAYCku249EgrS8VhyNSOuuXtNG2FOcsj10wb35n10R5un6gsxiA13k1wGcNLxvf5WUhQowmiXdh8nbdFh0RLL2WvluEYnYEmTmjGDTa2gYaKnhxigRmksWb135B0Wv2CNWCAF13La0Qy4qoy8bq024dVhIbiI0cMwU95QcMhTmkH9GxcKUenzeK4gQev3ICiCbAGWLmVccnywFqi6nx0Q0lp46s.mntmi4o+9PdGVbGmZaeT6vHCNI1er0jIGr43KimNtk.eZbLLaJsqXBB8gZifFiZa55HpUZQZtuCDN5xpq5X8kZjqgT1ImqSVuUV+0I.QeJN9YwoJ1bCWqsFWUmPNz7AoOu6JVmc1X+.H5FkX55AkhfRQRonnTzTBanG.8.taOvryeYYmWtKY0iM8o3Km0rhTGyZyt.2sGwcc5kO31+I6H3bufWOS1v6FlrQwUQ4dL2TFCwruEmt4lpNayn7tAyLSdylkjwNMqYw6wLphqxLyxNhYIUeJyC23Eo58PFQbQSxL4XI4LtDHkjwPH6ZtijTyhSkbl7mHIphSmLpRyhLixlC4IqbAjWt5rIezQykTaiqh7GMsdRDd2.I9XdNxvh6EIiOwWgL0j2LY9o7FTasUx5lzaSs2NIaY5uG0leHYuK7io1cOj5W9mQs8WRtbgeM09UQRbieGIkR+dxj1dcjoW1QHKrhehr7J+YRg66DjWp5SQ11A+MR4G82Ie0w+SxOz3EI+x49KxYa5eHW4p+Kc88.8NhtAwGSOfA26HggEWzvXhumv3SrW.Yv8AlZxwByZX8ClWJ8GVzXF.rzwk.j23GHTvjFDTDII34yXHvqN0aCdyoOT3cl0vg2etiD187tc3yW3ng8unwBGXwoBGZo9gis76BNYdS.Nc9SDNeA2MboBMg.EAPjEmNzmWXJv.138.I8ZSCFdoYAi8stWXBaelPZu6rgLKaNv88A2OL+Jd.3w9jGDxoxE.O0W7Pv512i.V6+QgRpNaXSGXIvVN3iC63PKC10QyE9zi8DvdO9Jfu8jqDpowUAG9zqFp+bqAZ37qENSSqGtvkdF3xW8Y4DF+c8we72kTn+tjb7eLHeuaMHdl7NJtis.uam89Pt9x8UGkePtNx0syeez7urbKZNAiDMeTNiL7MZeYkWtOMkwjBWa20mTvOmtulagCZKFyWKFoycfYl8Rxek9RsU2D15GW6n2e6n+NaU8TEx3PmHmsGBRp6UG6qVfgMu1pET+ONwkwdunb4JJOESF0VSjgU7tdZaXnVzCfd.zCbiqGPTizt1n7Xz7eeC6a.wXsCtX0DWbZhqeZh6l0DW+0DW7Zha.Zh6VzDWBZhKQMwMPMwcqZhaPZhavZhKINNkWmR3EkIkW2v8dGW2rUtAYiDWw6.Xa0rpu1BOwp+UVe7yMG5maF8Gn+.uWf0Ev5.Xc.rNPHOWSC7mc.+6Vk7825V3gjDvdjQLOf6RbKwe44DyCr8.R+gaki4AXdfyb.48.4XRYj6NxSTi6pxXd.lGv7.Xd.lGf4Atm6AW+820x+G0pAR+hW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82kF..."
										}
,
										"fileref" : 										{
											"name" : "Serum",
											"filename" : "Serum_20191229.maxsnap",
											"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
											"filepos" : -1,
											"snapshotfileid" : "a95fce74fa1d99bf25252e27dab65bb0"
										}

									}
 ]
							}

						}

					}
,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-17",
					"lockeddragscroll" : 0,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 9,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "multichannelsignal", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 1263.0, 1028.0, 103.0, 23.0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"text" : "mc.vst~ Serum",
					"varname" : "mc.vst~",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1254.0, 677.0, 50.0, 23.0 ],
					"textcolor" : [ 0.996078431372549, 0.643137254901961, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1254.0, 605.0, 50.0, 23.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-18",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1117.0, 952.0, 76.333343999999997, 23.0 ],
					"text" : "208 0"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1117.0, 906.0, 47.0, 23.0 ],
					"text" : "thresh"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 964.5, 691.0, 29.5, 23.0 ],
					"text" : "join"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 975.0, 644.0, 29.5, 23.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 974.899999999999977, 611.0, 50.0, 23.0 ],
					"text" : "unpack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1183.0, 666.0, 50.0, 23.0 ],
					"text" : "64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "gridmeter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1145.0, 1125.0, 110.0, 72.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "setvalue", "int" ],
					"patching_rect" : [ 1087.0, 838.0, 74.0, 23.0 ],
					"text" : "mc.target 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 922.5, 790.0, 125.0, 23.0 ],
					"text" : "midiformat @hires 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 828.0, 465.0, 42.0, 23.0 ],
					"text" : "midiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 862.0, 387.0, 50.0, 23.0 ],
					"text" : "256"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "int" ],
					"patching_rect" : [ 835.0, 321.0, 50.5, 23.0 ],
					"text" : "key"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1948.322544957824903, 179.259320139884949, 83.0, 23.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-194",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1951.929616881225684, 147.000004410743713, 31.0, 23.0 ],
					"text" : "r d4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-170",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1712.54844856262207, 614.278269410133362, 80.0, 23.0 ],
					"text" : "r gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1846.145222187042691, 211.338450968265533, 40.0, 23.0 ],
					"text" : "join 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1844.645222187042691, 147.000004410743713, 30.0, 23.0 ],
					"text" : "r v4"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1848.106043291900733, 255.997196674346924, 108.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "int", "int", "int", "int", "int" ],
					"patching_rect" : [ 1844.645222187042691, 177.818310618400574, 105.0, 23.0 ],
					"text" : "unpack 0 0 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1690.511427640914917, 221.188684344291687, 79.0, 23.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1718.053825855255127, 255.997196674346924, 114.0, 23.0 ],
					"text" : "r patchparameters"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1861.106043291900733, 662.5, 50.0, 52.0 ],
					"text" : "4 0.366723"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1767.645222187042236, 655.519649624824524, 43.0, 23.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1682.645222187042236, 655.519649624824524, 43.0, 23.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1767.645222187042236, 699.852960586547852, 80.0, 23.0 ],
					"text" : "send~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1682.645222187042236, 699.852960586547852, 72.0, 23.0 ],
					"text" : "send~ left1"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1682.645222187042236, 316.968622326850891, 467.0, 276.292685747146606 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[22]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5199.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................PwD3EP6b0GTUUDE+Bp7koHXhAlxSZ7K7CDozQx2d.bRAC0zlL0JKzIErDSLQklXtFS4yrRMlJ+XpzgoxfxxgohxoLGpQSwTGpQJGSPpQSqTGm7yonyt269dWV9Z6e.8s6Ny4c1y47a28rmy4tu66ximwulbxFFlfEYvZgiulfwvY8MMMZvRqAhg1nX80J1Hh.nRllAHIt.kDWmjDWmkDWWjDWPRhKXIwEhj3BURbgIIttJItaQRbcSRbcWRbgyv4Y0q9Tm+aNIbx5p6H0sl5ArTp1FZ3DZtNNnqCzWGnOGPeNf9b.09b.e2roQszyCv140bkJNv1rNdQWGXELTsqCbTBv5pqCz0AzHftNPsqC3mCZEEz2e.OdnJbw7tnrpDGT88oXdWTV0iOpx9WLuKJqJwAUeeZXDH1vrO84E3kB.a7Jhlg6EGcLMHzREU5jZlw2VpzOOK094Yoy+57O8LFccftNPWGnuNP7b.G2+fpe+ap592QI.qqpFGT88stNvJBnqCZbkfpGOT08OuJfu+Ek450b+6yMDy6hx57u+c9mmeEy6hxbbZt+c8fXdWTVm+8uy+77qgQa721fWXzNxMwmwUJPXYVAYnUtKR1q8pDbwg03wiaJmZy1YfeHych1Lgz9qP45o1bRTnba18oLSFlJV2hXysEF13nFwVJL66Xh0QWSZCkMgKM3GlsdVprd8UyeXia+msZxAqpJNVzf07KN95K34Hh5rlS1ZyVSq0hM2TYVqlsXRnzx+pFHWqv3AJ0R8wA.GrpLIIkT4LxZFLY8o5uVgkxliMuonAJQwaSVPY9tIMtvaNry1Wndpcq8HJf1cpmJy7A93nbrwlSdezGYwKzG1DKGf9FZiOOtv9Txv.wwricoxHkBaL1iiBAalLrXGNmokJaMdSx.xHRtMNmioM3z41DJqzvP5LNiKzwAWp7QxrS6KP1XsFukMZeJt1SNasvkk2Dk45o9USZMmtl.5+iBg+7nJiHFiry6M5ZKwPGOdy4Micu4OJFG37p2dLs35fiwBq00bNmCVemyoX8N2e7t17qi84Kx6G7qgsuFGmBW37RIK+v1+nqoCeBeOKpFqFp2zFeJLLz4jOu9vXMe7AIv8ddINW343VmcJfwqH8bUGqOWusOvEaDmYiedK+72FgvR.ww1KNMw1SNUX2u0zKZyVtw0C7XdmElb5di9EfnSHQs0EjBBofQJDjBEIcSGAzQ.0NBL07VPV4ly7VZBoNdWYurEk3HVdV4q1QD0Z2yuws+k2wlyhBAFv3LBbUiynn8f7.bu9z5u6dTTptWekY4Nj.Wk6ziaCt8j11buu4tS2gTzdbmdIGwsmJqy8gq+Bn8.HCNxPIoGWDjrGUzDOo0ex1m9PH6atIRNcdikDRQoRFVwYPxrjoQdxxmE4kpLKxGTcNjppeIj+3hqjDTfqhDUXOOYfQtFxni4kISHtMPlY7uFNWahrhjeSb91FYiS5cv478IUL6ODm2cRpYgeBN2eN4JE7k37uGRLq8aIwW72QRdKGjLoRNBY1k8ijEV9OQJXWGm7hUVGYyG32HkV8uS9hi8mj8W+EH+7Y+axYt3UIW65+Ct9A.cOnNAQEVWf908fgAFYnvHhpqvnioa.oegCSHtHfoLvdByH9dAyYD8Fl+nhFxcz8AxO49BERhEdgz5O7JS3NfWeRC.dqoLH3cm9PfcLigBe5rGNr64j.r24lHbn4mDbzEdWvIxcLvoxarv4x+tgKWfangBAH3hREBe0iG58ZuGH10MQXPEmAjvabuvX1xjgTd6oBoWxzf668teXlk8.vi8QOHjc4yBdpO6gfUrqGAL28iBdpLKX86cdvFOviCa8PK.1d04.e7QeBnhisH3qOwhg8U+RfCepkB0b1kA0dtkCm9hqDN+keF3JW+YYjN+q74ewueJptri2Xf+b2p09dxau3NbAV2NZ+fu9b+p8JNvWG951w6G99lkah0DTxt4B4Txv0vckQt47zHiJ0bscTSrd+b5t70ZNnMQmqlnoiUwjyZd4sXWI1hNgk8Q0F1SpMremsncz.OOzAxo9fWRzWc3WMACcbsVyq8uerK.qYLYxkUZ7toTqMPJV6m0SqCSaUGAzQ.cD3l2Hf8YjVmMx2F99cKrGMXqqMvEgj3hTRb8TRb2pj35kj3hRRb8VRb2lj3hVRbwHIt9HItaWRb8URb8SRbwxvI73TZdQdQ4ML7.G4MLtxMINRjEsUf5pYTSUEb7k9Kz9p9mSVu+0++souNPeNf9b.84.5yAZ54.NtylZY26f92sJ9yuUU3NJAXc00AVQDUI+y2m55.0Luyy+bttNPWG3rFPrtfKq4pQcBuVfmuEk450b+65Aw7tnrN+6em+44Ww7tnLGml6eWObi2uqk+GdZsWSgW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8W+F..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5199.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................PwD3EP6b0GTUUDE+Bp7koHXhAlxSZ7K7CDozQx2d.bRAC0zlL0JKzIErDSLQklXtFS4yrRMlJ+XpzgoxfxxgohxoLGpQSwTGpQJGSPpQSqTGm7yonyt269dWV9Z6e.8s6Ny4c1y47a28rmy4tu66ximwulbxFFlfEYvZgiulfwvY8MMMZvRqAhg1nX80J1Hh.nRllAHIt.kDWmjDWmkDWWjDWPRhKXIwEhj3BURbgIIttJItaQRbcSRbcWRbgyv4Y0q9Tm+aNIbx5p6H0sl5ArTp1FZ3DZtNNnqCzWGnOGPeNf9b.09b.e2roQszyCv140bkJNv1rNdQWGXELTsqCbTBv5pqCz0AzHftNPsqC3mCZEEz2e.OdnJbw7tnrpDGT88oXdWTV0iOpx9WLuKJqJwAUeeZXDH1vrO84E3kB.a7Jhlg6EGcLMHzREU5jZlw2VpzOOK094Yoy+57O8LFccftNPWGnuNP7b.G2+fpe+ap592QI.qqpFGT88stNvJBnqCZbkfpGOT08OuJfu+Ek450b+6yMDy6hx57u+c9mmeEy6hxbbZt+c8fXdWTVm+8uy+77qgQa721fWXzNxMwmwUJPXYVAYnUtKR1q8pDbwg03wiaJmZy1YfeHych1Lgz9qP45o1bRTnba18oLSFlJV2hXysEF13nFwVJL66Xh0QWSZCkMgKM3GlsdVprd8UyeXia+msZxAqpJNVzf07KN95K34Hh5rlS1ZyVSq0hM2TYVqlsXRnzx+pFHWqv3AJ0R8wA.GrpLIIkT4LxZFLY8o5uVgkxliMuonAJQwaSVPY9tIMtvaNry1Wndpcq8HJf1cpmJy7A93nbrwlSdezGYwKzG1DKGf9FZiOOtv9Txv.wwricoxHkBaL1iiBAalLrXGNmokJaMdSx.xHRtMNmioM3z41DJqzvP5LNiKzwAWp7QxrS6KP1XsFukMZeJt1SNasvkk2Dk45o9USZMmtl.5+iBg+7nJiHFiry6M5ZKwPGOdy4Micu4OJFG37p2dLs35fiwBq00bNmCVemyoX8N2e7t17qi84Kx6G7qgsuFGmBW37RIK+v1+nqoCeBeOKpFqFp2zFeJLLz4jOu9vXMe7AIv8ddINW343VmcJfwqH8bUGqOWusOvEaDmYiedK+72FgvR.ww1KNMw1SNUX2u0zKZyVtw0C7XdmElb5di9EfnSHQs0EjBBofQJDjBEIcSGAzQ.0NBL07VPV4ly7VZBoNdWYurEk3HVdV4q1QD0Z2yuws+k2wlyhBAFv3LBbUiynn8f7.bu9z5u6dTTptWekY4Nj.Wk6ziaCt8j11buu4tS2gTzdbmdIGwsmJqy8gq+Bn8.HCNxPIoGWDjrGUzDOo0ex1m9PH6atIRNcdikDRQoRFVwYPxrjoQdxxmE4kpLKxGTcNjppeIj+3hqjDTfqhDUXOOYfQtFxni4kISHtMPlY7uFNWahrhjeSb91FYiS5cv478IUL6ODm2cRpYgeBN2eN4JE7k37uGRLq8aIwW72QRdKGjLoRNBY1k8ijEV9OQJXWGm7hUVGYyG32HkV8uS9hi8mj8W+EH+7Y+axYt3UIW65+Ct9A.cOnNAQEVWf908fgAFYnvHhpqvnioa.oegCSHtHfoLvdByH9dAyYD8Fl+nhFxcz8AxO49BERhEdgz5O7JS3NfWeRC.dqoLH3cm9PfcLigBe5rGNr64j.r24lHbn4mDbzEdWvIxcLvoxarv4x+tgKWfangBAH3hREBe0iG58ZuGH10MQXPEmAjvabuvX1xjgTd6oBoWxzf668teXlk8.vi8QOHjc4yBdpO6gfUrqGAL28iBdpLKX86cdvFOviCa8PK.1d04.e7QeBnhisH3qOwhg8U+RfCepkB0b1kA0dtkCm9hqDN+keF3JW+YYjN+q74ewueJptri2Xf+b2p09dxau3NbAV2NZ+fu9b+p8JNvWG951w6G99lkah0DTxt4B4Txv0vckQt47zHiJ0bscTSrd+b5t70ZNnMQmqlnoiUwjyZd4sXWI1hNgk8Q0F1SpMremsncz.OOzAxo9fWRzWc3WMACcbsVyq8uerK.qYLYxkUZ7toTqMPJV6m0SqCSaUGAzQ.cD3l2Hf8YjVmMx2F99cKrGMXqqMvEgj3hTRb8TRb2pj35kj3hRRb8VRb2lj3hVRbwHIt9HItaWRb8URb8SRbwxvI73TZdQdQ4ML7.G4MLtxMINRjEsUf5pYTSUEb7k9Kz9p9mSVu+0++souNPeNf9b.84.5yAZ54.NtylZY26f92sJ9yuUU3NJAXc00AVQDUI+y2m55.0Luyy+bttNPWG3rFPrtfKq4pQcBuVfmuEk450b+65Aw7tnrN+6em+44Ww7tnLGml6eWObi2uqk+GdZsWSgW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8W+F..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[6]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1546.172099828720093, 552.688196420669556, 82.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1702.0, 889.0, 150.0, 36.0 ],
					"text" : "just 3 params accessed for AT, cc74 etc"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 845.0, 1108.0, 80.0, 23.0 ],
					"text" : "mc.dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1145.0, 993.0, 73.0, 23.0 ],
					"text" : "mc.stereo~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1024.0, 993.0, 104.0, 23.0 ],
					"text" : "mc.interleave~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "setvalue", "int" ],
					"patching_rect" : [ 1263.0, 758.0, 478.0, 23.0 ],
					"text" : "mc.targetlist @chans 4"
				}

			}
, 			{
				"box" : 				{
					"align" : 0,
					"attr" : "mcisolate",
					"id" : "obj-28",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 922.5, 838.0, 150.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"data" : 					{
						"autosave" : 1,
						"snapshot" : 						{
							"filetype" : "C74Snapshot",
							"version" : 2,
							"minorversion" : 0,
							"name" : "snapshotlist",
							"origin" : "vst~",
							"type" : "list",
							"subtype" : "Undefined",
							"embed" : 1,
							"snapshot" : 							{
								"pluginname" : "Serum.vstinfo",
								"plugindisplayname" : "Serum",
								"pluginsavedname" : "",
								"pluginsaveduniqueid" : 1483109208,
								"version" : 1,
								"isbank" : 0,
								"isbase64" : 1,
								"blob" : "5121.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQw3EP6b0GaTUDD+cGzOQnzhTrEgdTCeU4CKmVBUd6zVhPKxGBFQ.UzBQfhRQJRApwlGoQsJ9AXiJEhJjFUrUQIMpnDEIMFPoHPHFpRPJoZ.AM.gnfPzyc22tW260qsa7OZg9lMYtY2c9syN6Lyat28tKmwujYlFFVfMYvawwe0C+UKKi.1yZHvvvZ2XHpvHdNPKKOJ3XxaFGaTy37pIttoIttqItHzDWjZhKJMwEsl3hQSbwpItdnItaPSb8TSb8RSbwIw0Xf.m.noHHG8CXd.dc.VG.qCf0Av5.x5.raeTzZjkWPamG4tJ+.+vp7BlGX6LbaWGnjBv6h4AXd.yCf4At67.YcPau.d+AR+gag6Lt6brawO31OmNi6NG618Otkyuy3tywtE+fa+bZX3k1nQe1yKHH4g1jYDggGDGaMAbzxlNoJEl02dSIetFHGeNmrbMLO.yCv7.75.rN.VGfUGPo41u+M254WIEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9cNVNOx6ZW2vYb24XL920N9KiuNi6NGKwg7t14CNi6NGiw+t1weY70vnc9tMjIFcXb+98KelFLtJwrA53rTkSmxRfgOuJdEbs1ZrHgYOnSYqqsOoSZRGvZTcYA+0vdPJdKod4Bd0hGw3YxOP80KwRGxw.ga8gaNa8y1ifDsKuOiazvlsHLZUeU.xUJMMfQsVe15NP8Sg32esbhq.CKde17Wozp45XSUlDvHFdAYCka6b+hXrpbKwYmIm2WrV04swS2KodUVCWFUuVR+E0FpjGCrwK0iO5ZXjgA0u5HFkEeMh0IrQ45LD6kXZtufsdKxfyKAoLIOHn1tCS2VPMUGKkNCyVTabYg5K31fxdXudaLr9L4cjb9doZysVeEaNHjvMWPg+e533qG00Lj5qDw8Pt1xoKT5uk7vHOX7jgQAWv4EqoU2G5ZrwpbMpbebpSm46s.mntmx5z2NjWCKtFmpaeT8vHCNIrO1dxFGro7kwSm2RfOKNFlNk5Ur.g7P0QPkQ0MceD0JsHM2WAgRWVcUk8WJQtGxwpbtLY8VY8WU.h9Tb7yhpH1ZCWqsl2oLw3PyGj97t6P6ryF6G.Q2nDSVDTJRJEEkhlRwPIrgd.zC3t8.SunEkegErfUL5rmfuEuxkl9nVU9E6t8HtqSu7F29WYGAm6E75Y7FdW63MJaOTtGy0myfL6cYYat95x2LZuq0L2T2fY44rUy8M+cXFcY6wL2pNrY40cRyC0zEnx8PFVBwPxM03IKdLIQJOmAQ11LGNYeyOcxoKZbjnKKaxHpHOxTpZFjGu14Pdw5xm7AGo.R8Msbxuew0Phz6ZIIF6yPFRBOOIijeIxDScCjYm1qQ0UkjUm4aR02VIabxuCUmuOYmy8Co5cGjFVxmP08mStbIeIU+6gj759FRZU7sjL27AHStpCSlaM+.YI09ijR10wIuPcmjro8+qjpOxuQ9hi8GjuqoKP9oy9mjybw+lbkq9Oz82CzqH6FjXrQ.CrWQACIgXfQkXOfLRtm.YfwASL03goMj9.yJs9ByaT8CV3XRBJLi9CEm4.fRIo.OaNCBd4IdKvqO4ACu0zFJ7tyb3v1m0sBe5bGIr64MZXuyOc3fKzObzkbGvIJbrvoJZbv4J9NgKUhIDnT.hprrg3dtI.8ac2EjxqLIXnUjGL523tgwt4oBY81SGxspY.2y6cuvrq49fG4iteXw0NG3I9rG.V8tdHvZ2OLTdc4CqeuK.139eTXKGbQv1NRAvGezGC14wVJ70mXYv9ZZ4vgN0JfFN6JgFO2pfSew0.m+ROEb4q9zbBi+t93O96RJzeWRJuwf74t0n3dx6n3Jl.uamscH2eoc0Q4Gj6ibe67sil+kkaQyIXjn4ixYjguQ5KuBK3IoL1nv01dCoD7yo6q4V3f1h470hY5bmXp4ufhVluzaUivV9XZG49aG42dqJmJPFG5D4LaHH4zVUrqVfgst1pET92OtEwdtn7w0TcZlLpsVHCq3Y8z1vPonG.8.nG35WOfnFocsQ4wn4+OB6c.wbsCt30DWBZhqOZh6F0DWe0DWhZhqeZh6lzDWRZhKYMw0eMwcyZha.ZhafZhKENNGONkvOTlTdMC26scMiobchgjPYaAXlZdMTeIGeE+LqO94lC8yMi9CzefWWf0Ev5.Xc.rNPH2WSi76c.+eqR97acK7PRBX2xHlGvcItk3u7bh4A1d.o+vsxw7.LOPMGPdcfbN4Xj6NxSbF2cNFyCv7.lG.yCv7.LOv8bcv0d+uV9eeOxRCgW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82qF..."
							}
,
							"snapshotlist" : 							{
								"current_snapshot" : 0,
								"entries" : [ 									{
										"filetype" : "C74Snapshot",
										"version" : 2,
										"minorversion" : 0,
										"name" : "Serum",
										"origin" : "Serum.vstinfo",
										"type" : "VST",
										"subtype" : "Instrument",
										"embed" : 0,
										"snapshot" : 										{
											"pluginname" : "Serum.vstinfo",
											"plugindisplayname" : "Serum",
											"pluginsavedname" : "",
											"pluginsaveduniqueid" : 1483109208,
											"version" : 1,
											"isbank" : 0,
											"isbase64" : 1,
											"blob" : "5121.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQw3EP6b0GaTUDD+cGzOQnzhTrEgdTCeU4CKmVBUd6zVhPKxGBFQ.UzBQfhRQJRApwlGoQsJ9AXiJEhJjFUrUQIMpnDEIMFPoHPHFpRPJoZ.AM.gnfPzyc22tW260qsa7OZg9lMYtY2c9syN6Lyat28tKmwujYlFFVfMYvawwe0C+UKKi.1yZHvvvZ2XHpvHdNPKKOJ3XxaFGaTy37pIttoIttqItHzDWjZhKJMwEsl3hQSbwpItdnItaPSb8TSb8RSbwIw0Xf.m.noHHG8CXd.dc.VG.qCf0Av5.x5.raeTzZjkWPamG4tJ+.+vp7BlGX6LbaWGnjBv6h4AXd.yCf4At67.YcPau.d+AR+gag6Lt6brawO31OmNi6NG618Otkyuy3tywtE+fa+bZX3k1nQe1yKHH4g1jYDggGDGaMAbzxlNoJEl02dSIetFHGeNmrbMLO.yCv7.75.rN.VGfUGPo41u+M254WIEf20s5Gb6maLOv1Cf4AglI318Gt0yuLKPd9cNVNOx6ZW2vYb24XL920N9KiuNi6NGKwg7t14CNi6NGiw+t1weY70vnc9tMjIFcXb+98KelFLtJwrA53rTkSmxRfgOuJdEbs1ZrHgYOnSYqqsOoSZRGvZTcYA+0vdPJdKod4Bd0hGw3YxOP80KwRGxw.ga8gaNa8y1ifDsKuOiazvlsHLZUeU.xUJMMfQsVe15NP8Sg32esbhq.CKde17Wozp45XSUlDvHFdAYCka6b+hXrpbKwYmIm2WrV04swS2KodUVCWFUuVR+E0FpjGCrwK0iO5ZXjgA0u5HFkEeMh0IrQ45LD6kXZtufsdKxfyKAoLIOHn1tCS2VPMUGKkNCyVTabYg5K31fxdXudaLr9L4cjb9doZysVeEaNHjvMWPg+e533qG00Lj5qDw8Pt1xoKT5uk7vHOX7jgQAWv4EqoU2G5ZrwpbMpbebpSm46s.mntmx5z2NjWCKtFmpaeT8vHCNIrO1dxFGro7kwSm2RfOKNFlNk5Ur.g7P0QPkQ0MceD0JsHM2WAgRWVcUk8WJQtGxwpbtLY8VY8WU.h9Tb7yhpH1ZCWqsl2oLw3PyGj97t6P6ryF6G.Q2nDSVDTJRJEEkhlRwPIrgd.zC3t8.SunEkegErfUL5rmfuEuxkl9nVU9E6t8HtqSu7F29WYGAm6E75Y7FdW63MJaOTtGy0myfL6cYYat95x2LZuq0L2T2fY44rUy8M+cXFcY6wL2pNrY40cRyC0zEnx8PFVBwPxM03IKdLIQJOmAQ11LGNYeyOcxoKZbjnKKaxHpHOxTpZFjGu14Pdw5xm7AGo.R8Msbxuew0Phz6ZIIF6yPFRBOOIijeIxDScCjYm1qQ0UkjUm4aR02VIabxuCUmuOYmy8Co5cGjFVxmP08mStbIeIU+6gj759FRZU7sjL27AHStpCSlaM+.YI09ijR10wIuPcmjro8+qjpOxuQ9hi8GjuqoKP9oy9mjybw+lbkq9Oz82CzqH6FjXrQ.CrWQACIgXfQkXOfLRtm.YfwASL03goMj9.yJs9ByaT8CV3XRBJLi9CEm4.fRIo.OaNCBd4IdKvqO4ACu0zFJ7tyb3v1m0sBe5bGIr64MZXuyOc3fKzObzkbGvIJbrvoJZbv4J9NgKUhIDnT.hprrg3dtI.8ac2EjxqLIXnUjGL523tgwt4oBY81SGxspY.2y6cuvrq49fG4iteXw0NG3I9rG.V8tdHvZ2OLTdc4CqeuK.139eTXKGbQv1NRAvGezGC14wVJ70mXYv9ZZ4vgN0JfFN6JgFO2pfSew0.m+ROEb4q9zbBi+t93O96RJzeWRJuwf74t0n3dx6n3Jl.uamscH2eoc0Q4Gj6ibe67sil+kkaQyIXjn4ixYjguQ5KuBK3IoL1nv01dCoD7yo6q4V3f1h470hY5bmXp4ufhVluzaUivV9XZG49aG42dqJmJPFG5D4LaHH4zVUrqVfgst1pET92OtEwdtn7w0TcZlLpsVHCq3Y8z1vPonG.8.nG35WOfnFocsQ4wn4+OB6c.wbsCt30DWBZhqOZh6F0DWe0DWhZhqeZh6lzDWRZhKYMw0eMwcyZha.ZhafZhKENNGONkvOTlTdMC26scMiobchgjPYaAXlZdMTeIGeE+LqO94lC8yMi9CzefWWf0Ev5.Xc.rNPH2WSi76c.+eqR97acK7PRBX2xHlGvcItk3u7bh4A1d.o+vsxw7.LOPMGPdcfbN4Xj6NxSbF2cNFyCv7.lG.yCv7.LOv8bcv0d+uV9eeOxRCgW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82qF..."
										}
,
										"fileref" : 										{
											"name" : "Serum",
											"filename" : "Serum_20191229_3.maxsnap",
											"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
											"filepos" : -1,
											"snapshotfileid" : "9be488269310ccd0ce7be2971317546a"
										}

									}
 ]
							}

						}

					}
,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-23",
					"lockeddragscroll" : 0,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 9,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "multichannelsignal", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 922.5, 925.0, 158.0, 23.0 ],
					"saved_object_attributes" : 					{
						"mcisolate" : 1,
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"text" : "mc.vst~ Serum @chans 2",
					"varname" : "mc.vst~[1]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 202.0, 431.0, 93.0, 23.0 ],
					"text" : "mc.interleave~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 6,
					"outlettype" : [ "list", "list", "int", "int", "int", "int" ],
					"patching_rect" : [ 922.5, 540.0, 248.0, 23.0 ],
					"text" : "mc.noteallocator~ @voices 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 828.0, 496.0, 113.5, 23.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@autoconnect", 0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-11",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "demompe.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "int" ],
					"patching_rect" : [ 918.0, 414.411778926849365, 207.0, 42.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"data" : 					{
						"autosave" : 1
					}
,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-27",
					"lockeddragscroll" : 0,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 9,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "multichannelsignal", "", "", "", "", "", "", "" ],
					"patching_rect" : [ 663.0, 378.0, 103.0, 23.0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"text" : "mc.vst~",
					"varname" : "mc.vst~[2]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 255.0, 26.0, 144.0, 23.0 ],
					"text" : "buffer~ strings anton.aif"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 744.0, 170.0, 76.0, 23.0 ],
					"text" : "routepass 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 74.0, 26.0, 164.0, 23.0 ],
					"text" : "buffer~ drums drumloop.aif"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "multichannelsignal", "multichannelsignal", "multichannelsignal" ],
					"patching_rect" : [ 506.0, 230.0, 224.0, 22.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"loopend" : [ 0.0, "ms" ],
						"loopstart" : [ 0.0, "ms" ],
						"mode" : "basic",
						"originallength" : [ 3339.319666614755988, "ticks" ],
						"originaltempo" : 120.0,
						"phase" : [ 0.0, "ticks" ],
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"text" : "mc.groove~ drums 2 @chans 2 @loop 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "mc.ezdac~",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 506.0, 291.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 658.0, 170.0, 76.0, 23.0 ],
					"text" : "routepass 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 694.799999999999955, 71.0, 55.0, 55.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_shortname" : "toggle",
							"parameter_enum" : [ "off", "on" ],
							"parameter_type" : 2,
							"parameter_longname" : "toggle",
							"parameter_mmax" : 1
						}

					}
,
					"varname" : "toggle"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 506.0, 170.0, 127.0, 23.0 ],
					"text" : "mc.sig~ 1 @chans 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 6,
					"outlettype" : [ "list", "list", "int", "int", "int", "int" ],
					"patching_rect" : [ 864.0, 80.0, 111.0, 23.0 ],
					"text" : "mc.noteallocator~"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 1 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 2 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"order" : 1,
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"order" : 0,
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-12", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 1 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 1 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"disabled" : 1,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-148", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 1 ],
					"order" : 2,
					"source" : [ "obj-15", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 1 ],
					"order" : 0,
					"source" : [ "obj-15", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"order" : 3,
					"source" : [ "obj-15", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"order" : 0,
					"source" : [ "obj-15", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 1 ],
					"source" : [ "obj-15", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 4 ],
					"order" : 1,
					"source" : [ "obj-15", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 1 ],
					"order" : 2,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 1 ],
					"order" : 0,
					"source" : [ "obj-15", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 1 ],
					"order" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 1 ],
					"source" : [ "obj-153", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"source" : [ "obj-155", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 1 ],
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 3 ],
					"disabled" : 1,
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-16", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 1 ],
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"order" : 0,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"order" : 1,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"order" : 2,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 1 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 1 ],
					"source" : [ "obj-168", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 1 ],
					"source" : [ "obj-168", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 1 ],
					"source" : [ "obj-168", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-179", 1 ],
					"source" : [ "obj-168", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 1 ],
					"source" : [ "obj-168", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 1 ],
					"source" : [ "obj-168", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 0 ],
					"midpoints" : [ 2822.5, 417.5, 2695.5, 417.5 ],
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 1 ],
					"order" : 0,
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 1 ],
					"order" : 1,
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 1 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-182", 0 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 0 ],
					"source" : [ "obj-182", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-187", 1 ],
					"order" : 0,
					"source" : [ "obj-185", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 1 ],
					"source" : [ "obj-185", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"order" : 1,
					"source" : [ "obj-185", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 3 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 0 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 1 ],
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 4 ],
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"source" : [ "obj-195", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"source" : [ "obj-196", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"order" : 1,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 1 ],
					"order" : 0,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-241", 1 ],
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 1 ],
					"source" : [ "obj-215", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"source" : [ "obj-218", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"source" : [ "obj-220", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 1 ],
					"order" : 0,
					"source" : [ "obj-221", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"order" : 1,
					"source" : [ "obj-221", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"order" : 0,
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"order" : 1,
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"order" : 2,
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 1 ],
					"order" : 0,
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"order" : 1,
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"order" : 1,
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"order" : 0,
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 1 ],
					"source" : [ "obj-23", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"source" : [ "obj-232", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-235", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"source" : [ "obj-236", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"source" : [ "obj-237", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-238", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-238", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-238", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 1,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 1 ],
					"source" : [ "obj-245", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"source" : [ "obj-247", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-251", 0 ],
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-33", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"order" : 1,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"order" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 1 ],
					"order" : 2,
					"source" : [ "obj-47", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"order" : 0,
					"source" : [ "obj-47", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"order" : 3,
					"source" : [ "obj-47", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 1 ],
					"order" : 1,
					"source" : [ "obj-47", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-508", 0 ],
					"source" : [ "obj-479", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 1 ],
					"source" : [ "obj-48", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 1 ],
					"source" : [ "obj-48", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 1 ],
					"source" : [ "obj-48", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 1 ],
					"source" : [ "obj-48", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 1 ],
					"source" : [ "obj-48", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 1 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"order" : 1,
					"source" : [ "obj-49", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 1 ],
					"order" : 0,
					"source" : [ "obj-49", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-488", 0 ],
					"source" : [ "obj-491", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-489", 0 ],
					"source" : [ "obj-491", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-492", 0 ],
					"source" : [ "obj-495", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-493", 0 ],
					"source" : [ "obj-495", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-496", 0 ],
					"source" : [ "obj-499", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-497", 0 ],
					"source" : [ "obj-499", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-500", 0 ],
					"source" : [ "obj-503", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-501", 0 ],
					"source" : [ "obj-503", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-502", 0 ],
					"source" : [ "obj-504", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-503", 0 ],
					"source" : [ "obj-504", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-498", 0 ],
					"source" : [ "obj-505", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-499", 0 ],
					"source" : [ "obj-505", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 1 ],
					"source" : [ "obj-505", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-494", 0 ],
					"source" : [ "obj-506", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-495", 0 ],
					"source" : [ "obj-506", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"source" : [ "obj-506", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-490", 0 ],
					"source" : [ "obj-507", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-491", 0 ],
					"source" : [ "obj-507", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 1 ],
					"source" : [ "obj-507", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-504", 0 ],
					"source" : [ "obj-508", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-505", 0 ],
					"source" : [ "obj-508", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-506", 0 ],
					"source" : [ "obj-508", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-507", 0 ],
					"source" : [ "obj-508", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"order" : 1,
					"source" : [ "obj-53", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 0,
					"source" : [ "obj-53", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"order" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"order" : 1,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 1 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 1 ],
					"order" : 1,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 1 ],
					"source" : [ "obj-60", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 1 ],
					"source" : [ "obj-60", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 1 ],
					"source" : [ "obj-60", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 1 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 1 ],
					"source" : [ "obj-71", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 1 ],
					"source" : [ "obj-71", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 1 ],
					"source" : [ "obj-71", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-198", 1 ],
					"source" : [ "obj-71", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 1 ],
					"source" : [ "obj-71", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 1 ],
					"source" : [ "obj-71", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"order" : 0,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 1 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"order" : 1,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"order" : 0,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"order" : 1,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 1 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 3 ],
					"source" : [ "obj-99", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"source" : [ "", -1 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-73.2::obj-26" : [ "toggle[16]", "toggle[5]", 0 ],
			"obj-73.4::obj-214" : [ "button[41]", "button", 0 ],
			"obj-105.2::obj-32" : [ "toggle[39]", "toggle[1]", 0 ],
			"obj-105.3::obj-32" : [ "toggle[42]", "toggle[1]", 0 ],
			"obj-105.4::obj-32" : [ "toggle[51]", "toggle[1]", 0 ],
			"obj-221" : [ "vst~[21]", "vst~[1]", 0 ],
			"obj-73.3::obj-225" : [ "rslider[46]", "rslider", 0 ],
			"obj-105.1::obj-124" : [ "button[100]", "button[7]", 0 ],
			"obj-105.4::obj-202" : [ "multislider[71]", "multislider", 0 ],
			"obj-73.1::obj-195" : [ "button[30]", "button[7]", 0 ],
			"obj-73.2::obj-4" : [ "live.gain~[22]", "live.gain~", 0 ],
			"obj-73.4::obj-196" : [ "button[40]", "button", 0 ],
			"obj-105.2::obj-26" : [ "toggle[26]", "toggle[5]", 0 ],
			"obj-105.3::obj-201" : [ "rslider[59]", "rslider", 0 ],
			"obj-138" : [ "mc.live.gain~[4]", "mc.live.gain~[1]", 0 ],
			"obj-73.3::obj-17" : [ "slider[11]", "slider", 0 ],
			"obj-73.4::obj-81" : [ "multislider[9]", "multislider", 0 ],
			"obj-105.1::obj-30" : [ "button[94]", "button[2]", 0 ],
			"obj-105.2::obj-230" : [ "slider[16]", "slider[1]", 0 ],
			"obj-105.4::obj-230" : [ "slider[22]", "slider[1]", 0 ],
			"obj-73.2::obj-119" : [ "button[20]", "button", 0 ],
			"obj-73.4::obj-124" : [ "button[14]", "button[7]", 0 ],
			"obj-105.2::obj-23" : [ "vst~[7]", "vst~[35]", 0 ],
			"obj-105.3::obj-195" : [ "button[79]", "button[7]", 0 ],
			"obj-137" : [ "mc.live.gain~[3]", "mc.live.gain~[1]", 0 ],
			"obj-73.1::obj-34" : [ "button[6]", "button[1]", 0 ],
			"obj-73.3::obj-74" : [ "multislider[64]", "multislider", 0 ],
			"obj-105.1::obj-55" : [ "rslider[2]", "rslider", 0 ],
			"obj-105.2::obj-168" : [ "button[63]", "button[7]", 0 ],
			"obj-105.4::obj-17" : [ "slider[21]", "slider", 0 ],
			"obj-73.3::obj-201" : [ "rslider[47]", "rslider", 0 ],
			"obj-73.4::obj-219" : [ "button[36]", "button", 0 ],
			"obj-105.2::obj-175" : [ "multislider[68]", "multislider", 0 ],
			"obj-105.3::obj-4" : [ "live.gain~[5]", "live.gain~", 0 ],
			"obj-73.1::obj-293" : [ "toggle[7]", "toggle[5]", 0 ],
			"obj-73.2::obj-5" : [ "toggle[17]", "toggle[5]", 0 ],
			"obj-73.4::obj-293" : [ "toggle[22]", "toggle[5]", 0 ],
			"obj-105.1::obj-201" : [ "rslider[71]", "rslider", 0 ],
			"obj-105.3::obj-5" : [ "toggle[47]", "toggle[5]", 0 ],
			"obj-105.4::obj-30" : [ "button[88]", "button[2]", 0 ],
			"obj-80" : [ "vst~[2]", "vst~[2]", 0 ],
			"obj-73.1::obj-32" : [ "toggle[4]", "toggle[1]", 0 ],
			"obj-73.2::obj-30" : [ "button[59]", "button[2]", 0 ],
			"obj-73.3::obj-124" : [ "button[17]", "button[7]", 0 ],
			"obj-73.3::obj-44" : [ "slider[2]", "slider[1]", 0 ],
			"obj-73.4::obj-131" : [ "button[24]", "button", 0 ],
			"obj-105.2::obj-124" : [ "button[66]", "button[7]", 0 ],
			"obj-105.3::obj-219" : [ "button[55]", "button", 0 ],
			"obj-73.2::obj-124" : [ "button[21]", "button[7]", 0 ],
			"obj-73.4::obj-175" : [ "multislider[18]", "multislider", 0 ],
			"obj-105.1::obj-40" : [ "toggle[64]", "toggle[5]", 0 ],
			"obj-105.3::obj-223" : [ "multislider[14]", "multislider", 0 ],
			"obj-105.4::obj-34" : [ "button[83]", "button[1]", 0 ],
			"obj-73.2::obj-32" : [ "toggle[1]", "toggle[1]", 0 ],
			"obj-73.3::obj-55" : [ "rslider[30]", "rslider", 0 ],
			"obj-105.1::obj-291" : [ "toggle[62]", "toggle[5]", 0 ],
			"obj-105.2::obj-202" : [ "multislider[38]", "multislider", 0 ],
			"obj-105.4::obj-214" : [ "button[87]", "button", 0 ],
			"obj-73.2::obj-291" : [ "toggle[12]", "toggle[5]", 0 ],
			"obj-73.3::obj-48" : [ "toggle[9]", "toggle[5]", 0 ],
			"obj-73.4::obj-83" : [ "rslider[11]", "rslider", 0 ],
			"obj-105.1::obj-119" : [ "button[96]", "button", 0 ],
			"obj-105.3::obj-218" : [ "button[75]", "button[7]", 0 ],
			"obj-105.4::obj-219" : [ "button[85]", "button", 0 ],
			"obj-73.3::obj-81" : [ "multislider[53]", "multislider", 0 ],
			"obj-105.1::obj-227" : [ "rslider[74]", "rslider", 0 ],
			"obj-105.4::obj-44" : [ "slider[20]", "slider[1]", 0 ],
			"obj-73.2::obj-44" : [ "slider[6]", "slider[1]", 0 ],
			"obj-73.4::obj-225" : [ "rslider[43]", "rslider", 0 ],
			"obj-105.2::obj-219" : [ "button[70]", "button", 0 ],
			"obj-105.3::obj-3" : [ "vst~[13]", "vst~[35]", 0 ],
			"obj-73.3::obj-202" : [ "multislider[22]", "multislider", 0 ],
			"obj-105.1::obj-16" : [ "toggle[58]", "toggle[5]", 0 ],
			"obj-105.4::obj-169" : [ "button[86]", "button", 0 ],
			"obj-73.1::obj-214" : [ "button[32]", "button", 0 ],
			"obj-73.2::obj-225" : [ "rslider[50]", "rslider", 0 ],
			"obj-73.3::obj-34" : [ "button[3]", "button[1]", 0 ],
			"obj-73.4::obj-17" : [ "slider[12]", "slider", 0 ],
			"obj-105.2::obj-169" : [ "button[9]", "button", 0 ],
			"obj-105.2::obj-131" : [ "button[65]", "button", 0 ],
			"obj-105.3::obj-119" : [ "button[73]", "button", 0 ],
			"obj-60" : [ "vst~[22]", "vst~[16]", 0 ],
			"obj-27" : [ "mc.vst~[5]", "mc.vst~[5]", 0 ],
			"obj-87" : [ "mc.live.gain~[1]", "mc.live.gain~[1]", 0 ],
			"obj-73.3::obj-174" : [ "rslider[45]", "rslider", 0 ],
			"obj-105.1::obj-219" : [ "button[105]", "button", 0 ],
			"obj-105.2::obj-81" : [ "multislider[37]", "multislider", 0 ],
			"obj-105.4::obj-55" : [ "rslider[1]", "rslider", 0 ],
			"obj-73.1::obj-201" : [ "rslider[38]", "rslider", 0 ],
			"obj-73.2::obj-83" : [ "rslider[26]", "rslider", 0 ],
			"obj-73.4::obj-34" : [ "button[4]", "button[1]", 0 ],
			"obj-73.4::obj-202" : [ "multislider[20]", "multislider", 0 ],
			"obj-105.2::obj-48" : [ "toggle[38]", "toggle[5]", 0 ],
			"obj-105.3::obj-44" : [ "slider[19]", "slider[1]", 0 ],
			"obj-73.2::obj-74" : [ "multislider[16]", "multislider", 0 ],
			"obj-73.3::obj-32" : [ "toggle[21]", "toggle[1]", 0 ],
			"obj-105.1::obj-230" : [ "slider[25]", "slider[1]", 0 ],
			"obj-105.3::obj-16" : [ "toggle[46]", "toggle[5]", 0 ],
			"obj-105.4::obj-226" : [ "multislider[74]", "multislider", 0 ],
			"obj-73.1::obj-17" : [ "slider[10]", "slider", 0 ],
			"obj-73.3::obj-195" : [ "button[46]", "button[7]", 0 ],
			"obj-73.4::obj-30" : [ "button[60]", "button[2]", 0 ],
			"obj-105.2::obj-291" : [ "toggle[31]", "toggle[5]", 0 ],
			"obj-105.3::obj-214" : [ "button[74]", "button", 0 ],
			"obj-150" : [ "number[1]", "number[1]", 0 ],
			"obj-73.1::obj-169" : [ "button[28]", "button", 0 ],
			"obj-73.2::obj-16" : [ "toggle[2]", "toggle[5]", 0 ],
			"obj-73.2::obj-227" : [ "rslider[49]", "rslider", 0 ],
			"obj-73.4::obj-48" : [ "toggle[29]", "toggle[5]", 0 ],
			"obj-105.1::obj-81" : [ "multislider[79]", "multislider", 0 ],
			"obj-105.3::obj-34" : [ "button[81]", "button[1]", 0 ],
			"obj-105.4::obj-81" : [ "multislider[72]", "multislider", 0 ],
			"obj-25" : [ "mc.live.gain~", "mc.live.gain~", 0 ],
			"obj-73.1::obj-23" : [ "vst~[4]", "vst~[35]", 0 ],
			"obj-73.1::obj-16" : [ "toggle[15]", "toggle[5]", 0 ],
			"obj-73.1::obj-230" : [ "slider[5]", "slider[1]", 0 ],
			"obj-73.3::obj-5" : [ "toggle[10]", "toggle[5]", 0 ],
			"obj-105.1::obj-32" : [ "toggle[61]", "toggle[1]", 0 ],
			"obj-105.2::obj-17" : [ "slider[14]", "slider", 0 ],
			"obj-105.4::obj-5" : [ "toggle[32]", "toggle[5]", 0 ],
			"obj-73.2::obj-230" : [ "slider[8]", "slider[1]", 0 ],
			"obj-73.4::obj-5" : [ "toggle[36]", "toggle[5]", 0 ],
			"obj-105.1::obj-223" : [ "multislider[80]", "multislider", 0 ],
			"obj-105.3::obj-124" : [ "button[77]", "button[7]", 0 ],
			"obj-105.4::obj-225" : [ "rslider[65]", "rslider", 0 ],
			"obj-73.1::obj-5" : [ "toggle[14]", "toggle[5]", 0 ],
			"obj-73.2::obj-23" : [ "vst~[5]", "vst~[35]", 0 ],
			"obj-73.3::obj-175" : [ "multislider[36]", "multislider", 0 ],
			"obj-105.1::obj-225" : [ "rslider[72]", "rslider", 0 ],
			"obj-105.4::obj-174" : [ "rslider[66]", "rslider", 0 ],
			"obj-73.1::obj-48" : [ "toggle[34]", "toggle[5]", 0 ],
			"obj-73.2::obj-214" : [ "button[58]", "button", 0 ],
			"obj-73.4::obj-26" : [ "toggle[27]", "toggle[5]", 0 ],
			"obj-105.2::obj-201" : [ "rslider[57]", "rslider", 0 ],
			"obj-105.3::obj-175" : [ "multislider[15]", "multislider", 0 ],
			"obj-139" : [ "vst~[1]", "vst~[1]", 0 ],
			"obj-73.3::obj-131" : [ "button[25]", "button", 0 ],
			"obj-105.1::obj-83" : [ "rslider[73]", "rslider", 0 ],
			"obj-105.4::obj-227" : [ "rslider[67]", "rslider", 0 ],
			"obj-73.2::obj-196" : [ "button[54]", "button", 0 ],
			"obj-73.3::obj-30" : [ "button[7]", "button[2]", 0 ],
			"obj-73.4::obj-227" : [ "rslider[41]", "rslider", 0 ],
			"obj-105.2::obj-5" : [ "toggle[40]", "toggle[5]", 0 ],
			"obj-105.3::obj-227" : [ "rslider[62]", "rslider", 0 ],
			"obj-23" : [ "mc.vst~[2]", "mc.vst~[2]", 0 ],
			"obj-73.3::obj-16" : [ "toggle[19]", "toggle[5]", 0 ],
			"obj-105.1::obj-196" : [ "button[95]", "button", 0 ],
			"obj-105.2::obj-225" : [ "rslider[58]", "rslider", 0 ],
			"obj-105.4::obj-196" : [ "button[92]", "button", 0 ],
			"obj-73.1::obj-196" : [ "button[31]", "button", 0 ],
			"obj-73.1::obj-131" : [ "button[23]", "button", 0 ],
			"obj-73.1::obj-124" : [ "button[12]", "button[7]", 0 ],
			"obj-73.2::obj-201" : [ "rslider[52]", "rslider", 0 ],
			"obj-73.4::obj-55" : [ "rslider[10]", "rslider", 0 ],
			"obj-105.2::obj-34" : [ "button[61]", "button[1]", 0 ],
			"obj-105.3::obj-226" : [ "multislider[70]", "multislider", 0 ],
			"obj-73.1::obj-55" : [ "rslider[33]", "rslider", 0 ],
			"obj-73.3::obj-83" : [ "rslider[29]", "rslider", 0 ],
			"obj-105.1::obj-4" : [ "live.gain~[7]", "live.gain~", 0 ],
			"obj-105.2::obj-196" : [ "button[69]", "button", 0 ],
			"obj-105.4::obj-16" : [ "toggle[50]", "toggle[5]", 0 ],
			"obj-73.3::obj-227" : [ "rslider[48]", "rslider", 0 ],
			"obj-73.4::obj-44" : [ "slider[3]", "slider[1]", 0 ],
			"obj-105.2::obj-4" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-105.3::obj-48" : [ "toggle[43]", "toggle[5]", 0 ],
			"obj-17" : [ "mc.vst~[9]", "mc.vst~[9]", 0 ],
			"obj-73.1::obj-119" : [ "button", "button", 0 ],
			"obj-73.2::obj-195" : [ "button[57]", "button[7]", 0 ],
			"obj-73.4::obj-226" : [ "multislider[19]", "multislider", 0 ],
			"obj-105.1::obj-226" : [ "multislider[78]", "multislider", 0 ],
			"obj-105.3::obj-225" : [ "rslider[60]", "rslider", 0 ],
			"obj-105.4::obj-131" : [ "button[84]", "button", 0 ],
			"obj-73.1::obj-81" : [ "multislider[11]", "multislider", 0 ],
			"obj-73.2::obj-168" : [ "button[53]", "button[7]", 0 ],
			"obj-73.2::obj-55" : [ "rslider", "rslider", 0 ],
			"obj-73.3::obj-4" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-105.1::obj-293" : [ "toggle[59]", "toggle[5]", 0 ],
			"obj-105.2::obj-55" : [ "rslider[54]", "rslider", 0 ],
			"obj-105.3::obj-169" : [ "button[76]", "button", 0 ],
			"obj-73.1::obj-291" : [ "toggle[6]", "toggle[5]", 0 ],
			"obj-73.1::obj-3" : [ "vst~[11]", "vst~[35]", 0 ],
			"obj-73.2::obj-48" : [ "toggle[35]", "toggle[5]", 0 ],
			"obj-73.4::obj-223" : [ "multislider[8]", "multislider", 0 ],
			"obj-105.1::obj-17" : [ "slider[23]", "slider", 0 ],
			"obj-105.3::obj-40" : [ "toggle[49]", "toggle[5]", 0 ],
			"obj-105.4::obj-291" : [ "toggle[56]", "toggle[5]", 0 ],
			"obj-13" : [ "vst~", "vst~", 0 ],
			"obj-73.1::obj-74" : [ "multislider[63]", "multislider", 0 ],
			"obj-73.3::obj-219" : [ "button[47]", "button", 0 ],
			"obj-105.1::obj-3" : [ "vst~[17]", "vst~[35]", 0 ],
			"obj-105.2::obj-218" : [ "button[62]", "button[7]", 0 ],
			"obj-105.4::obj-26" : [ "toggle[52]", "toggle[5]", 0 ],
			"obj-73.1::obj-174" : [ "rslider[37]", "rslider", 0 ],
			"obj-73.2::obj-202" : [ "multislider[23]", "multislider", 0 ],
			"obj-73.4::obj-40" : [ "toggle[24]", "toggle[5]", 0 ],
			"obj-73.4::obj-74" : [ "multislider[10]", "multislider", 0 ],
			"obj-105.1::obj-34" : [ "button[98]", "button[1]", 0 ],
			"obj-105.3::obj-23" : [ "vst~[14]", "vst~[35]", 0 ],
			"obj-105.4::obj-175" : [ "multislider[75]", "multislider", 0 ],
			"obj-73.3::obj-293" : [ "toggle[11]", "toggle[5]", 0 ],
			"obj-105.1::obj-174" : [ "rslider[70]", "rslider", 0 ],
			"obj-105.3::obj-202" : [ "multislider[12]", "multislider", 0 ],
			"obj-105.4::obj-48" : [ "toggle[54]", "toggle[5]", 0 ],
			"obj-73.1::obj-175" : [ "multislider[35]", "multislider", 0 ],
			"obj-73.2::obj-40" : [ "toggle[8]", "toggle[5]", 0 ],
			"obj-73.2::obj-81" : [ "multislider[61]", "multislider", 0 ],
			"obj-73.4::obj-174" : [ "rslider[44]", "rslider", 0 ],
			"obj-105.2::obj-83" : [ "rslider[56]", "rslider", 0 ],
			"obj-105.3::obj-196" : [ "button[72]", "button", 0 ],
			"obj-71" : [ "vst~[19]", "vst~[19]", 0 ],
			"obj-73.3::obj-40" : [ "toggle[20]", "toggle[5]", 0 ],
			"obj-105.1::obj-195" : [ "button[103]", "button[7]", 0 ],
			"obj-105.4::obj-223" : [ "multislider[25]", "multislider", 0 ],
			"obj-73.1::obj-30" : [ "button[2]", "button[2]", 0 ],
			"obj-73.2::obj-174" : [ "rslider[51]", "rslider", 0 ],
			"obj-73.4::obj-16" : [ "toggle[28]", "toggle[5]", 0 ],
			"obj-105.2::obj-74" : [ "multislider[39]", "multislider", 0 ],
			"obj-105.3::obj-17" : [ "slider[17]", "slider", 0 ],
			"obj-105.4::obj-3" : [ "vst~[16]", "vst~[35]", 0 ],
			"obj-73.3::obj-230" : [ "slider[7]", "slider[1]", 0 ],
			"obj-105.1::obj-44" : [ "slider[24]", "slider[1]", 0 ],
			"obj-105.2::obj-227" : [ "rslider[53]", "rslider", 0 ],
			"obj-105.4::obj-40" : [ "toggle[53]", "toggle[5]", 0 ],
			"obj-73.1::obj-218" : [ "button[34]", "button[7]", 0 ],
			"obj-73.3::obj-196" : [ "button[43]", "button", 0 ],
			"obj-73.4::obj-168" : [ "button[37]", "button[7]", 0 ],
			"obj-105.2::obj-223" : [ "multislider[1]", "multislider", 0 ],
			"obj-105.3::obj-81" : [ "multislider[13]", "multislider", 0 ],
			"obj-73.1::obj-4" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-73.1::obj-168" : [ "button[27]", "button[7]", 0 ],
			"obj-73.1::obj-44" : [ "slider[4]", "slider[1]", 0 ],
			"obj-73.2::obj-3" : [ "vst~[8]", "vst~[35]", 0 ],
			"obj-73.3::obj-3" : [ "vst~[9]", "vst~[35]", 0 ],
			"obj-105.1::obj-168" : [ "button[99]", "button[7]", 0 ],
			"obj-105.3::obj-74" : [ "multislider[69]", "multislider", 0 ],
			"obj-105.4::obj-83" : [ "rslider[69]", "rslider", 0 ],
			"obj-73.1::obj-219" : [ "button[35]", "button", 0 ],
			"obj-73.2::obj-34" : [ "button[1]", "button[1]", 0 ],
			"obj-73.3::obj-226" : [ "multislider[21]", "multislider", 0 ],
			"obj-73.4::obj-169" : [ "button[29]", "button", 0 ],
			"obj-105.2::obj-214" : [ "button[67]", "button", 0 ],
			"obj-105.3::obj-30" : [ "button[71]", "button[2]", 0 ],
			"obj-73.1::obj-83" : [ "rslider[32]", "rslider", 0 ],
			"obj-73.2::obj-175" : [ "multislider[67]", "multislider", 0 ],
			"obj-73.4::obj-195" : [ "button[42]", "button[7]", 0 ],
			"obj-105.1::obj-131" : [ "button[97]", "button", 0 ],
			"obj-105.3::obj-293" : [ "toggle[44]", "toggle[5]", 0 ],
			"obj-105.4::obj-23" : [ "vst~[15]", "vst~[35]", 0 ],
			"obj-73.1::obj-223" : [ "multislider[65]", "multislider", 0 ],
			"obj-73.2::obj-17" : [ "slider[1]", "slider", 0 ],
			"obj-73.3::obj-214" : [ "button[48]", "button", 0 ],
			"obj-105.1::obj-5" : [ "toggle[60]", "toggle[5]", 0 ],
			"obj-105.2::obj-16" : [ "toggle[37]", "toggle[5]", 0 ],
			"obj-105.4::obj-4" : [ "live.gain~[6]", "live.gain~", 0 ],
			"obj-73.2::obj-226" : [ "multislider[24]", "multislider", 0 ],
			"obj-73.4::obj-201" : [ "rslider[42]", "rslider", 0 ],
			"obj-105.1::obj-175" : [ "multislider[81]", "multislider", 0 ],
			"obj-105.3::obj-26" : [ "toggle[48]", "toggle[5]", 0 ],
			"obj-105.4::obj-124" : [ "button[82]", "button[7]", 0 ],
			"obj-73.1::obj-225" : [ "rslider[39]", "rslider", 0 ],
			"obj-73.3::obj-119" : [ "button[18]", "button", 0 ],
			"obj-105.1::obj-74" : [ "multislider[77]", "multislider", 0 ],
			"obj-105.4::obj-119" : [ "button[91]", "button", 0 ],
			"obj-4" : [ "toggle", "toggle", 0 ],
			"obj-73.1::obj-26" : [ "toggle[5]", "toggle[5]", 0 ],
			"obj-73.1::obj-202" : [ "multislider[56]", "multislider", 0 ],
			"obj-73.2::obj-169" : [ "button[51]", "button", 0 ],
			"obj-73.4::obj-218" : [ "button[38]", "button[7]", 0 ],
			"obj-73.4::obj-23" : [ "vst~[6]", "vst~[35]", 0 ],
			"obj-105.2::obj-119" : [ "button[68]", "button", 0 ],
			"obj-105.2::obj-195" : [ "button[8]", "button[7]", 0 ],
			"obj-105.3::obj-55" : [ "rslider[64]", "rslider", 0 ],
			"obj-73.1::obj-226" : [ "multislider[17]", "multislider", 0 ],
			"obj-73.3::obj-291" : [ "toggle[18]", "toggle[5]", 0 ],
			"obj-105.1::obj-23" : [ "vst~[18]", "vst~[35]", 0 ],
			"obj-105.4::obj-218" : [ "button[90]", "button[7]", 0 ],
			"obj-73.1::obj-40" : [ "toggle[33]", "toggle[5]", 0 ],
			"obj-73.2::obj-219" : [ "button[56]", "button", 0 ],
			"obj-73.4::obj-291" : [ "toggle[23]", "toggle[5]", 0 ],
			"obj-105.2::obj-226" : [ "multislider[40]", "multislider", 0 ],
			"obj-105.3::obj-168" : [ "button[80]", "button[7]", 0 ],
			"obj-73.1::obj-227" : [ "rslider[40]", "rslider", 0 ],
			"obj-73.3::obj-223" : [ "multislider[66]", "multislider", 0 ],
			"obj-105.1::obj-202" : [ "multislider[76]", "multislider", 0 ],
			"obj-105.2::obj-44" : [ "slider[15]", "slider[1]", 0 ],
			"obj-105.4::obj-168" : [ "button[89]", "button[7]", 0 ],
			"obj-73.2::obj-223" : [ "multislider[43]", "multislider", 0 ],
			"obj-73.4::obj-230" : [ "slider[13]", "slider[1]", 0 ],
			"obj-105.2::obj-3" : [ "vst~[12]", "vst~[35]", 0 ],
			"obj-105.3::obj-131" : [ "button[78]", "button", 0 ],
			"obj-73.3::obj-218" : [ "button[45]", "button[7]", 0 ],
			"obj-73.4::obj-32" : [ "toggle[3]", "toggle[1]", 0 ],
			"obj-105.1::obj-26" : [ "toggle[57]", "toggle[5]", 0 ],
			"obj-105.2::obj-174" : [ "rslider[55]", "rslider", 0 ],
			"obj-105.4::obj-195" : [ "button[93]", "button[7]", 0 ],
			"obj-73.3::obj-168" : [ "button[49]", "button[7]", 0 ],
			"obj-73.4::obj-4" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-105.2::obj-293" : [ "toggle[41]", "toggle[5]", 0 ],
			"obj-105.3::obj-291" : [ "toggle[45]", "toggle[5]", 0 ],
			"obj-73.2::obj-218" : [ "button[52]", "button[7]", 0 ],
			"obj-73.4::obj-3" : [ "vst~[10]", "vst~[35]", 0 ],
			"obj-105.1::obj-169" : [ "button[101]", "button", 0 ],
			"obj-105.3::obj-83" : [ "rslider[63]", "rslider", 0 ],
			"obj-105.4::obj-74" : [ "multislider[73]", "multislider", 0 ],
			"obj-73.2::obj-131" : [ "button[26]", "button", 0 ],
			"obj-73.3::obj-26" : [ "toggle[30]", "toggle[5]", 0 ],
			"obj-105.1::obj-218" : [ "button[104]", "button[7]", 0 ],
			"obj-105.2::obj-40" : [ "toggle[25]", "toggle[5]", 0 ],
			"obj-105.3::obj-174" : [ "rslider[61]", "rslider", 0 ],
			"obj-73.2::obj-293" : [ "toggle[13]", "toggle[5]", 0 ],
			"obj-73.3::obj-23" : [ "vst~[3]", "vst~[35]", 0 ],
			"obj-73.4::obj-119" : [ "button[15]", "button", 0 ],
			"obj-105.1::obj-214" : [ "button[102]", "button", 0 ],
			"obj-105.3::obj-230" : [ "slider[18]", "slider[1]", 0 ],
			"obj-105.4::obj-201" : [ "rslider[68]", "rslider", 0 ],
			"obj-73.3::obj-169" : [ "button[50]", "button", 0 ],
			"obj-105.1::obj-48" : [ "toggle[63]", "toggle[5]", 0 ],
			"obj-105.2::obj-30" : [ "button[64]", "button[2]", 0 ],
			"obj-105.4::obj-293" : [ "toggle[55]", "toggle[5]", 0 ],
			"parameterbanks" : 			{

			}
,
			"parameter_overrides" : 			{
				"obj-73.2::obj-4" : 				{
					"parameter_longname" : "live.gain~[22]"
				}
,
				"obj-105.3::obj-4" : 				{
					"parameter_longname" : "live.gain~[5]"
				}
,
				"obj-73.1::obj-55" : 				{
					"parameter_initial" : [ 0, 128 ]
				}
,
				"obj-105.1::obj-4" : 				{
					"parameter_longname" : "live.gain~[7]"
				}
,
				"obj-105.2::obj-4" : 				{
					"parameter_longname" : "live.gain~[4]"
				}
,
				"obj-73.1::obj-81" : 				{
					"parameter_initial" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127 ],
					"parameter_initial_enable" : 1
				}
,
				"obj-73.3::obj-4" : 				{
					"parameter_longname" : "live.gain~[1]"
				}
,
				"obj-73.1::obj-74" : 				{
					"parameter_initial" : [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127 ]
				}
,
				"obj-73.1::obj-4" : 				{
					"parameter_longname" : "live.gain~[3]"
				}
,
				"obj-73.1::obj-83" : 				{
					"parameter_initial" : [ 0, 128 ]
				}
,
				"obj-105.4::obj-4" : 				{
					"parameter_longname" : "live.gain~[6]"
				}
,
				"obj-73.4::obj-4" : 				{
					"parameter_longname" : "live.gain~[2]"
				}

			}

		}
,
		"parameter_map" : 		{
			"key" : 			{
				"toggle" : 				{
					"srcname" : "0.modifiers.-2.code.key",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 1
				}

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "demompe.maxpat",
				"bootpath" : "C74:/help/msp",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "interfacecolor.js",
				"bootpath" : "C74:/interfaces",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "Serum_20190510.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Serum_20191229_2.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Synth.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Serum_20200106.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "LABS.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Serum_20200102.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Serum_20200103.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Serum.maxsnap",
				"bootpath" : "~/Documents/Max 8/Snapshots",
				"patcherrelativepath" : "../../../Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "Serum[1].maxsnap",
				"bootpath" : "~/Documents/Max 8/Snapshots",
				"patcherrelativepath" : "../../../Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "AUFilter.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "mperoute.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "risekeys.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "Dark Mode",
				"default" : 				{
					"editing_bgcolor" : [ 0.050980392156863, 0.050980392156863, 0.050980392156863, 1.0 ],
					"textcolor_inverse" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ],
					"color" : [ 0.988235294117647, 0.886274509803922, 0.0, 1.0 ],
					"patchlinecolor" : [ 0.423529411764706, 0.423529411764706, 0.423529411764706, 1.0 ],
					"fontface" : [ 0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.235294117647059, 0.235294117647059, 0.235294117647059, 1.0 ],
						"color2" : [ 0.235294117647059, 0.235294117647059, 0.235294117647059, 1.0 ],
						"angle" : 269.577713564497685,
						"proportion" : 0.39,
						"autogradient" : 0.0,
						"pt1" : [ 0.504951, 0.278261 ],
						"pt2" : [ 0.5, 0.95 ]
					}
,
					"stripecolor" : [ 0.090196078431373, 0.090196078431373, 0.090196078431373, 1.0 ],
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ],
					"locked_bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"accentcolor" : [ 0.552941176470588, 0.552941176470588, 0.552941176470588, 1.0 ],
					"textjustification" : [ 1 ],
					"fontsize" : [ 13.0 ],
					"elementcolor" : [ 0.364705882352941, 0.364705882352941, 0.364705882352941, 1.0 ]
				}
,
				"parentstyle" : "velvet",
				"multi" : 0
			}
, 			{
				"name" : "Jamoma_highlighted_orange",
				"default" : 				{
					"accentcolor" : [ 1.0, 0.5, 0.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "ezdac~001",
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "ksliderWhite",
				"default" : 				{
					"color" : [ 1, 1, 1, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "tap",
				"default" : 				{
					"fontname" : [ "Lato Light" ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"toolbaradditions" : [ "audiomute" ],
		"color" : [ 0.996078431372549, 0.705882352941177, 0.0, 1.0 ]
	}

}
