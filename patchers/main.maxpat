{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 1,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 191.0, 79.0, 972.0, 787.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-286",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1102.0, 650.0, 20.0, 22.0 ],
					"saved_object_attributes" : 					{
						"filename" : "",
						"parameter_enable" : 0
					}
,
					"text" : "js"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-285",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1113.0, 512.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-173",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "demompe.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1192.419338019149563, 196.0, 207.0, 42.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1244.419338019149563, 312.0, 77.0, 22.0 ],
					"text" : "set 61 62 63"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-177",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1326.530422019149682, 297.5, 200.0, 51.0 ],
					"text" : "change routing from 48 50 52 to 61 62 63 - it's not possible to add outlets with the set message"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-182",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1284.419338019149563, 251.5, 259.5, 37.0 ],
					"text" : "Use the set message to change the current routing i.e. set 60 will change note 48 to 60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1237.808254019149445, 259.0, 43.0, 22.0 ],
					"text" : "set 60"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-195",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1705.419338019149563, 541.0, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-197",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1645.419338019149563, 541.0, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-199",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1554.419338019149563, 541.0, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-201",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1494.419338019149563, 541.0, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-203",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1403.419338019149563, 541.0, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-215",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1343.419338019149563, 541.0, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-216",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1252.419338019149563, 541.0, 54.0, 21.0 ],
					"text" : "velocity",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-218",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1192.419338019149563, 541.0, 36.0, 21.0 ],
					"text" : "note",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-220",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1672.419338019149563, 352.5, 133.0, 37.0 ],
					"text" : "Route all MPE data for three notes..."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-221",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1505.919338019149563, 420.0, 56.0, 21.0 ],
					"text" : "note: E",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-222",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1660.419338019149563, 402.0, 136.0, 36.0 ],
					"text" : "Unrouted data comes out rightmost outlet",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-224",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1352.919338019149563, 420.0, 52.0, 21.0 ],
					"text" : "note: D",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-234",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1202.919338019149563, 420.0, 50.0, 21.0 ],
					"text" : "note: C",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1252.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-241",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1192.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-243",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1284.419338019149563, 478.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-244",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1192.419338019149563, 478.0, 79.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-245",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1403.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-246",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1343.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1435.419338019149563, 478.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1343.419338019149563, 478.0, 79.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-271",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1554.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-272",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1494.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-273",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1586.419338019149563, 475.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1494.419338019149563, 475.0, 79.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1705.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1645.419338019149563, 515.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-277",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1733.419338019149563, 475.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-278",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1645.419338019149563, 475.0, 79.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-279",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 1645.419338019149563, 443.0, 113.5, 22.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-280",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 1494.419338019149563, 443.0, 113.5, 22.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-281",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 1343.419338019149563, 443.0, 113.5, 22.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 10,
					"outlettype" : [ "", "", "", "", "int", "", "int", "int", "int", "" ],
					"patching_rect" : [ 1192.419338019149563, 443.0, 113.5, 22.0 ],
					"text" : "mpeparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-283",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "int", "int", "int", "int", "int", "int", "int", "int", "int" ],
					"patching_rect" : [ 1192.419338019149563, 360.0, 193.0, 22.0 ],
					"text" : "mperoute 48 49 50 51 52 53 54 55"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1003.0, 1075.0, 62.0, 22.0 ],
					"text" : "polymidiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-232",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 85.0, 646.411778926849365, 62.0, 22.0 ],
					"text" : "polymidiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 93.0, 262.0, 40.0, 22.0 ],
					"text" : "midiin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 85.0, 489.0, 50.0, 22.0 ],
					"text" : "60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 85.0, 389.411778926849365, 50.0, 22.0 ],
					"text" : "80"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-228",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 33.0, 389.411778926849365, 50.0, 22.0 ],
					"text" : "60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 117.0, 345.911778926849365, 51.0, 22.0 ],
					"text" : "notein 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 64.0, 345.911778926849365, 51.0, 22.0 ],
					"text" : "notein 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 117.0, 427.911778926849365, 51.0, 22.0 ],
					"text" : "notein 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 33.0, 489.0, 50.0, 22.0 ],
					"text" : "45"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-217",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 64.0, 427.911778926849365, 51.0, 22.0 ],
					"text" : "notein 3"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-214",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 476.892168045043945, 919.0, 100.0, 23.0 ],
					"text" : "print @popup 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1768.491821836211784, 1183.0, 66.0, 22.0 ],
					"text" : "route 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1754.618831681396159, 1127.687402963638306, 135.0, 22.0 ],
					"text" : "receive automationData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1417.530422019149682, 1205.0, 76.0, 22.0 ],
					"text" : "route 1 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-207",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1403.657431864334058, 1149.687402963638306, 135.0, 22.0 ],
					"text" : "receive automationData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 394.0, 907.0, 52.0, 35.0 ],
					"text" : "13 0 25 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1398.0, 1276.0, 50.0, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1792.0, 1325.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-196",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1732.0, 1275.0, 50.0, 22.0 ],
					"text" : "2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 2397.392928076599219, 1221.996621370315552, 77.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-194",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2401.0, 1189.737305641174316, 29.0, 22.0 ],
					"text" : "r d4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 2063.392928076599219, 1221.996621370315552, 77.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-192",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2067.0, 1189.737305641174316, 29.0, 22.0 ],
					"text" : "r d3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-189",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1712.148986769531348, 1221.996621370315552, 77.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1715.756058692932129, 1189.737305641174316, 29.0, 22.0 ],
					"text" : "r d2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1493.0, 1205.0, 50.0, 22.0 ],
					"text" : "33"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-186",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 1377.148986769531348, 1231.259315729141235, 77.0, 22.0 ],
					"text" : "unpack 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1625.722295999526978, 1056.987969040870667, 31.0, 22.0 ],
					"text" : "s d4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-180",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1589.722295999526978, 1056.987969040870667, 31.0, 22.0 ],
					"text" : "s d3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1551.722295999526978, 1056.987969040870667, 31.0, 22.0 ],
					"text" : "s d2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1516.722295999526978, 1056.987969040870667, 31.0, 22.0 ],
					"text" : "s d1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1475.407479286193848, 976.299249768257141, 83.0, 22.0 ],
					"text" : ". route 1 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-174",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1751.0, 1585.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2067.0, 1100.0, 100.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-170",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2192.618831681396387, 1509.131234765052795, 75.0, 22.0 ],
					"text" : "r gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1821.618831681396387, 1509.131234765052795, 75.0, 22.0 ],
					"text" : "r gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2295.215605305817007, 1254.075752198696136, 37.0, 22.0 ],
					"text" : "join 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2293.715605305817007, 1189.737305641174316, 29.0, 22.0 ],
					"text" : "r v4"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 2295.215605305817007, 1298.390568912029266, 108.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "int", "int", "int", "int", "int" ],
					"patching_rect" : [ 2293.715605305817007, 1220.555611848831177, 97.0, 22.0 ],
					"text" : "unpack 0 0 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1957.537112712860107, 1254.075752198696136, 37.0, 22.0 ],
					"text" : "join 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1956.037112712860107, 1189.737305641174316, 29.0, 22.0 ],
					"text" : "r v3"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1957.537112712860107, 1298.390568912029266, 108.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "int", "int", "int", "int", "int" ],
					"patching_rect" : [ 1956.037112712860107, 1220.555611848831177, 97.0, 22.0 ],
					"text" : "unpack 0 0 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1553.618831681396387, 1509.131234765052795, 75.0, 22.0 ],
					"text" : "r gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 2164.0, 724.0, 92.5, 22.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[28]",
							"parameter_shortname" : "vst~[28]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"text" : "vst~",
					"varname" : "vst~[3]",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2472.174418496276758, 1269.629689455032349, 73.0, 22.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2154.581810759689233, 1265.92598557472229, 73.0, 22.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1816.618831681396387, 1262.222281694412231, 73.0, 22.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1485.118831681396387, 1270.555615425109863, 73.0, 22.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1048.333382606506348, 1287.222282886505127, 73.0, 22.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1528.530422019149682, 642.592623233795166, 73.0, 22.0 ],
					"text" : "receive plug"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-264",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1936.11120343208313, 650.925956964492798, 61.0, 22.0 ],
					"text" : "send plug"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"bubblepoint" : 1.0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-249",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1930.444531202316284, 549.388901472091675, 115.0, 25.0 ],
					"text" : "all usable plugs"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"bubblepoint" : 1.0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-250",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2193.944531202316284, 537.388901472091675, 130.0, 40.0 ],
					"text" : "all unusable \"blacklisted\" plugs"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-251",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2144.444531202316284, 533.388901472091675, 37.0, 23.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-252",
					"items" : [ "LABS.vst3", ",", "Reaktor 6", ",", "Kontakt", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6 MFX", ",", "LABS", ",", "Massive", ",", "Guitar Rig 5 MFX", ",", "Guitar Rig 5 FX", ",", "Reaktor 6", ",", "Kontakt", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6 MFX", ",", "LABS", ",", "Massive", ",", "Guitar Rig 5 MFX", ",", "Guitar Rig 5 FX", ",", "Reaktor 6", ",", "Kontakt", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6 MFX", ",", "LABS", ",", "Massive", ",", "Guitar Rig 5 MFX", ",", "Guitar Rig 5 FX" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2121.944531202316284, 579.388901472091675, 192.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-253",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2121.944531202316284, 495.388901472091675, 112.0, 23.0 ],
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-254",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1789.185267925262451, 583.203714609146118, 37.0, 23.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-255",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1935.944531202316284, 618.388901472091675, 84.0, 23.0 ],
					"text" : "prepend plug"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-256",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1873.444531202316284, 533.388901472091675, 37.0, 23.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-257",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1865.444531202316284, 326.888901472091675, 53.0, 23.0 ],
					"text" : "listvst3"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-258",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1884.444531202316284, 357.888901472091675, 42.0, 23.0 ],
					"text" : "listau"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-259",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1849.444531202316284, 293.888901472091675, 46.0, 23.0 ],
					"text" : "listvst"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-260",
					"items" : [ "3-Band EQ", ",", "Arcade", ",", "Atlas", ",", "AUAudioFilePlayer", ",", "AUBandpass", ",", "AUDelay", ",", "AUDistortion", ",", "AUDynamicsProcessor", ",", "AUFilter", ",", "AUGraphicEQ", ",", "AUHighShelfFilter", ",", "AUHipass", ",", "AULowpass", ",", "AULowShelfFilter", ",", "AUMatrixMixer", ",", "AUMatrixReverb", ",", "AUMIDISynth", ",", "AUMixer", ",", "AUMixer3D", ",", "AUMultibandCompressor", ",", "AUMultiChannelMixer", ",", "AUMultiSplitter", ",", "AUNBandEQ", ",", "AUNetReceive", ",", "AUNetSend", ",", "AUNewPitch", ",", "AUParametricEQ", ",", "AUPeakLimiter", ",", "AUPitch", ",", "AUReverb2", ",", "AURogerBeep", ",", "AURoundTripAAC", ",", "AUSampleDelay", ",", "AUSampler", ",", "AUScheduledSoundPlayer", ",", "AUSoundFieldPanner", ",", "AUSpatialMixer", ",", "AUSpeechSynthesis", ",", "AUSphericalHeadPanner", ",", "AUVectorPanner", ",", "Battery 4", ",", "Bitcrush", ",", "Carve EQ", ",", "Chorus", ",", "Comb Filter", ",", "Compressor", ",", "Crave EQ", ",", "Cypher2", ",", "Delay", ",", "Dimension Expander", ",", "Disperser", ",", "Distortion", ",", "DLSMusicDevice", ",", "Ensemble", ",", "Equator", ",", "Faturator", ",", "FF Pro-C 2", ",", "FF Pro-L 2", ",", "FF Pro-MB", ",", "FF Pro-Q 3", ",", "FF Pro-R", ",", "Filter", ",", "Flanger", ",", "FM8 MFX", ",", "FM8", ",", "Formant Filter", ",", "Frequency Shifter", ",", "Gain", ",", "Gate", ",", "Haas", ",", "HRTFPanner", ",", "kHs ONE", ",", "Kontakt", ",", "Ladder Filter", ",", "Limiter", ",", "Massive X", ",", "MSoundFactory", ",", "MSoundFactory6out", ",", "Multipass", ",", "Ozone Imager", ",", "Phase Distortion", ",", "Phase Plant", ",", "Phaser", ",", "Pitch Shifter", ",", "Reaktor 6 MFX", ",", "Reaktor 6 MIDIFX", ",", "Reaktor 6", ",", "Resonator", ",", "Reverb", ",", "Reverser", ",", "Ring Mod", ",", "ROLI Studio Drums", ",", "ROLI Studio Player", ",", "Serum", ",", "SerumFX", ",", "Slice EQ", ",", "Snap Heap", ",", "Stereo", ",", "Strobe2", ",", "SubLab", ",", "Supercharger", ",", "Tape Stop", ",", "TDR Nova", ",", "Trance Gate", ",", "Transient Shaper", ",", "VocalSynth 2", ",", "XO" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1849.444531202316284, 579.388901472091675, 192.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1849.444531202316284, 495.388901472091675, 112.0, 23.0 ],
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-262",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 7,
					"outlettype" : [ "", "", "", "", "", "", "" ],
					"patching_rect" : [ 1849.444531202316284, 440.388901472091675, 564.0, 23.0 ],
					"text" : "route plug_vst plug_au plug_vst3 plug_vst_blacklisted plug_vst3_blacklisted plug_au_blacklisted"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 13.0,
					"id" : "obj-263",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1849.444531202316284, 406.388901472091675, 58.0, 23.0 ],
					"text" : "vstscan"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1451.851921081542969, 1168.518574237823486, 50.0, 22.0 ],
					"text" : "57 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1359.756058692932129, 1162.37042498588562, 50.0, 22.0 ],
					"text" : "62 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1603.537112712860107, 1254.075752198696136, 37.0, 22.0 ],
					"text" : "join 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1602.037112712860107, 1189.737305641174316, 29.0, 22.0 ],
					"text" : "r v2"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-237",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1603.537112712860107, 1298.390568912029266, 108.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-238",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "int", "int", "int", "int", "int" ],
					"patching_rect" : [ 1602.037112712860107, 1220.555611848831177, 97.0, 22.0 ],
					"text" : "unpack 0 0 0 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1113.888942003250122, 1037.037086486816406, 55.0, 22.0 ],
					"text" : "stripnote"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1320.370433330535889, 950.925971269607544, 50.0, 22.0 ],
					"text" : "2 57 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1391.530422019149682, 868.106884121894836, 135.0, 22.0 ],
					"text" : "receive automationData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 270.808794045043953, 985.434713125228882, 123.0, 22.0 ],
					"text" : "send automationData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 345.058794045043896, 850.514290690422058, 103.0, 22.0 ],
					"text" : "join 4 @triggers 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 206.892168045043945, 850.514290690422058, 103.0, 22.0 ],
					"text" : "join 3 @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 824.074113368988037, 1042.592642307281494, 42.0, 22.0 ],
					"text" : "thresh"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-185",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1254.277837514877319, 1242.779456079006195, 37.0, 22.0 ],
					"text" : "join 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1252.777837514877319, 1178.441009521484375, 29.0, 22.0 ],
					"text" : "r v1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-183",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1254.277837514877319, 1287.094272792339325, 108.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1252.777837514877319, 1209.259315729141235, 67.0, 22.0 ],
					"text" : "unpack 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1283.333394527435303, 868.106884121894836, 107.0, 22.0 ],
					"text" : ".receive voiceData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1437.833357834411572, 1081.987969040870667, 31.0, 22.0 ],
					"text" : "s v4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1403.657431864334058, 1081.987969040870667, 31.0, 22.0 ],
					"text" : "s v3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1369.256058692932129, 1081.987969040870667, 31.0, 22.0 ],
					"text" : "s v2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1334.722237848831128, 1081.987969040870667, 31.0, 22.0 ],
					"text" : "s v1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1283.333394527435303, 1001.299249768257141, 155.62963342666626, 22.0 ],
					"text" : "route 1 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 171.392168045043945, 985.434713125228882, 91.0, 22.0 ],
					"text" : "send voiceData"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 12,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 942.651457548141479, 594.300657749176025, 750.5, 22.0 ],
					"text" : "join 12 @triggers -1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1194.444501399993896, 879.606884121894836, 50.0, 49.0 ],
					"text" : "midievent 144 57 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 282.407420873641968, 1240.740799903869629, 150.0, 34.0 ],
					"text" : "midi data: note on off\n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 351.577308678222664, 959.21667377867891, 182.5, 22.0 ],
					"text" : "0 62 0 0 42 63"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 832.392168045043945, 913.411778926849365, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 755.28536593914032, 463.922529148246781, 63.0, 22.0 ],
					"text" : "risedevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 801.998847246170044, 1252.094272792339325, 150.0, 33.0 ],
					"text" : "join data together, then unpack"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 881.728591442108154, 1287.094272792339325, 66.0, 22.0 ],
					"text" : "route 1 2 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 612.947141223741482, 1143.187402963638306, 50.0, 35.0 ],
					"text" : "61 0 0 25 64"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1174.530422019149682, 835.911778973724381, 108.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1174.530422019149682, 805.911778973724381, 32.5, 23.0 ],
					"text" : "join"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "float", "float" ],
					"patching_rect" : [ 1174.530422019149682, 775.911778973724381, 107.0, 23.0 ],
					"text" : "makenote 60 4n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "kslider",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1174.530422019149682, 686.411778926849365, 336.0, 53.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1134.715605305816553, 1287.094272792339325, 106.0, 22.0 ],
					"text" : "r patchparameters"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1488.715605305816553, 1300.734497904777527, 106.0, 22.0 ],
					"text" : "r patchparameters"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2514.124208974029443, 1300.734497904777527, 106.0, 22.0 ],
					"text" : "r patchparameters"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2182.124208974029443, 1300.734497904777527, 106.0, 22.0 ],
					"text" : "r patchparameters"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1843.124208974029443, 1300.734497904777527, 106.0, 22.0 ],
					"text" : "r patchparameters"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1711.766878515524922, 1008.401028752326965, 108.0, 22.0 ],
					"text" : "s patchparameters"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1528.530422019149682, 686.411778926849365, 446.551731824875333, 241.05854594707489 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[27]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5258.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................PgS3EP6b0.aUTDD9ZA5eH+zhTZKB8QM7WkerzJDPd6zVhPKxOBFQ.UzBQfhRwVjBTCMG1nTE+ArQkehJjFUrUQIMJXK8OaLfRQfPLTkfTRigeJM.gPeHD8bm8t88t20Wau.IsZ2cSl2r6Ne6ryNybSu28tTEEEPg0TM35iJPIT+vdppJZ5yzd37yl372l35lMw0cahqG1DW.1DWf1DWP1DWv1DWH1DWOsIt6wl35kMw0aahqObb0qocVfliI4R+fLOPdcfrNfrNfrNfrN.uNfwsehr5w7BZ6pRtP4GXGVSeHyCzcFh10AlRAXck4Ax7.zCHyCD67.dcPcuf79C39CQgaMtacrn3GD8yo03t0wht+QTN+Vi6VGKJ9AQ+bpn3OsQi93yKvM4GswyH7A2MNbMZVZIQmzL4i02dSwetFRt74bh4Zx7.YdfLOPdcfrNfrN.VGvTSzu+MQ87aJEf0UT8Ch94VlGn6Aj4AdmIH59CQ87yyB3meqi4yK4csqaXMtacrL920N9yiuVi6VGywI4csyGrF2sNVF+6ZG+4wWEk1421fmXzgwybCKh9LMRDetFVIzF3xL5iLUCb2IqQknqS26EpPZSWW6cZmyo9XTtJz7HdZJdUb+b2durG0joCfiVasbrzgLLf002PNuJw5b55D0uWDpe26Sc6TkfzZqPibqbiEPp05iq6n0NCR7wWBiPEQsGVeb9akaQLcrisGIfDUHmXH0sclewXrY4pF1DJm02XslmWGOcu350zZXxP6g6un1v1Yw.c7b83ftFjTTn9UKwnDYqwXcF1HecJF6kwzLeAtdUxPSMLtLN2Mn1tCpaUn3hBgRWBsEyMlLu8ELav7dXfAmG0UGMmsmls4VquYaliwWywkcGws7yiJLCoNKi3uWWa41GVQyMooigMka+9AoyevlUnDWtJ3oOpSdSEJ8FLLFS3YepFWKUGkaRGXeSWixUBpOy5zc9dYntughlwdvvoqCVcOSqSEpReeZgcv1+lUTpzk9YAuF9fz9303kxViCpdPhYCXcLrO13bkxbcYM5OFuV4tZTSedUC7IBk65hZnN40Fpz0kLvnqOllneTFc9C3RQa+T7nNn6iQsRUhm9nc1nVE55fuTErtJ9x.reprxo5.wf5.IN1x8dMLY75s75unBKkhqTpNPttNXmETjhgtv05ql64qf5OpfpCjSAhyCUq6eUJ2vOqq.UhgemMzntpR2oiphdFph5S+AJGOa3K.Q2nDJqGTJ.JEHkBhRASIYS38.XtlrItd.sYm0xRKizWxpGaRSwwxWyJiaLqMsrEW2gvcxceea+i6d5cXdB+8axJ9uwIqjW0TteN2RxCwYeyKImaolzbFj+azYJwrUm4m7tcd3EuOmAkW0NSovS3L+ZNmyi2v0nx8iLhvBljRLgRV93hjjexCgrm4NRxgWbbjKj0DIAkWRjQUPpjYT3bHuXIKf7V0jF4KOY5jZaHSxku95IA3+FIgGxqQFVXuAY7Q81joFyVIyO12mpqsSV2j9Hp91MYaS+So57KHGXgeEUu6iT2J9Vpt+dxMyobp9qlD0l+QRrE7SjIsyiRldgmfrvh+UxJJ42H4T1YHuYMmirii7mjhN4EIkd5lH+bCWi76MdCxkt9eQt0s+a596Gz6.5FDdH8.FbuCDFVXACiI7dBiOpdAjA2GXpwDJLqg0OXdw1eXQiY.vRGWjPFiefP1SZPPtjngWO4g.uyTue3Cl9PgOdVCG9r4NRXuy6AfuagiFpbQiENzhiCN1RiGN0JdH3rYLA37YMQ3JY+vfqbbBZ4BPf4kDzmMMEX.a9Qfne2oACufTgw9gOJLgcNSHwOY1PJENG3w97GGlewOA7be8SBKujE.uz9eJXck8LfZkOKjeMoAa4PKA11QddXWGaYvdNY5v2bpW.NvoWIT0YWEb3FxDN94WMTWiqAp+JqEtv0WObUWuBbyauAFIi+Be7W9dI486kjo+3.+4tUuw8u2QwMYBrtc11Ae+41UGkefuO78sy2N77lkqRyIPxn4fxQRwwncjZFo+xTFNxWs8VWzt+d5N7z7EzVLmiVLSmyDtZxkVSzub+LSaIYsJGw0pFgt7w0NxiucjmPqJmJfGG5D4nM3lrZqlrqVfAWWa0bK+Wl3xvmKJabwEEqSjZqEhX89Yb21nkRkd.oGP5A9enGvnFodsQt864+ug8UyXt1AWn1DWX1DW+rIt60l35uMwEtMwM.ahKBahKRahKJahaf1D28YSbCxl3FrMwEMCmkGmhuGlPBZIbWRQDQDYxysu649+f285nqoF7UbBOogk2t.TVp0UaNmY0+APmR98l896MK8GR+g75BYcAYc.Yc.Yc.ut8n5Q+Aswe9kRtX3OzOkd9TlGHFwcqWe6ICPumLOPlGfd.YdfXmGvqSn6Ej2e.2eHJbqwcqiEE+fneNsF2sNVz8Ohx42Zb25XQwOH5my+68+0x+kaxfwR3EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+gyA..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5258.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................PgS3EP6b0.aUTDD9ZA5eH+zhTZKB8QM7WkerzJDPd6zVhPKxOBFQ.UzBQfhRwVjBTCMG1nTE+ArQkehJjFUrUQIMJXK8OaLfRQfPLTkfTRigeJM.gPeHD8bm8t88t20Wau.IsZ2cSl2r6Ne6ryNybSu28tTEEEPg0TM35iJPIT+vdppJZ5yzd37yl372l35lMw0cahqG1DW.1DWf1DWP1DWv1DWH1DWOsIt6wl35kMw0aahqObb0qocVfliI4R+fLOPdcfrNfrNfrNfrN.uNfwsehr5w7BZ6pRtP4GXGVSeHyCzcFh10AlRAXck4Ax7.zCHyCD67.dcPcuf79C39CQgaMtacrn3GD8yo03t0wht+QTN+Vi6VGKJ9AQ+bpn3OsQi93yKvM4GswyH7A2MNbMZVZIQmzL4i02dSwetFRt74bh4Zx7.YdfLOPdcfrNfrN.VGvTSzu+MQ87aJEf0UT8Ch94VlGn6Aj4AdmIH59CQ87yyB3meqi4yK4csqaXMtacrL920N9yiuVi6VGywI4csyGrF2sNVF+6ZG+4wWEk1421fmXzgwybCKh9LMRDetFVIzF3xL5iLUCb2IqQknqS26EpPZSWW6cZmyo9XTtJz7HdZJdUb+b2durG0joCfiVasbrzgLLf002PNuJw5b55D0uWDpe26Sc6TkfzZqPibqbiEPp05iq6n0NCR7wWBiPEQsGVeb9akaQLcrisGIfDUHmXH0sclewXrY4pF1DJm02XslmWGOcu350zZXxP6g6un1v1Yw.c7b83ftFjTTn9UKwnDYqwXcF1HecJF6kwzLeAtdUxPSMLtLN2Mn1tCpaUn3hBgRWBsEyMlLu8ELav7dXfAmG0UGMmsmls4VquYaliwWywkcGws7yiJLCoNKi3uWWa41GVQyMooigMka+9AoyevlUnDWtJ3oOpSdSEJ8FLLFS3YepFWKUGkaRGXeSWixUBpOy5zc9dYntughlwdvvoqCVcOSqSEpReeZgcv1+lUTpzk9YAuF9fz9303kxViCpdPhYCXcLrO13bkxbcYM5OFuV4tZTSedUC7IBk65hZnN40Fpz0kLvnqOllneTFc9C3RQa+T7nNn6iQsRUhm9nc1nVE55fuTErtJ9x.reprxo5.wf5.IN1x8dMLY75s75unBKkhqTpNPttNXmETjhgtv05ql64qf5OpfpCjSAhyCUq6eUJ2vOqq.UhgemMzntpR2oiphdFph5S+AJGOa3K.Q2nDJqGTJ.JEHkBhRASIYS38.XtlrItd.sYm0xRKizWxpGaRSwwxWyJiaLqMsrEW2gvcxceea+i6d5cXdB+8axJ9uwIqjW0TteN2RxCwYeyKImaolzbFj+azYJwrUm4m7tcd3EuOmAkW0NSovS3L+ZNmyi2v0nx8iLhvBljRLgRV93hjjexCgrm4NRxgWbbjKj0DIAkWRjQUPpjYT3bHuXIKf7V0jF4KOY5jZaHSxku95IA3+FIgGxqQFVXuAY7Q81joFyVIyO12mpqsSV2j9Hp91MYaS+So57KHGXgeEUu6iT2J9Vpt+dxMyobp9qlD0l+QRrE7SjIsyiRldgmfrvh+UxJJ42H4T1YHuYMmirii7mjhN4EIkd5lH+bCWi76MdCxkt9eQt0s+a596Gz6.5FDdH8.FbuCDFVXACiI7dBiOpdAjA2GXpwDJLqg0OXdw1eXQiY.vRGWjPFiefP1SZPPtjngWO4g.uyTue3Cl9PgOdVCG9r4NRXuy6AfuagiFpbQiENzhiCN1RiGN0JdH3rYLA37YMQ3JY+vfqbbBZ4BPf4kDzmMMEX.a9Qfne2oACufTgw9gOJLgcNSHwOY1PJENG3w97GGlewOA7be8SBKujE.uz9eJXck8LfZkOKjeMoAa4PKA11QddXWGaYvdNY5v2bpW.NvoWIT0YWEb3FxDN94WMTWiqAp+JqEtv0WObUWuBbyauAFIi+Be7W9dI486kjo+3.+4tUuw8u2QwMYBrtc11Ae+41UGkefuO78sy2N77lkqRyIPxn4fxQRwwncjZFo+xTFNxWs8VWzt+d5N7z7EzVLmiVLSmyDtZxkVSzub+LSaIYsJGw0pFgt7w0NxiucjmPqJmJfGG5D4nM3lrZqlrqVfAWWa0bK+Wl3xvmKJabwEEqSjZqEhX89Yb21nkRkd.oGP5A9enGvnFodsQt864+ug8UyXt1AWn1DWX1DW+rIt60l35uMwEtMwM.ahKBahKRahKJahaf1D28YSbCxl3FrMwEMCmkGmhuGlPBZIbWRQDQDYxysu649+f285nqoF7UbBOogk2t.TVp0UaNmY0+APmR98l896MK8GR+g75BYcAYc.Yc.Yc.ut8n5Q+Aswe9kRtX3OzOkd9TlGHFwcqWe6ICPumLOPlGfd.YdfXmGvqSn6Ej2e.2eHJbqwcqiEE+fneNsF2sNVz8Ohx42Zb25XQwOH5my+68+0x+kaxfwR3EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+gyA..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[11]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1400.648986769531348, 1577.941200256347656, 50.0, 49.0 ],
					"text" : "46 0.357696"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2341.176426410675049, 1557.352965354919434, 50.0, 22.0 ],
					"text" : "181 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2038.235255718231201, 1557.352965354919434, 50.0, 22.0 ],
					"text" : "181 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2601.018113523764441, 1499.372615098953247, 77.0, 22.0 ],
					"text" : "s linkparam1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 706.25426459312439, 1350.96356189250946, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 719.142168045043945, 1133.070638179779053, 106.0, 22.0 ],
					"text" : "poly~ testCycles 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1906.618831681396387, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1821.618831681396387, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 889.728591442108154, 1601.629089832305908, 50.0, 35.0 ],
					"text" : "0.665888"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 992.036293506622314, 1695.705923557281494, 75.0, 22.0 ],
					"text" : "r gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1155.692694425582886, 1509.131234765052795, 75.0, 22.0 ],
					"text" : "r gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 796.429891020102332, 1585.475242137908936, 77.0, 22.0 ],
					"text" : "s gaincontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1034.715605305816553, 1753.579773426055908, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 949.715605305816553, 1753.579773426055908, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-135",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 796.429891020102332, 1550.372614979743958, 140.0, 20.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1197.215605305816553, 1737.629001140594482, 29.5, 22.0 ],
					"text" : "+~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1130.88235330581665, 1737.629001140594482, 29.5, 22.0 ],
					"text" : "+~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1034.715605305816553, 1796.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 949.715605305816553, 1796.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 870.429891020102332, 1768.705923557281494, 70.0, 22.0 ],
					"text" : ".cycle~ 770"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2885.715605305816553, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2833.715605305816553, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2918.715605305816553, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2833.715605305816553, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2493.694100546028039, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2578.694100546028039, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2493.694100546028039, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2885.715605305816553, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2833.715605305816553, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2918.715605305816553, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2833.715605305816553, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2578.694100546028039, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2493.694100546028039, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2578.694100546028039, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2493.694100546028039, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2247.715605305816553, 1550.372614979743958, 40.408603668212891, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2162.715605305816553, 1550.372614979743958, 42.193547248840332, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2247.715605305816553, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2162.715605305816553, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1873.618831681396387, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1821.618831681396387, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1553.618831681396387, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1501.618831681396387, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1586.618831681396387, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1501.618831681396387, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 914.88235330581665, 1432.567988634109497, 70.0, 22.0 ],
					"text" : ".cycle~ 900"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1219.88235330581665, 1671.705923557281494, 88.0, 22.0 ],
					"text" : "receive~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1157.715605305816553, 1805.705923557281494, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1130.88235330581665, 1671.705923557281494, 80.0, 22.0 ],
					"text" : "receive~ left1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1186.715605305816553, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1134.715605305816553, 1550.372614979743958, 40.0, 22.0 ],
					"text" : "*~ 0.2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 908.549046874046326, 1499.372615098953247, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1219.715605305816553, 1594.705925941467285, 75.0, 22.0 ],
					"text" : "send~ right1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1134.715605305816553, 1594.705925941467285, 68.0, 22.0 ],
					"text" : "send~ left1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1269.88235330581665, 1768.705923557281494, 18.0, 22.0 ],
					"text" : "s"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 4159.715605305817007, 1335.705923557281494, 300.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[23]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1632983935,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5127.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................Lwx3EP6b0GaTUDD+cGP+B4idHEaQnG0vWU9vRUHE41osDgVjODLh.pHsDgVTJRQJPM17HMpEwO.aTgRTgznBsZURiJJQQRiATJBDhgpDfRZLffFfPTPH54t6a2q6850da7OZq8MaxvryN+1Y2clYGd26tTCCCvf2LEbKoxMh0EqmoogeqQBGNWZhysl35ll35tl35gl3hPSbQpItnzDWzZhKFMw0SMwcKZhqWZhq2ZhqORbM52+Y.ZNFxQ+.lGf2Cv5.Xc.rN.VGPVGP73mLVir7BZ6xH2Q4G3GVk+AyCrbFNs6AJo.7tXd.lGv7.XdfyNOPVGzxKfOefze3T31i61kcJ9Am94zdb2trS2+3TN+1i61kcJ9Am94zvvMsQi9r2WP.xEsIyHBAO.N1b7aqkAcPUJDyObCIeuFHGeOmrbMLO.yCv7.7d.VG.qCvpCnzb5O+lS87qjBv65T8CN8yMlGX4Av7ffyDb59Cm54WlEHO+1kkii7t10MrG2sKiw+t1weY70db2trDGx6ZmOXOtaWFi+csi+x3qgQX9tMjIFsa7JRqdhwh1CohckKTQZGiPWX42uBaOP6mt7cdH3lB87wE8Y33TXliop8E1iMEKaUyTOqOlDsQ0YB+4HdTJdSEbFFudQiZRL8Gt95kXohbLPnlenFyx9r0H.Q6x6y3FMrMSBiVyW6mbiRRFXTq0mMuCW+zIolZsbha.CSde132njp31nhsFOvHFdAYAku249EgrpdSwYmom2WLW0wsvSWKocUlCWG0tlR+EcOrUdLvBuzNdoygQFFT+psXT574HlmXOJmmgXsDCy8Er4aRFZ1dj5j7.fZ6NLaaBUWULT5Br8hZiqKXeAeOntFBLrwY1p8lyWS08bq0WcOKwDpwj59Ows80i5XDoNKQ7On6V18gR+sjGB8AhmLLJ3BLtXNs55PmiEVk6nx0wtMsmu2Bbh5dJyS+8g7Nr3NN01do1gQFbRr+XqISNPS4KimNto.e5bLLaJsqXBB8Aai.FiZa55HpUZRZtuBBktr5pJquTibMjxpbtNY8VY8WU.h9Tb7yhpJ1bCUqsF2tNgbv4CRed2sYc1Yi8CfnaThoqGTJBJEIkhhRQSIrgd.zC3r8.ypvklSA4u3UM1Llr27V8xSYLqImhb1dDm0oW9fa+irify8BtcMIC2qeRFkteJ2kuMk4P702Ryv2lpKGeQ4d89xJoM6qrL2guCl6t8EUo62WVUdLekU2Y8czltBUuKxH7DMIqjhkj23hmTVlCgry4LRxAyMEx4KLMRTklAYTkmMY5UNaxSU67Iubc4P9vimOo9lVI42t55HQ3d8j3h44ICyyFHiOgWgLkj1LYdI+FTasUxZm3aSs2NHaYZuG0l6hrmE7QT6taRCK6So19KHWu3uhZ+8SRXieKI4x+NxD21gISqxiQVP0+HYY09Sjh26oHuTcmkTwg9ERUG+WIe4I+cx22zUH+7E+CxEt5eQtwM+a556B5cDcChKld.Ct2QBCySzvXhqmv3SnW.Yv8AlRRwBybX8Clax8GV3XF.rjwEOTv3GHTzDGDTBIQ3ExbHvqNk6.dyoMT3cl4vg2eNiDpYt2I7YKXzv9V3XgCjaJvQVRpvIV18.mofI.mqvzfKUz8BWqXef+R.HxRy.5yKNYX.a79fDesoBCu7rgw9V2OLgsMCH82cVPVUNa3A9fGDlW0ODrnO9gg7pc9vS+4OBr189Xf49dbnr5xA1zAVLrkC8Dv1OxRgcd77gO4DOIrmStb3aNyJfC1zJgidtUAMbwUCMdo0.m+pqCt70dV352743DF+c7we72kTv+tjT9OFju2sFEOSd6EWYKv61QuOjqube0d4Gjqibc632GM+KK2jlSvHQyKkyHCui1a1Ej+yPYLoP0pogDC74z81bKTPawXdawHcrCLibVbgqvaJs5lvR+3Bi9TCi96tU0SUHiCcfb1dH.YeupruZAF17ZqV.8+PZKk8dQ4xUWUx9XTaMQFVw65osggZQO.5APOv+e8.hZjV0FkGil+6aXe8KFKL3hUSbdzDW+zD2spIt9qIt3zD2.zD2soIt30DWBZhafZh610D2fzD2f0DWhbb1dcJgVTlT1og69t5zrU5jsQ7H2O8cNa4zWtl7RRH6ozsCrtY2P8EepUcZVe7yMG7maF8Gn+.uWf0Ev5.Xc.rNf34FrXMxe1A7uaUx2eqSgGTR.6QFw7.tKwoD+kmSLOvxCH8GNUNlGf4Ap4.x6AxwjxH2YjmXOtaWFyCv7.lG.yCv7.LOv4bOny2eWK+W.sTT0iW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8WsF..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1632983935,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5127.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................Lwx3EP6b0GaTUDD+cGP+B4idHEaQnG0vWU9vRUHE41osDgVjODLh.pHsDgVTJRQJPM17HMpEwO.aTgRTgznBsZURiJJQQRiATJBDhgpDfRZLffFfPTPH54t6a2q6850da7OZq8MaxvryN+1Y2clYGd26tTCCCvf2LEbKoxMh0EqmoogeqQBGNWZhysl35ll35tl35gl3hPSbQpItnzDWzZhKFMw0SMwcKZhqWZhq2ZhqORbM52+Y.ZNFxQ+.lGf2Cv5.Xc.rN.VGPVGP73mLVir7BZ6xH2Q4G3GVk+AyCrbFNs6AJo.7tXd.lGv7.XdfyNOPVGzxKfOefze3T31i61kcJ9Am94zdb2trS2+3TN+1i61kcJ9Am94zvvMsQi9r2WP.xEsIyHBAO.N1b7aqkAcPUJDyObCIeuFHGeOmrbMLO.yCv7.7d.VG.qCvpCnzb5O+lS87qjBv65T8CN8yMlGX4Av7ffyDb59Cm54WlEHO+1kkii7t10MrG2sKiw+t1weY70db2trDGx6ZmOXOtaWFi+csi+x3qgQX9tMjIFsa7JRqdhwh1CohckKTQZGiPWX42uBaOP6mt7cdH3lB87wE8Y33TXliop8E1iMEKaUyTOqOlDsQ0YB+4HdTJdSEbFFudQiZRL8Gt95kXohbLPnlenFyx9r0H.Q6x6y3FMrMSBiVyW6mbiRRFXTq0mMuCW+zIolZsbha.CSde132njp31nhsFOvHFdAYAku249EgrpdSwYmom2WLW0wsvSWKocUlCWG0tlR+EcOrUdLvBuzNdoygQFFT+psXT574HlmXOJmmgXsDCy8Er4aRFZ1dj5j7.fZ6NLaaBUWULT5Br8hZiqKXeAeOntFBLrwY1p8lyWS08bq0WcOKwDpwj59Ows80i5XDoNKQ7On6V18gR+sjGB8AhmLLJ3BLtXNs55PmiEVk6nx0wtMsmu2Bbh5dJyS+8g7Nr3NN01do1gQFbRr+XqISNPS4KimNto.e5bLLaJsqXBB8Aai.FiZa55HpUZRZtuBBktr5pJquTibMjxpbtNY8VY8WU.h9Tb7yhpJ1bCUqsF2tNgbv4CRed2sYc1Yi8CfnaThoqGTJBJEIkhhRQSIrgd.zC3r8.ypvklSA4u3UM1Llr27V8xSYLqImhb1dDm0oW9fa+irify8BtcMIC2qeRFkteJ2kuMk4P702Ryv2lpKGeQ4d89xJoM6qrL2guCl6t8EUo62WVUdLekU2Y8czltBUuKxH7DMIqjhkj23hmTVlCgry4LRxAyMEx4KLMRTklAYTkmMY5UNaxSU67Iubc4P9vimOo9lVI42t55HQ3d8j3h44ICyyFHiOgWgLkj1LYdI+FTasUxZm3aSs2NHaYZuG0l6hrmE7QT6taRCK6So19KHWu3uhZ+8SRXieKI4x+NxD21gISqxiQVP0+HYY09Sjh26oHuTcmkTwg9ERUG+WIe4I+cx22zUH+7E+CxEt5eQtwM+a556B5cDcChKld.Ct2QBCySzvXhqmv3SnW.Yv8AlRRwBybX8Clax8GV3XF.rjwEOTv3GHTzDGDTBIQ3ExbHvqNk6.dyoMT3cl4vg2eNiDpYt2I7YKXzv9V3XgCjaJvQVRpvIV18.mofI.mqvzfKUz8BWqXef+R.HxRy.5yKNYX.a79fDesoBCu7rgw9V2OLgsMCH82cVPVUNa3A9fGDlW0ODrnO9gg7pc9vS+4OBr189Xf49dbnr5xA1zAVLrkC8Dv1OxRgcd77gO4DOIrmStb3aNyJfC1zJgidtUAMbwUCMdo0.m+pqCt70dV352743DF+c7we72kTv+tjT9OFju2sFEOSd6EWYKv61QuOjqube0d4Gjqibc632GM+KK2jlSvHQyKkyHCui1a1Ej+yPYLoP0pogDC74z81bKTPawXdawHcrCLibVbgqvaJs5lvR+3Bi9TCi96tU0SUHiCcfb1dH.YeupruZAF17ZqV.8+PZKk8dQ4xUWUx9XTaMQFVw65osggZQO.5APOv+e8.hZjV0FkGil+6aXe8KFKL3hUSbdzDW+zD2spIt9qIt3zD2.zD2soIt30DWBZhafZh610D2fzD2f0DWhbb1dcJgVTlT1og69t5zrU5jsQ7H2O8cNa4zWtl7RRH6ozsCrtY2P8EepUcZVe7yMG7maF8Gn+.uWf0Ev5.Xc.rNf34FrXMxe1A7uaUx2eqSgGTR.6QFw7.tKwoD+kmSLOvxCH8GNUNlGf4Ap4.x6AxwjxH2YjmXOtaWFyCv7.lG.yCv7.LOv4bOny2eWK+W.sTT0iW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8WsF..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[7]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 3820.715605305816553, 1335.705923557281494, 300.000000000000455, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[24]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1632983935,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5144.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LA23EP6b0GTUUDE+xSkuL+.Lg.S4IM9EoXHkNh91CfSJX9QZSlZoEZohkXhIpzDy0gohx9Pio7ClJcXJMnrbXprbJygoQKwTyoQJGSbXZzzZTGmRCmh1yd28w8s7.V+GnX2cly6rm87aO6tmy4d39t22fkkEXwZ1btiToVQEB1y11pQmQZKbgnHNOJhqKJhqqJhqaJhKTEwElh3BWQbQnHtHUDW2UD2MoHtdnHtdpHtdIvUWiMdZfliY3F+fIOvbcfoNfoNfoNfoNfnN.+1OQVcXdAscICWq7CrCqqOL4ANNCc65.Wo.rtl7.Sd.5AL4A5cdfnNniWvb+AB+gtvki6xx5heP2OmxwcYYc2+nKme43trrt3Gz8yokkGZiF8wmWfeJDZSjQDDteb3bZTpkAcP2TPleaMj34ZX3lmyIlqYxCL4Al7.y0Al5.l5.Xc.WMc+92z0yuqT.VWc0On6maSdfiGvjGDXlft6Oz0yuHKPb9kkEia3ctqaHG2kkMw+N2weQ7UNtKKKvY3ctyGji6xxl3em63uH9ZY0FuaCQhQ6FeS4NNXQg1.wpttGv6UguAnikt3Ydv41bbrw488+tavoQGioi2GY1bb1DG8AfmNDCOrqIcFeHZZih2F9yg9PT7r45LJ8yWqfgOdT+gqoFAVpHCSPmevsIZ+.HpHSF4V0VlMAoU+kMRZnnj.jZo937NbMSgjZpUwHlArrY8wwannJX1XqaIN.IDOmbfx16L+BW1sdaDKsg5Y84y083N3oqkvttlCSGNeg+htG1BKF3fWXGuz4fjkE0uJEiRmMG97PHzlXdV70xYT5m35f6UxfxNZgNA2OnVuCZaanxJhjRmG2KtaLc70GsqaRfiiA0g1p8lyVSwdo033dStErwjwbCIK85Q0FQpShG+C3ZKYemveK3AQu+3IhwEN+iymSKtNz43f000nh0Q1lx46MCGutmq4o99PbML+Zbps8RsCRVLhu+v0Dk82b8x3oiaywmNCCZSgc4SfqOPa32XTaSWGdsRaRS8cgvUWrtpq0WnQrFBY2blNQ8VQ8W2.38o3XmE2pv4FrVqMtrNtbf4CBedWkrNd1ve.DcgRnttQoPoTXTJbJEAkLMiGv3AzaOvzyew4jWtKbkiLiI3cIqZYoj7pyo.81inWmdwMt8OhNbNyK3Ijwa4Yci2p38S4g3aCYNPe8t3L7sgpywW3dVmurRbi9JIys66fKX29Bu386KqxOluRp9L9NZ8WlpODxPiNBRVIFEYIiJNRIYNPxNm4vHGbAoPNW9ikDdwYPFdoYSlR4yf7DUMGxKUcNj2+34Rpo9UP9sqrVRndVGIlHeVxfi9EHiN9WlLwD2HY1I85TasExZR6Mo1a6jMO42gZy2irm49AT6taRsK8io19yHWqvufZ+8She8eMIoR+FRZkcXxjK+Xj4V4OPVZU+Hov8dJxKV8YHa8P+Bohi+qjO+j+N4aq+xje5B+A47W4uHMb8+lt9g.8Lzt.wDY2fAzyvfAGcDPxwzcXzw2CfLfdASLwnfoM39.yJo9ByO4XgEMp3f7Fc+fBRq+PQjDfmKyABuxDuM3Ml7ff2ZZCAd2YNLXWy51gOYti.127GIbfEjBbjEkJbhkdWvoyaLvYyervEKXbvUKzGzXQ.DVwY.854m.D65uaHgWcRvPJMaXja5dfwT1Tgze6oCYU9Lf6cG2GL6Jue3Q9vG.VRUyAdxO8Ag0r24A166ggRpNGXCGXgvlOziAa6HKF14wyE9nS73vdN4xfu5zKGNX8q.N5YWIT6EVET2EWMbtqrV3RW8ogqc8mgQl3u1G+M+tjB72kjq+vf34tUG+dxau3t1Brtcz6Cw5K1WsW9Aw5HV2N98QS+xxso4DHwadobjr7NBuYmWtOEkgRAqsqZSv+2S2aSsfAsYi4sYizwNvTyYg4ubuozhaBG8ipMzmZan+NaQ8TEh3PGHG2C9I48pq8Uyvfyq0Z90+cicw3yEkIWYEI4CoVahHV9y5o0gYzZ7.FOfwC7+WO.uFoSsQwwno++F16F4i0F3hRQbQqHt9nHtaVQb8UQbwnHtXUD2snHt3TDW7JhqeJh6VUDW+UD2.TDWBLbRONkfKJRJuQ4INuM88WX2OZrNyKsxN+NR9F0DAGum6H3iaFsk7.QW71.ptPyt1ZJ7Tq7mw9lu2bfeuYi+v3OLWWXpKXpCXpCXpCDvMRTG5OnMwyuzv0C+gyoroOM4A5QbW956lx.b5YxCL4AnGvjGn24Ah5DNdAy8GH7G5BWNtKKqK9Ac+bJG2kk0c+itb9ki6xx5heP2Om+26+qk+6pHJkB3EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+YrA..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1632983935,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5144.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LA23EP6b0GTUUDE+xSkuL+.Lg.S4IM9EoXHkNh91CfSJX9QZSlZoEZohkXhIpzDy0gohx9Pio7ClJcXJMnrbXprbJygoQKwTyoQJGSbXZzzZTGmRCmh1yd28w8s7.V+GnX2cly6rm87aO6tmy4d39t22fkkEXwZ1btiToVQEB1y11pQmQZKbgnHNOJhqKJhqqJhqaJhKTEwElh3BWQbQnHtHUDW2UD2MoHtdnHtdpHtdIvUWiMdZfliY3F+fIOvbcfoNfoNfoNfoNfnN.+1OQVcXdAscICWq7CrCqqOL4ANNCc65.Wo.rtl7.Sd.5AL4A5cdfnNniWvb+AB+gtvki6xx5heP2OmxwcYYc2+nKme43trrt3Gz8yokkGZiF8wmWfeJDZSjQDDteb3bZTpkAcP2TPleaMj34ZX3lmyIlqYxCL4Al7.y0Al5.l5.Xc.WMc+92z0yuqT.VWc0On6maSdfiGvjGDXlft6Oz0yuHKPb9kkEia3ctqaHG2kkMw+N2weQ7UNtKKKvY3ctyGji6xxl3em63uH9ZY0FuaCQhQ6FeS4NNXQg1.wpttGv6UguAnikt3Ydv41bbrw488+tavoQGioi2GY1bb1DG8AfmNDCOrqIcFeHZZih2F9yg9PT7r45LJ8yWqfgOdT+gqoFAVpHCSPmevsIZ+.HpHSF4V0VlMAoU+kMRZnnj.jZo937NbMSgjZpUwHlArrY8wwannJX1XqaIN.IDOmbfx16L+BW1sdaDKsg5Y84y083N3oqkvttlCSGNeg+htG1BKF3fWXGuz4fjkE0uJEiRmMG97PHzlXdV70xYT5m35f6UxfxNZgNA2OnVuCZaanxJhjRmG2KtaLc70GsqaRfiiA0g1p8lyVSwdo033dStErwjwbCIK85Q0FQpShG+C3ZKYemveK3AQu+3IhwEN+iymSKtNz43f000nh0Q1lx46MCGutmq4o99PbML+Zbps8RsCRVLhu+v0Dk82b8x3oiaywmNCCZSgc4SfqOPa32XTaSWGdsRaRS8cgvUWrtpq0WnQrFBY2blNQ8VQ8W2.38o3XmE2pv4FrVqMtrNtbf4CBedWkrNd1ve.DcgRnttQoPoTXTJbJEAkLMiGv3AzaOvzyew4jWtKbkiLiI3cIqZYoj7pyo.81inWmdwMt8OhNbNyK3Ijwa4Yci2p38S4g3aCYNPe8t3L7sgpywW3dVmurRbi9JIys66fKX29Bu386KqxOluRp9L9NZ8WlpODxPiNBRVIFEYIiJNRIYNPxNm4vHGbAoPNW9ikDdwYPFdoYSlR4yf7DUMGxKUcNj2+34Rpo9UP9sqrVRndVGIlHeVxfi9EHiN9WlLwD2HY1I85TasExZR6Mo1a6jMO42gZy2irm49AT6taRsK8io19yHWqvufZ+8She8eMIoR+FRZkcXxjK+Xj4V4OPVZU+Hov8dJxKV8YHa8P+Bohi+qjO+j+N4aq+xje5B+A47W4uHMb8+lt9g.8Lzt.wDY2fAzyvfAGcDPxwzcXzw2CfLfdASLwnfoM39.yJo9ByO4XgEMp3f7Fc+fBRq+PQjDfmKyABuxDuM3Ml7ff2ZZCAd2YNLXWy51gOYti.127GIbfEjBbjEkJbhkdWvoyaLvYyervEKXbvUKzGzXQ.DVwY.854m.D65uaHgWcRvPJMaXja5dfwT1Tgze6oCYU9Lf6cG2GL6Jue3Q9vG.VRUyAdxO8Ag0r24A166ggRpNGXCGXgvlOziAa6HKF14wyE9nS73vdN4xfu5zKGNX8q.N5YWIT6EVET2EWMbtqrV3RW8ogqc8mgQl3u1G+M+tjB72kjq+vf34tUG+dxau3t1Brtcz6Cw5K1WsW9Aw5HV2N98QS+xxso4DHwadobjr7NBuYmWtOEkgRAqsqZSv+2S2aSsfAsYi4sYizwNvTyYg4ubuozhaBG8ipMzmZan+NaQ8TEh3PGHG2C9I48pq8Uyvfyq0Z90+cicw3yEkIWYEI4CoVahHV9y5o0gYzZ7.FOfwC7+WO.uFoSsQwwno++F16F4i0F3hRQbQqHt9nHtaVQb8UQbwnHtXUD2snHt3TDW7JhqeJh6VUDW+UD2.TDWBLbRONkfKJRJuQ4INuM88WX2OZrNyKsxN+NR9F0DAGum6H3iaFsk7.QW71.ptPyt1ZJ7Tq7mw9lu2bfeuYi+v3OLWWXpKXpCXpCXpCDvMRTG5OnMwyuzv0C+gyoroOM4A5QbW956lx.b5YxCL4AnGvjGn24Ah5DNdAy8GH7G5BWNtKKqK9Ac+bJG2kk0c+itb9ki6xx5heP2Om+26+qk+6pHJkB3EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+YrA..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[8]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 3500.715605305816553, 1335.705923557281494, 300.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[25]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1632983935,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5139.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................Lw03EP6b0.aTTDEduCn+g7SKRosHziZ3uJ+XoUHEYmWaIBsH+HXDATQaIBTTJRQJPM1rjF0h3OfMpPIpPZTgVEkznhRTjzX.kVABwPUBRwFCHfAHDADhdNyryrcuoW6MQRZ0Nyj75ady6adyLu2aecucubFFFfAsYw31RkaDsGROKKC+1iDJbdjDmWIw0EIw0UIw0MIwElj3BWRbQHItHkDWTRhq6Rh6VjDWOjDWOkDWu33Zzu+SB3bLMW6Gz4A5qCz0Az0Az0Az0A30AX29Ig0HIu.2tnlqT9A5g00ez4A1NCU65.Wo.zt57.cd.wCnyCT67.dcPauf99C39CUgKF2EkUE+fpeNEi6hxpt+QUN+hwcQYUwOn5mSCCu3FN5SddANjGbimQDDtCNxb7KzxDOnaJHyOTCwetFZt94bRx0z4A57.cdf95.cc.cc.Rc.WMU+92T0yuqT.ZWU0On5macdfsGPmGDXlfp6OT0yOOKfe9Ek4iq4ctqaHF2Ek0w+N2wed7ULtKJywo4ctyGDi6hx53em63OO9ZXDh2sAOwn8i66WP0e9qfpXG4AUj9QP3El+9UH6Ab+L3OyCF2homNNqOAGkBwbrbael8HSw1V6bxmxjHgaXcVvUF1Ciwa4BmgwqUzHl.Qe80UGGKVjhAB17C1X11mrFNDtKsOgazvVrPDZUekez0KIYfPsVex7putohRM0ZnD0.FVz9jwudIUQsQEaNdfPD7LxFJcuS8KLY25sXmchdZe1bcOtMd7Zwsqq4P0gsqE2eg2CalFCrwysiO7bHjgA1uJDixfNG17X6Q97LXqEaXpufLeKzfyIFtNN2ATa2gXaKn5phBSmkrWb2n5BzWP2CtWCFFx3Da0dyoqo68bq028dliIXiw08uhK75QUFQryhE+C3ZKQeH2ey4AQuS7jfwENmwYyoUWG7brw55ZT95HZSw78VfiU2y07jeevuFlcMN119v1gPFThs+HqIQ1o45kwiG2hgOCJFhM41kMAl9.sgiwv1FuNrZkVnl66Bgqtj5ptVetF9Zvkcyo530a40ecCf0GiidVbqhL2f0ZqwE0wjCLef6y6pf0ImMxW.htfIhttgovvT3XJBLEIlzMk2C3W48.psCv+LJbQ4VP9KXEiNyI5awqbooLpUkaQpsKQoN8N221e6zytC0K30yDL7t1IXT59vbOlaHqAY16RyzbC0lqYDdWqY1IsQyxxZalGHucYFQo6yL6JOhYY0dJyC2zkv58fFVLQhxNonQKdLwiJKqAg19rFN5.4kB5LElNJhRyDMhxyAM0JmI5IqYtnWp1bQevQyGUWSKGc9KuFTXdWKJ1ndNzPhYcnwlvKilTRaDMmjecrs1LZ0i+sv1aanMMk2Eaycf1879Prc2Epgk7IXa+4nqU7Whs+9PIr9uAkb4eKZ7aodzTp7Hn4U8OfVRM+Hp38bBzKV6oPUbveEU0Q+MzWb7eG8cMcIzOct+.c1K+mnqei+Bu9dfdFVWfXipav.6Y3vPhIRXTw1cXrIzC.MvdASJongoOj9.yN49ByeT8CV3XhGJXr8GJZ7C.JAkH77YMH3UlzsCuwTFL71Senv6MqgC6b12A7oyajvdm+ng8mWJvgVXpvwVxcAmrfwAmtvzgKTzcCWsXSveI.DdoYB85ElHzu0eOPhu5jggVdNvney6EF2VlFjw6LCH6JmIbeu+8Cyo5G.drO5AgEWybgm5ydHX064Q.q89nPY0lKrg8u.XSG7wgsdnEAa+n4Ce7wdBX2GeovWexkAGnokCG9zq.Z3bqDZ7BqBNykWCbwq9Lv0twyRIc7W4i+5uWRA98Rx0+Xf+b2ZjcO4sWbWaAZ2N58Ae846q1K+.ec3qaG+9n4uY4V3bBBwZ9vbBY3aj9xof7eZLiHEr1NaHQmOmtulaACZKFyWKFoicfok6BJbY9RoU2D15GSHzmZHzmVqpGqfGG5.4j8fCItWcsuZAFx7Zqli9uO8EQdtnT4pqJYSB0VSjfk8rdZaXZsZOf1Cn8.++0CvpQZWajeLZ922vd6mMVHvEsj3hQRb8QRb2pj35qj3hURb8SRbwIIt3kDWBRhq+Rh61jD2.jD2.kDWhTbBONkfKlVZ9S6ljhKt3VNO29lm68Nu4sQmSKDr3D4jFSoaEH5xog5J9Dq3mA7P5O2bfetYs+P6OzWWnqKnqCnqCnqCDvsG0Hwefa7meolqF9C6SYy+UmGnFwcwquaNCvtmNOPmGP7.57.0NOfWmv1Knu+.t+PU3hwcQYUwOn5mSw3tnrp6eTkyuXbWTVU7Cp947+d+tV9OSP+cLgW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8WvF..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1632983935,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5139.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................Lw03EP6b0.aTTDEduCn+g7SKRosHziZ3uJ+XoUHEYmWaIBsH+HXDATQaIBTTJRQJPM1rjF0h3OfMpPIpPZTgVEkznhRTjzX.kVABwPUBRwFCHfAHDADhdNyryrcuoW6MQRZ0Nyj75ady6adyLu2aecucubFFFfAsYw31RkaDsGROKKC+1iDJbdjDmWIw0EIw0UIw0MIwElj3BWRbQHItHkDWTRhq6Rh6VjDWOjDWOkDWu33Zzu+SB3bLMW6Gz4A5qCz0Az0Az0Az0A30AX29Ig0HIu.2tnlqT9A5g00ez4A1NCU65.Wo.zt57.cd.wCnyCT67.dcPauf99C39CUgKF2EkUE+fpeNEi6hxpt+QUN+hwcQYUwOn5mSCCu3FN5SddANjGbimQDDtCNxb7KzxDOnaJHyOTCwetFZt94bRx0z4A57.cdf95.cc.cc.Rc.WMU+92T0yuqT.ZWU0On5macdfsGPmGDXlfp6OT0yOOKfe9Ek4iq4ctqaHF2Ek0w+N2wed7ULtKJywo4ctyGDi6hx53em63OO9ZXDh2sAOwn8i66WP0e9qfpXG4AUj9QP3El+9UH6Ab+L3OyCF2homNNqOAGkBwbrbael8HSw1V6bxmxjHgaXcVvUF1Ciwa4BmgwqUzHl.Qe80UGGKVjhAB17C1X11mrFNDtKsOgazvVrPDZUekez0KIYfPsVex7putohRM0ZnD0.FVz9jwudIUQsQEaNdfPD7LxFJcuS8KLY25sXmchdZe1bcOtMd7Zwsqq4P0gsqE2eg2CalFCrwysiO7bHjgA1uJDixfNG17X6Q97LXqEaXpufLeKzfyIFtNN2ATa2gXaKn5phBSmkrWb2n5BzWP2CtWCFFx3Da0dyoqo68bq028dliIXiw08uhK75QUFQryhE+C3ZKQeH2ey4AQuS7jfwENmwYyoUWG7brw55ZT95HZSw78VfiU2y07jeevuFlcMN119v1gPFThs+HqIQ1o45kwiG2hgOCJFhM41kMAl9.sgiwv1FuNrZkVnl66Bgqtj5ptVetF9Zvkcyo530a40ecCf0GiidVbqhL2f0ZqwE0wjCLef6y6pf0ImMxW.htfIhttgovvT3XJBLEIlzMk2C3W48.psCv+LJbQ4VP9KXEiNyI5awqbooLpUkaQpsKQoN8N221e6zytC0K30yDL7t1IXT59vbOlaHqAY16RyzbC0lqYDdWqY1IsQyxxZalGHucYFQo6yL6JOhYY0dJyC2zkv58fFVLQhxNonQKdLwiJKqAg19rFN5.4kB5LElNJhRyDMhxyAM0JmI5IqYtnWp1bQevQyGUWSKGc9KuFTXdWKJ1ndNzPhYcnwlvKilTRaDMmjecrs1LZ0i+sv1aanMMk2Eaycf1879Prc2Epgk7IXa+4nqU7Whs+9PIr9uAkb4eKZ7aodzTp7Hn4U8OfVRM+Hp38bBzKV6oPUbveEU0Q+MzWb7eG8cMcIzOct+.c1K+mnqei+Bu9dfdFVWfXipav.6Y3vPhIRXTw1cXrIzC.MvdASJongoOj9.yN49ByeT8CV3XhGJXr8GJZ7C.JAkH77YMH3UlzsCuwTFL71Senv6MqgC6b12A7oyajvdm+ng8mWJvgVXpvwVxcAmrfwAmtvzgKTzcCWsXSveI.DdoYB85ElHzu0eOPhu5jggVdNvney6EF2VlFjw6LCH6JmIbeu+8Cyo5G.drO5AgEWybgm5ydHX064Q.q89nPY0lKrg8u.XSG7wgsdnEAa+n4Ce7wdBX2GeovWexkAGnokCG9zq.Z3bqDZ7BqBNykWCbwq9Lv0twyRIc7W4i+5uWRA98Rx0+Xf+b2ZjcO4sWbWaAZ2N58Ae846q1K+.ec3qaG+9n4uY4V3bBBwZ9vbBY3aj9xof7eZLiHEr1NaHQmOmtulaACZKFyWKFoicfok6BJbY9RoU2D15GSHzmZHzmVqpGqfGG5.4j8fCItWcsuZAFx7Zqli9uO8EQdtnT4pqJYSB0VSjfk8rdZaXZsZOf1Cn8.++0CvpQZWajeLZ922vd6mMVHvEsj3hQRb8QRb2pj35qj3hURb8SRbwIIt3kDWBRhq+Rh61jD2.jD2.kDWhTbBONkfKlVZ9S6ljhKt3VNO29lm68Nu4sQmSKDr3D4jFSoaEH5xog5J9Dq3mA7P5O2bfetYs+P6OzWWnqKnqCnqCnqCDvsG0Hwefa7meolqF9C6SYy+UmGnFwcwquaNCvtmNOPmGP7.57.0NOfWmv1Knu+.t+PU3hwcQYUwOn5mSw3tnrp6eTkyuXbWTVU7Cp947+d+tV9OSP+cLgW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8WvF..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[9]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 3146.715605305816553, 1335.705923557281494, 300.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[26]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1632983935,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5165.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQ73EP6b0.TUUDE99dn7mo.OSLve3IM9G4OgT5f4cO.VJX9SZSlZkJ3jJThIlnRSLWGlJL6GMlJEmJcXpTnrbXprbJygoQMwTGmFQcTwgozzZTGlRSm50t6c2GWVdvamlQnX2clCm8rmu8r6dNm6gK26imggAXPaVLtsTYFw3hzyxxvm8HACmKIw4VRbgHIttHIttJItPkDWXRhKbIwEgj3hTRbcSRb2lj35tj35gj3hhiqde9NKfywzbsePmGnuNPWGPWGPWGPWGfWGfc6mDV8j7Bb6JZtR4GnGVG+PmGX6LTsqCbjBP6pyCz4ADOfNOPsyC30As8B56Of6OTEtXbWTVU7Cp94TLtKJq59GU47KF2EkUE+fpeNMLbia3nO44E3mbga7Lh.v8iiLGeBszwC5jBv7C1P7mqglqeNmjbMcdfNOPmGnuNPWGPWGfTGvQS0u+MU876HEf1UU8Cp94VmGX6Az4AMOSP08Gp54mmEvO+hx7w07N20MDi6hx53em63OO9JF2Ek43z7N24ChwcQYc7uyc7mGeMLBx61fmXztwKO0ZQFyeWnx2dNP4odTDdg4ueExd.2OM9y7fwsX5oiy5SvQofLGKm1mYOxTrs0Nl34LIR3FVmE7GC4ww3sbfyv3MJbXiin+P0VKGKVjhABz7CzX11mrF9IbWZeB2ntMagHzJ+FenaTbR.gZs9j4cnZmLJkTplRTCXXQ6SF+FEWI0Fkuo3.BQvyHanz8N0uvjcp2hc1I5o8Yy04313wqE2tNlCUG1tVb+EdOrIZLvFO2NdwygPFFX+pPLJM5bXyisG4yyfsVrgo9Bx7sPCLKObcbtePscGhssfppLRLcQxdwYipq49B5dv4ZvvPFmXq1aNcMctmas9N2ybLAZLtt+UbgWOpxHhcVr3eyt1RzGx82bd.z6OdRv3.m+wYyoUWG7brw53ZT95HZSw78VfiU2yw7jeevuFlcMN11dw1gPFThs+HqIQ1eywKiGOtECeZTLDaxsKaBL8M2F9MF1130gUqzB0TeGHbzkTW0w5y0vWCtrSNUGudKu9qS.r9XbzyhSUj4FnVaMtnNlbyyG397tHXcxYi7AfHDLQz0ULEJlBCSgioHvjto8.ZOfZ6AlVAKJ67ycAKejoOduKdEKI4QrxrKTs8Hp0omeia+MuCiS8BtcMNC2qYbFkrWL2k45yX.lQWR5lqulrMC28ZLyLwMXVZFa0b+4rSyvKYulYVwQMKslyYdjFtJVuKzP7DAJyDiAs3QEGpzLF.ZayXnn8mSxnKTPpnvKIczvJKKzjqX5nmt5YidkZxF8QGKWTsMrLzu13pQg5dMnXi7EPCxyZQiN9WEMgD2.ZVI8lXasIzpF66fs2VQabRuO1laGsq47wX6tSTc48YXa+knqWzWis+dQwutuCkTYG.M1MeHzjp3nn4T0Ohxq5SfJZ2mF8x0bNT4G7mPUdreA8Um52PeeCWEcxK86nK13ehtwM+K756B5Qng.wFYWg92ivfA4IBXDw1MXzw2c.0+nfIjXLvTGTOgYlTuf4NhdCKbTwA4O59.EN19AEiR.dwLF.7ZS3Ng2ZRCDd2oNX3ClwPgcLy6B974LbXOycjv9xIY3vKLE334cuvYyeLv4KHU3xEdev0JxD7UL.gUR5PTuz3gdutG.R30mHL3xxBF4a+fvX17TfzduoAYVwzgG5CeXXVU8Hv7+jGEVb0yFdlu3wfUs6m.r1y7fRqIaX86aAvFO3SBa4vKB11wxE9zi+TvtN0Rfu8rKE1eCKCNx4WNT2kVAT+kWIbgFWMbkq8bv0u4ySIc7W4i+5OWRM+ykjiew.+4tUO6dxau3N1Bztcz6C95y2WsW9A95vW2N98QSexxsv4DDh07h4Dxv6v8lU949rXFQpks4E8NpKA++c5dap0RnAXDuAXrNxglR1Knfk5M4VcKXqeTAQeJAQ+8zp5wJ3wgNPNYO3mD2qN1Ws.CYdsUyu9eH0EQdtnT4ppLISB0VSjfk8rdZaXZsZOf1Cn8.++0CvpQZWajeLZ562vn8wFKH3hQRbdjDWOkD2sKItdIItXkDWukD2cHIt3jDW7RhqORhquRhqeRhq+RhKAJNgGmRfE6qggG+2ByOelKbBRFIm2XFgbx6ORCiCf4QtVd96sJdwTC69tuUY+NK1k3mhFS4gicjlmR1BPhYYUWsEc5keFbe8+We5uWO0eulpuNPWGPWGPWGPWGnE0AvtDdqdx8Kfa7meolqF9C6SYS+TmGnFwcwquaJCvtmNOPmGP7.57.0NOfWmv1Knu+.t+PU3hwcQYUwOn5mSw3tnrp6eTkyuXbWTVU7Cp947+deuV9O.r20063EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+ssA..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1632983935,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5165.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQ73EP6b0.TUUDE99dn7mo.OSLve3IM9G4OgT5f4cO.VJX9SZSlZkJ3jJThIlnRSLWGlJL6GMlJEmJcXpTnrbXprbJygoQMwTGmFQcTwgozzZTGlRSm50t6c2GWVdvamlQnX2clCm8rmu8r6dNm6gK26imggAXPaVLtsTYFw3hzyxxvm8HACmKIw4VRbgHIttHIttJItPkDWXRhKbIwEgj3hTRbcSRb2lj35tj35gj3hhiqde9NKfywzbsePmGnuNPWGPWGPWGPWGfWGfc6mDV8j7Bb6JZtR4GnGVG+PmGX6LTsqCbjBP6pyCz4ADOfNOPsyC30As8B56Of6OTEtXbWTVU7Cp94TLtKJq59GU47KF2EkUE+fpeNMLbia3nO44E3mbga7Lh.v8iiLGeBszwC5jBv7C1P7mqglqeNmjbMcdfNOPmGnuNPWGPWGfTGvQS0u+MU876HEf1UU8Cp94VmGX6Az4AMOSP08Gp54mmEvO+hx7w07N20MDi6hx53em63OO9JF2Ek43z7N24ChwcQYc7uyc7mGeMLBx61fmXztwKO0ZQFyeWnx2dNP4odTDdg4ueExd.2OM9y7fwsX5oiy5SvQofLGKm1mYOxTrs0Nl34LIR3FVmE7GC4ww3sbfyv3MJbXiin+P0VKGKVjhABz7CzX11mrF9IbWZeB2ntMagHzJ+FenaTbR.gZs9j4cnZmLJkTplRTCXXQ6SF+FEWI0Fkuo3.BQvyHanz8N0uvjcp2hc1I5o8Yy04313wqE2tNlCUG1tVb+EdOrIZLvFO2NdwygPFFX+pPLJM5bXyisG4yyfsVrgo9Bx7sPCLKObcbtePscGhssfppLRLcQxdwYipq49B5dv4ZvvPFmXq1aNcMctmas9N2ybLAZLtt+UbgWOpxHhcVr3eyt1RzGx82bd.z6OdRv3.m+wYyoUWG7brw53ZT95HZSw78VfiU2yw7jeevuFlcMN11dw1gPFThs+HqIQ1eywKiGOtECeZTLDaxsKaBL8M2F9MF1130gUqzB0TeGHbzkTW0w5y0vWCtrSNUGudKu9qS.r9XbzyhSUj4FnVaMtnNlbyyG397tHXcxYi7AfHDLQz0ULEJlBCSgioHvjto8.ZOfZ6AlVAKJ67ycAKejoOduKdEKI4QrxrKTs8Hp0omeia+MuCiS8BtcMNC2qYbFkrWL2k45yX.lQWR5lqulrMC28ZLyLwMXVZFa0b+4rSyvKYulYVwQMKslyYdjFtJVuKzP7DAJyDiAs3QEGpzLF.ZayXnn8mSxnKTPpnvKIczvJKKzjqX5nmt5YidkZxF8QGKWTsMrLzu13pQg5dMnXi7EPCxyZQiN9WEMgD2.ZVI8lXasIzpF66fs2VQabRuO1laGsq47wX6tSTc48YXa+knqWzWis+dQwutuCkTYG.M1MeHzjp3nn4T0Ohxq5SfJZ2mF8x0bNT4G7mPUdreA8Um52PeeCWEcxK86nK13ehtwM+K756B5Qng.wFYWg92ivfA4IBXDw1MXzw2c.0+nfIjXLvTGTOgYlTuf4NhdCKbTwA4O59.EN19AEiR.dwLF.7ZS3Ng2ZRCDd2oNX3ClwPgcLy6B974LbXOycjv9xIY3vKLE334cuvYyeLv4KHU3xEdev0JxD7UL.gUR5PTuz3gdutG.R30mHL3xxBF4a+fvX17TfzduoAYVwzgG5CeXXVU8Hv7+jGEVb0yFdlu3wfUs6m.r1y7fRqIaX86aAvFO3SBa4vKB11wxE9zi+TvtN0Rfu8rKE1eCKCNx4WNT2kVAT+kWIbgFWMbkq8bv0u4ySIc7W4i+5OWRM+ykjiew.+4tUO6dxau3N1Bztcz6C95y2WsW9A95vW2N98QSexxsv4DDh07h4Dxv6v8lU949rXFQpks4E8NpKA++c5dap0RnAXDuAXrNxglR1Knfk5M4VcKXqeTAQeJAQ+8zp5wJ3wgNPNYO3mD2qN1Ws.CYdsUyu9eH0EQdtnT4ppLISB0VSjfk8rdZaXZsZOf1Cn8.++0CvpQZWajeLZ562vn8wFKH3hQRbdjDWOkD2sKItdIItXkDWukD2cHIt3jDW7RhqORhquRhqeRhq+RhKAJNgGmRfE6qggG+2ByOelKbBRFIm2XFgbx6ORCiCf4QtVd96sJdwTC69tuUY+NK1k3mhFS4gicjlmR1BPhYYUWsEc5keFbe8+We5uWO0eulpuNPWGPWGPWGPWGnE0AvtDdqdx8Kfa7meolqF9C6SYS+TmGnFwcwquaJCvtmNOPmGP7.57.0NOfWmv1Knu+.t+PU3hwcQYUwOn5mSw3tnrp6eTkyuXbWTVU7Cp947+deuV9O.r20063EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+ssA..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[10]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 2817.715605305816553, 1335.705923557281494, 302.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[18]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1632983935,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5116.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LAv3EP6b0GaUTDD+dEneg7QKRwVD5iZ3qJeXoJD931osDgVjODLh.pnEh.EkhTjBTiMGoQsJ9AXixWQERiJ1pnjFUThhjFCnTDHDCUIHkzX.AM.gnfknO2Yuce8dWesc8eZwdylLc1Yme6r6NybSu2cu7LLL.CQyRxskJyHNeXOKKi.1izZ37oItHzDWmzDWm0DWWzDWjZhKJMwEsl3hQSbwpIttpItaRSbcSSbcWSb8Pgqt.ANMvywHN4Gn7.55.pN.UGfpCP0AT0Aj29IxpCyK3sKQbOkePbXc7GJOv1Y30tNvQJfnKkGP4AnGfxC714Ap5f1dA59CT9CuB2cb2srWwO30Omti6tk859Gux42cb2srWwO30OmFFQva7nO97BBR93MUFQX3AwgyIfqVl7AcRgY9s1PpmqAwomyIlqQ4ATd.kGPWGP0An5.Xc.GMu98u4UO+NRADc8p9Au94lxCr8.TdPnYBdc+gW87qxBTme2xpwIdG65Fti6tko3eG63uJ95Nt6VVgi3cryGbG2cKSw+N1weU70vnUd2FpDi1L9lxa7vhhrAlQccMj2qhbCvGKC0y7Pxsj3DiK6G7c2fSiOlPmrOxrj3rX15CAOeHAdXWS9LlHZdii2B9yg7Pb7h4ZOJ+uuVgCaBn9CWSMJrbQAlvN+vaSz9gPbQgLxMpcaVLjV8WEf0PwoBH0b8w4c3ZlJK8zqRPBCXXI5ii2PwUHrwV2Rh.RHdIYCUr2E9EorS8VHVdC0K5KmqywswyWKkccLGgNb9J+EeOrEQLvFuxN94yAICCte0ULJCwbjyCgvap4YHWK6Q4+EWGbuxFXNwqzo3AA0xcPaaAUVQrb573dwYSnSt9nccRJbRLnNzVs0bwZp1KsDG2atagaL2X9OI650i5YD4NIY7Ojqsb66T9aEOL5CFOQLNvEbb4bZ10gOGarNtFUsNtso678lfSV2yw7zeentFVdMN21941AICAI2e3ZhxAaNdY77wsj3yPfAsoxtxIH0GpMBZLts4qirVoEqw9NP3nKVW0w5qznVCkrStPmpdqp9qS.x9bbhyhSU3bCWqkF2sNobn4CJedmcYc7rgeAH5DmPccgSQxon3TzbJFNQMxCPd.usGXFEr3byOuEtxQl4D8ujUsrzFwpysPusGwac5U231+n5H4BuPD9lfQDqaBFkreN2m4FxZ.l8rjLM2P04ZFcDqyL6T1nYoYsCyCtfcaFcI62L6xOlYoUeFyiV+k458wFR7wvxNk3XKYTIxJMqAv14rFJ6fKHM14JXrrnKIS1vJKG1TKelrmnp4xdopyk8AGOOVM0uB1uck0xhLh0wRH1mkMn3eA1nS5kYSJkMxlSpuN2Vagslw8lb6sC1lmx6vs46y1y79Ptc2Mq1k9Iba+4rqUzWxs+9YIs9ugkZYeKaba6vroT9wXyqxefszp9QVQ68TrWr5yv15g9EVEG+WYewI+c12U+kY+zE9C14uxewZ35+Me88AcOxNAIDaWf928nfAEeLvHRnqvnSpa.q+8.lTJwASeP8Blcp8Fl+H5CrnQkHj+n6KT335GTLKY34xZ.vqLoaCdioLP3sl9fg2cVCE10ruc3Sm2vg8M+QBGXAoAGYQoCmXo2Eb57GCb1BFKbwBGOb0hLg.ECPTkjIzimehPeV+cCI+pSFFbY4.ibS2CLlsMMHi2dFP1kOS3deu6ClSk2O7nezC.Kop4BO4m8fvZ16CCV66QfRqNWXCGXgvlOziAa+HKF14wyC93S73vdN4xfu9zKGNX8q.N5YWIT6EVET2EWMbtqrV3RW8ogqc8mQPT72yG+ouWRg98Rxw+XP8b2pSdO4sUbGaAQ2168gZ8U6q1J+fZcTqa6+9nwuY4V7bBjjM+bNRF9Gt+bxOumhyPov01UsIG7yo6uwV3f1jw72jQZeGXZ4tvBVt+zZ1Mgs9Q0J5SuUzemMqdtBUbncji6gfj68pi8USvfyqkZA0+8icw3yEUHWYEoZhTKMQDq7Y8zxvHsjGf7.jG3+ud.YMR6ZipiQi+9F1y.xwZEbwoIt30DWuzD2MqItdqItDzDWezD2snItD0DWRZhquZh6V0DW+zDW+0DWxBbtdbJgWTkTdCCOh63Flsx+S1HwWx1AbqlSs0TzoV4Oi8oO2bnetYxeP9C55Bpt.UGfpCP0AB49ZpSbuCzuaUpmeqWgGRR.dKiTdfvk3Uh+pyIkGX6AT9CuJmxCn7.m4.pqCTiojIt2HOwcb2sLkGP4AnGfxCn7.JOv6bcvMd+tV9uH76KEGdAzd16u+Gx0+wwehggg47bdXXXXXNOauXmOe97Q1L6DaXXX3YRRRRRZIokjjjjjjz1yjjjjjjjzRRRRRRR8Ueuoqu+W78ytttsGetusq8Cutde89WdFw68q737aGeDMRVCEqmnuQzU5N8lFik4xxI4J3ZX0rFtMtKtOJdDdBdFVOuLuNuMwIDQmnyzE5JaOcicitSOnmzK5M8gig9RiAxPYjLVlHSkYxbY9rPVJKmUvJYUjbobYb4bEbkbUb0bMbsbcb8rZtAtQtIVC2L2B2J2F2N2A2I2E2M2C2K2G2OO.qkhGjGhGlGgGkGiGmmfmjmhmlmgmkmimm0yKvKxKwKyqvqxqwqyavaxaway6v+l2kneQrArgrQzI1X1D1T5LaFaNaAcgsjshsltx1v1x1w1yNvNxNQ2XmYWXWY2X2YOXOo6rWr2rOzC1W1O1e5IG.GHGD8hClCgCkdygwgyQPe3H4n3n4X3X4333oubBzO5OMNQNIF.CjAwfYHLTFFCmQvHYTLZFCikww3YBLQlDSlovTYZLclAyjYwrYNLWNYNElGymSkEvowBYQrXVBKkSmyfkwx4L4r3rYEbNbtbdrRNet.tPVEWDWLWBY+hj38YItTKw62RbYVhOfk3xsDePKwUXI9PVhqzR7gsDWkk3iXItZKwG0RbMVhOlk3ZsDebKw0YI9DVhq2R7IsDq1R7orD2fk3SaItQKwmwRbSVhOqkXMVhOmk3lsDedKwsXI9BVha0R7EsD2lk3KYItcKwW1RbGVhuhk3NsDeUKwcYI9ZVh61R70sD2ik3aXItWKw2zRbeVhukk39sDeaKwCXI9NVh0ZIVmknrDeWKwCZI9dVhGxR78sDOrk3GXIdDKwOzR7nVhejk3wrD+XKwiaI9IVhmvR7SsDOok3mYIdJKwO2R7zVhegk3YrD+RKwyZI9UVhmyR7qsDOuk32XIVuk32ZIdAKwuyR7hVheuk3krD+AKwKaI9iVhWwR7mrDupk3OaIdMKwewR75Vh+pk3MrD+MKwaZI96Vh2xR7OrDusk3eZIdGKw+xR7usD+OVh20R7er3KAiFIEwFnIoH1PMIEwFoIoH5jljhXi0jTDahljhXS0jTDcVSRQrYZRJhMWSRQrEZRJhtnIoH1RMIEwVoIoH1ZMIEQW0jTDailjhXa0jTDamljhX60jTD6fljhXG0jTD6jljhnaZRJhcVSRQrKZRJhcUSRQraZRJhcWSRQrGZRJh8TSRQzcMIEwdoIoH1aMIEw9nIoH5gljhXe0jTD6mljhX+0jTD8TSRQb.ZRJhCTSRQbPZRJhdoIoHNXMIEwgnIoHNTMIEQu0jTDGlljh3v0jTDGgljhnOZRJhiTSRQbTZRJhiVSRQbLZRJhiUSRQbbZRJhiWSRQzWMIEwInIoH5mljhn+ZRJDQijh3D0jTDmjljhX.ZRJhApIoHFjljhXvZRJhgnIoHFpljhXXZRJhgqIoHFgljhXjZRJhQoIoHFsljhXLZRJhwpIoHFmljhX7ZRJhInIoHlnljhXRZRJhIqIoHlhljhXpZRJhooIoHltljhXFZRJhYpIoHlkljhX1ZRJh4nIoHlqljh3j0jTDmhljhXdZRJh4qIoHNUMIEwBzjTDmlljhXgZRJhEoIoHVrljhXIZRJhkpIoHNcMIEwYnIoHVlljhX4ZRJhyTSRQbVZRJhyVSRQrBMIEw4nIoHNWMIEw4oIoHVoljh370jTDWfljh3B0jTDqRSRQbQZRJhKVSRQbIZRp+q04mWm+r04uac92noQPcQZZDTqRSif5B0zHnt.MMBpyWSifZkZZDTmmlFA04poQPcNZZDTqPSif5r0zHnNKMMBpyTSifZ4ZZDTKSSif5LzzHnNcMMBpkpoQPsDMMBpEqoQPsHMMBpEpoQPcZZZDTKPSif5T0zHnlulFA07zzHnNEMMBpSVSifZtZZDTyQSifZ1ZZDTyRSifZlZZDTyPSifZ5ZZDTSSSifZpZZDTSQSifZxZZDTSRSifZhZZDTSPSifZ7ZZDTiSSifZrZZDTiQSifZzZZDTiRSifZjZZDTiPSifZ3ZZDTCSSifZnZZDTCQSifZvZZDTCRSifZfZZDTCPSif5jzzHnNQMMBpllFAU+0zHn5mlFA0InoQP0WMMBpiWSif53zzHnNVMMBpiQSif5n0zHnNJMMBpiTSifpOZZDTGglFA0gqoQPcXZZDT8VSif5P0zHnNDMMBpCVSifpWZZDTGjlFA0ApoQPc.ZZDT8TSifZ+0zHn1OMMBp8USifpGZZDT6ilFA0dqoQPsWZZDTcWSifZO0zHn1CMMBpcWSifZ2zzHn1UMMBpcQSifZm0zHn5llFA0NooQPsiZZDT6flFA01qoQPscZZDTaqlFA01noQP0UMMBpsVSifZqzzHn1RMMBptnoQPsEZZDTatlFA0looQP0YMMBpMUSifZSzzHn1XMMBpNooQPsQZZDTanlFA0FnoQPEZZDu2O+eVaDjuqk7+wR9usj+KK46XI+mVx21R9Orjukk7uaIeSK4eyR9FVx+pk70sj+EK4qYI+yVxW0R9mrjuhk7OZIeYK4evR9RVxeuk7Esj+NK4KXI+sVx0aI+MVxm2R9qsjOmk7WYIeVK4uzR9LVxegk7osj+bK4SYI+YVxmzR9SsjOgk7mXIebK4O1R9XVxejk7Qsj+PK4iXI+AVxG1R98sjOjk76YIePK420RVVx0YIWqk76XIe.K421Rd+Vxukk79rjeSK48ZI+FVx6wR90sj2sk7qYIuKK4W0RdmVxuhk7NrjeYK4saI+RVxayR9Esj2pk7KXIuEK4m2RdyVxOmkbMVxOqk7lrjeFK4MZI+zVxavR9orjq1R9IsjWuk7SXIuNK4G2RdsVxOlk7ZrjeTK4UaI+HVxqxR9gsjWok7CYIuBK4GzRd4VxOfk7xrjueK4kZIeeVxz9dtDtXtHVEWHW.mOqjyiykygUvYyYwYxxYYbFb5rTVBKlEwB4zXAbpLelGmBmLyk4vrYVLSlASmowTYJLYlDSjIv3YbLVFCilQwHYDLbFFCkgvfYPLPF.mDmHM5O8iSf9xwywwwxwvQyQwQRe3H3v4vn2bnbHbvzKNHNPN.5I6O6G6K8f8g8l8htydxdvtytwtxtvNS2XmXGYGX6Y6XaYanqr0rUrkzE1B1b1L5LaJaBaLchMhMjMff2863644c3s4s3M4M30403U4U3k4k3E4EX8777b7r7L7z7T7j7D737X7n7H7v7P7fTrVd.tetOtWtGtatKtStCtctMtUtEtYVC2D2H2.qlqmqiqkqgqlqhqjqfKmKiKkjUwJYErbVJKj4ybYlLUlHikQxPYfznubLzG5M8hdROn6razM1d5JcgNSmH3se.uy7xrddFdBdDJtOtKtMVCqlqgqfjkybYrzn2zc5JAq+a6+GVCIMh26mue6+2u539+cb++Mrecb++A3MXfLHFLCggxvX3LBFIihQyXXrLNFOSfIxjXxLElJSioyLXlLKlMyg4xIyov7X9bpr.NMVHKhEyRXob5bFrLVNmImEmMqfygykyiUx4yEvExp3h3h4RH6WG2+eMdG9bdGni6+6c3A8N78rzw8+8N7hdG98V539+8ui6+uc8ui6+uO8ui6+eD8ui6+OPee.IEwfzjTDCVSRQLDMIEwP0jTDCSSRQLbMIEwHzjTDiTSRQLJMIEwn0jTDiQSRQLVMIEw3zjTDiWSRQLAMIEwD0jTDSRSRQLYMIEwTzjTDSUSRQLMMIEwz0jTDyPSRQLSMIEwrzjTDyVSRQLGMIEwb0jTDmrljh3TzjTDySSRQLeMIEwopIoHVfljh3zzjTDKTSRQrHMIEwh0jTDKQSRQrTMIEwoqIoHNCMIEwxzjTDKWSRQblZRJhyRSRQb1ZRJhUnIoHNGMIEw4pIoHNOMIEwJ0jTDmuljh3BzjTDWnljhXUZRJhKRSRQbwZRJhKQSR8estNt++xVWG2+eNqy2MrNeGgOOPifZVZZDTyTSifZFZZDTSWSifZZZZDTSUSifZJZZDTSVSifZRZZDTSTSifZBZZDTiWSifZbZZDTiUSifZLZZDTiVSifZTZZDTiTSifZDZZDTCWSifZXZZDTCUSifZHZZDTCVSifZPZZDTCTSifZ.ZZDTmjlFA0IpoQP0zzHni6+683P8dPG2+2mG1CuGzw8+84gsx6wV58fNt++Z639+O6Z639+eKeNfNt+u2gU6c3SZoi6+6cXorDVLKhExowB3TY9LONENYlKygYyrXlLClNSioxTXxLIlHSfwy3XrLFFMihQxHX3LLFJCgAyfXfL.NINQZze5Gm.8kimiiikigilihij9vQvgygQu4P4P3foWbPbfb.zS1e1O1W5A6C6M6Ecm8j8fcmcickcgcltwNwNxNv1y1w1x1PWYqYqXKoKrEr4rYzY1T1D1X5DaDaHa.Acb++++78++egJE82epZ...."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1632983935,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5116.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LAv3EP6b0GaUTDD+dEneg7QKRwVD5iZ3qJeXoJD931osDgVjODLh.pnEh.EkhTjBTiMGoQsJ9AXixWQERiJ1pnjFUThhjFCnTDHDCUIHkzX.AM.gnfknO2Yuce8dWesc8eZwdylLc1Yme6r6NybSu2cu7LLL.CQyRxskJyHNeXOKKi.1izZ37oItHzDWmzDWm0DWWzDWjZhKJMwEsl3hQSbwpIttpItaRSbcSSbcWSb8Pgqt.ANMvywHN4Gn7.55.pN.UGfpCP0AT0Aj29IxpCyK3sKQbOkePbXc7GJOv1Y30tNvQJfnKkGP4AnGfxC714Ap5f1dA59CT9CuB2cb2srWwO30Omti6tk859Gux42cb2srWwO30OmFFQva7nO97BBR93MUFQX3AwgyIfqVl7AcRgY9s1PpmqAwomyIlqQ4ATd.kGPWGP0An5.Xc.GMu98u4UO+NRADc8p9Au94lxCr8.TdPnYBdc+gW87qxBTme2xpwIdG65Fti6tko3eG63uJ95Nt6VVgi3cryGbG2cKSw+N1weU70vnUd2FpDi1L9lxa7vhhrAlQccMj2qhbCvGKC0y7Pxsj3DiK6G7c2fSiOlPmrOxrj3rX15CAOeHAdXWS9LlHZdii2B9yg7Pb7h4ZOJ+uuVgCaBn9CWSMJrbQAlvN+vaSz9gPbQgLxMpcaVLjV8WEf0PwoBH0b8w4c3ZlJK8zqRPBCXXI5ii2PwUHrwV2Rh.RHdIYCUr2E9EorS8VHVdC0K5KmqywswyWKkccLGgNb9J+EeOrEQLvFuxN94yAICCte0ULJCwbjyCgvap4YHWK6Q4+EWGbuxFXNwqzo3AA0xcPaaAUVQrb573dwYSnSt9nccRJbRLnNzVs0bwZp1KsDG2atagaL2X9OI650i5YD4NIY7Ojqsb66T9aEOL5CFOQLNvEbb4bZ10gOGarNtFUsNtso678lfSV2yw7zeentFVdMN21941AICAI2e3ZhxAaNdY77wsj3yPfAsoxtxIH0GpMBZLts4qirVoEqw9NP3nKVW0w5qznVCkrStPmpdqp9qS.x9bbhyhSU3bCWqkF2sNobn4CJedmcYc7rgeAH5DmPccgSQxon3TzbJFNQMxCPd.usGXFEr3byOuEtxQl4D8ujUsrzFwpysPusGwac5U231+n5H4BuPD9lfQDqaBFkreN2m4FxZ.l8rjLM2P04ZFcDqyL6T1nYoYsCyCtfcaFcI62L6xOlYoUeFyiV+k458wFR7wvxNk3XKYTIxJMqAv14rFJ6fKHM14JXrrnKIS1vJKG1TKelrmnp4xdopyk8AGOOVM0uB1uck0xhLh0wRH1mkMn3eA1nS5kYSJkMxlSpuN2Vagslw8lb6sC1lmx6vs46y1y79Ptc2Mq1k9Iba+4rqUzWxs+9YIs9ugkZYeKaba6vroT9wXyqxefszp9QVQ68TrWr5yv15g9EVEG+WYewI+c12U+kY+zE9C14uxewZ35+Me88AcOxNAIDaWf928nfAEeLvHRnqvnSpa.q+8.lTJwASeP8Blcp8Fl+H5CrnQkHj+n6KT335GTLKY34xZ.vqLoaCdioLP3sl9fg2cVCE10ruc3Sm2vg8M+QBGXAoAGYQoCmXo2Eb57GCb1BFKbwBGOb0hLg.ECPTkjIzimehPeV+cCI+pSFFbY4.ibS2CLlsMMHi2dFP1kOS3deu6ClSk2O7nezC.Kop4BO4m8fvZ16CCV66QfRqNWXCGXgvlOziAa+HKF14wyC93S73vdN4xfu9zKGNX8q.N5YWIT6EVET2EWMbtqrV3RW8ogqc8mQPT72yG+ouWRg98Rxw+XP8b2pSdO4sUbGaAQ2168gZ8U6q1J+fZcTqa6+9nwuY4V7bBjjM+bNRF9Gt+bxOumhyPov01UsIG7yo6uwV3f1jw72jQZeGXZ4tvBVt+zZ1Mgs9Q0J5SuUzemMqdtBUbncji6gfj68pi8USvfyqkZA0+8icw3yEUHWYEoZhTKMQDq7Y8zxvHsjGf7.jG3+ud.YMR6ZipiQi+9F1y.xwZEbwoIt30DWuzD2MqItdqItDzDWezD2snItD0DWRZhquZh6V0DW+zDW+0DWxBbtdbJgWTkTdCCOh63Flsx+S1HwWx1AbqlSs0TzoV4Oi8oO2bnetYxeP9C55Bpt.UGfpCP0AB49ZpSbuCzuaUpmeqWgGRR.dKiTdfvk3Uh+pyIkGX6AT9CuJmxCn7.m4.pqCTiojIt2HOwcb2sLkGP4AnGfxCn7.JOv6bcvMd+tV9uH76KEGdAzd16u+Gx0+wwehggg47bdXXXXXNOauXmOe97Q1L6DaXXX3YRRRRRZIokjjjjjjz1yjjjjjjjzRRRRRRR8Ueuoqu+W78ytttsGetusq8Cutde89WdFw68q737aGeDMRVCEqmnuQzU5N8lFik4xxI4J3ZX0rFtMtKtOJdDdBdFVOuLuNuMwIDQmnyzE5JaOcicitSOnmzK5M8gig9RiAxPYjLVlHSkYxbY9rPVJKmUvJYUjbobYb4bEbkbUb0bMbsbcb8rZtAtQtIVC2L2B2J2F2N2A2I2E2M2C2K2G2OO.qkhGjGhGlGgGkGiGmmfmjmhmlmgmkmimm0yKvKxKwKyqvqxqwqyavaxaway6v+l2kneQrArgrQzI1X1D1T5LaFaNaAcgsjshsltx1v1x1w1yNvNxNQ2XmYWXWY2X2YOXOo6rWr2rOzC1W1O1e5IG.GHGD8hClCgCkdygwgyQPe3H4n3n4X3X4333oubBzO5OMNQNIF.CjAwfYHLTFFCmQvHYTLZFCikww3YBLQlDSlovTYZLclAyjYwrYNLWNYNElGymSkEvowBYQrXVBKkSmyfkwx4L4r3rYEbNbtbdrRNet.tPVEWDWLWBY+hj38YItTKw62RbYVhOfk3xsDePKwUXI9PVhqzR7gsDWkk3iXItZKwG0RbMVhOlk3ZsDebKw0YI9DVhq2R7IsDq1R7orD2fk3SaItQKwmwRbSVhOqkXMVhOmk3lsDedKwsXI9BVha0R7EsD2lk3KYItcKwW1RbGVhuhk3NsDeUKwcYI9ZVh61R70sD2ik3aXItWKw2zRbeVhukk39sDeaKwCXI9NVh0ZIVmknrDeWKwCZI9dVhGxR78sDOrk3GXIdDKwOzR7nVhejk3wrD+XKwiaI9IVhmvR7SsDOok3mYIdJKwO2R7zVhegk3YrD+RKwyZI9UVhmyR7qsDOuk32XIVuk32ZIdAKwuyR7hVheuk3krD+AKwKaI9iVhWwR7mrDupk3OaIdMKwewR75Vh+pk3MrD+MKwaZI96Vh2xR7OrDusk3eZIdGKw+xR7usD+OVh20R7er3KAiFIEwFnIoH1PMIEwFoIoH5jljhXi0jTDahljhXS0jTDcVSRQrYZRJhMWSRQrEZRJhtnIoH1RMIEwVoIoH1ZMIEQW0jTDailjhXa0jTDamljhX60jTD6fljhXG0jTD6jljhnaZRJhcVSRQrKZRJhcUSRQraZRJhcWSRQrGZRJh8TSRQzcMIEwdoIoH1aMIEw9nIoH5gljhXe0jTD6mljhX+0jTD8TSRQb.ZRJhCTSRQbPZRJhdoIoHNXMIEwgnIoHNTMIEQu0jTDGlljh3v0jTDGgljhnOZRJhiTSRQbTZRJhiVSRQbLZRJhiUSRQbbZRJhiWSRQzWMIEwInIoH5mljhn+ZRJDQijh3D0jTDmjljhX.ZRJhApIoHFjljhXvZRJhgnIoHFpljhXXZRJhgqIoHFgljhXjZRJhQoIoHFsljhXLZRJhwpIoHFmljhX7ZRJhInIoHlnljhXRZRJhIqIoHlhljhXpZRJhooIoHltljhXFZRJhYpIoHlkljhX1ZRJh4nIoHlqljh3j0jTDmhljhXdZRJh4qIoHNUMIEwBzjTDmlljhXgZRJhEoIoHVrljhXIZRJhkpIoHNcMIEwYnIoHVlljhX4ZRJhyTSRQbVZRJhyVSRQrBMIEw4nIoHNWMIEw4oIoHVoljh370jTDWfljh3B0jTDqRSRQbQZRJhKVSRQbIZRp+q04mWm+r04uac92noQPcQZZDTqRSif5B0zHnt.MMBpyWSifZkZZDTmmlFA04poQPcNZZDTqPSif5r0zHnNKMMBpyTSifZ4ZZDTKSSif5LzzHnNcMMBpkpoQPsDMMBpEqoQPsHMMBpEpoQPcZZZDTKPSif5T0zHnlulFA07zzHnNEMMBpSVSifZtZZDTyQSifZ1ZZDTyRSifZlZZDTyPSifZ5ZZDTSSSifZpZZDTSQSifZxZZDTSRSifZhZZDTSPSifZ7ZZDTiSSifZrZZDTiQSifZzZZDTiRSifZjZZDTiPSifZ3ZZDTCSSifZnZZDTCQSifZvZZDTCRSifZfZZDTCPSif5jzzHnNQMMBpllFAU+0zHn5mlFA0InoQP0WMMBpiWSif53zzHnNVMMBpiQSif5n0zHnNJMMBpiTSifpOZZDTGglFA0gqoQPcXZZDT8VSif5P0zHnNDMMBpCVSifpWZZDTGjlFA0ApoQPc.ZZDT8TSifZ+0zHn1OMMBp8USifpGZZDT6ilFA0dqoQPsWZZDTcWSifZO0zHn1CMMBpcWSifZ2zzHn1UMMBpcQSifZm0zHn5llFA0NooQPsiZZDT6flFA01qoQPscZZDTaqlFA01noQP0UMMBpsVSifZqzzHn1RMMBptnoQPsEZZDTatlFA0looQP0YMMBpMUSifZSzzHn1XMMBpNooQPsQZZDTanlFA0FnoQPEZZDu2O+eVaDjuqk7+wR9usj+KK46XI+mVx21R9Orjukk7uaIeSK4eyR9FVx+pk70sj+EK4qYI+yVxW0R9mrjuhk7OZIeYK4evR9RVxeuk7Esj+NK4KXI+sVx0aI+MVxm2R9qsjOmk7WYIeVK4uzR9LVxegk7osj+bK4SYI+YVxmzR9SsjOgk7mXIebK4O1R9XVxejk7Qsj+PK4iXI+AVxG1R98sjOjk76YIePK420RVVx0YIWqk76XIe.K421Rd+Vxukk79rjeSK48ZI+FVx6wR90sj2sk7qYIuKK4W0RdmVxuhk7NrjeYK4saI+RVxayR9Esj2pk7KXIuEK4m2RdyVxOmkbMVxOqk7lrjeFK4MZI+zVxavR9orjq1R9IsjWuk7SXIuNK4G2RdsVxOlk7ZrjeTK4UaI+HVxqxR9gsjWok7CYIuBK4GzRd4VxOfk7xrjueK4kZIeeVxz9dtDtXtHVEWHW.mOqjyiykygUvYyYwYxxYYbFb5rTVBKlEwB4zXAbpLelGmBmLyk4vrYVLSlASmowTYJLYlDSjIv3YbLVFCilQwHYDLbFFCkgvfYPLPF.mDmHM5O8iSf9xwywwwxwvQyQwQRe3H3v4vn2bnbHbvzKNHNPN.5I6O6G6K8f8g8l8htydxdvtytwtxtvNS2XmXGYGX6Y6XaYanqr0rUrkzE1B1b1L5LaJaBaLchMhMjMff2863644c3s4s3M4M30403U4U3k4k3E4EX8777b7r7L7z7T7j7D737X7n7H7v7P7fTrVd.tetOtWtGtatKtStCtctMtUtEtYVC2D2H2.qlqmqiqkqgqlqhqjqfKmKiKkjUwJYErbVJKj4ybYlLUlHikQxPYfznubLzG5M8hdROn6razM1d5JcgNSmH3se.uy7xrddFdBdDJtOtKtMVCqlqgqfjkybYrzn2zc5JAq+a6+GVCIMh26mue6+2u539+cb++Mrecb++A3MXfLHFLCggxvX3LBFIihQyXXrLNFOSfIxjXxLElJSioyLXlLKlMyg4xIyov7X9bpr.NMVHKhEyRXob5bFrLVNmImEmMqfygykyiUx4yEvExp3h3h4RH6WG2+eMdG9bdGni6+6c3A8N78rzw8+8N7hdG98V539+8ui6+uc8ui6+uO8ui6+eD8ui6+OPee.IEwfzjTDCVSRQLDMIEwP0jTDCSSRQLbMIEwHzjTDiTSRQLJMIEwn0jTDiQSRQLVMIEw3zjTDiWSRQLAMIEwD0jTDSRSRQLYMIEwTzjTDSUSRQLMMIEwz0jTDyPSRQLSMIEwrzjTDyVSRQLGMIEwb0jTDmrljh3TzjTDySSRQLeMIEwopIoHVfljh3zzjTDKTSRQrHMIEwh0jTDKQSRQrTMIEwoqIoHNCMIEwxzjTDKWSRQblZRJhyRSRQb1ZRJhUnIoHNGMIEw4pIoHNOMIEwJ0jTDmuljh3BzjTDWnljhXUZRJhKRSRQbwZRJhKQSR8estNt++xVWG2+eNqy2MrNeGgOOPifZVZZDTyTSifZFZZDTSWSifZZZZDTSUSifZJZZDTSVSifZRZZDTSTSifZBZZDTiWSifZbZZDTiUSifZLZZDTiVSifZTZZDTiTSifZDZZDTCWSifZXZZDTCUSifZHZZDTCVSifZPZZDTCTSifZ.ZZDTmjlFA0IpoQP0zzHni6+683P8dPG2+2mG1CuGzw8+84gsx6wV58fNt++Z639+O6Z639+eKeNfNt+u2gU6c3SZoi6+6cXorDVLKhExowB3TY9LONENYlKygYyrXlLClNSioxTXxLIlHSfwy3XrLFFMihQxHX3LLFJCgAyfXfL.NINQZze5Gm.8kimiiikigilihij9vQvgygQu4P4P3foWbPbfb.zS1e1O1W5A6C6M6Ecm8j8fcmcickcgcltwNwNxNv1y1w1x1PWYqYqXKoKrEr4rYzY1T1D1X5DaDaHa.Acb++++78++egJE82epZ...."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[4]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 2479.715605305816553, 1335.705923557281494, 302.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[21]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5119.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................Lwv3EP6b0GaUTDD+dOf9EBkVjhsHzG0vWUfZoJDPtcZKQnE4CAiHfJZgHPQoHEo.0PygMpUwO.aT4inBoQEaUTRiJJQQRiATJBDhgpDjRZLffF.IJHD84t6s606s801M7GsXuYSlN6tyuc1YmY1o265K0vv.LLrDjAuEK8moYLLdeKKif1yxvwZLrM1J2HNerQVV9zDmeMw0IMw0YMw0EMwEgl3hTSbQoItn0DWLZhqqZh6FzDW2zDW20DWrRb0GL3I.ZJDxQ+.lGf2Cv5.Xc.rN.VGPVGnwG1zndVdAscdj6o7C7Cqqef4A1NCu18.Wo.7tXd.lGv7.Xdf2NOPVGz1KfOefze3U3pwc0wdE+fW+bpF2UG608OdkyuZbWcrWwO30OmFF9oMZzm89BbHezlLiHLbGbr0DTokEcR2TXVeqMk78Zfb78bxx0v7.LO.yCv6AXc.rN.qNfqlW+427pmeWo.7tdU+fW+bi4A1d.LOHzLAut+vqd9kYAxyu5X47HuiccC03t5XL92wN9Kiupwc0wRbHuic9fZbWcLF+6XG+kwWCiV4usgLwnMiuzUOG56zHS160PkX1fTlnOiYIvcsrFKhsNc1KlBoMacs8IbRS6wL4VveM3Gjh2hseNsWsngN1+XSkQNPs0JwRkww.pqugheZh5b15jo+PHl9c1m51rEgQq3qBRtRIoBLp45yV2ApcRjLxnZNwTD0d38YyekRpjqiMswDAFQEJINRaam6WDicK2RXSL479h05dda7z8RpWWqgKiYOTaj6un1vF4w.a7R8DftFFYXPwoDixjuFw5D1nbcFh8RLM2WvVuEY.4FuTlj6.pk6vzsETUkwPoyvrE2MtrP8Ebav0dXud5hDXaq4r82cScriLEb74C2bNK3ZoixedTOyPpuRD+C4tkpKT5uk7vH2I9wv3Bmy7h0zr6CcM1XccGUtOp5TMeuI3j2iazVz2Nj2gE2wopH.c+YjAmD1GaOYicZt9iwSm2RfOSNFlNk5Ur.g7P0gixn5ltOhZkVz53x9tP3pKqtpq8WJQtGxwt4bYx5sx5utAH5SwwOKtEwVa3Zsz7pxDiCMeP5y6rh1YmM1W.hNQIlrtPoHnTjTJJJEMkvF5APOf21CL0BWPdEj+7VVZYMt.Kb4KN8guh7Jxa6Q7Vmd4Ct8uxNBN2K322XM7ulwZT5dnbelqK69a1iRyxbc0jmYT9WiYNordyxxdql6at6vLpR2iYNUbXyxp4jlGpgKPk6iL33iljSJwQV3HRjTV18mrsoODx9la5jSW3nIQUZVjgVdtjIUwzHOd0yh7h0jG4CNR9jZaXoje6hqhDg+0PRHlmgLv3edxHS5kHiOk0SlYpuFUWajrxw7lT8sUxFl36P046S14r+Ppd2AotE8ITc+4jKW7WR0+dHIs1ugjZ4eKYLa9.jIVwgIytpefrnp+QRw653jWnlSR1z9+ERkG4WIeww9cx20vEH+zY+SxYt3eStxU+G596C5dDcBRHlt.8q6QBCL9nggmPWgQlT2.R+hEFeJwASYf8DlQp8Blyv6ML+QjHTvH6CTzX5KTBIY3Yyt+vKO9aAd8IN.3slxff2c5CA19LtU3Sm8vfcOmzf8N2zgCN+Lfitn6.NQAiBNUgiFNWQ2IbohMgfk.PjklED6yMNn2q8tfjekI.Cp7bgzdi6FF0lmLj4aOUHmJlFbOu28Byrp6CdjO59gEV8rfm3yd.Xk65g.qc+vPY0jGrt8NOXC6+QgsbvE.a6H4Ce7QeLXmGawvWehk.6qgkBG5TKCp6rKGp+bq.N8EWEb9K8Tvku5p4DF+87we76kTneujb8KFju2s5EOSdaE2kIv61daGx8WZWsU9A49H221e6nwuY4VzbBFIZAnbFYDXXAxsf7eRJiMJbssWWxNeN8.M1BGzlLWflLS66DSNu4U3RBjdyZD1xGQqHOiVQ9s2rxoBjwg1QNyFbHUa0kc0DLr00RMG4e+nW.68hxGWUkoZxnVZgLrh20SKCCkhd.zCfdf++5AD0HsqMJOFM9+svdDTLWqfKNMwEul35ol3tQMw0KMwkfl35sl3tIMwknl3RRSb8QSb2rl35ql35ml3RliS40oD9gxjxqa39usqaLk+mXHwW5V.lolac0V7wW1Oy5iet4P+byn+.8G38Brt.VG.qCf0AB44Zpm+rC3+2pju+VuBOjj.1iLh4AbWhWI9KOmXdfsGP5O7pbLO.yCbmCHuGHmSNF4di7D03t5XLO.yCXd.LO.yCv7.uy8fq+9+Z4+A393M4JdAzd16u+Gx0+wwehggg47bdXXXXXNOauXmOe97Q1L6DaXXX3YRRRRRZIokjjjjjjz1yjjjjjjjzRRRRRRR8Ueuoqu+W78ytttsGetusq8Cutde89WdFw68q737aGeDMRVCEqmnuQzU5N8lFik4xxI4J3ZX0rFtMtKtOJdDdBdFVOuLuNuMwIDQmnyzE5JaOcicitSOnmzK5M8gig9RiAxPYjLVlHSkYxbY9rPVJKmUvJYUjbobYb4bEbkbUb0bMbsbcb8rZtAtQtIVC2L2B2J2F2N2A2I2E2M2C2K2G2OO.qkhGjGhGlGgGkGiGmmfmjmhmlmgmkmimm0yKvKxKwKyqvqxqwqyavaxaway6v+l2kneQrArgrQzI1X1D1T5LaFaNaAcgsjshsltx1v1x1w1yNvNxNQ2XmYWXWY2X2YOXOo6rWr2rOzC1W1O1e5IG.GHGD8hClCgCkdygwgyQPe3H4n3n4X3X4333oubBzO5OMNQNIF.CjAwfYHLTFFCmQvHYTLZFCikww3YBLQlDSlovTYZLclAyjYwrYNLWNYNElGymSkEvowBYQrXVBKkSmyfkwx4L4r3rYEbNbtbdrRNet.tPVEWDWLWBY+hj38YItTKw62RbYVhOfk3xsDePKwUXI9PVhqzR7gsDWkk3iXItZKwG0RbMVhOlk3ZsDebKw0YI9DVhq2R7IsDq1R7orD2fk3SaItQKwmwRbSVhOqkXMVhOmk3lsDedKwsXI9BVha0R7EsD2lk3KYItcKwW1RbGVhuhk3NsDeUKwcYI9ZVh61R70sD2ik3aXItWKw2zRbeVhukk39sDeaKwCXI9NVh0ZIVmknrDeWKwCZI9dVhGxR78sDOrk3GXIdDKwOzR7nVhejk3wrD+XKwiaI9IVhmvR7SsDOok3mYIdJKwO2R7zVhegk3YrD+RKwyZI9UVhmyR7qsDOuk32XIVuk32ZIdAKwuyR7hVheuk3krD+AKwKaI9iVhWwR7mrDupk3OaIdMKwewR75Vh+pk3MrD+MKwaZI96Vh2xR7OrDusk3eZIdGKw+xR7usD+OVh20R7er3KAiFIEwFnIoH1PMIEwFoIoH5jljhXi0jTDahljhXS0jTDcVSRQrYZRJhMWSRQrEZRJhtnIoH1RMIEwVoIoH1ZMIEQW0jTDailjhXa0jTDamljhX60jTD6fljhXG0jTD6jljhnaZRJhcVSRQrKZRJhcUSRQraZRJhcWSRQrGZRJh8TSRQzcMIEwdoIoH1aMIEw9nIoH5gljhXe0jTD6mljhX+0jTD8TSRQb.ZRJhCTSRQbPZRJhdoIoHNXMIEwgnIoHNTMIEQu0jTDGlljh3v0jTDGgljhnOZRJhiTSRQbTZRJhiVSRQbLZRJhiUSRQbbZRJhiWSRQzWMIEwInIoH5mljhn+ZRJDQijh3D0jTDmjljhX.ZRJhApIoHFjljhXvZRJhgnIoHFpljhXXZRJhgqIoHFgljhXjZRJhQoIoHFsljhXLZRJhwpIoHFmljhX7ZRJhInIoHlnljhXRZRJhIqIoHlhljhXpZRJhooIoHltljhXFZRJhYpIoHlkljhX1ZRJh4nIoHlqljh3j0jTDmhljhXdZRJh4qIoHNUMIEwBzjTDmlljhXgZRJhEoIoHVrljhXIZRJhkpIoHNcMIEwYnIoHVlljhX4ZRJhyTSRQbVZRJhyVSRQrBMIEw4nIoHNWMIEw4oIoHVoljh370jTDWfljh3B0jTDqRSRQbQZRJhKVSRQbIZRp+q04mWm+r04uac92noQPcQZZDTqRSif5B0zHnt.MMBpyWSifZkZZDTmmlFA04poQPcNZZDTqPSif5r0zHnNKMMBpyTSifZ4ZZDTKSSif5LzzHnNcMMBpkpoQPsDMMBpEqoQPsHMMBpEpoQPcZZZDTKPSif5T0zHnlulFA07zzHnNEMMBpSVSifZtZZDTyQSifZ1ZZDTyRSifZlZZDTyPSifZ5ZZDTSSSifZpZZDTSQSifZxZZDTSRSifZhZZDTSPSifZ7ZZDTiSSifZrZZDTiQSifZzZZDTiRSifZjZZDTiPSifZ3ZZDTCSSifZnZZDTCQSifZvZZDTCRSifZfZZDTCPSif5jzzHnNQMMBpllFAU+0zHn5mlFA0InoQP0WMMBpiWSif53zzHnNVMMBpiQSif5n0zHnNJMMBpiTSifpOZZDTGglFA0gqoQPcXZZDT8VSif5P0zHnNDMMBpCVSifpWZZDTGjlFA0ApoQPc.ZZDT8TSifZ+0zHn1OMMBp8USifpGZZDT6ilFA0dqoQPsWZZDTcWSifZO0zHn1CMMBpcWSifZ2zzHn1UMMBpcQSifZm0zHn5llFA0NooQPsiZZDT6flFA01qoQPscZZDTaqlFA01noQP0UMMBpsVSifZqzzHn1RMMBptnoQPsEZZDTatlFA0looQP0YMMBpMUSifZSzzHn1XMMBpNooQPsQZZDTanlFA0FnoQPEZZDu2O+eVaDjuqk7+wR9usj+KK46XI+mVx21R9Orjukk7uaIeSK4eyR9FVx+pk70sj+EK4qYI+yVxW0R9mrjuhk7OZIeYK4evR9RVxeuk7Esj+NK4KXI+sVx0aI+MVxm2R9qsjOmk7WYIeVK4uzR9LVxegk7osj+bK4SYI+YVxmzR9SsjOgk7mXIebK4O1R9XVxejk7Qsj+PK4iXI+AVxG1R98sjOjk76YIePK420RVVx0YIWqk76XIe.K421Rd+Vxukk79rjeSK48ZI+FVx6wR90sj2sk7qYIuKK4W0RdmVxuhk7NrjeYK4saI+RVxayR9Esj2pk7KXIuEK4m2RdyVxOmkbMVxOqk7lrjeFK4MZI+zVxavR9orjq1R9IsjWuk7SXIuNK4G2RdsVxOlk7ZrjeTK4UaI+HVxqxR9gsjWok7CYIuBK4GzRd4VxOfk7xrjueK4kZIeeVxz9dtDtXtHVEWHW.mOqjyiykygUvYyYwYxxYYbFb5rTVBKlEwB4zXAbpLelGmBmLyk4vrYVLSlASmowTYJLYlDSjIv3YbLVFCilQwHYDLbFFCkgvfYPLPF.mDmHM5O8iSf9xwywwwxwvQyQwQRe3H3v4vn2bnbHbvzKNHNPN.5I6O6G6K8f8g8l8htydxdvtytwtxtvNS2XmXGYGX6Y6XaYanqr0rUrkzE1B1b1L5LaJaBaLchMhMjMff2863644c3s4s3M4M30403U4U3k4k3E4EX8777b7r7L7z7T7j7D737X7n7H7v7P7fTrVd.tetOtWtGtatKtStCtctMtUtEtYVC2D2H2.qlqmqiqkqgqlqhqjqfKmKiKkjUwJYErbVJKj4ybYlLUlHikQxPYfznubLzG5M8hdROn6razM1d5JcgNSmH3se.uy7xrddFdBdDJtOtKtMVCqlqgqfjkybYrzn2zc5JAq+a6+GVCIMh26mue6+2u539+cb++Mrecb++A3MXfLHFLCggxvX3LBFIihQyXXrLNFOSfIxjXxLElJSioyLXlLKlMyg4xIyov7X9bpr.NMVHKhEyRXob5bFrLVNmImEmMqfygykyiUx4yEvExp3h3h4RH6WG2+eMdG9bdGni6+6c3A8N78rzw8+8N7hdG98V539+8ui6+uc8ui6+uO8ui6+eD8ui6+OPee.IEwfzjTDCVSRQLDMIEwP0jTDCSSRQLbMIEwHzjTDiTSRQLJMIEwn0jTDiQSRQLVMIEw3zjTDiWSRQLAMIEwD0jTDSRSRQLYMIEwTzjTDSUSRQLMMIEwz0jTDyPSRQLSMIEwrzjTDyVSRQLGMIEwb0jTDmrljh3TzjTDySSRQLeMIEwopIoHVfljh3zzjTDKTSRQrHMIEwh0jTDKQSRQrTMIEwoqIoHNCMIEwxzjTDKWSRQblZRJhyRSRQb1ZRJhUnIoHNGMIEw4pIoHNOMIEwJ0jTDmuljh3BzjTDWnljhXUZRJhKRSRQbwZRJhKQSR8estNt++xVWG2+eNqy2MrNeGgOOPifZVZZDTyTSifZFZZDTSWSifZZZZDTSUSifZJZZDTSVSifZRZZDTSTSifZBZZDTiWSifZbZZDTiUSifZLZZDTiVSifZTZZDTiTSifZDZZDTCWSifZXZZDTCUSifZHZZDTCVSifZPZZDTCTSifZ.ZZDTmjlFA0IpoQP0zzHni6+683P8dPG2+2mG1CuGzw8+84gsx6wV58fNt++Z639+O6Z639+eKeNfNt+u2gU6c3SZoi6+6cXorDVLKhExowB3TY9LONENYlKygYyrXlLClNSioxTXxLIlHSfwy3XrLFFMihQxHX3LLFJCgAyfXfL.NINQZze5Gm.8kimiiikigilihij9vQvgygQu4P4P3foWbPbfb.zS1e1O1W5A6C6M6Ecm8j8fcmcickcgcltwNwNxNv1y1w1x1PWYqYqXKoKrEr4rYzY1T1D1X5DaDaHa.Acb++++78++egJE82esZ...."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5119.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................Lwv3EP6b0GaUTDD+dOf9EBkVjhsHzG0vWUfZoJDPtcZKQnE4CAiHfJZgHPQoHEo.0PygMpUwO.aT4inBoQEaUTRiJJQQRiATJBDhgpDjRZLffF.IJHD84t6s606s801M7GsXuYSlN6tyuc1YmY1o265K0vv.LLrDjAuEK8moYLLdeKKif1yxvwZLrM1J2HNerQVV9zDmeMw0IMw0YMw0EMwEgl3hTSbQoItn0DWLZhqqZh6FzDW2zDW20DWrRb0GL3I.ZJDxQ+.lGf2Cv5.Xc.rN.VGPVGnwG1zndVdAscdj6o7C7Cqqef4A1NCu18.Wo.7tXd.lGv7.Xdf2NOPVGz1KfOefze3U3pwc0wdE+fW+bpF2UG608OdkyuZbWcrWwO30OmFF9oMZzm89BbHezlLiHLbGbr0DTokEcR2TXVeqMk78Zfb78bxx0v7.LO.yCv6AXc.rN.qNfqlW+427pmeWo.7tdU+fW+bi4A1d.LOHzLAut+vqd9kYAxyu5X47HuiccC03t5XL92wN9Kiupwc0wRbHuic9fZbWcLF+6XG+kwWCiV4usgLwnMiuzUOG56zHS160PkX1fTlnOiYIvcsrFKhsNc1KlBoMacs8IbRS6wL4VveM3Gjh2hseNsWsngN1+XSkQNPs0JwRkww.pqugheZh5b15jo+PHl9c1m51rEgQq3qBRtRIoBLp45yV2ApcRjLxnZNwTD0d38YyekRpjqiMswDAFQEJINRaam6WDicK2RXSL479h05dda7z8RpWWqgKiYOTaj6un1vF4w.a7R8DftFFYXPwoDixjuFw5D1nbcFh8RLM2WvVuEY.4FuTlj6.pk6vzsETUkwPoyvrE2MtrP8Ebav0dXud5hDXaq4r82cScriLEb74C2bNK3ZoixedTOyPpuRD+C4tkpKT5uk7vH2I9wv3Bmy7h0zr6CcM1XccGUtOp5TMeuI3j2iazVz2Nj2gE2wopH.c+YjAmD1GaOYicZt9iwSm2RfOSNFlNk5Ur.g7P0gixn5ltOhZkVz53x9tP3pKqtpq8WJQtGxwt4bYx5sx5utAH5SwwOKtEwVa3Zsz7pxDiCMeP5y6rh1YmM1W.hNQIlrtPoHnTjTJJJEMkvF5APOf21CL0BWPdEj+7VVZYMt.Kb4KN8guh7Jxa6Q7Vmd4Ct8uxNBN2K322XM7ulwZT5dnbelqK69a1iRyxbc0jmYT9WiYNordyxxdql6at6vLpR2iYNUbXyxp4jlGpgKPk6iL33iljSJwQV3HRjTV18mrsoODx9la5jSW3nIQUZVjgVdtjIUwzHOd0yh7h0jG4CNR9jZaXoje6hqhDg+0PRHlmgLv3edxHS5kHiOk0SlYpuFUWajrxw7lT8sUxFl36P046S14r+Ppd2AotE8ITc+4jKW7WR0+dHIs1ugjZ4eKYLa9.jIVwgIytpefrnp+QRw653jWnlSR1z9+ERkG4WIeww9cx20vEH+zY+SxYt3eStxU+G596C5dDcBRHlt.8q6QBCL9nggmPWgQlT2.R+hEFeJwASYf8DlQp8Blyv6ML+QjHTvH6CTzX5KTBIY3Yyt+vKO9aAd8IN.3slxff2c5CA19LtU3Sm8vfcOmzf8N2zgCN+Lfitn6.NQAiBNUgiFNWQ2IbohMgfk.PjklED6yMNn2q8tfjekI.Cp7bgzdi6FF0lmLj4aOUHmJlFbOu28Byrp6CdjO59gEV8rfm3yd.Xk65g.qc+vPY0jGrt8NOXC6+QgsbvE.a6H4Ce7QeLXmGawvWehk.6qgkBG5TKCp6rKGp+bq.N8EWEb9K8Tvku5p4DF+87we76kTneujb8KFju2s5EOSdaE2kIv61daGx8WZWsU9A49H221e6nwuY4VzbBFIZAnbFYDXXAxsf7eRJiMJbssWWxNeN8.M1BGzlLWflLS66DSNu4U3RBjdyZD1xGQqHOiVQ9s2rxoBjwg1QNyFbHUa0kc0DLr00RMG4e+nW.68hxGWUkoZxnVZgLrh20SKCCkhd.zCfdf++5AD0HsqMJOFM9+svdDTLWqfKNMwEul35ol3tQMw0KMwkfl35sl3tIMwknl3RRSb8QSb2rl35ql35ml3RliS40oD9gxjxqa39usqaLk+mXHwW5V.lolac0V7wW1Oy5iet4P+byn+.8G38Brt.VG.qCf0AB44Zpm+rC3+2pju+VuBOjj.1iLh4AbWhWI9KOmXdfsGP5O7pbLO.yCbmCHuGHmSNF4di7D03t5XLO.yCXd.LO.yCv7.uy8fq+9+Z4+A393M4JdAzd16u+Gx0+wwehggg47bdXXXXXNOauXmOe97Q1L6DaXXX3YRRRRRZIokjjjjjjz1yjjjjjjjzRRRRRRR8Ueuoqu+W78ytttsGetusq8Cutde89WdFw68q737aGeDMRVCEqmnuQzU5N8lFik4xxI4J3ZX0rFtMtKtOJdDdBdFVOuLuNuMwIDQmnyzE5JaOcicitSOnmzK5M8gig9RiAxPYjLVlHSkYxbY9rPVJKmUvJYUjbobYb4bEbkbUb0bMbsbcb8rZtAtQtIVC2L2B2J2F2N2A2I2E2M2C2K2G2OO.qkhGjGhGlGgGkGiGmmfmjmhmlmgmkmimm0yKvKxKwKyqvqxqwqyavaxaway6v+l2kneQrArgrQzI1X1D1T5LaFaNaAcgsjshsltx1v1x1w1yNvNxNQ2XmYWXWY2X2YOXOo6rWr2rOzC1W1O1e5IG.GHGD8hClCgCkdygwgyQPe3H4n3n4X3X4333oubBzO5OMNQNIF.CjAwfYHLTFFCmQvHYTLZFCikww3YBLQlDSlovTYZLclAyjYwrYNLWNYNElGymSkEvowBYQrXVBKkSmyfkwx4L4r3rYEbNbtbdrRNet.tPVEWDWLWBY+hj38YItTKw62RbYVhOfk3xsDePKwUXI9PVhqzR7gsDWkk3iXItZKwG0RbMVhOlk3ZsDebKw0YI9DVhq2R7IsDq1R7orD2fk3SaItQKwmwRbSVhOqkXMVhOmk3lsDedKwsXI9BVha0R7EsD2lk3KYItcKwW1RbGVhuhk3NsDeUKwcYI9ZVh61R70sD2ik3aXItWKw2zRbeVhukk39sDeaKwCXI9NVh0ZIVmknrDeWKwCZI9dVhGxR78sDOrk3GXIdDKwOzR7nVhejk3wrD+XKwiaI9IVhmvR7SsDOok3mYIdJKwO2R7zVhegk3YrD+RKwyZI9UVhmyR7qsDOuk32XIVuk32ZIdAKwuyR7hVheuk3krD+AKwKaI9iVhWwR7mrDupk3OaIdMKwewR75Vh+pk3MrD+MKwaZI96Vh2xR7OrDusk3eZIdGKw+xR7usD+OVh20R7er3KAiFIEwFnIoH1PMIEwFoIoH5jljhXi0jTDahljhXS0jTDcVSRQrYZRJhMWSRQrEZRJhtnIoH1RMIEwVoIoH1ZMIEQW0jTDailjhXa0jTDamljhX60jTD6fljhXG0jTD6jljhnaZRJhcVSRQrKZRJhcUSRQraZRJhcWSRQrGZRJh8TSRQzcMIEwdoIoH1aMIEw9nIoH5gljhXe0jTD6mljhX+0jTD8TSRQb.ZRJhCTSRQbPZRJhdoIoHNXMIEwgnIoHNTMIEQu0jTDGlljh3v0jTDGgljhnOZRJhiTSRQbTZRJhiVSRQbLZRJhiUSRQbbZRJhiWSRQzWMIEwInIoH5mljhn+ZRJDQijh3D0jTDmjljhX.ZRJhApIoHFjljhXvZRJhgnIoHFpljhXXZRJhgqIoHFgljhXjZRJhQoIoHFsljhXLZRJhwpIoHFmljhX7ZRJhInIoHlnljhXRZRJhIqIoHlhljhXpZRJhooIoHltljhXFZRJhYpIoHlkljhX1ZRJh4nIoHlqljh3j0jTDmhljhXdZRJh4qIoHNUMIEwBzjTDmlljhXgZRJhEoIoHVrljhXIZRJhkpIoHNcMIEwYnIoHVlljhX4ZRJhyTSRQbVZRJhyVSRQrBMIEw4nIoHNWMIEw4oIoHVoljh370jTDWfljh3B0jTDqRSRQbQZRJhKVSRQbIZRp+q04mWm+r04uac92noQPcQZZDTqRSif5B0zHnt.MMBpyWSifZkZZDTmmlFA04poQPcNZZDTqPSif5r0zHnNKMMBpyTSifZ4ZZDTKSSif5LzzHnNcMMBpkpoQPsDMMBpEqoQPsHMMBpEpoQPcZZZDTKPSif5T0zHnlulFA07zzHnNEMMBpSVSifZtZZDTyQSifZ1ZZDTyRSifZlZZDTyPSifZ5ZZDTSSSifZpZZDTSQSifZxZZDTSRSifZhZZDTSPSifZ7ZZDTiSSifZrZZDTiQSifZzZZDTiRSifZjZZDTiPSifZ3ZZDTCSSifZnZZDTCQSifZvZZDTCRSifZfZZDTCPSif5jzzHnNQMMBpllFAU+0zHn5mlFA0InoQP0WMMBpiWSif53zzHnNVMMBpiQSif5n0zHnNJMMBpiTSifpOZZDTGglFA0gqoQPcXZZDT8VSif5P0zHnNDMMBpCVSifpWZZDTGjlFA0ApoQPc.ZZDT8TSifZ+0zHn1OMMBp8USifpGZZDT6ilFA0dqoQPsWZZDTcWSifZO0zHn1CMMBpcWSifZ2zzHn1UMMBpcQSifZm0zHn5llFA0NooQPsiZZDT6flFA01qoQPscZZDTaqlFA01noQP0UMMBpsVSifZqzzHn1RMMBptnoQPsEZZDTatlFA0looQP0YMMBpMUSifZSzzHn1XMMBpNooQPsQZZDTanlFA0FnoQPEZZDu2O+eVaDjuqk7+wR9usj+KK46XI+mVx21R9Orjukk7uaIeSK4eyR9FVx+pk70sj+EK4qYI+yVxW0R9mrjuhk7OZIeYK4evR9RVxeuk7Esj+NK4KXI+sVx0aI+MVxm2R9qsjOmk7WYIeVK4uzR9LVxegk7osj+bK4SYI+YVxmzR9SsjOgk7mXIebK4O1R9XVxejk7Qsj+PK4iXI+AVxG1R98sjOjk76YIePK420RVVx0YIWqk76XIe.K421Rd+Vxukk79rjeSK48ZI+FVx6wR90sj2sk7qYIuKK4W0RdmVxuhk7NrjeYK4saI+RVxayR9Esj2pk7KXIuEK4m2RdyVxOmkbMVxOqk7lrjeFK4MZI+zVxavR9orjq1R9IsjWuk7SXIuNK4G2RdsVxOlk7ZrjeTK4UaI+HVxqxR9gsjWok7CYIuBK4GzRd4VxOfk7xrjueK4kZIeeVxz9dtDtXtHVEWHW.mOqjyiykygUvYyYwYxxYYbFb5rTVBKlEwB4zXAbpLelGmBmLyk4vrYVLSlASmowTYJLYlDSjIv3YbLVFCilQwHYDLbFFCkgvfYPLPF.mDmHM5O8iSf9xwywwwxwvQyQwQRe3H3v4vn2bnbHbvzKNHNPN.5I6O6G6K8f8g8l8htydxdvtytwtxtvNS2XmXGYGX6Y6XaYanqr0rUrkzE1B1b1L5LaJaBaLchMhMjMff2863644c3s4s3M4M30403U4U3k4k3E4EX8777b7r7L7z7T7j7D737X7n7H7v7P7fTrVd.tetOtWtGtatKtStCtctMtUtEtYVC2D2H2.qlqmqiqkqgqlqhqjqfKmKiKkjUwJYErbVJKj4ybYlLUlHikQxPYfznubLzG5M8hdROn6razM1d5JcgNSmH3se.uy7xrddFdBdDJtOtKtMVCqlqgqfjkybYrzn2zc5JAq+a6+GVCIMh26mue6+2u539+cb++Mrecb++A3MXfLHFLCggxvX3LBFIihQyXXrLNFOSfIxjXxLElJSioyLXlLKlMyg4xIyov7X9bpr.NMVHKhEyRXob5bFrLVNmImEmMqfygykyiUx4yEvExp3h3h4RH6WG2+eMdG9bdGni6+6c3A8N78rzw8+8N7hdG98V539+8ui6+uc8ui6+uO8ui6+eD8ui6+OPee.IEwfzjTDCVSRQLDMIEwP0jTDCSSRQLbMIEwHzjTDiTSRQLJMIEwn0jTDiQSRQLVMIEw3zjTDiWSRQLAMIEwD0jTDSRSRQLYMIEwTzjTDSUSRQLMMIEwz0jTDyPSRQLSMIEwrzjTDyVSRQLGMIEwb0jTDmrljh3TzjTDySSRQLeMIEwopIoHVfljh3zzjTDKTSRQrHMIEwh0jTDKQSRQrTMIEwoqIoHNCMIEwxzjTDKWSRQblZRJhyRSRQb1ZRJhUnIoHNGMIEw4pIoHNOMIEwJ0jTDmuljh3BzjTDWnljhXUZRJhKRSRQbwZRJhKQSR8estNt++xVWG2+eNqy2MrNeGgOOPifZVZZDTyTSifZFZZDTSWSifZZZZDTSUSifZJZZDTSVSifZRZZDTSTSifZBZZDTiWSifZbZZDTiUSifZLZZDTiVSifZTZZDTiTSifZDZZDTCWSifZXZZDTCUSifZHZZDTCVSifZPZZDTCTSifZ.ZZDTmjlFA0IpoQP0zzHni6+683P8dPG2+2mG1CuGzw8+84gsx6wV58fNt++Z639+O6Z639+eKeNfNt+u2gU6c3SZoi6+6cXorDVLKhExowB3TY9LONENYlKygYyrXlLClNSioxTXxLIlHSfwy3XrLFFMihQxHX3LLFJCgAyfXfL.NINQZze5Gm.8kimiiikigilihij9vQvgygQu4P4P3foWbPbfb.zS1e1O1W5A6C6M6Ecm8j8fcmcickcgcltwNwNxNv1y1w1x1PWYqYqXKoKrEr4rYzY1T1D1X5DaDaHa.Acb++++78++egJE82esZ...."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[5]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 2147.715605305816553, 1335.705923557281494, 298.529411792755127, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[22]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5121.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQw3EP6b0GaUTDD+5CnegPoEoXKB8QM7UEnVpBAjam1RDZQ9PvHBnhVHBTTJRQJPMzbXiRU7CvFU9HpPZTwVEkznhRTjzX.gh.owPUBRIMFPPCPHJHD84t6c606s801M7GsP2YSFlc142N6ryL6x8t2K0vf0r.ahKXDCkkpwv3BVVFArG0fhg0XXarUlQrgwjrrBSQb9TDWmTDWmUDWWTDW3JhKBEwEoh3hRQbQqHttpHtaQQbcSQbcWQbwHvUef.mDnkPHGiCXc.dN.uG.uG.uG.uGPbOPiOroQ8r5BZ6BHWqhC7Mqm+AqCrCF514.Ok.7tXc.VGvh.XcfdWGHtGzNJfOefHdnKb47trrtDGz88obdWVV2iO5x9WNuKKqKwAceeZX3i1nYe16KvkBi1DUDgf6hiMm.RsLoC5kBw7asgDuWCjiumSVsFVGf0AXc.dN.uG.uGfcOfmlt+7a5592SI.uqtFGz88MVGXGAv5ffqDz83gtt+EUAh8urrXbj2w9dC47trLl+6Xm+E4W47trr.Gx6XWOHm2kkw7eG67uH+ZXzJe2FhBi1L9RW8bnuSiLXuWCYh4CBcN8YLKGbWOywhXaS20hYPZy1V6XBmxzVlo2B96A+nT7Vr0ys8FENzwdfyUK4P0Ti.KUGGCHO+FJ54IxiYaSl8ChX12ccpaKVDFshuM.4pEmBvnlqOadGplIQRO8p3DyPT+g2mM9UKtBtM17lR.XDUof3Hs8cdbwQ1qdKGehom22YtdG2FOcsD10yb35X9C0G4wKpOrIdNvFuvN9oygQFFTbR4nL3ywYdN9nXdFNqkyv7XAa9VjAjSbBcBtKnVtCy1VPkUDMkNKyW7135BNVv8AOqg87oSxAaaMms9daxxt5jvwGOTi4NgqmNRe8nZiHMV4j+C5rkbHTDuE7Pn2M+wv3Am63NyoYWG5brw54LpXcjsob8dSvING2nunteHNC6bFmZB+z0mQFbxw+XqIS1s44KimNtkC9L3XX1TXWmI3nOXa3ZLpsoqiyckVz6wE88fvSW18pdVegFwZHj8x45D22Jt+0K.m9Tb78hWUr4FpVKMtrNG4fqGDw7NKYc1di8CfnSThoqKTJbJEAkhjRQQIro8Qf.ZeDPuC.AlZAKH27yadKK0LGm+Et7Em1vWQtEp2gD8Z2Kdvs+SzwgyiB9BarF9VyXMJYuTdXlqOq9a1iRxzb8UmqYj9ViY1IuAyRyZal6et6zLxR1qY1keTyRq9TlGogKR0GFYvwEEI6jikrvQj.ozr5OY6SeHj8O2zHmofQShrjLICsrbHSp7oQd5plE4UpNWxGWadjZZXoj+3RqhDtu0PhO5WfLv3dIxHS7UIiO4MPlYJuI0Vahrxw7NT6sMxFm36Ss4GQ10r+Dpc2IotE84Ta+UjqTz2Ps+dIIttumjRY+.YLa4PjIV9QIytxehrnp9YRQ69DjWt5SQ17A+MRE096ju93+I4.MbQxubt+hb1K8Ojqds+kt9gAcO7NAwGcWf908HfAFWTvviuqvHSra.oew.iO4XgoLvdByHkdAyY38Fl+HR.xej8AJbL8EJljD7hY0e30F+c.u0DG.7tSYPvGL8g.6XF2I7EydXvdlSpv9laZvgme5vwVz8.mL+QAmtfQCmuv6EtbQlPfhAHhRxDhYsiC585tOHoWeBvfJKGH0299gQskICY7dSExt7oAOvG9fvLq7gfm3SeXXgUMK3Y9xGAV4teLvZOONTZ04BqeeyC13AeRXqGdAv1qMO3yN1SA653KF9tStDX+MrT3HmdYPcma4P8meEvYtzpfKb4mCtx0VMmv7u1m+weWRA+6Rxy+wf38tUuyyj2Vw83B7ts29gX8E9UaUbPrNh0s82OZ7WVtEslfQNM+TNiL7OL+4jedOKkwjBUaG0kj6mS2eisPAsIi4uIiz9NvjycdErD+o0rNgs9QzJ5SuUze2MqdpBQdncjy7AWR1W83WMACadsTyU+ON5Evdunb4JqHESF0RSjg04c8zxvPsXD.i.XD3l2Hfycj12MJ1FM92svdDvYrVAWrJhKNEw0SEwcqJhqWJhKdEw0aEwcaJhKAEwknh35ih3tcEw0WEw0OEwkDGmzqSIzhhhxaX39tqaXbkaRbj3JYq.yUyotZJ5DK6WY8wO2bvetYLdfwC7bAdu.dO.dO.dOPPOWS87mc.+6Vk382pK7fJBXOxHVGvCI5R9WrOw5.6HfHdnqbrN.qC7VCHNGHFSHib8nNQNuKKi0AXc.KBf0AXc.VGnOmCtw6uqk+OeK3SgiW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82qF..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5121.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQw3EP6b0GaUTDD+5CnegPoEoXKB8QM7UEnVpBAjam1RDZQ9PvHBnhVHBTTJRQJPMzbXiRU7CvFU9HpPZTwVEkznhRTjzX.gh.owPUBRIMFPPCPHJHD84t6c606s801M7GsP2YSFlc142N6ryL6x8t2K0vf0r.ahKXDCkkpwv3BVVFArG0fhg0XXarUlQrgwjrrBSQb9TDWmTDWmUDWWTDW3JhKBEwEoh3hRQbQqHttpHtaQQbcSQbcWQbwHvUef.mDnkPHGiCXc.dN.uG.uG.uG.uGPbOPiOroQ8r5BZ6BHWqhC7Mqm+AqCrCF514.Ok.7tXc.VGvh.XcfdWGHtGzNJfOefHdnKb47trrtDGz88obdWVV2iO5x9WNuKKqKwAceeZX3i1nYe16KvkBi1DUDgf6hiMm.RsLoC5kBw7asgDuWCjiumSVsFVGf0AXc.dN.uG.uGfcOfmlt+7a5592SI.uqtFGz88MVGXGAv5ffqDz83gtt+EUAh8urrXbj2w9dC47trLl+6Xm+E4W47trr.Gx6XWOHm2kkw7eG67uH+ZXzJe2FhBi1L9RW8bnuSiLXuWCYh4CBcN8YLKGbWOywhXaS20hYPZy1V6XBmxzVlo2B96A+nT7Vr0ys8FENzwdfyUK4P0Ti.KUGGCHO+FJ54IxiYaSl8ChX12ccpaKVDFshuM.4pEmBvnlqOadGplIQRO8p3DyPT+g2mM9UKtBtM17lR.XDUof3Hs8cdbwQ1qdKGehom22YtdG2FOcsD10yb35X9C0G4wKpOrIdNvFuvN9oygQFFTbR4nL3ywYdN9nXdFNqkyv7XAa9VjAjSbBcBtKnVtCy1VPkUDMkNKyW7135BNVv8AOqg87oSxAaaMms9daxxt5jvwGOTi4NgqmNRe8nZiHMV4j+C5rkbHTDuE7Pn2M+wv3Am63NyoYWG5brw54LpXcjsob8dSvING2nunteHNC6bFmZB+z0mQFbxw+XqIS1s44KimNtkC9L3XX1TXWmI3nOXa3ZLpsoqiyckVz6wE88fvSW18pdVegFwZHj8x45D22Jt+0K.m9Tb78hWUr4FpVKMtrNG4fqGDw7NKYc1di8CfnSThoqKTJbJEAkhjRQQIro8Qf.ZeDPuC.AlZAKH27yadKK0LGm+Et7Em1vWQtEp2gD8Z2Kdvs+SzwgyiB9BarF9VyXMJYuTdXlqOq9a1iRxzb8UmqYj9ViY1IuAyRyZal6et6zLxR1qY1keTyRq9TlGogKR0GFYvwEEI6jikrvQj.ozr5OY6SeHj8O2zHmofQShrjLICsrbHSp7oQd5plE4UpNWxGWadjZZXoj+3RqhDtu0PhO5WfLv3dIxHS7UIiO4MPlYJuI0Vahrxw7NT6sMxFm36Ss4GQ10r+Dpc2IotE84Ta+UjqTz2Ps+dIIttumjRY+.YLa4PjIV9QIytxehrnp9YRQ69DjWt5SQ17A+MRE096ju93+I4.MbQxubt+hb1K8Ojqds+kt9gAcO7NAwGcWf908HfAFWTvviuqvHSra.oew.iO4XgoLvdByHkdAyY38Fl+HR.xej8AJbL8EJljD7hY0e30F+c.u0DG.7tSYPvGL8g.6XF2I7EydXvdlSpv9laZvgme5vwVz8.mL+QAmtfQCmuv6EtbQlPfhAHhRxDhYsiC585tOHoWeBvfJKGH0299gQskICY7dSExt7oAOvG9fvLq7gfm3SeXXgUMK3Y9xGAV4teLvZOONTZ04BqeeyC13AeRXqGdAv1qMO3yN1SA653KF9tStDX+MrT3HmdYPcma4P8meEvYtzpfKb4mCtx0VMmv7u1m+weWRA+6Rxy+wf38tUuyyj2Vw83B7ts29gX8E9UaUbPrNh0s82OZ7WVtEslfQNM+TNiL7OL+4jedOKkwjBUaG0kj6mS2eisPAsIi4uIiz9NvjycdErD+o0rNgs9QzJ5SuUze2MqdpBQdncjy7AWR1W83WMACadsTyU+ON5Evdunb4JqHESF0RSjg04c8zxvPsXD.i.XD3l2Hfycj12MJ1FM92svdDvYrVAWrJhKNEw0SEwcqJhqWJhKdEw0aEwcaJhKAEwknh35ih3tcEw0WEw0OEwkDGmzqSIzhhhxaX39tqaXbkaRbj3JYq.yUyotZJ5DK6WY8wO2bvetYLdfwC7bAdu.dO.dO.dOPPOWS87mc.+6Vk382pK7fJBXOxHVGvCI5R9WrOw5.6HfHdnqbrN.qC7VCHNGHFSHib8nNQNuKKi0AXc.KBf0AXc.VGnOmCtw6uqk+OeK3SgiW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+82qF..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[6]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-54",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4159.715605305817007, 887.705923557281494, 75.0, 23.0 ],
					"text" : "plug Serum"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 991.69717001914978, 887.401028752326965, 150.0, 20.0 ],
					"text" : "cc 74 -> cc 86"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1943.790956974029541, 1076.401028752326965, 59.0, 22.0 ],
					"text" : "mperoute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1869.624208974029443, 1076.401028752326965, 53.0, 22.0 ],
					"text" : "mpesplit"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 542.651025497508954, 1143.187402963638306, 50.0, 35.0 ],
					"text" : "64 0 0 50 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 472.354909771276425, 1143.187402963638306, 50.0, 35.0 ],
					"text" : "65 0 0 50 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 402.058794045043896, 1143.187402963638306, 50.0, 35.0 ],
					"text" : "62 0 0 33 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 7,
					"outlettype" : [ "", "", "", "", "", "", "" ],
					"patching_rect" : [ 366.71565842628479, 1082.49868369102478, 96.0, 22.0 ],
					"text" : "route 1 2 3 4 5 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 974.69717001914978, 930.648340106010437, 184.0, 20.0 ],
					"text" : "Macro 1: 219 - Macro 4: 222"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 991.69717001914978, 835.911778973724381, 150.0, 20.0 ],
					"text" : "macro 1 individually"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1808.715605305816553, 1335.705923557281494, 303.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[19]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5113.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQu3EP6b0GaUTDD+dEnegPoEoXKB8QM7UEnVpBAjam1RDZQ9PvHBnhVHBTTJRQJPMzbXiZU7CvFU9HpPZTwVEkznhRTjzX.kh.gXnJAojFCHnAHDEDh9b2818d2a6qsa3OZgdylLL6ryuc1YmY1k6cuWpgAqYA1DWvHNJKcigwErrLBXOpAECqwvFrUgQ79XRVV9zDWDZhqSZhqyZhqKZhKRMwEkl3hVSbwnItX0DWW0D2MoIttoIttqIt3j3ZHPfS.zRHjiwArN.OGf2Cf2Cf2Cf2CHuGH3CaZz.qtf1NOx8TwA9l00+f0A1ACu14.Wk.7tXc.VGvh.Xcf2tNPdOncT.e9.Y7vqvUy6pxdk3fWeepl2Uk85wGux9WMuqJ6UhCd88ogQDzFM6ydeANjOZSVQDFtCN1bBnzxlNnaJLyu0FR9dMPN9dNY0ZXc.VGf0A34.7d.7d.18.tZd8meyqt+cUBv65UiCd88MVGXGAv5fPqD75wCu59WVEH2+pxxwQdG66MTy6pxX9uic9WleUy6pxRbHuic8fZdWUFy+cry+x7qgQq7caHKLZy3Kc0yg9NMxh8dMTIlOH0I5yXVBbWKywhXaSm0hYPZy1VaeBmzzVlo2B96A+vT7Vr0yo85EOzwxDNPc0IwRk3X.042XIOKQcLaaxreHDyjNqS8a1hvnU7MAHWozz.F0b8Yy6.0MIRlYVCmXFh5O79rwuRoUwswl1XR.inJkDGosuyiKBY25sD9DSOuuXttG2FOcsj100b35X9C0G4wKpOrQdNvFuzN9oygQ73pRNJK9bDyS3ix4YHVKwv7XQcLekLf7RPpSxc.0xcX11BptpXozYX9h6FWWnwBtO3ZMrmOcRBrs0b156toJ6nSAGe7vMlyDtV5n70i5YDowJQ9OjyVpgPY7VxCidm7GCiKbNiKlSytNz4Xi00YT45nZS058lfSdNNnunueHOCSsgXc7SWeFYvIg+wVSlrSy0WFOcbKA9r3XX1TZWwDD5C0FNFiZa55HtqzhDruKDt5xtW005K0HWCoraNWm79V48utAH5Sww2KtUwla3Zsz3p5DxgVOHi4cVw5r8F6G.QmnDSWWnTjTJJJEMkhgRXCi.XDvaGAlZQKH+BKXdKK8rGm+Et7EmwvWQ9E6siHdqcu7A29OYGAmGEhv2XMhXMi0nr8P49LWWN82rGkks45pMeyniXMl4l55MKOmsZtu4tCynKaOl4V4gMKu1SZdnFu.UuOxfSHFRtoFOYgiHIR44zex1l9PH6atYPNcQilDcYYSFZE4QlTkSi7j0LKxKWa9jO5HEPpqwkR9iKtJRjQrFRhw9bjAlvKRFYxuBY7otdxLS6Mn1ZijUNl2lZusR1vDeOpM+PxNm8GSs6NH0unOiZ6ujb4R9Zp82CI409cjzp36IiYyGfLwJOLY1U+SjEUyOSJYWGm7R0dRxl1+uQp5H+N4qN1eR9gFu.4WN6eQNyE+GxUt5+RWeeP2irSPhw1EnecOJXfIDCL7D6JLxj6FP5Wbv3SMdXJCrmvLRqWvbFdug4OhjfBGYefhGSegRIo.OeN8Gd0weavaNwA.uyTFD79SeHv1mwsCe9rGFr64jNr24lAbv4mIbzEcWvIJbTvoJZzv4J9tgKUhIDnT.hprrg3dgwA8ds2CjxqMAXPUjGj9acuvn17jgrd2oB4V4zf66CteXlU+.vi8IOHrvZlE7TewCAqbWOBXs6GEJu17g0s24AaX+ONrkCt.XaGo.3SO5S.67XKF91SrDXeMtT3PmZYP8mc4PCmaEvou3pfyeomAt7UWMmv7umO+i+tjB82kjq+iA46cqAwyj2Vwc4B7ts29gb8k9UaUbPtNx0s82OB9KK2hVSvHQyOkyHC+CyedEVvSSYLov01d8o374z8GrENnMYL+MYj12Alb9yqnk3Oil0Ir0OhVQelsh96rY0SUHyCsiblO3Pp9pK+pIXXyqkZN5+wQu.ZMiEWt5pRyjQszDYXEuqmVFFpEi.XD.i.23FAD2QZe2nbaD7uag8HfXrVAW7ZhKAMw0SMwcyZhqWZhKQMw0aMwcKZhKIMwkrl35il3tUMw0WMw0OMwkBGmxqSI7hxhxqa3QbGW23J2f3HIT1V.lqlW80UxwW1ux5iet4P+byX7.iG34B7dA7d.7d.7dfPdtlF3O6.92sJ46u0qvCoHf8HiXc.Oj3Ux+x8IVGXGAjwCuJGqCv5.20.xyAxwjxH2aTmnl2Ukw5.rNfEAv5.rN.qC7NmCt96uqk+OnvTK0AdAzd16u+Gx0+wwehggg47bdXXXXXNOauXmOe97Q1L6DaXXX3YRRRRRZIokjjjjjjz1yjjjjjjjzRRRRRRR8Ueuoqu+W78ytttsGetusq8Cutde89WdFw68q737aGeDMRVCEqmnuQzU5N8lFik4xxI4J3ZX0rFtMtKtOJdDdBdFVOuLuNuMwIDQmnyzE5JaOcicitSOnmzK5M8gig9RiAxPYjLVlHSkYxbY9rPVJKmUvJYUjbobYb4bEbkbUb0bMbsbcb8rZtAtQtIVC2L2B2J2F2N2A2I2E2M2C2K2G2OO.qkhGjGhGlGgGkGiGmmfmjmhmlmgmkmimm0yKvKxKwKyqvqxqwqyavaxaway6v+l2kneQrArgrQzI1X1D1T5LaFaNaAcgsjshsltx1v1x1w1yNvNxNQ2XmYWXWY2X2YOXOo6rWr2rOzC1W1O1e5IG.GHGD8hClCgCkdygwgyQPe3H4n3n4X3X4333oubBzO5OMNQNIF.CjAwfYHLTFFCmQvHYTLZFCikww3YBLQlDSlovTYZLclAyjYwrYNLWNYNElGymSkEvowBYQrXVBKkSmyfkwx4L4r3rYEbNbtbdrRNet.tPVEWDWLWBY+hj38YItTKw62RbYVhOfk3xsDePKwUXI9PVhqzR7gsDWkk3iXItZKwG0RbMVhOlk3ZsDebKw0YI9DVhq2R7IsDq1R7orD2fk3SaItQKwmwRbSVhOqkXMVhOmk3lsDedKwsXI9BVha0R7EsD2lk3KYItcKwW1RbGVhuhk3NsDeUKwcYI9ZVh61R70sD2ik3aXItWKw2zRbeVhukk39sDeaKwCXI9NVh0ZIVmknrDeWKwCZI9dVhGxR78sDOrk3GXIdDKwOzR7nVhejk3wrD+XKwiaI9IVhmvR7SsDOok3mYIdJKwO2R7zVhegk3YrD+RKwyZI9UVhmyR7qsDOuk32XIVuk32ZIdAKwuyR7hVheuk3krD+AKwKaI9iVhWwR7mrDupk3OaIdMKwewR75Vh+pk3MrD+MKwaZI96Vh2xR7OrDusk3eZIdGKw+xR7usD+OVh20R7er3KAiFIEwFnIoH1PMIEwFoIoH5jljhXi0jTDahljhXS0jTDcVSRQrYZRJhMWSRQrEZRJhtnIoH1RMIEwVoIoH1ZMIEQW0jTDailjhXa0jTDamljhX60jTD6fljhXG0jTD6jljhnaZRJhcVSRQrKZRJhcUSRQraZRJhcWSRQrGZRJh8TSRQzcMIEwdoIoH1aMIEw9nIoH5gljhXe0jTD6mljhX+0jTD8TSRQb.ZRJhCTSRQbPZRJhdoIoHNXMIEwgnIoHNTMIEQu0jTDGlljh3v0jTDGgljhnOZRJhiTSRQbTZRJhiVSRQbLZRJhiUSRQbbZRJhiWSRQzWMIEwInIoH5mljhn+ZRJDQijh3D0jTDmjljhX.ZRJhApIoHFjljhXvZRJhgnIoHFpljhXXZRJhgqIoHFgljhXjZRJhQoIoHFsljhXLZRJhwpIoHFmljhX7ZRJhInIoHlnljhXRZRJhIqIoHlhljhXpZRJhooIoHltljhXFZRJhYpIoHlkljhX1ZRJh4nIoHlqljh3j0jTDmhljhXdZRJh4qIoHNUMIEwBzjTDmlljhXgZRJhEoIoHVrljhXIZRJhkpIoHNcMIEwYnIoHVlljhX4ZRJhyTSRQbVZRJhyVSRQrBMIEw4nIoHNWMIEw4oIoHVoljh370jTDWfljh3B0jTDqRSRQbQZRJhKVSRQbIZRp+q04mWm+r04uac92noQPcQZZDTqRSif5B0zHnt.MMBpyWSifZkZZDTmmlFA04poQPcNZZDTqPSif5r0zHnNKMMBpyTSifZ4ZZDTKSSif5LzzHnNcMMBpkpoQPsDMMBpEqoQPsHMMBpEpoQPcZZZDTKPSif5T0zHnlulFA07zzHnNEMMBpSVSifZtZZDTyQSifZ1ZZDTyRSifZlZZDTyPSifZ5ZZDTSSSifZpZZDTSQSifZxZZDTSRSifZhZZDTSPSifZ7ZZDTiSSifZrZZDTiQSifZzZZDTiRSifZjZZDTiPSifZ3ZZDTCSSifZnZZDTCQSifZvZZDTCRSifZfZZDTCPSif5jzzHnNQMMBpllFAU+0zHn5mlFA0InoQP0WMMBpiWSif53zzHnNVMMBpiQSif5n0zHnNJMMBpiTSifpOZZDTGglFA0gqoQPcXZZDT8VSif5P0zHnNDMMBpCVSifpWZZDTGjlFA0ApoQPc.ZZDT8TSifZ+0zHn1OMMBp8USifpGZZDT6ilFA0dqoQPsWZZDTcWSifZO0zHn1CMMBpcWSifZ2zzHn1UMMBpcQSifZm0zHn5llFA0NooQPsiZZDT6flFA01qoQPscZZDTaqlFA01noQP0UMMBpsVSifZqzzHn1RMMBptnoQPsEZZDTatlFA0looQP0YMMBpMUSifZSzzHn1XMMBpNooQPsQZZDTanlFA0FnoQPEZZDu2O+eVaDjuqk7+wR9usj+KK46XI+mVx21R9Orjukk7uaIeSK4eyR9FVx+pk70sj+EK4qYI+yVxW0R9mrjuhk7OZIeYK4evR9RVxeuk7Esj+NK4KXI+sVx0aI+MVxm2R9qsjOmk7WYIeVK4uzR9LVxegk7osj+bK4SYI+YVxmzR9SsjOgk7mXIebK4O1R9XVxejk7Qsj+PK4iXI+AVxG1R98sjOjk76YIePK420RVVx0YIWqk76XIe.K421Rd+Vxukk79rjeSK48ZI+FVx6wR90sj2sk7qYIuKK4W0RdmVxuhk7NrjeYK4saI+RVxayR9Esj2pk7KXIuEK4m2RdyVxOmkbMVxOqk7lrjeFK4MZI+zVxavR9orjq1R9IsjWuk7SXIuNK4G2RdsVxOlk7ZrjeTK4UaI+HVxqxR9gsjWok7CYIuBK4GzRd4VxOfk7xrjueK4kZIeeVxz9dtDtXtHVEWHW.mOqjyiykygUvYyYwYxxYYbFb5rTVBKlEwB4zXAbpLelGmBmLyk4vrYVLSlASmowTYJLYlDSjIv3YbLVFCilQwHYDLbFFCkgvfYPLPF.mDmHM5O8iSf9xwywwwxwvQyQwQRe3H3v4vn2bnbHbvzKNHNPN.5I6O6G6K8f8g8l8htydxdvtytwtxtvNS2XmXGYGX6Y6XaYanqr0rUrkzE1B1b1L5LaJaBaLchMhMjMff2863644c3s4s3M4M30403U4U3k4k3E4EX8777b7r7L7z7T7j7D737X7n7H7v7P7fTrVd.tetOtWtGtatKtStCtctMtUtEtYVC2D2H2.qlqmqiqkqgqlqhqjqfKmKiKkjUwJYErbVJKj4ybYlLUlHikQxPYfznubLzG5M8hdROn6razM1d5JcgNSmH3se.uy7xrddFdBdDJtOtKtMVCqlqgqfjkybYrzn2zc5JAq+a6+GVCIMh26mue6+2u539+cb++Mrecb++A3MXfLHFLCggxvX3LBFIihQyXXrLNFOSfIxjXxLElJSioyLXlLKlMyg4xIyov7X9bpr.NMVHKhEyRXob5bFrLVNmImEmMqfygykyiUx4yEvExp3h3h4RH6WG2+eMdG9bdGni6+6c3A8N78rzw8+8N7hdG98V539+8ui6+uc8ui6+uO8ui6+eD8ui6+OPee.IEwfzjTDCVSRQLDMIEwP0jTDCSSRQLbMIEwHzjTDiTSRQLJMIEwn0jTDiQSRQLVMIEw3zjTDiWSRQLAMIEwD0jTDSRSRQLYMIEwTzjTDSUSRQLMMIEwz0jTDyPSRQLSMIEwrzjTDyVSRQLGMIEwb0jTDmrljh3TzjTDySSRQLeMIEwopIoHVfljh3zzjTDKTSRQrHMIEwh0jTDKQSRQrTMIEwoqIoHNCMIEwxzjTDKWSRQblZRJhyRSRQb1ZRJhUnIoHNGMIEw4pIoHNOMIEwJ0jTDmuljh3BzjTDWnljhXUZRJhKRSRQbwZRJhKQSR8estNt++xVWG2+eNqy2MrNeGgOOPifZVZZDTyTSifZFZZDTSWSifZZZZDTSUSifZJZZDTSVSifZRZZDTSTSifZBZZDTiWSifZbZZDTiUSifZLZZDTiVSifZTZZDTiTSifZDZZDTCWSifZXZZDTCUSifZHZZDTCVSifZPZZDTCTSifZ.ZZDTmjlFA0IpoQP0zzHni6+683P8dPG2+2mG1CuGzw8+84gsx6wV58fNt++Z639+O6Z639+eKeNfNt+u2gU6c3SZoi6+6cXorDVLKhExowB3TY9LONENYlKygYyrXlLClNSioxTXxLIlHSfwy3XrLFFMihQxHX3LLFJCgAyfXfL.NINQZze5Gm.8kimiiikigilihij9vQvgygQu4P4P3foWbPbfb.zS1e1O1W5A6C6M6Ecm8j8fcmcickcgcltwNwNxNv1y1w1x1PWYqYqXKoKrEr4rYzY1T1D1X5DaDaHa.Acb++++78++egJE82emZ...."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5113.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LQu3EP6b0GaUTDD+dEnegPoEoXKB8QM7UEnVpBAjam1RDZQ9PvHBnhVHBTTJRQJPMzbXiZU7CvFU9HpPZTwVEkznhRTjzX.kh.gXnJAojFCHnAHDEDh9b2818d2a6qsa3OZgdylLL6ryuc1YmY1k6cuWpgAqYA1DWvHNJKcigwErrLBXOpAECqwvFrUgQ79XRVV9zDWDZhqSZhqyZhqKZhKRMwEkl3hVSbwnItX0DWW0D2MoIttoIttqIt3j3ZHPfS.zRHjiwArN.OGf2Cf2Cf2Cf2CHuGH3CaZz.qtf1NOx8TwA9l00+f0A1ACu14.Wk.7tXc.VGvh.Xcf2tNPdOncT.e9.Y7vqvUy6pxdk3fWeepl2Uk85wGux9WMuqJ6UhCd88ogQDzFM6ydeANjOZSVQDFtCN1bBnzxlNnaJLyu0FR9dMPN9dNY0ZXc.VGf0A34.7d.7d.18.tZd8meyqt+cUBv65UiCd88MVGXGAv5fPqD75wCu59WVEH2+pxxwQdG66MTy6pxX9uic9WleUy6pxRbHuic8fZdWUFy+cry+x7qgQq7caHKLZy3Kc0yg9NMxh8dMTIlOH0I5yXVBbWKywhXaSm0hYPZy1VaeBmzzVlo2B96A+vT7Vr0yo85EOzwxDNPc0IwRk3X.042XIOKQcLaaxreHDyjNqS8a1hvnU7MAHWozz.F0b8Yy6.0MIRlYVCmXFh5O79rwuRoUwswl1XR.inJkDGosuyiKBY25sD9DSOuuXttG2FOcsj100b35X9C0G4wKpOrQdNvFuzN9oygQ73pRNJK9bDyS3ix4YHVKwv7XQcLekLf7RPpSxc.0xcX11BptpXozYX9h6FWWnwBtO3ZMrmOcRBrs0b156toJ6nSAGe7vMlyDtV5n70i5YDowJQ9OjyVpgPY7VxCidm7GCiKbNiKlSytNz4Xi00YT45nZS058lfSdNNnunueHOCSsgXc7SWeFYvIg+wVSlrSy0WFOcbKA9r3XX1TZWwDD5C0FNFiZa55HtqzhDruKDt5xtW005K0HWCoraNWm79V48utAH5Sww2KtUwla3Zsz3p5DxgVOHi4cVw5r8F6G.QmnDSWWnTjTJJJEMkhgRXCi.XDvaGAlZQKH+BKXdKK8rGm+Et7EmwvWQ9E6siHdqcu7A29OYGAmGEhv2XMhXMi0nr8P49LWWN82rGkks45pMeyniXMl4l55MKOmsZtu4tCynKaOl4V4gMKu1SZdnFu.UuOxfSHFRtoFOYgiHIR44zex1l9PH6atYPNcQilDcYYSFZE4QlTkSi7j0LKxKWa9jO5HEPpqwkR9iKtJRjQrFRhw9bjAlvKRFYxuBY7otdxLS6Mn1ZijUNl2lZusR1vDeOpM+PxNm8GSs6NH0unOiZ6ujb4R9Zp82CI409cjzp36IiYyGfLwJOLY1U+SjEUyOSJYWGm7R0dRxl1+uQp5H+N4qN1eR9gFu.4WN6eQNyE+GxUt5+RWeeP2irSPhw1EnecOJXfIDCL7D6JLxj6FP5Wbv3SMdXJCrmvLRqWvbFdug4OhjfBGYefhGSegRIo.OeN8Gd0weavaNwA.uyTFD79SeHv1mwsCe9rGFr64jNr24lAbv4mIbzEcWvIJbTvoJZzv4J9tgKUhIDnT.hprrg3dgwA8ds2CjxqMAXPUjGj9acuvn17jgrd2oB4V4zf66CteXlU+.vi8IOHrvZlE7TewCAqbWOBXs6GEJu17g0s24AaX+ONrkCt.XaGo.3SO5S.67XKF91SrDXeMtT3PmZYP8mc4PCmaEvou3pfyeomAt7UWMmv7umO+i+tjB82kjq+iA46cqAwyj2Vwc4B7ts29gb8k9UaUbPtNx0s82OB9KK2hVSvHQyOkyHC+CyedEVvSSYLov01d8o374z8GrENnMYL+MYj12Alb9yqnk3Oil0Ir0OhVQelsh96rY0SUHyCsiblO3Pp9pK+pIXXyqkZN5+wQu.ZMiEWt5pRyjQszDYXEuqmVFFpEi.XD.i.23FAD2QZe2nbaD7uag8HfXrVAW7ZhKAMw0SMwcyZhqWZhKQMw0aMwcKZhKIMwkrl35il3tUMw0WMw0OMwkBGmxqSI7hxhxqa3QbGW23J2f3HIT1V.lqlW80UxwW1ux5iet4P+byX7.iG34B7dA7d.7d.7dfPdtlF3O6.92sJ46u0qvCoHf8HiXc.Oj3Ux+x8IVGXGAjwCuJGqCv5.20.xyAxwjxH2aTmnl2Ukw5.rNfEAv5.rN.qC7NmCt96uqk+OnvTK0AdAzd16u+Gx0+wwehggg47bdXXXXXNOauXmOe97Q1L6DaXXX3YRRRRRZIokjjjjjjz1yjjjjjjjzRRRRRRR8Ueuoqu+W78ytttsGetusq8Cutde89WdFw68q737aGeDMRVCEqmnuQzU5N8lFik4xxI4J3ZX0rFtMtKtOJdDdBdFVOuLuNuMwIDQmnyzE5JaOcicitSOnmzK5M8gig9RiAxPYjLVlHSkYxbY9rPVJKmUvJYUjbobYb4bEbkbUb0bMbsbcb8rZtAtQtIVC2L2B2J2F2N2A2I2E2M2C2K2G2OO.qkhGjGhGlGgGkGiGmmfmjmhmlmgmkmimm0yKvKxKwKyqvqxqwqyavaxaway6v+l2kneQrArgrQzI1X1D1T5LaFaNaAcgsjshsltx1v1x1w1yNvNxNQ2XmYWXWY2X2YOXOo6rWr2rOzC1W1O1e5IG.GHGD8hClCgCkdygwgyQPe3H4n3n4X3X4333oubBzO5OMNQNIF.CjAwfYHLTFFCmQvHYTLZFCikww3YBLQlDSlovTYZLclAyjYwrYNLWNYNElGymSkEvowBYQrXVBKkSmyfkwx4L4r3rYEbNbtbdrRNet.tPVEWDWLWBY+hj38YItTKw62RbYVhOfk3xsDePKwUXI9PVhqzR7gsDWkk3iXItZKwG0RbMVhOlk3ZsDebKw0YI9DVhq2R7IsDq1R7orD2fk3SaItQKwmwRbSVhOqkXMVhOmk3lsDedKwsXI9BVha0R7EsD2lk3KYItcKwW1RbGVhuhk3NsDeUKwcYI9ZVh61R70sD2ik3aXItWKw2zRbeVhukk39sDeaKwCXI9NVh0ZIVmknrDeWKwCZI9dVhGxR78sDOrk3GXIdDKwOzR7nVhejk3wrD+XKwiaI9IVhmvR7SsDOok3mYIdJKwO2R7zVhegk3YrD+RKwyZI9UVhmyR7qsDOuk32XIVuk32ZIdAKwuyR7hVheuk3krD+AKwKaI9iVhWwR7mrDupk3OaIdMKwewR75Vh+pk3MrD+MKwaZI96Vh2xR7OrDusk3eZIdGKw+xR7usD+OVh20R7er3KAiFIEwFnIoH1PMIEwFoIoH5jljhXi0jTDahljhXS0jTDcVSRQrYZRJhMWSRQrEZRJhtnIoH1RMIEwVoIoH1ZMIEQW0jTDailjhXa0jTDamljhX60jTD6fljhXG0jTD6jljhnaZRJhcVSRQrKZRJhcUSRQraZRJhcWSRQrGZRJh8TSRQzcMIEwdoIoH1aMIEw9nIoH5gljhXe0jTD6mljhX+0jTD8TSRQb.ZRJhCTSRQbPZRJhdoIoHNXMIEwgnIoHNTMIEQu0jTDGlljh3v0jTDGgljhnOZRJhiTSRQbTZRJhiVSRQbLZRJhiUSRQbbZRJhiWSRQzWMIEwInIoH5mljhn+ZRJDQijh3D0jTDmjljhX.ZRJhApIoHFjljhXvZRJhgnIoHFpljhXXZRJhgqIoHFgljhXjZRJhQoIoHFsljhXLZRJhwpIoHFmljhX7ZRJhInIoHlnljhXRZRJhIqIoHlhljhXpZRJhooIoHltljhXFZRJhYpIoHlkljhX1ZRJh4nIoHlqljh3j0jTDmhljhXdZRJh4qIoHNUMIEwBzjTDmlljhXgZRJhEoIoHVrljhXIZRJhkpIoHNcMIEwYnIoHVlljhX4ZRJhyTSRQbVZRJhyVSRQrBMIEw4nIoHNWMIEw4oIoHVoljh370jTDWfljh3B0jTDqRSRQbQZRJhKVSRQbIZRp+q04mWm+r04uac92noQPcQZZDTqRSif5B0zHnt.MMBpyWSifZkZZDTmmlFA04poQPcNZZDTqPSif5r0zHnNKMMBpyTSifZ4ZZDTKSSif5LzzHnNcMMBpkpoQPsDMMBpEqoQPsHMMBpEpoQPcZZZDTKPSif5T0zHnlulFA07zzHnNEMMBpSVSifZtZZDTyQSifZ1ZZDTyRSifZlZZDTyPSifZ5ZZDTSSSifZpZZDTSQSifZxZZDTSRSifZhZZDTSPSifZ7ZZDTiSSifZrZZDTiQSifZzZZDTiRSifZjZZDTiPSifZ3ZZDTCSSifZnZZDTCQSifZvZZDTCRSifZfZZDTCPSif5jzzHnNQMMBpllFAU+0zHn5mlFA0InoQP0WMMBpiWSif53zzHnNVMMBpiQSif5n0zHnNJMMBpiTSifpOZZDTGglFA0gqoQPcXZZDT8VSif5P0zHnNDMMBpCVSifpWZZDTGjlFA0ApoQPc.ZZDT8TSifZ+0zHn1OMMBp8USifpGZZDT6ilFA0dqoQPsWZZDTcWSifZO0zHn1CMMBpcWSifZ2zzHn1UMMBpcQSifZm0zHn5llFA0NooQPsiZZDT6flFA01qoQPscZZDTaqlFA01noQP0UMMBpsVSifZqzzHn1RMMBptnoQPsEZZDTatlFA0looQP0YMMBpMUSifZSzzHn1XMMBpNooQPsQZZDTanlFA0FnoQPEZZDu2O+eVaDjuqk7+wR9usj+KK46XI+mVx21R9Orjukk7uaIeSK4eyR9FVx+pk70sj+EK4qYI+yVxW0R9mrjuhk7OZIeYK4evR9RVxeuk7Esj+NK4KXI+sVx0aI+MVxm2R9qsjOmk7WYIeVK4uzR9LVxegk7osj+bK4SYI+YVxmzR9SsjOgk7mXIebK4O1R9XVxejk7Qsj+PK4iXI+AVxG1R98sjOjk76YIePK420RVVx0YIWqk76XIe.K421Rd+Vxukk79rjeSK48ZI+FVx6wR90sj2sk7qYIuKK4W0RdmVxuhk7NrjeYK4saI+RVxayR9Esj2pk7KXIuEK4m2RdyVxOmkbMVxOqk7lrjeFK4MZI+zVxavR9orjq1R9IsjWuk7SXIuNK4G2RdsVxOlk7ZrjeTK4UaI+HVxqxR9gsjWok7CYIuBK4GzRd4VxOfk7xrjueK4kZIeeVxz9dtDtXtHVEWHW.mOqjyiykygUvYyYwYxxYYbFb5rTVBKlEwB4zXAbpLelGmBmLyk4vrYVLSlASmowTYJLYlDSjIv3YbLVFCilQwHYDLbFFCkgvfYPLPF.mDmHM5O8iSf9xwywwwxwvQyQwQRe3H3v4vn2bnbHbvzKNHNPN.5I6O6G6K8f8g8l8htydxdvtytwtxtvNS2XmXGYGX6Y6XaYanqr0rUrkzE1B1b1L5LaJaBaLchMhMjMff2863644c3s4s3M4M30403U4U3k4k3E4EX8777b7r7L7z7T7j7D737X7n7H7v7P7fTrVd.tetOtWtGtatKtStCtctMtUtEtYVC2D2H2.qlqmqiqkqgqlqhqjqfKmKiKkjUwJYErbVJKj4ybYlLUlHikQxPYfznubLzG5M8hdROn6razM1d5JcgNSmH3se.uy7xrddFdBdDJtOtKtMVCqlqgqfjkybYrzn2zc5JAq+a6+GVCIMh26mue6+2u539+cb++Mrecb++A3MXfLHFLCggxvX3LBFIihQyXXrLNFOSfIxjXxLElJSioyLXlLKlMyg4xIyov7X9bpr.NMVHKhEyRXob5bFrLVNmImEmMqfygykyiUx4yEvExp3h3h4RH6WG2+eMdG9bdGni6+6c3A8N78rzw8+8N7hdG98V539+8ui6+uc8ui6+uO8ui6+eD8ui6+OPee.IEwfzjTDCVSRQLDMIEwP0jTDCSSRQLbMIEwHzjTDiTSRQLJMIEwn0jTDiQSRQLVMIEw3zjTDiWSRQLAMIEwD0jTDSRSRQLYMIEwTzjTDSUSRQLMMIEwz0jTDyPSRQLSMIEwrzjTDyVSRQLGMIEwb0jTDmrljh3TzjTDySSRQLeMIEwopIoHVfljh3zzjTDKTSRQrHMIEwh0jTDKQSRQrTMIEwoqIoHNCMIEwxzjTDKWSRQblZRJhyRSRQb1ZRJhUnIoHNGMIEw4pIoHNOMIEwJ0jTDmuljh3BzjTDWnljhXUZRJhKRSRQbwZRJhKQSR8estNt++xVWG2+eNqy2MrNeGgOOPifZVZZDTyTSifZFZZDTSWSifZZZZDTSUSifZJZZDTSVSifZRZZDTSTSifZBZZDTiWSifZbZZDTiUSifZLZZDTiVSifZTZZDTiTSifZDZZDTCWSifZXZZDTCUSifZHZZDTCVSifZPZZDTCTSifZ.ZZDTmjlFA0IpoQP0zzHni6+683P8dPG2+2mG1CuGzw8+84gsx6wV58fNt++Z639+O6Z639+eKeNfNt+u2gU6c3SZoi6+6cXorDVLKhExowB3TY9LONENYlKygYyrXlLClNSioxTXxLIlHSfwy3XrLFFMihQxHX3LLFJCgAyfXfL.NINQZze5Gm.8kimiiikigilihij9vQvgygQu4P4P3foWbPbfb.zS1e1O1W5A6C6M6Ecm8j8fcmcickcgcltwNwNxNv1y1w1x1PWYqYqXKoKrEr4rYzY1T1D1X5DaDaHa.Acb++++78++egJE82emZ...."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1485.38235330581665, 1817.705923557281494, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1488.715605305816553, 1335.705923557281494, 300.0, 150.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[17]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5160.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LA63EP6b8.SVUEE+8gxeMEASLvT9jl+iTIjRGpu6AvkBl+IskoVYgtTwRLwDUpXOGqhxJ0XUnVlNVlAkki9ikqLGqokXpy0jxYhi0zzZoyUZ5J5beu68883xGvc0FTbu2sy24dume2y8bOmy6vy26yutGZEoOiqjDXXXgjSKZjkhwvsGXYYzHaZl7.3nyWlQL9nbKKeRhKDIw0EIw0UIwEpj3BSRbgKItHjDWjRhKJIw0MIwccRhq6RhqGRhKZNt5arwSAXJjlq8C57.80A55.55.55.55.75.zauj0pmlWfsKn4Jkev9v54CcdfiyP0tNvSJfcWcdfNOf5Az4Apcd.uNniWPe+Ab+gpvEi6hiUE+fpeNEi6hiUc+ipb9Ei6hiUE+fpeNMLBAaXzm97BbIeXimQDDtKN5ZZTnkINoWJHquslh+bMzb8y4jlqoyCz4A57.80A55.55.z5.dZp98uopmeOo.1cUU+fpet04ANd.cdPSyDTc+gpd94YA7yu3X97Zdm65Fhwcww53em63OO9JF2EGywo4ctyGDi6hi0w+N2wed70vnMd2F7Di1MdZokF9LMxf9bMDIpMvkw5SYVLb+SViEwQmt6EUgXyQW6bhm1zYLUtE76C49P7Vz8ys8RENrw8AkuNxgpsVNVTlMFHXqOXygKfcFb4T86tO0sYKBkV4m2H4pEmLPoVpOccGp1ISRKspsIphP6wtOc9qVbk15XSaLdfRnPNYizw1s8KrwdkawrIpb69r05cdG73dw0qm0XKiZOnMZ6uPaXi1w.G7b83GWCkLLPbBwnLrWCacLajuNC1dwl11WPWuEYf4DKWFm6Bp06P0sETUkQgz4n1h2lsrl5KrsA28XnOwzYXnyS0U6M2dO8XyhicE4Zyty3ja3Y3+9tBudTkYH54Xw+lbsknCkGC37fH2M9Qw3Am67r0zh6CtFGrdtFkuOh5TLeuY33WGGvVj2N3WCytFGUgeb+ojgMwrO5dRG6177x3w4sX3yvFCUmb8xV.SdS0gqxPci6CqVoEVGm22CBOco0U8r+bI78fO1K2VFudKu9qW.r9HN6yhWQz0FrVqMunL13llOv84cUP6zyF8K.QWPhJKTjBCovQJBjhDIcS48.h4XJuCQwb.vzJXg4ledye4oj438unUrjTGwJysPEyGnxGWfeia+EuCia6TBw23LBYMiynj8gbelqOqAX1yRxzb80jqYDgrFyrSZClkl01LOv71kYDkrOyrq3nlkVyoMORCWDk6iLjXijjcRwPVzHimTZVCfriYLTxAlWpjyVP5jHJISxvJKGxjqX5jGo5YSd9Zxk7NGKORsMrLxOeoUSBKj0PhKpmhLnXeVxnR3EHSHoMPlUxuLpqMRV0XdcTeaiT9jdSTmuMY2y4cQ8tKRcK9CQc+IjqTzmg5eejDV6WRRtruhLlMeHxjp3nj4T02RVb0eGon8bRxyUyoIa5f+Hoxi8SjO8D+B4qa3hju+7+F4bW5OHW8Z+It+9fdDVWf3hJTn+8HbXPwFILh35FLpD5NP5ezvDRJFXpCpWvLSt2vbGQefELx3g7GUegBGS+fhIIBOcVC.dwIbSvqLoABaYpCF19LFJryYdyvGMmgC6cto.6edoBGdAoAGew2Fbp7GMblBRG90BGKb4hLgFKFfvKISH5mY7PeV6sCIttIBCtrbfTd06.F8lmBjwaLMH6JlNbmu0cAypp6Fdv26dfEU8rgG8iuWXU649Aq89.Po0jKr98Oen7C9PvVO7Bgcbr7f2+3OLr6SrD3KN0RgCzvxfiblkC0c9U.0+qqDN6kVMbgK+3vUt1SZS53uxG+0eujZ52KIO+cB9ycqd18O0dw8XB1c6nsC99ysq1K+.ee36aGucD3aVtElSPIVyOxojg+g6Om7y6wPFcTvZ6rtDc+2o6OPKXPa1b9a1LcrSLkbmeAK0epsnQ3Hejsg7zZC42ZKJGEviCcfbpM3Rh1pG6pYXnqq0Ztx+lzWH84hZOtpJS1jRs1BoXYOqmVGlVp1Cn8.ZOv+e8.rZjN0F4Gi.+tE1yFYy0F3hQRbwJItdIItqWRb8VRbwIIt9HItaPRbwKItDjDWekD2MJIt9IIt9KItDswI73TB9PdRYGHOYCiwhumbjnsPtEGt9yl6AJGuupMgzqgzVPZqHYXDaIaELL1tYN0UaQmb4+.1W++qO8uqm5eWS0WGnqCnqCnqCnqCzr5.nKg2pmd+BXi+7K0b0ve3bJC7oNOPMh6hWeGHCvomNOPmGP8.57.0NOfWmvwKnu+.t+PU3hwcwwpheP0Omhwcwwpt+QUN+hwcwwpheP0Om+2620x+Fh3aUqfW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8m0F..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5160.CMlaKA....fQPMDZ....AfkYygE...P.....AzBHI4VZzARK.............................LA63EP6b8.SVUEE+8gxeMEASLvT9jl+iTIjRGpu6AvkBl+IskoVYgtTwRLwDUpXOGqhxJ0XUnVlNVlAkki9ikqLGqokXpy0jxYhi0zzZoyUZ5J5beu68883xGvc0FTbu2sy24dume2y8bOmy6vy26yutGZEoOiqjDXXXgjSKZjkhwvsGXYYzHaZl7.3nyWlQL9nbKKeRhKDIw0EIw0UIwEpj3BSRbgKItHjDWjRhKJIw0MIwccRhq6RhqGRhKZNt5arwSAXJjlq8C57.80A55.55.55.55.75.zauj0pmlWfsKn4Jkev9v54CcdfiyP0tNvSJfcWcdfNOf5Az4Apcd.uNniWPe+Ab+gpvEi6hiUE+fpeNEi6hiUc+ipb9Ei6hiUE+fpeNMLBAaXzm97BbIeXimQDDtKN5ZZTnkINoWJHquslh+bMzb8y4jlqoyCz4A57.80A55.55.z5.dZp98uopmeOo.1cUU+fpet04ANd.cdPSyDTc+gpd94YA7yu3X97Zdm65Fhwcww53em63OO9JF2EGywo4ctyGDi6hi0w+N2wed70vnMd2F7Di1MdZokF9LMxf9bMDIpMvkw5SYVLb+SViEwQmt6EUgXyQW6bhm1zYLUtE76C49P7Vz8ys8RENrw8AkuNxgpsVNVTlMFHXqOXygKfcFb4T86tO0sYKBkV4m2H4pEmLPoVpOccGp1ISRKspsIphP6wtOc9qVbk15XSaLdfRnPNYizw1s8KrwdkawrIpb69r05cdG73dw0qm0XKiZOnMZ6uPaXi1w.G7b83GWCkLLPbBwnLrWCacLajuNC1dwl11WPWuEYf4DKWFm6Bp06P0sETUkQgz4n1h2lsrl5KrsA28XnOwzYXnyS0U6M2dO8XyhicE4Zyty3ja3Y3+9tBudTkYH54Xw+lbsknCkGC37fH2M9Qw3Am67r0zh6CtFGrdtFkuOh5TLeuY33WGGvVj2N3WCytFGUgeb+ojgMwrO5dRG6177x3w4sX3yvFCUmb8xV.SdS0gqxPci6CqVoEVGm22CBOco0U8r+bI78fO1K2VFudKu9qW.r9HN6yhWQz0FrVqMunL13llOv84cUP6zyF8K.QWPhJKTjBCovQJBjhDIcS48.h4XJuCQwb.vzJXg4ledye4oj438unUrjTGwJysPEyGnxGWfeia+EuCia6TBw23LBYMiynj8gbelqOqAX1yRxzb80jqYDgrFyrSZClkl01LOv71kYDkrOyrq3nlkVyoMORCWDk6iLjXijjcRwPVzHimTZVCfriYLTxAlWpjyVP5jHJISxvJKGxjqX5jGo5YSd9Zxk7NGKORsMrLxOeoUSBKj0PhKpmhLnXeVxnR3EHSHoMPlUxuLpqMRV0XdcTeaiT9jdSTmuMY2y4cQ8tKRcK9CQc+IjqTzmg5eejDV6WRRtruhLlMeHxjp3nj4T02RVb0eGon8bRxyUyoIa5f+Hoxi8SjO8D+B4qa3hju+7+F4bW5OHW8Z+It+9fdDVWf3hJTn+8HbXPwFILh35FLpD5NP5ezvDRJFXpCpWvLSt2vbGQefELx3g7GUegBGS+fhIIBOcVC.dwIbSvqLoABaYpCF19LFJryYdyvGMmgC6cto.6edoBGdAoAGew2Fbp7GMblBRG90BGKb4hLgFKFfvKISH5mY7PeV6sCIttIBCtrbfTd06.F8lmBjwaLMH6JlNbmu0cAypp6Fdv26dfEU8rgG8iuWXU649Aq89.Po0jKr98Oen7C9PvVO7Bgcbr7f2+3OLr6SrD3KN0RgCzvxfiblkC0c9U.0+qqDN6kVMbgK+3vUt1SZS53uxG+0eujZ52KIO+cB9ycqd18O0dw8XB1c6nsC99ysq1K+.ee36aGucD3aVtElSPIVyOxojg+g6Om7y6wPFcTvZ6rtDc+2o6OPKXPa1b9a1LcrSLkbmeAK0epsnQ3Hejsg7zZC42ZKJGEviCcfbpM3Rh1pG6pYXnqq0Ztx+lzWH84hZOtpJS1jRs1BoXYOqmVGlVp1Cn8.ZOv+e8.rZjN0F4Gi.+tE1yFYy0F3hQRbwJItdIItqWRb8VRbwIIt9HItaPRbwKItDjDWekD2MJIt9IIt9KItDswI73TB9PdRYGHOYCiwhumbjnsPtEGt9yl6AJGuupMgzqgzVPZqHYXDaIaELL1tYN0UaQmb4+.1W++qO8uqm5eWS0WGnqCnqCnqCnqCzr5.nKg2pmd+BXi+7K0b0ve3bJC7oNOPMh6hWeGHCvomNOPmGP8.57.0NOfWmvwKnu+.t+PU3hwcwwpheP0Omhwcwwpt+QUN+hwcwwpheP0Om+2620x+Fh3aUqfW.sm896+gb8eb7mXXXXNOmGFFFFlyy1K14ymOejMyNwFFFFdljjjjjVRZIIIIIIIs8LIIIIIIIsjjjjjjTe02a556+Ee+rqqa6wm6a6Z+vqq2Wu+kmQ7d+JON+1wGQij0Pw5I5aDcktSuowXYtrbRtBtFVMqgai6h6ihGgmfmg0yKyqyaSbBQzI5Lcgtx1S2X2n6zC5I8hdSe3XnuzXfLTFIikIxTYlLWlOKjkxxYErRVEIWJWFWNWAWIWEWMWCWKWGWOqlafajah0vMysvsxswsycvcxcwcy8v8x8w8yCvZo3A4g3g4Q3Q4w3w4I3I4o3o4Y3Y4434Y87B7h7R7x7J7p7Z757F7l7V717N7u4cI5WDa.aHaDchMlMgMkNylwlyVPWXKYqXqoqrMrsrcr8rCrirSzM1Y1E1U1M1c1C1S5N6E6M6C8f8k8i8mdxAvAxAQu3f4P3Po2bXb3bDzGNRNJNZNFNVNNNd5Km.8i9SiSjShAv.YPLXFBCkgwvYDLRFEilwvXYbLdl.SjIwjYJLUlFSmYvLYVLalCykSlSg4w74TYAbZrPVDKlkvR4z4LXYrbNSNKNaVAmCmKmGqjymKfKjUwEwEykP1uHIdeVhK0R79sDWlk3CXItbKwGzRbEVhOjk3JsDeXKwUYI9HVhq1R7QsDWik3iYItVKwG2RbcVhOgk35sDeRKwpsDeJKwMXI9zVhazR7YrD2jk3yZIVik3yYItYKwm2RbKVhufk3VsDeQKwsYI9RVha2R7ksD2gk3qXItSKwW0RbWVhulk3tsDecKw8XI9FVh60R7MsD2mk3aYIteKw21R7.VhuikXsVh0YIJKw20R7fVhumk3grDeeKwCaI9AVhGwR7CsDOpk3GYIdLKwO1R73Vhehk3IrD+TKwSZI9YVhmxR7ysDOsk3WXIdFKwuzR7rVhekk34rD+ZKwyaI9MVh0aI9sVhWvR76rDunk32aIdIKwevR7xVh+nk3UrD+IKwqZI9yVhWyR7WrDutk3uZIdCKweyR7lVh+tk3srD+CKwaaI9mVh2wR7urD+aKw+ik3csD+GK9RvnQRQrAZRJhMTSRQrQZRJhNoIoH1XMIEwlnIoH1TMIEQm0jTDalljhXy0jTDagljhnKZRJhsTSRQrUZRJhsVSRQzUMIEw1nIoH1VMIEw1oIoH1dMIEwNnIoH1QMIEwNoIoH5lljhXm0jTD6hljhXW0jTD6lljhX20jTD6gljhXO0jTDcWSRQrWZRJh8VSRQrOZRJhdnIoH1WMIEw9oIoH1eMIEQO0jTDGfljh3.0jTDGjljhnWZRJhCVSRQbHZRJhCUSRQzaMIEwgoIoHNbMIEwQnIoH5iljh3H0jTDGkljh3n0jTDGiljh3X0jTDGmljh330jTD8USRQbBZRJh9oIoH5uljBQzHoHNQMIEwIoIoHFfljhXfZRJhAoIoHFrljhXHZRJhgpIoHFlljhX3ZRJhQnIoHFoljhXTZRJhQqIoHFiljhXrZRJhwoIoHFuljhXBZRJhIpIoHljljhXxZRJhonIoHlpljhXZZRJhoqIoHlgljhXlZRJhYoIoHlsljhXNZRJh4pIoHNYMIEwonIoHlmljhX9ZRJhSUSRQr.MIEwooIoHVnljhXQZRJhEqIoHVhljhXoZRJhSWSRQbFZRJhkoIoHVtljh3L0jTDmkljh3r0jTDqPSRQbNZRJhyUSRQbdZRJhUpIoHNeMIEwEnIoHtPMIEwpzjTDWjljh3h0jTDWhlj5+Zc9404Oac96Vm+MZZDTWjlFA0pzzHntPMMBpKPSif570zHnVolFA04ooQPctZZDTmilFA0JzzHnNaMMBpyRSif5L0zHnVtlFA0xzzHnNCMMBpSWSifZoZZDTKQSifZwZZDTKRSifZgZZDTmllFA0BzzHnNUMMBp4qoQPMOMMBpSQSif5j0zHnlqlFA0bzzHnlslFA0rzzHnlolFA0LzzHnltlFA0zzzHnlplFA0TzzHnlrlFA0jzzHnlnlFA0DzzHnFulFA03zzHnFqlFA0XzzHnFslFA0nzzHnFolFA0HzzHnFtlFA0vzzHnFplFA0PzzHnFrlFA0fzzHnFnlFA0.zzHnNIMMBpSTSifpooQP0eMMBp9ooQPcBZZDT8USif530zHnNNMMBpiUSif5XzzHnNZMMBpiRSif5H0zHn5ilFA0QnoQPc3ZZDTGllFAUu0zHnNTMMBpCQSif5f0zHn5klFA0AooQPcfZZDTGflFAUO0zHn1eMMBp8SSifZe0zHn5glFA09noQPs2ZZDT6klFAU20zHn1SMMBp8PSifZ20zHn1MMMBpcUSifZWzzHn1YMMBptooQPsSZZDT6nlFA0NnoQPs8ZZDTamlFA01poQPsMZZDTcUSifZq0zHn1JMMBpsTSifpKZZDTaglFA0lqoQPsYZZDTcVSifZS0zHn1DMMBpMVSifpSZZDTajlFA0FpoQPsAZZDTglFw68y+m0FA46ZI+erj+aK4+xR9NVx+ok7ssj+CK4aYI+6Vx2zR92rjugk7uZIecK4ewR9ZVx+rk7Usj+IK4qXI+iVxW1R9Grjujk72aIeQK4uyR9BVxeqkb8Vxeik74sj+ZK4yYI+UVxm0R9KsjOik7WXIeZK4O2R9TVxelk7Isj+TK4SXI+IVxG2R9isjOlk7GYIeTK4OzR9HVxefk7gsjeeK4CYI+dVxGzR9csjkkbcVx0ZI+NVxGvR9ssj2uk7aYIuOK42zRduVxugk7drjecK4caI+ZVx6xR9Usj2ok7qXIuCK4W1Rd6Vxujk71rjeQK4sZI+BVxawR94sj2rk7yYIWik7yZIuIK4mwRdiVxOsk7FrjeJK4psjeRK40aI+DVxqyR9wsjWqk7iYIuFK4G0Rd0VxOhk7prjeXK4UZI+PVxqvR9AsjWtk7CXIuLK462RdoVx2mkLsumKgKlKhUwExEv4yJ473b4bXEb1bVblrbVFmAmNKkkvhYQrPNMV.mJym4wovIybYNLalEyjYvzYZLUlBSlIwDYBLdFGikwvnYTLRFACmgwPYHLXFDCjAvIwIRi9S+3Dnub7bbbrbLbzbTbjzGNBNbNL5MGJGBGL8hChCjCfdx9y9w9ROXeXuYun6rmrGr6rarqrKryzM1I1Q1A1d1N1V1F5JaMaEaIcgsfMmMiNylxlvFSmXiXCYCH3c+N9dddGdadKdSdCdcdMdUdEdYdIdQdAVOOOOGOKOCOMOEOIOAONOFOJOBOLODOHEqkGf6m6i6k6g6l6h6j6famaiakagal0vMwMxMvp45453Z4Z3p4p3J4J3x4x3RIYUrRVAKmkxBY9LWlISkIxXYjLTFHM5KGC8gdSunmzC5N6FcismtRWnyzIBd6Gv6LuLqmmgmfGgh6i6hai0vp4Z3JHY4LWFKM5McmtRv5+19+g0PRi3894629+8qNt+eG2+eC6WG2+e.dCFHChAyPXnLLFNifQxnXzLFFKiiwyDXhLIlLSgoxzX5LClIyhYybXtbxbJLOlOmJKfSiExhXwrDVJmNmAKikyYxYwYyJ3b3b47Xkb9bAbgrJtHtXtDx90w8+Wi2gOm2A539+dGdPuCeOKcb+euCun2geukNt+e+639+aW+639+6S+639+GQ+639+Cz2GPRQLHMIEwf0jTDCQSRQLTMIEwvzjTDCWSRQLBMIEwH0jTDiRSRQLZMIEwXzjTDiUSRQLNMIEw30jTDSPSRQLQMIEwjzjTDSVSRQLEMIEwT0jTDSSSRQLcMIEwLzjTDyTSRQLKMIEwr0jTDyQSRQLWMIEwIqIoHNEMIEw7zjTDyWSRQbpZRJhEnIoHNMMIEwB0jTDKRSRQrXMIEwRzjTDKUSRQb5ZRJhyPSRQrLMIEwx0jTDmoljh3rzjTDmsljhXEZRJhyQSRQbtZRJhySSRQrRMIEw4qIoHt.MIEwEpIoHVkljh3hzjTDWrljh3RzjT+Wqqi6+ur00w8+my57cCqy2Q3yCzHnlklFA0L0zHnlglFA0z0zHnlllFA0T0zHnlhlFA0j0zHnljlFA0D0zHnlflFA030zHnFmlFA0X0zHnFilFA0n0zHnFklFA0H0zHnFglFA0v0zHnFllFA0P0zHnFhlFA0f0zHnFjlFA0.0zHnFflFA0IooQPchZZDTMMMB539+dONTuGzw8+84g8v6Acb+eedXq7drkdOni6+u1Nt++yt1Nt++2xmCni6+6cX0dG9jV539+dGVJKgEyhXgbZr.NUlOyiSgSl4xbX1LKlIyfoyzXpLElLShIxDX7LNFKigQynXjLBFNCigxPXvLHFHCfShSjF8m9wIPe43433X4X3n4n3HoObDb3bXzaNTNDNX5EGDGHG.8j8m8i8kdv9vdydQ2YOYOX2Y2XWYWXmoarSrirCr8rcrsrMzU1Z1J1R5BaAaNaFclMkMgMlNwFwFxFPPG2+++Oe+++WnRQ+8m0F..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 798.392168045043945, 367.411778926849365, 100.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 827.892168045043945, 269.411778926849365, 63.0, 22.0 ],
					"text" : "jit.boids3d"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 874.392168045043945, 499.411778926849365, 100.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 792.392168045043945, 555.411778926849365, 63.0, 22.0 ],
					"text" : "risedevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 897.892168045043945, 431.411778926849365, 208.0, 22.0 ],
					"text" : "mpeevent 1 1 7 135 63 30"
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1134.715605305816553, 1335.705923557281494, 315.740741491317749, 150.000002384185791 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, "Serum.auinfo", ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~[29]",
							"parameter_shortname" : "vst~[16]",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"snapshot" : 					{
						"filetype" : "C74Snapshot",
						"version" : 2,
						"minorversion" : 0,
						"name" : "snapshotlist",
						"origin" : "vst~",
						"type" : "list",
						"subtype" : "Undefined",
						"embed" : 1,
						"snapshot" : 						{
							"pluginname" : "Serum.vstinfo",
							"plugindisplayname" : "Serum",
							"pluginsavedname" : "",
							"pluginsaveduniqueid" : 1483109208,
							"version" : 1,
							"isbank" : 0,
							"isbase64" : 1,
							"sliderorder" : [  ],
							"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
							"blob" : "5297.CMlaKA....fQPMDZ....AfkYygE...P.....APWYyQWKo4VZz0hYowFckIG..................PQc3EP6b0GTUUDE+xS91TDLw.S4ApnhhHPgiJu8.nxm9QZSlZkF5f.lhJfnRi3UIURKSio7qoRi9v.yxgozxwmOFpAKQTGGGQQELFG+pRMmRCmj1cu28x8ckOti+ATb2clCmc2yuyY28bN6gKWdyy7QJKRAAQPhDnMOv+LDgfo8EEEZTZVALFRifsoVgBd5.YjnnC5DmIchqK5Dmi5DmS5Dmy5DmK5Dmq5Dma5Dm65DWW0ItmPm35lNw0cchyCFt5ZrwZAbJDmy8C77.98.dc.dc.dc.dc.VcfldXSg5H4E31s4bCkefdXU8CddfjyvncOPUJ.sKOOfmGP7.77.icd.qNnjWf+7AL+gQgqMtqcrQwOXzOmZi6ZGaz8OFkyu13t1wFE+fQ+bJHXB2vQex6KPgb.2XYDMCWAGQmF0zhFOoZpYzuslh8dM3b964jjqwyC34A77.98.dc.dc.Rc.UMi9yuYTO+pRAncMp9Ai94lmGH4A34A1mIXz8GF0yOKKfc90NlMOm24ttg13t1w73em63OK9pMtqcLCGm24NePabW6Xd7uyc7mEeEDZi+2FrDi1M9RV4rvuSinHuWCsDYOvjI2mvDkw83niHRxlJqEwf3ljs1WbW1hzXhbQ3uF7KiwKRVOk16lyvh726+APGuxJYXwxnX.s5WetqAocNIaRrucDw9JqS06TDQnkYsQTC4EDPnVpOQuiW4DPgGdoThXH79g1mLeC4UL0F6X69.DBKjQTjR6cpeQdrZ4hx6IhbZeYcUOuDd7ZwrqJcnxH6G7dj5uv6gsSiAR3Y1wLVGBIHfwoIFEEUGY8j2iL8DjWK4oo9Bh9hnAlfWLYLtBnVuCw1hPIE6NltAYuntQkYuuftGTsFR5iURFa6Mmr9paZGqHSCN57M2bJJ73zQy+dTCyPruRN9a2cKstPl+lwaF4JwOBFU3TlWVmVbcv5HgU0cT15n0lZy2eDbr6wMsWz+9fcGV9NN1DlwqOgDnj79irljwJMU+y3wyKJiOJJFhMY1UVAY41aCEigsMdcjqUJhqiy5qBgptj5ppVelD1ZvFqlSkwp2xp+pFfbeLN5YQsHhtMWq0lWqL4w1mOv74Npw5jyF4C.QWvDQlSXxYL4BlbEStgIdi6A3d.isGXxYlZxYj9byJjnGq4zV5BCc3KK4bL1dDi0om8faOj0QlS8BlbHRASqNRg7weWQXxAKaNF+szi7i1xlKOYKtZZ0VhOfsXofX1skiNm8aw07KyR7EcJKET9ksbx5uCVtCnA6kan3CvSTZg4CpfX7GsmoNDzQmSnnqk4nPtlezngUXBnITzTPKnzYfdqxSFs2SmNpx5WB5Wu6JPNaZ0Hucesn.85MQQ36aihMfsfldPuG1VaGs7Q+AX6saz1R7Sw17KPGbleI1t6GU87+Frs+Nz8y8vX6WFx2M9infJ7mPidmGGkXQmBMyRNCZ9kdNTtG5hnMT9kQ63XWAU7ouN56q42P+b82Ac9a9mnab2+F0vC9G756.zcm6B3s6NA8q6t.A5kavv8tqPD91M.0OOfXCvSXRA1SXZA0KXVCu2v7ByGHiH5Cjyn6KjGxOXcw3OroX6O79INP3CmzffOapCA12zFJ7syLX3HyJDnh4DJbh4ENb14+rPsYLR3pYNJ3V4LF3d4ZAZLO.bI+nAOV+XgduwwC98NwACpvDfP1ZRvH24Dgn9nICwWzTfm6yedX5k7Bvq9UuHjVoy.V7AdIX4G5U.wiLanfxSF1bEyE11wRA10IRE1yoSG95y9ZvAqYgfsZWDbz5WBbxqlET8MWJT2sVFbs6tB31260g6+fURId72vG+4etjr+ykjpew.68tUm7yj2dwUsEnc6n2Gr0msuZu7Cr0gstc76il9jkKhyIHjbyLlSHgrSIqrCN8LRO6fmW5KH6TxjLm8s8UseJ+c5lapYOnVXj4VX9NpomXxyMyEYNzVb4kjGVaHO71P9yzhxwBXwgNPNYOnPZ2qp1WOBFhdsVSQdUiJUx6EkNtjhCxBgZMEIXkeWOsNLtTtGf6A3df++5AjqQJUajcLZ56svdzn7bsANO0INuzItdpSbOoNw0Kchyachq25D2SoSb9nSb9pSb8Qm3dZchqu5DW+zIN+n3z75TZ9gkcNqw9CWv5dpnVqG9X+hUwSdEq+wYtlUOuvMsFwkBy1jmzHrclpB11.l8Ps8wObv1N7dCz10Sa.11cXAX69GJIvsUkDzPzIAU5XRvppHQnmaHQX8SHQ3BcOQfjaWSJiEFnSiCp5SFGb1wMdHlqLdnaqKVXLCJNnvMU.57tudTUqasnEa5MP1V4ZvOGgoQvtmv416AhLO2r8vy6hsZ.mss085nsTtTbTerW4uKHU+iCRn5Jy8hYcIxb7+tY6+6l49Ct+feufWWfWGfWGfWGvtGqnN5yPv+dqh89aMJb6RBHOxHOOf5RLJwe14jmGH4AX9CiJmmGvyCTmCvtGvliMlyMF4IZi6ZGyyC34ADO.OOfmGvyCLN2C9u22qk+KPcWBZ83EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+80A..."
						}
,
						"snapshotlist" : 						{
							"current_snapshot" : 0,
							"entries" : [ 								{
									"filetype" : "C74Snapshot",
									"version" : 2,
									"minorversion" : 0,
									"name" : "Serum",
									"origin" : "Serum.vstinfo",
									"type" : "VST",
									"subtype" : "Instrument",
									"embed" : 1,
									"snapshot" : 									{
										"pluginname" : "Serum.vstinfo",
										"plugindisplayname" : "Serum",
										"pluginsavedname" : "",
										"pluginsaveduniqueid" : 1483109208,
										"version" : 1,
										"isbank" : 0,
										"isbase64" : 1,
										"sliderorder" : [  ],
										"slidervisibility" : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
										"blob" : "5297.CMlaKA....fQPMDZ....AfkYygE...P.....APWYyQWKo4VZz0hYowFckIG..................PQc3EP6b0GTUUDE+xS91TDLw.S4ApnhhHPgiJu8.nxm9QZSlZkF5f.lhJfnRi3UIURKSio7qoRi9v.yxgozxwmOFpAKQTGGGQQELFG+pRMmRCmj1cu28x8ckOti+ATb2clCmc2yuyY28bN6gKWdyy7QJKRAAQPhDnMOv+LDgfo8EEEZTZVALFRifsoVgBd5.YjnnC5DmIchqK5Dmi5DmS5Dmy5DmK5Dmq5Dma5Dm65DWW0ItmPm35lNw0cchyCFt5ZrwZAbJDmy8C77.98.dc.dc.dc.dc.VcfldXSg5H4E31s4bCkefdXU8CddfjyvncOPUJ.sKOOfmGP7.77.icd.qNnjWf+7AL+gQgqMtqcrQwOXzOmZi6ZGaz8OFkyu13t1wFE+fQ+bJHXB2vQex6KPgb.2XYDMCWAGQmF0zhFOoZpYzuslh8dM3b964jjqwyC34A77.98.dc.dc.Rc.UMi9yuYTO+pRAncMp9Ai94lmGH4A34A1mIXz8GF0yOKKfc90NlMOm24ttg13t1w73em63OK9pMtqcLCGm24NePabW6Xd7uyc7mEeEDZi+2FrDi1M9RV4rvuSinHuWCsDYOvjI2mvDkw83niHRxlJqEwf3ljs1WbW1hzXhbQ3uF7KiwKRVOk16lyvh726+APGuxJYXwxnX.s5WetqAocNIaRrucDw9JqS06TDQnkYsQTC4EDPnVpOQuiW4DPgGdoThXH79g1mLeC4UL0F6X69.DBKjQTjR6cpeQdrZ4hx6IhbZeYcUOuDd7ZwrqJcnxH6G7dj5uv6gsSiAR3Y1wLVGBIHfwoIFEEUGY8j2iL8DjWK4oo9Bh9hnAlfWLYLtBnVuCw1hPIE6NltAYuntQkYuuftGTsFR5iURFa6Mmr9paZGqHSCN57M2bJJ73zQy+dTCyPruRN9a2cKstPl+lwaF4JwOBFU3TlWVmVbcv5HgU0cT15n0lZy2eDbr6wMsWz+9fcGV9NN1DlwqOgDnj79irljwJMU+y3wyKJiOJJFhMY1UVAY41aCEigsMdcjqUJhqiy5qBgptj5ppVelD1ZvFqlSkwp2xp+pFfbeLN5YQsHhtMWq0lWqL4w1mOv74Npw5jyF4C.QWvDQlSXxYL4BlbEStgIdi6A3d.isGXxYlZxYj9byJjnGq4zV5BCc3KK4bL1dDi0om8faOj0QlS8BlbHRASqNRg7weWQXxAKaNF+szi7i1xlKOYKtZZ0VhOfsXofX1skiNm8aw07KyR7EcJKET9ksbx5uCVtCnA6kan3CvSTZg4CpfX7GsmoNDzQmSnnqk4nPtlezngUXBnITzTPKnzYfdqxSFs2SmNpx5WB5Wu6JPNaZ0Hucesn.85MQQ36aihMfsfldPuG1VaGs7Q+AX6saz1R7Sw17KPGbleI1t6GU87+Frs+Nz8y8vX6WFx2M9infJ7mPidmGGkXQmBMyRNCZ9kdNTtG5hnMT9kQ63XWAU7ouN56q42P+b82Ac9a9mnab2+F0vC9G756.zcm6B3s6NA8q6t.A5kavv8tqPD91M.0OOfXCvSXRA1SXZA0KXVCu2v7ByGHiH5Cjyn6KjGxOXcw3OroX6O79INP3CmzffOapCA12zFJ7syLX3HyJDnh4DJbh4ENb14+rPsYLR3pYNJ3V4LF3d4ZAZLO.bI+nAOV+XgduwwC98NwACpvDfP1ZRvH24Dgn9nICwWzTfm6yedX5k7Bvq9UuHjVoy.V7AdIX4G5U.wiLanfxSF1bEyE11wRA10IRE1yoSG95y9ZvAqYgfsZWDbz5WBbxqlET8MWJT2sVFbs6tB31260g6+fURId72vG+4etjr+ykjpew.68tUm7yj2dwUsEnc6n2Gr0msuZu7Cr0gstc76il9jkKhyIHjbyLlSHgrSIqrCN8LRO6fmW5KH6TxjLm8s8UseJ+c5lapYOnVXj4VX9NpomXxyMyEYNzVb4kjGVaHO71P9yzhxwBXwgNPNYOnPZ2qp1WOBFhdsVSQdUiJUx6EkNtjhCxBgZMEIXkeWOsNLtTtGf6A3df++5AjqQJUajcLZ56svdzn7bsANO0INuzItdpSbOoNw0Kchyachq25D2SoSb9nSb9pSb8Qm3dZchqu5DW+zIN+n3z75TZ9gkcNqw9CWv5dpnVqG9X+hUwSdEq+wYtlUOuvMsFwkBy1jmzHrclpB11.l8Ps8wObv1N7dCz10Sa.11cXAX69GJIvsUkDzPzIAU5XRvppHQnmaHQX8SHQ3BcOQfjaWSJiEFnSiCp5SFGb1wMdHlqLdnaqKVXLCJNnvMU.57tudTUqasnEa5MP1V4ZvOGgoQvtmv416AhLO2r8vy6hsZ.mss085nsTtTbTerW4uKHU+iCRn5Jy8hYcIxb7+tY6+6l49Ct+feufWWfWGfWGfWGvtGqnN5yPv+dqh89aMJb6RBHOxHOOf5RLJwe14jmGH4AX9CiJmmGvyCTmCvtGvliMlyMF4IZi6ZGyyC34ADO.OOfmGvyCLN2C9u22qk+KPcWBZ83EP6Yu+9eHW+GG+IFFFlyy4ggggg47r8hc974yGYyrSrggggmIIIIIokjVRRRRRRRaOSRRRRRRRKIIIIII0W88lt99ew2O6551d7491t1O755806e4YDu2uxiyuc7QzHYMTrdh9FQWo6zaZLVlKKmjqfqgUyZ313t39n3Q3I3YX87x7571DmPDchNSWnqr8zM1M5N8fdRun2zGNF5KMFHCkQxXYhLUlIyk4yBYorbVAqjUQxkxkwkyUvUxUwUy0v0x0w0yp4F3F4lXMbybKbqbab6bGbmbWb2bObubeb+7.rVJdPdHdXdDdTdLdbdBdRdJdZdFdVdNddVOu.uHuDuLuBuJuFuNuAuIuEuMuC+adWh9EwFvFxFQmXiYSXSoyrYr4rEzE1R1J1Z5JaCaKaGaO6.6H6Dciclcgckcicm8f8jtydwdy9POXeY+X+omb.bfbPzKNXNDNT5MGFGNGA8gijihiligikiiim9xIP+n+z3D4jX.LPFDClgvPYXLbFAijQwnYLLVFGimIvDYRLYlBSkowzYFLSlEyl4vb4j4TXdLeNUV.mFKjEwhYIrTNcNCVFKmyjyhylUv4v4x4wJ474B3BYUbQbwbIj8KRh2mk3RsDueKwkYI9.VhK2R7AsDWgk3CYItRKwG1RbUVhOhk3psDeTKw0XI9XVhq0R7wsDWmk3SXItdKwmzRrZKwmxRbCVhOsk3FsDeFKwMYI9rVh0XI9bVha1R74sD2hk3KXItUKwWzRbaVhujk31sDeYKwcXI9JVh6zR7UsD2kk3qYItaKwW2RbOVhugk3dsDeSKw8YI9VVh62R7ssDOfk36XIVqkXcVhxR7csDOnk36YIdHKw22R7vVhefk3QrD+PKwiZI9QVhGyR7isDOtk3mXIdBKwO0R7jVhelk3orD+bKwSaI9EVhmwR7KsDOqk3WYIdNKwu1R77VheikX8Vheqk3ErD+NKwKZI98VhWxR7GrDurk3OZIdEKwexR7pVh+rk30rD+EKwqaI9qVh2vR72rDuok3uaIdKKw+vR71Vh+ok3crD+KKw+1R7+XIdWKw+whuDLZjTDafljhXC0jTDajljhnSZRJhMVSRQrIZRJhMUSRQzYMIEwloIoH1bMIEwVnIoH5hljhXK0jTDakljhXq0jTDcUSRQrMZRJhsUSRQrcZRJhsWSRQrCZRJhcTSRQrSZRJhtoIoH1YMIEwtnIoH1UMIEwtoIoH1cMIEwdnIoH1SMIEQ20jTD6kljhXu0jTD6iljhnGZRJh8USRQreZRJh8WSRQzSMIEwAnIoHNPMIEwAoIoH5kljh3f0jTDGhljh3P0jTD8VSRQbXZRJhCWSRQbDZRJh9nIoHNRMIEwQoIoHNZMIEwwnIoHNVMIEwwoIoHNdMIEQe0jTDmfljhneZRJh9qIoPDMRJhSTSRQbRZRJhAnIoHFnljhXPZRJhAqIoHFhljhXnZRJhgoIoHFtljhXDZRJhQpIoHFkljhXzZRJhwnIoHFqljhXbZRJhwqIoHlfljhXhZRJhIoIoHlrljhXJZRJhopIoHllljhX5ZRJhYnIoHloljhXVZRJhYqIoHliljhXtZRJhSVSRQbJZRJh4oIoHluljh3T0jTDKPSRQbZZRJhEpIoHVjljhXwZRJhknIoHVpljh3z0jTDmgljhXYZRJhkqIoHNSMIEwYoIoHNaMIEwJzjTDmiljh3b0jTDmmljhXkZRJhyWSRQbAZRJhKTSRQrJMIEwEoIoHtXMIEwknIo9uVmedc9yVm+t04eilFA0EooQPsJMMBpKTSif5BzzHnNeMMBpUpoQPcdZZDTmqlFA04noQPsBMMBpyVSif5rzzHnNSMMBpkqoQPsLMMBpyPSif5z0zHnVplFA0RzzHnVrlFA0hzzHnVnlFA0oooQPs.MMBpSUSifZ9ZZDTySSif5TzzHnNYMMBp4poQPMGMMBpYqoQPMKMMBpYpoQPMCMMBpoqoQPMMMMBpopoQPMEMMBpIqoQPMIMMBpIpoQPMAMMBpwqoQPMNMMBpwpoQPMFMMBpQqoQPMJMMBpQpoQPMBMMBpgqoQPMLMMBpgpoQPMDMMBpAqoQPMHMMBpApoQPM.MMBpSRSif5D0zHnZZZDT8WSifpeZZDTmflFAUe0zHnNdMMBpiSSif5X0zHnNFMMBpiVSif5nzzHnNRMMBp9noQPcDZZDTGtlFA0gooQP0aMMBpCUSif5PzzHnNXMMBpdooQPcPZZDTGnlFA0AnoQP0SMMBp8WSifZ+zzHn1WMMBpdnoQPsOZZDT6slFA0dooQP0cMMBp8TSifZOzzHn1cMMBpcSSifZW0zHn1EMMBpcVSifpaZZDT6jlFA0NpoQPsCZZDTaulFA01ooQPssZZDTailFAUW0zHn1ZMMBpsRSifZK0zHn5hlFA0VnoQPs4ZZDTallFAUm0zHn1TMMBpMQSifZi0zHn5jlFA0FooQPsgZZDTaflFAUnoQ7d+7+YsQP9tVx+GK4+1R9urjuik7eZIeaK4+vR9VVx+tk7Msj+MK4aXI+qVxW2R9Wrjulk7OaIeUK4exR9JVx+nk7ksj+AK4KYI+8VxWzR96rjufk72ZIWuk72XIedK4u1R9bVxekk7Ysj+RK4yXI+EVxm1R9ysjOkk7mYIeRK4O0R9DVxehk7wsj+XK4iYI+QVxG0R9CsjOhk7GXIeXK422R9PVxumk7AsjeWKYYIWmkbsVxuik7ArjeaK48aI+VVx6yR9Msj2qk7aXIuGK4W2Rd2Vxulk7trjeUK4cZI+JVx6vR9ksj2tk7KYIuMK4WzRdqVxufk7VrjedK4MaI+bVx0XI+rVxaxR9Yrj2nk7SaIuAK4mxRtZK4mzRd8VxOgk75rjebK40ZI+XVxqwR9QsjWsk7iXIuJK4G1RdkVxOjk7JrjePK4kaI+.VxKyR99sjWpk78YIS664R3h4hXUbgbAb9rRNONWNGVAmMmEmIKmkwYvoyRYIrXVDKjSiEvox7YdbJbxLWlCylYwLYFLclFSkovjYRLQl.imwwXYLLZFEijQvvYXLTFBClAw.Y.bRbhzn+zONA5KGOGGGKGCGMGEGI8gifCmCidygxgvASu3f3.4.nmr+reruzC1G1a1K5N6I6A6N6F6J6B6LcichcjcfsmsisksgtxVyVwVRWXKXyYynyrorIrwzI1H1P1.Bd2uiumm2g2l2h2j2fWmWiWkWgWlWhWjWf0yyyywyxyvSySwSxSviyiwixivCyCwCRwZ4A39493d4d3t4t3N4N31413V4V3lYMbSbibCrZtdtNtVtFtZtJtRtBtbtLtTRVEqjUvxYorPlOykYxTYhLVFICkARi9xwPen2zK5I8ftytQ2X6oqzE5Lchf29A7NyKy54Y3I3Qn393t31XMrZtFtBRVNykwRidS2oqDr9us+eXMjzHdue99s+e+pi6+2w8+2v90w8+Gf2fAxfXvLDFJCigyHXjLJFMigwx3X7LAlHShIyTXpLMlNyfYxrX1LGlKmLmByi4yoxB3zXgrHVLKgkxoyYvxX4blbVb1rBNGNWNOVImOW.WHqhKhKlKgrecb++03c3y4cfNt+u2gGz6v2yRG2+26vK5c32aoi6+2+Nt++10+Nt++9z+Nt++Qz+Nt++.88AjTDCRSRQLXMIEwPzjTDCUSRQLLMIEwv0jTDiPSRQLRMIEwnzjTDiVSRQLFMIEwX0jTDiSSRQLdMIEwDzjTDSTSRQLIMIEwj0jTDSQSRQLUMIEwzzjTDSWSRQLCMIEwL0jTDyRSRQLaMIEwbzjTDyUSRQbxZRJhSQSRQLOMIEw70jTDmpljhXAZRJhSSSRQrPMIEwhzjTDKVSRQrDMIEwR0jTDmtljh3LzjTDKSSRQrbMIEwYpIoHNKMIEwYqIoHVgljh3bzjTDmqljh37zjTDqTSRQb9ZRJhKPSRQbgZRJhUoIoHtHMIEwEqIoHtDMI0+05539+Kaccb++4rNe2v57cD97.MBpYooQPMSMMBpYnoQPMcMMBpoooQPMUMMBponoQPMYMMBpIooQPMQMMBpInoQPMdMMBpwooQPMVMMBpwnoQPMZMMBpQooQPMRMMBpQnoQPMbMMBpgooQPMTMMBpgnoQPMXMMBpAooQPMPMMBpAnoQPcRZZDTmnlFAUSSifNt+u2iC06Acb+eedXO7dPG2+2mG1JuGao2C539+qsi6++rqsi6++s74.539+dGVs2gOokNt+u2gkxRXwrHVHmFKfSk4y73T3jYtLGlMyhYxLX5LMlJSgIyjXhLAFOiiwxXXzLJFIifgyvXnLDFLChAx.3j3DoQ+oebBzWNdNNNVNFNZNJNR5CGAGNGF8lCkCgCldwAwAxAPOY+Y+XeoGrOr2rWzc1S1C1c1M1U1E1Y5F6D6H6.aOaGaKaCckslshsjtvVvlylQmYSYSXioSrQrgrADzw8+++y2+++EpTze+80A..."
									}
,
									"fileref" : 									{
										"name" : "Serum",
										"filename" : "Serum_20190510.maxsnap",
										"filepath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
										"filepos" : -1,
										"snapshotfileid" : "3c0e5c9165d96e98081de25cefdb1535"
									}

								}
 ]
						}

					}
,
					"text" : "vst~ Serum.auinfo",
					"varname" : "vst~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "int", "int", "", "int", "" ],
					"patching_rect" : [ 555.892168045043945, 991.411778926849365, 92.5, 22.0 ],
					"text" : "midiparse"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 520.392168045043945, 910.411778926849365, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 34.940556085514061, 974.190247714519501, 96.0, 22.0 ],
					"text" : "poly~ VSTpoly 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 91.151421785354614, 840.716673731803894, 101.0, 22.0 ],
					"text" : "prepend midinote"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 362.392168045043945, 1016.411778926849365, 38.0, 22.0 ],
					"text" : "poly~"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.941176, 0.690196, 0.196078, 1.0 ],
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 759.225420045043961, 961.411778926849252, 125.0, 22.0 ],
					"text" : "mpepolygen @hires 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 628.392168045043945, 427.911778926849365, 37.0, 22.0 ],
					"text" : "swap"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 6,
					"outlettype" : [ "", "", "", "", "", "" ],
					"patching_rect" : [ 628.392168045043945, 371.411778926849365, 81.0, 22.0 ],
					"text" : "mira.mt.touch"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 91.151421785354614, 904.745799958705902, 168.0, 22.0 ],
					"text" : "poly~ synthesiser 11 @steal 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 221.892168045043945, 389.411778926849365, 94.0, 21.0 ],
					"text" : "raw MIDI input",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@autoconnect", 0 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "demompe.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "int" ],
					"patching_rect" : [ 206.892168045043945, 342.411778926849365, 207.0, 42.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"border" : 0,
					"filename" : "helpdetails.js",
					"id" : "obj-51",
					"ignoreclick" : 1,
					"jsarguments" : [ "risekeys" ],
					"maxclass" : "jsui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 197.392168045043945, 246.411778926849365, 335.5, 79.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-76",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 923.892168045043945, 751.411778926849365, 72.0, 36.0 ],
					"text" : "Zone Index",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-77",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 834.225420045043961, 751.411778926849365, 66.0, 36.0 ],
					"text" : "Zone 1st\nChannel",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-78",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 837.892168045043945, 701.411778926849365, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 837.892168045043945, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 927.892168045043945, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-29",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 743.392168045043945, 751.411778926849365, 57.5, 36.0 ],
					"text" : "Voice\nNumber",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-81",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 927.892168045043945, 701.411778926849365, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-21",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 653.892168045043945, 751.411778926849365, 59.5, 36.0 ],
					"text" : "Pitch Bend",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-26",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 563.892168045043945, 751.411778926849365, 69.5, 21.0 ],
					"text" : "Aftertouch",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-25",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 473.225481045043921, 751.411778926849365, 73.5, 36.0 ],
					"text" : "Pgm Change",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-24",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 383.058794045043953, 751.411778926849365, 89.5, 36.0 ],
					"text" : "Control Change",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-22",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 293.058794045043953, 751.411778926849365, 78.5, 36.0 ],
					"text" : "Poly Pressure",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-23",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 202.892168045043945, 751.411778926849365, 67.5, 36.0 ],
					"text" : "Note On/Off",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 206.892168045043945, 674.411778926849365, 65.0, 23.0 ],
					"text" : "unpack i i"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-31",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 206.892168045043945, 701.411778926849365, 38.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 206.892168045043945, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-33",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 252.892168045043945, 701.411778926849365, 38.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-34",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 343.058794045043953, 701.411778926849365, 38.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-18",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 297.058794045043953, 701.411778926849365, 38.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 297.058794045043953, 674.411778926849365, 65.0, 23.0 ],
					"text" : "unpack i i"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-36",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 433.058794045043953, 701.411778926849365, 38.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-37",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 387.058794045043953, 701.411778926849365, 38.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 387.058794045043953, 674.411778926849365, 65.0, 23.0 ],
					"text" : "unpack i i"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-39",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 476.892168045043945, 701.411778926849365, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 476.892168045043945, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "", "", "", "int", "int", "", "int", "" ],
					"patching_rect" : [ 206.892168045043945, 646.411778926849365, 674.5, 23.0 ],
					"text" : "midiparse"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-41",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 747.891924045043993, 701.411778926849365, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 747.891924045043993, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-43",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 657.892168045043945, 701.411778926849365, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 657.892168045043945, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 567.892168045043945, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-46",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 567.892168045043945, 701.411778926849365, 53.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 297.058794045043953, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 387.058794045043953, 732.411778926849365, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.0, 0.0, 0.0 ],
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-20",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 378.132868074966439, 427.911778926849365, 204.0, 51.0 ],
					"text" : "autoconnect attribute (on by default) automatically receives MIDI from a connected Seaboard",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 227.558794045043953, 619.411778926849365, 177.0, 21.0 ],
					"text" : "outputs mpeevent messages",
					"textcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "risekeys",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 252.892168045043945, 494.782149076461792, 530.0, 107.0 ]
				}

			}
, 			{
				"box" : 				{
					"attr" : "annotation",
					"id" : "obj-6",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 252.892168045043945, 437.411778926849365, 124.0, 22.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 1 ],
					"order" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 1 ],
					"order" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"order" : 1,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"order" : 0,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-125", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 1 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"order" : 1,
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 1 ],
					"order" : 0,
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 1 ],
					"order" : 1,
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 1 ],
					"order" : 0,
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 1 ],
					"order" : 1,
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 1 ],
					"order" : 0,
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"order" : 1,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"order" : 1,
					"source" : [ "obj-145", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"source" : [ "obj-145", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-145", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 1 ],
					"order" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 1 ],
					"order" : 0,
					"source" : [ "obj-145", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-148", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 1 ],
					"source" : [ "obj-149", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"midpoints" : [ 1184.030422019149682, 830.911778973724381, 1184.030422019149682, 830.911778973724381 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 1 ],
					"midpoints" : [ 1272.030422019149682, 801.911778973724381, 1197.530422019149682, 801.911778973724381 ],
					"source" : [ "obj-151", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"midpoints" : [ 1184.030422019149682, 800.911778973724381, 1184.030422019149682, 800.911778973724381 ],
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 1 ],
					"midpoints" : [ 1501.030422019149682, 750.411778973724381, 1228.030422019149682, 750.411778973724381 ],
					"source" : [ "obj-152", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 1 ],
					"source" : [ "obj-153", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"order" : 1,
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 1 ],
					"order" : 0,
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 1 ],
					"order" : 0,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 1 ],
					"order" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 1 ],
					"order" : 0,
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 1 ],
					"order" : 1,
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 1 ],
					"source" : [ "obj-171", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-183", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 4 ],
					"order" : 1,
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-202", 1 ],
					"order" : 0,
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 1 ],
					"order" : 0,
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 4 ],
					"order" : 1,
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-19", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 4 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-148", 4 ],
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"order" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 1 ],
					"order" : 1,
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-208", 0 ],
					"order" : 2,
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 0 ],
					"order" : 0,
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-186", 0 ],
					"source" : [ "obj-210", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 1 ],
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 1 ],
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-228", 1 ],
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 1 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-237", 0 ],
					"source" : [ "obj-235", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-238", 0 ],
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-237", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 1 ],
					"source" : [ "obj-238", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-244", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-241", 0 ],
					"source" : [ "obj-244", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-248", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"source" : [ "obj-251", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"source" : [ "obj-253", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 0 ],
					"source" : [ "obj-255", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"source" : [ "obj-256", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"source" : [ "obj-257", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"source" : [ "obj-258", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-255", 0 ],
					"source" : [ "obj-260", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 0 ],
					"source" : [ "obj-262", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 0 ],
					"source" : [ "obj-262", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 0 ],
					"source" : [ "obj-262", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-262", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-262", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-262", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-262", 0 ],
					"source" : [ "obj-263", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-265", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-268", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"source" : [ "obj-274", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"source" : [ "obj-278", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-276", 0 ],
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-277", 0 ],
					"source" : [ "obj-279", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"source" : [ "obj-279", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"source" : [ "obj-280", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"source" : [ "obj-280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-247", 0 ],
					"source" : [ "obj-281", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"source" : [ "obj-281", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-243", 0 ],
					"source" : [ "obj-282", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 0 ],
					"source" : [ "obj-282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 0 ],
					"source" : [ "obj-282", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-279", 0 ],
					"source" : [ "obj-283", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"source" : [ "obj-283", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"source" : [ "obj-283", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-282", 0 ],
					"source" : [ "obj-283", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-30", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 1 ],
					"order" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 2 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 2 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-38", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"source" : [ "obj-4", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-4", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-4", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-4", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-4", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"order" : 2,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"order" : 1,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 3 ],
					"order" : 1,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"order" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 1 ],
					"order" : 1,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"order" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-49", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-52", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 1 ],
					"source" : [ "obj-52", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"order" : 4,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"order" : 0,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"order" : 1,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 2,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"order" : 3,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"source" : [ "obj-57", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 1 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 1 ],
					"source" : [ "obj-57", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 1 ],
					"source" : [ "obj-57", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-58", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 1 ],
					"source" : [ "obj-60", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"order" : 1,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"order" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"order" : 0,
					"source" : [ "obj-71", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 1,
					"source" : [ "obj-71", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 1 ],
					"source" : [ "obj-86", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-86", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"order" : 1,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"order" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-7.5::obj-2" : [ "vst~[4]", "vst~", 0 ],
			"obj-61" : [ "vst~[23]", "vst~[16]", 0 ],
			"obj-7.6::obj-2" : [ "vst~[5]", "vst~", 0 ],
			"obj-66.4::obj-2" : [ "vst~[14]", "vst~", 0 ],
			"obj-58" : [ "vst~[21]", "vst~[16]", 0 ],
			"obj-7.1::obj-2" : [ "vst~", "vst~", 0 ],
			"obj-7.3::obj-2" : [ "vst~[2]", "vst~", 0 ],
			"obj-66.2::obj-2" : [ "vst~[12]", "vst~", 0 ],
			"obj-11" : [ "vst~[28]", "vst~[28]", 0 ],
			"obj-7.9::obj-2" : [ "vst~[8]", "vst~", 0 ],
			"obj-52" : [ "vst~[19]", "vst~[16]", 0 ],
			"obj-67" : [ "vst~[24]", "vst~[16]", 0 ],
			"obj-7.7::obj-2" : [ "vst~[6]", "vst~", 0 ],
			"obj-60" : [ "vst~[22]", "vst~[16]", 0 ],
			"obj-66.5::obj-2" : [ "vst~[15]", "vst~", 0 ],
			"obj-7.10::obj-2" : [ "vst~[9]", "vst~", 0 ],
			"obj-70" : [ "vst~[25]", "vst~[16]", 0 ],
			"obj-86" : [ "vst~[29]", "vst~[16]", 0 ],
			"obj-66.3::obj-2" : [ "vst~[13]", "vst~", 0 ],
			"obj-7.11::obj-2" : [ "vst~[10]", "vst~", 0 ],
			"obj-66.1::obj-2" : [ "vst~[11]", "vst~", 0 ],
			"obj-71" : [ "vst~[26]", "vst~[16]", 0 ],
			"obj-55" : [ "vst~[18]", "vst~[16]", 0 ],
			"obj-7.4::obj-2" : [ "vst~[3]", "vst~", 0 ],
			"obj-49" : [ "vst~[27]", "vst~[16]", 0 ],
			"obj-7.2::obj-2" : [ "vst~[1]", "vst~", 0 ],
			"obj-19" : [ "vst~[17]", "vst~[16]", 0 ],
			"obj-7.8::obj-2" : [ "vst~[7]", "vst~", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "helpdetails.js",
				"bootpath" : "C74:/help/resources",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "demompe.maxpat",
				"bootpath" : "C74:/help/msp",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "interfacecolor.js",
				"bootpath" : "C74:/interfaces",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "synthesiser.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Serum.maxsnap",
				"bootpath" : "~/Documents/Max 8/Snapshots",
				"patcherrelativepath" : "../../../Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "mira.mt.touch.maxpat",
				"bootpath" : "C74:/packages/mira/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "VSTpoly.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Serum_20190510.maxsnap",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/data",
				"patcherrelativepath" : "../data",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "testCycles.maxpat",
				"bootpath" : "~/Documents/Max 8/Projects/Expressive Keyboard/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "risekeys.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mpepolygen.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "risedevice.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mpesplit.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mperoute.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "ksliderWhite",
				"default" : 				{
					"color" : [ 1, 1, 1, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0, 0.533333, 0.168627, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"color" : [ 0.729411764705882, 0.835294117647059, 0.847058823529412, 1.0 ],
		"textcolor" : [ 0.976470588235294, 0.976470588235294, 0.976470588235294, 1.0 ],
		"clearcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 0.0 ],
		"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
		"editing_bgcolor" : [ 0.0, 0.0, 0.0, 0.76 ]
	}

}
