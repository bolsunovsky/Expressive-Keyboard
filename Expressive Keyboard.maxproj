{
	"name" : "Expressive Keyboard",
	"version" : 1,
	"creationdate" : 3640194967,
	"modificationdate" : 3660759577,
	"viewrect" : [ 25.0, 79.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"mctest.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"main.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"synthesiser.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"VSTpoly.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"testCycles.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Synth.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{
			"VoiceAllocator.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}
,
		"data" : 		{

		}
,
		"externals" : 		{

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
